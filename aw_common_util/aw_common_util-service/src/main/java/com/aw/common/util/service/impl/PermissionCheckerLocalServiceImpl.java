/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.aw.common.util.service.base.PermissionCheckerLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;

/**
 * The implementation of the permission checker local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.common.util.service.PermissionCheckerLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PermissionCheckerLocalServiceBaseImpl
 * @see com.aw.common.util.service.PermissionCheckerLocalServiceUtil
 */
public class PermissionCheckerLocalServiceImpl
	extends PermissionCheckerLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.common.util.service.PermissionCheckerLocalServiceUtil} to access the permission checker local service.
	 */
	public static final String ADMIN = "Administrator";
	public String getADMIN(){
		return ADMIN;
	}
	
	public Set<String> getRolesForUser(){
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
		
		Set<String> set = new HashSet<String>();
		
		try {
			List<Role> roles = user.getRoles();
			
			if(roles != null && !roles.isEmpty()){
				for (Role role : roles) {
					set.add(role.getName());
				}	
				
			}else{
				
			}
		} catch (SystemException e) { 
			e.printStackTrace();			
		}
		return set;
	}
	
	public User getCurrentUser(){		
		
		return PermissionThreadLocal.getPermissionChecker().getUser();

	}
	
	public User isAdmin(){
		
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
		
		try {
			List<Role> roles = user.getRoles();
			
			if(roles != null && !roles.isEmpty()){
				for (Role role : roles) {
					if (ADMIN.equals(role.getName())){
						return user;
					}
				}	
				return null;
			}else{
				return null;
			}
		} catch (SystemException e) { 
			e.printStackTrace();
			return null;
		}				
	}
	
	public User permissionCheck(Set<String> permitedRoles){
		
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
		
		try {
			List<Role> roles = user.getRoles();
			
			if(roles != null && !roles.isEmpty()){
				for (Role role : roles) {
					if (permitedRoles.contains(role.getName())){
						return user;
					}
				}	
				return null;
			}else{
				return null;
			}
		} catch (SystemException e) { 
			e.printStackTrace();
			return null;
		}				
	}
	
//	public User permissionCheck(String permitedRole){
//		
//		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
//		User user = pc.getUser();
//		
//		try {
//			List<Role> roles = user.getRoles();
//			
//			if(roles != null && !roles.isEmpty()){
//				for (Role role : roles) {
//					if (role.getName().equals(permitedRole) || role.getName().equals(ADMIN)){
//						return user;
//					}
//				}	
//				return null;
//			}else{
//				return null;
//			}
//		} catch (SystemException e) { 
//			e.printStackTrace();
//			return null;
//		}	
//	}
}