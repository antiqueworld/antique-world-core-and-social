package com.aw.common.util.service.logging;
import org.apache.log4j.Logger;


public class LoggerFactory {
	
	private static LoggerFactory factory = new LoggerFactory();
	
	private LoggerFactory() {
	}

	public static LoggerFactory getInstance() {
		return factory;
	}

	public ILoggerWrapper getLoggger(String loggerName, Logger customLoggingImpl) {
		
		AbstractLoggerWrapper logger = null;
		
		try {
			
			logger = (AbstractLoggerWrapper) Class.forName(loggerName).newInstance();
			
			logger.setLogger(customLoggingImpl);
			
		} catch(Exception ex) {
			Logger rootLogger = Logger.getLogger(LoggerFactory.class); 
			rootLogger.error("Unable to generate specialized looger", ex);
			
			//Fortify security issue fix
			//ex.printStackTrace();
		}
		
		return logger;
	}

}
