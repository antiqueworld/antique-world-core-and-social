/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service.impl;

import java.util.HashMap;
import java.util.Map;

import com.aw.common.util.service.base.CommonUtilServiceBaseImpl;
import com.aw.common.util.service.util.microservice.MicroServiceUtil;
import com.aw.common.util.service.util.microservice.SpringbootUtil;

/**
 * The implementation of the common util remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.common.util.service.CommonUtilService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CommonUtilServiceBaseImpl
 * @see com.aw.common.util.service.CommonUtilServiceUtil
 */
public class CommonUtilServiceImpl extends CommonUtilServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.common.util.service.CommonUtilServiceUtil} to access the common util remote service.
	 */
	public String getEthBalance(String address){
		String path = "balance";
		Map<String, String> map = new HashMap<String, String>();
		map.put("address", address);
		map.put("tokenAddress", "");
		return SpringbootUtil.callServices(path, map);
	}
	
	public String getBalance(String address, String tokenAddress){
		String path = "balance";
		Map<String, String> map = new HashMap<String, String>();
		map.put("address", address);
		map.put("tokenAddress", tokenAddress);
		return SpringbootUtil.callServices(path, map);
	}
	
}