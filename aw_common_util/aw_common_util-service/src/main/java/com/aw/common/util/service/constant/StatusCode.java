package com.aw.common.util.service.constant;

import java.util.HashMap;
import java.util.Map;
public class StatusCode{
	//languages
	public static final String EN = "en";
	public static final String ZH = "zh";
	
	
	public static final String SUCCESS = "00";
	public static final String PERMISSION = "99";		
	
	//TODO add any failure code to below for being invoked
	
	//101***user related
	public static final String USER_NOT_EXIST= "101000";
	public static final String CONTACT_NOT_EXIST = "101001";
	public static final String ADDRESS_NOT_FOUND = "101002";
	public static final String BIRTH_FORMAT_ERROR = "101003";
	public static final String IMAGE_FORMAT_ERROR = "101004";
	public static final String PHONE_FORMAT_ERROR = "101005";
	public static final String ADDRESS_FORMAT_ERROR = "101006";
	
	private static Map<String, Map<String,String>> map = new HashMap<String, Map<String,String>>();
	static{
		
		//initialize en map
		Map<String,String> map_en = new HashMap<String,String>();
		map.put(EN, map_en);
		map_en.put(SUCCESS, "success");
		map_en.put(PERMISSION, "no permission to invoke this service");
		map_en.put(USER_NOT_EXIST, "user not found");
		map_en.put(CONTACT_NOT_EXIST, "contact info not found");
		map_en.put(ADDRESS_NOT_FOUND, "address not found");
		map_en.put(BIRTH_FORMAT_ERROR, "birthday format error, must be yyyy/mm/dd");
		map_en.put(IMAGE_FORMAT_ERROR, "image file format error");
		map_en.put(PHONE_FORMAT_ERROR, "phone number format error");
		map_en.put(ADDRESS_FORMAT_ERROR, "address format error");
		
		//initialize zh map
		Map<String,String> map_zh = new HashMap<String,String>();
		map.put(ZH, map_zh);
		map_zh.put(SUCCESS, "成功");
		map_zh.put(PERMISSION, "无权访问该服务");
		map_zh.put(USER_NOT_EXIST, "用户不存在");
		map_zh.put(CONTACT_NOT_EXIST, "联系方式不存在");
		map_zh.put(ADDRESS_NOT_FOUND, "地址不存在");
		map_zh.put(BIRTH_FORMAT_ERROR, "生日格式错误， 必须是yyyy/mm/dd");
		map_zh.put(IMAGE_FORMAT_ERROR, "图片格式错误");
		map_zh.put(PHONE_FORMAT_ERROR, "电话号码格式错误");
		map_zh.put(ADDRESS_FORMAT_ERROR, "地址格式错误");
	}
	
	public static String getMsg(String code, String locale){
		if(map.containsKey(locale)){
			return map.get(locale).get(code);
		}else{
			return map.get(EN).get(code);
		}
	}
}

