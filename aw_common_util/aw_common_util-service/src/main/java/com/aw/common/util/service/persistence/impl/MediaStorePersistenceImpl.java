/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.common.util.exception.NoSuchMediaStoreException;
import com.aw.common.util.model.MediaStore;
import com.aw.common.util.model.impl.MediaStoreImpl;
import com.aw.common.util.model.impl.MediaStoreModelImpl;
import com.aw.common.util.service.persistence.MediaStorePersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the media store service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MediaStorePersistence
 * @see com.aw.common.util.service.persistence.MediaStoreUtil
 * @generated
 */
@ProviderType
public class MediaStorePersistenceImpl extends BasePersistenceImpl<MediaStore>
	implements MediaStorePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link MediaStoreUtil} to access the media store persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = MediaStoreImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
			MediaStoreModelImpl.FINDER_CACHE_ENABLED, MediaStoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
			MediaStoreModelImpl.FINDER_CACHE_ENABLED, MediaStoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
			MediaStoreModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public MediaStorePersistenceImpl() {
		setModelClass(MediaStore.class);
	}

	/**
	 * Caches the media store in the entity cache if it is enabled.
	 *
	 * @param mediaStore the media store
	 */
	@Override
	public void cacheResult(MediaStore mediaStore) {
		entityCache.putResult(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
			MediaStoreImpl.class, mediaStore.getPrimaryKey(), mediaStore);

		mediaStore.resetOriginalValues();
	}

	/**
	 * Caches the media stores in the entity cache if it is enabled.
	 *
	 * @param mediaStores the media stores
	 */
	@Override
	public void cacheResult(List<MediaStore> mediaStores) {
		for (MediaStore mediaStore : mediaStores) {
			if (entityCache.getResult(
						MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
						MediaStoreImpl.class, mediaStore.getPrimaryKey()) == null) {
				cacheResult(mediaStore);
			}
			else {
				mediaStore.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all media stores.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(MediaStoreImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the media store.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(MediaStore mediaStore) {
		entityCache.removeResult(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
			MediaStoreImpl.class, mediaStore.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<MediaStore> mediaStores) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (MediaStore mediaStore : mediaStores) {
			entityCache.removeResult(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
				MediaStoreImpl.class, mediaStore.getPrimaryKey());
		}
	}

	/**
	 * Creates a new media store with the primary key. Does not add the media store to the database.
	 *
	 * @param mediaUid the primary key for the new media store
	 * @return the new media store
	 */
	@Override
	public MediaStore create(String mediaUid) {
		MediaStore mediaStore = new MediaStoreImpl();

		mediaStore.setNew(true);
		mediaStore.setPrimaryKey(mediaUid);

		return mediaStore;
	}

	/**
	 * Removes the media store with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mediaUid the primary key of the media store
	 * @return the media store that was removed
	 * @throws NoSuchMediaStoreException if a media store with the primary key could not be found
	 */
	@Override
	public MediaStore remove(String mediaUid) throws NoSuchMediaStoreException {
		return remove((Serializable)mediaUid);
	}

	/**
	 * Removes the media store with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the media store
	 * @return the media store that was removed
	 * @throws NoSuchMediaStoreException if a media store with the primary key could not be found
	 */
	@Override
	public MediaStore remove(Serializable primaryKey)
		throws NoSuchMediaStoreException {
		Session session = null;

		try {
			session = openSession();

			MediaStore mediaStore = (MediaStore)session.get(MediaStoreImpl.class,
					primaryKey);

			if (mediaStore == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMediaStoreException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(mediaStore);
		}
		catch (NoSuchMediaStoreException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected MediaStore removeImpl(MediaStore mediaStore) {
		mediaStore = toUnwrappedModel(mediaStore);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(mediaStore)) {
				mediaStore = (MediaStore)session.get(MediaStoreImpl.class,
						mediaStore.getPrimaryKeyObj());
			}

			if (mediaStore != null) {
				session.delete(mediaStore);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (mediaStore != null) {
			clearCache(mediaStore);
		}

		return mediaStore;
	}

	@Override
	public MediaStore updateImpl(MediaStore mediaStore) {
		mediaStore = toUnwrappedModel(mediaStore);

		boolean isNew = mediaStore.isNew();

		Session session = null;

		try {
			session = openSession();

			if (mediaStore.isNew()) {
				session.save(mediaStore);

				mediaStore.setNew(false);
			}
			else {
				session.evict(mediaStore);
				session.saveOrUpdate(mediaStore);
			}

			session.flush();
			session.clear();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
			MediaStoreImpl.class, mediaStore.getPrimaryKey(), mediaStore, false);

		mediaStore.resetOriginalValues();

		return mediaStore;
	}

	protected MediaStore toUnwrappedModel(MediaStore mediaStore) {
		if (mediaStore instanceof MediaStoreImpl) {
			return mediaStore;
		}

		MediaStoreImpl mediaStoreImpl = new MediaStoreImpl();

		mediaStoreImpl.setNew(mediaStore.isNew());
		mediaStoreImpl.setPrimaryKey(mediaStore.getPrimaryKey());

		mediaStoreImpl.setMediaUid(mediaStore.getMediaUid());
		mediaStoreImpl.setContent(mediaStore.getContent());
		mediaStoreImpl.setIsActive(mediaStore.getIsActive());
		mediaStoreImpl.setOwnerUid(mediaStore.getOwnerUid());
		mediaStoreImpl.setPublicFlag(mediaStore.getPublicFlag());
		mediaStoreImpl.setFileType(mediaStore.getFileType());
		mediaStoreImpl.setFileExt(mediaStore.getFileExt());
		mediaStoreImpl.setFileSize(mediaStore.getFileSize());
		mediaStoreImpl.setAddTime(mediaStore.getAddTime());
		mediaStoreImpl.setUpdateTime(mediaStore.getUpdateTime());

		return mediaStoreImpl;
	}

	/**
	 * Returns the media store with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the media store
	 * @return the media store
	 * @throws NoSuchMediaStoreException if a media store with the primary key could not be found
	 */
	@Override
	public MediaStore findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMediaStoreException {
		MediaStore mediaStore = fetchByPrimaryKey(primaryKey);

		if (mediaStore == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMediaStoreException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return mediaStore;
	}

	/**
	 * Returns the media store with the primary key or throws a {@link NoSuchMediaStoreException} if it could not be found.
	 *
	 * @param mediaUid the primary key of the media store
	 * @return the media store
	 * @throws NoSuchMediaStoreException if a media store with the primary key could not be found
	 */
	@Override
	public MediaStore findByPrimaryKey(String mediaUid)
		throws NoSuchMediaStoreException {
		return findByPrimaryKey((Serializable)mediaUid);
	}

	/**
	 * Returns the media store with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the media store
	 * @return the media store, or <code>null</code> if a media store with the primary key could not be found
	 */
	@Override
	public MediaStore fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
				MediaStoreImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		MediaStore mediaStore = (MediaStore)serializable;

		if (mediaStore == null) {
			Session session = null;

			try {
				session = openSession();

				mediaStore = (MediaStore)session.get(MediaStoreImpl.class,
						primaryKey);

				if (mediaStore != null) {
					cacheResult(mediaStore);
				}
				else {
					entityCache.putResult(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
						MediaStoreImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
					MediaStoreImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return mediaStore;
	}

	/**
	 * Returns the media store with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param mediaUid the primary key of the media store
	 * @return the media store, or <code>null</code> if a media store with the primary key could not be found
	 */
	@Override
	public MediaStore fetchByPrimaryKey(String mediaUid) {
		return fetchByPrimaryKey((Serializable)mediaUid);
	}

	@Override
	public Map<Serializable, MediaStore> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, MediaStore> map = new HashMap<Serializable, MediaStore>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			MediaStore mediaStore = fetchByPrimaryKey(primaryKey);

			if (mediaStore != null) {
				map.put(primaryKey, mediaStore);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
					MediaStoreImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (MediaStore)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_MEDIASTORE_WHERE_PKS_IN);

		for (int i = 0; i < uncachedPrimaryKeys.size(); i++) {
			query.append(StringPool.QUESTION);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			QueryPos qPos = QueryPos.getInstance(q);

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				qPos.add((String)primaryKey);
			}

			for (MediaStore mediaStore : (List<MediaStore>)q.list()) {
				map.put(mediaStore.getPrimaryKeyObj(), mediaStore);

				cacheResult(mediaStore);

				uncachedPrimaryKeys.remove(mediaStore.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(MediaStoreModelImpl.ENTITY_CACHE_ENABLED,
					MediaStoreImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the media stores.
	 *
	 * @return the media stores
	 */
	@Override
	public List<MediaStore> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the media stores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of media stores
	 * @param end the upper bound of the range of media stores (not inclusive)
	 * @return the range of media stores
	 */
	@Override
	public List<MediaStore> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the media stores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of media stores
	 * @param end the upper bound of the range of media stores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of media stores
	 */
	@Override
	public List<MediaStore> findAll(int start, int end,
		OrderByComparator<MediaStore> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the media stores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of media stores
	 * @param end the upper bound of the range of media stores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of media stores
	 */
	@Override
	public List<MediaStore> findAll(int start, int end,
		OrderByComparator<MediaStore> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<MediaStore> list = null;

		if (retrieveFromCache) {
			list = (List<MediaStore>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_MEDIASTORE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_MEDIASTORE;

				if (pagination) {
					sql = sql.concat(MediaStoreModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<MediaStore>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<MediaStore>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the media stores from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (MediaStore mediaStore : findAll()) {
			remove(mediaStore);
		}
	}

	/**
	 * Returns the number of media stores.
	 *
	 * @return the number of media stores
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_MEDIASTORE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return MediaStoreModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the media store persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(MediaStoreImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_MEDIASTORE = "SELECT mediaStore FROM MediaStore mediaStore";
	private static final String _SQL_SELECT_MEDIASTORE_WHERE_PKS_IN = "SELECT mediaStore FROM MediaStore mediaStore WHERE mediaUid IN (";
	private static final String _SQL_COUNT_MEDIASTORE = "SELECT COUNT(mediaStore) FROM MediaStore mediaStore";
	private static final String _ORDER_BY_ENTITY_ALIAS = "mediaStore.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No MediaStore exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(MediaStorePersistenceImpl.class);
}