package com.aw.common.util.service.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.aw.common.util.service.constant.StatusCode;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

public class DataFormatUtil {

	public static final String STATUS = "status";
	public static final String STATUS_CODE = "code";
	public static final String STATUS_MSG = "message";
	public static final String DATA = "data";
	
	//generate final response by json content and status code
	public static JSONObject getResponse(JSONObject jo, JSONArray ja, String code, String locale){
		//System.out.println("get status "+code);
		JSONObject json = JSONFactoryUtil.createJSONObject();
		
		JSONObject status = JSONFactoryUtil.createJSONObject();
		
		status.put(STATUS_CODE, code);
		status.put(STATUS_MSG, StatusCode.getMsg(code, locale));
		json.put(STATUS, status);

		
		if(StatusCode.SUCCESS.equals(code)){
			//System.out.println("status code is success, start to insert content");
			if(jo != null){
			json.put(DATA, jo);
			}else if(ja != null){
				json.put(DATA, ja);
			}
		}
				
		return json;
	}
	
	public static JSONObject xmlToJson(String inputXml){
		Document doc;
		DocumentBuilder dBuilder;
		JSONObject json = JSONFactoryUtil.createJSONObject(); 
		
		try {
			dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			doc = dBuilder.parse(new ByteArrayInputStream(inputXml.getBytes()));
			
			Element element = doc.getDocumentElement();
			NodeList list = element.getChildNodes();
			for(int i = 0; i < list.getLength(); i++){
				if("column".equalsIgnoreCase(list.item(i).getNodeName())){
					NodeList pair = list.item(i).getChildNodes();
					json.put(pair.item(0).getTextContent(), getValue(pair.item(1).getTextContent()));
				}
			}
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
	private static String getValue(String input){
		if(input != null && input.length() > 0 && !input.equalsIgnoreCase("null")){
			return input;
		}else{
			return "";
		}
	}
}
