/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service.base;

import aQute.bnd.annotation.ProviderType;

import com.aw.common.util.model.MediaStore;
import com.aw.common.util.model.MediaStoreContentBlobModel;
import com.aw.common.util.service.MediaStoreLocalService;
import com.aw.common.util.service.persistence.MediaStorePersistence;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalServiceImpl;
import com.liferay.portal.kernel.service.PersistedModelLocalServiceRegistry;
import com.liferay.portal.kernel.service.persistence.ClassNamePersistence;
import com.liferay.portal.kernel.service.persistence.UserPersistence;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the media store local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link com.aw.common.util.service.impl.MediaStoreLocalServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.aw.common.util.service.impl.MediaStoreLocalServiceImpl
 * @see com.aw.common.util.service.MediaStoreLocalServiceUtil
 * @generated
 */
@ProviderType
public abstract class MediaStoreLocalServiceBaseImpl
	extends BaseLocalServiceImpl implements MediaStoreLocalService,
		IdentifiableOSGiService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link com.aw.common.util.service.MediaStoreLocalServiceUtil} to access the media store local service.
	 */

	/**
	 * Adds the media store to the database. Also notifies the appropriate model listeners.
	 *
	 * @param mediaStore the media store
	 * @return the media store that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public MediaStore addMediaStore(MediaStore mediaStore) {
		mediaStore.setNew(true);

		return mediaStorePersistence.update(mediaStore);
	}

	/**
	 * Creates a new media store with the primary key. Does not add the media store to the database.
	 *
	 * @param mediaUid the primary key for the new media store
	 * @return the new media store
	 */
	@Override
	public MediaStore createMediaStore(String mediaUid) {
		return mediaStorePersistence.create(mediaUid);
	}

	/**
	 * Deletes the media store with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mediaUid the primary key of the media store
	 * @return the media store that was removed
	 * @throws PortalException if a media store with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public MediaStore deleteMediaStore(String mediaUid)
		throws PortalException {
		return mediaStorePersistence.remove(mediaUid);
	}

	/**
	 * Deletes the media store from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mediaStore the media store
	 * @return the media store that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public MediaStore deleteMediaStore(MediaStore mediaStore) {
		return mediaStorePersistence.remove(mediaStore);
	}

	@Override
	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(MediaStore.class,
			clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return mediaStorePersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.common.util.model.impl.MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end) {
		return mediaStorePersistence.findWithDynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.common.util.model.impl.MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator) {
		return mediaStorePersistence.findWithDynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return mediaStorePersistence.countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection) {
		return mediaStorePersistence.countWithDynamicQuery(dynamicQuery,
			projection);
	}

	@Override
	public MediaStore fetchMediaStore(String mediaUid) {
		return mediaStorePersistence.fetchByPrimaryKey(mediaUid);
	}

	/**
	 * Returns the media store with the primary key.
	 *
	 * @param mediaUid the primary key of the media store
	 * @return the media store
	 * @throws PortalException if a media store with the primary key could not be found
	 */
	@Override
	public MediaStore getMediaStore(String mediaUid) throws PortalException {
		return mediaStorePersistence.findByPrimaryKey(mediaUid);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException {
		return mediaStoreLocalService.deleteMediaStore((MediaStore)persistedModel);
	}

	@Override
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {
		return mediaStorePersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the media stores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.common.util.model.impl.MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of media stores
	 * @param end the upper bound of the range of media stores (not inclusive)
	 * @return the range of media stores
	 */
	@Override
	public List<MediaStore> getMediaStores(int start, int end) {
		return mediaStorePersistence.findAll(start, end);
	}

	/**
	 * Returns the number of media stores.
	 *
	 * @return the number of media stores
	 */
	@Override
	public int getMediaStoresCount() {
		return mediaStorePersistence.countAll();
	}

	/**
	 * Updates the media store in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param mediaStore the media store
	 * @return the media store that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public MediaStore updateMediaStore(MediaStore mediaStore) {
		return mediaStorePersistence.update(mediaStore);
	}

	@Override
	public MediaStoreContentBlobModel getContentBlobModel(
		Serializable primaryKey) {
		Session session = null;

		try {
			session = mediaStorePersistence.openSession();

			return (MediaStoreContentBlobModel)session.get(MediaStoreContentBlobModel.class,
				primaryKey);
		}
		catch (Exception e) {
			throw mediaStorePersistence.processException(e);
		}
		finally {
			mediaStorePersistence.closeSession(session);
		}
	}

	/**
	 * Returns the common util local service.
	 *
	 * @return the common util local service
	 */
	public com.aw.common.util.service.CommonUtilLocalService getCommonUtilLocalService() {
		return commonUtilLocalService;
	}

	/**
	 * Sets the common util local service.
	 *
	 * @param commonUtilLocalService the common util local service
	 */
	public void setCommonUtilLocalService(
		com.aw.common.util.service.CommonUtilLocalService commonUtilLocalService) {
		this.commonUtilLocalService = commonUtilLocalService;
	}

	/**
	 * Returns the error code local service.
	 *
	 * @return the error code local service
	 */
	public com.aw.common.util.service.ErrorCodeLocalService getErrorCodeLocalService() {
		return errorCodeLocalService;
	}

	/**
	 * Sets the error code local service.
	 *
	 * @param errorCodeLocalService the error code local service
	 */
	public void setErrorCodeLocalService(
		com.aw.common.util.service.ErrorCodeLocalService errorCodeLocalService) {
		this.errorCodeLocalService = errorCodeLocalService;
	}

	/**
	 * Returns the log local service.
	 *
	 * @return the log local service
	 */
	public com.aw.common.util.service.LogLocalService getLogLocalService() {
		return logLocalService;
	}

	/**
	 * Sets the log local service.
	 *
	 * @param logLocalService the log local service
	 */
	public void setLogLocalService(
		com.aw.common.util.service.LogLocalService logLocalService) {
		this.logLocalService = logLocalService;
	}

	/**
	 * Returns the media store local service.
	 *
	 * @return the media store local service
	 */
	public MediaStoreLocalService getMediaStoreLocalService() {
		return mediaStoreLocalService;
	}

	/**
	 * Sets the media store local service.
	 *
	 * @param mediaStoreLocalService the media store local service
	 */
	public void setMediaStoreLocalService(
		MediaStoreLocalService mediaStoreLocalService) {
		this.mediaStoreLocalService = mediaStoreLocalService;
	}

	/**
	 * Returns the media store persistence.
	 *
	 * @return the media store persistence
	 */
	public MediaStorePersistence getMediaStorePersistence() {
		return mediaStorePersistence;
	}

	/**
	 * Sets the media store persistence.
	 *
	 * @param mediaStorePersistence the media store persistence
	 */
	public void setMediaStorePersistence(
		MediaStorePersistence mediaStorePersistence) {
		this.mediaStorePersistence = mediaStorePersistence;
	}

	/**
	 * Returns the permission checker local service.
	 *
	 * @return the permission checker local service
	 */
	public com.aw.common.util.service.PermissionCheckerLocalService getPermissionCheckerLocalService() {
		return permissionCheckerLocalService;
	}

	/**
	 * Sets the permission checker local service.
	 *
	 * @param permissionCheckerLocalService the permission checker local service
	 */
	public void setPermissionCheckerLocalService(
		com.aw.common.util.service.PermissionCheckerLocalService permissionCheckerLocalService) {
		this.permissionCheckerLocalService = permissionCheckerLocalService;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.kernel.service.CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.kernel.service.CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the class name local service.
	 *
	 * @return the class name local service
	 */
	public com.liferay.portal.kernel.service.ClassNameLocalService getClassNameLocalService() {
		return classNameLocalService;
	}

	/**
	 * Sets the class name local service.
	 *
	 * @param classNameLocalService the class name local service
	 */
	public void setClassNameLocalService(
		com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService) {
		this.classNameLocalService = classNameLocalService;
	}

	/**
	 * Returns the class name persistence.
	 *
	 * @return the class name persistence
	 */
	public ClassNamePersistence getClassNamePersistence() {
		return classNamePersistence;
	}

	/**
	 * Sets the class name persistence.
	 *
	 * @param classNamePersistence the class name persistence
	 */
	public void setClassNamePersistence(
		ClassNamePersistence classNamePersistence) {
		this.classNamePersistence = classNamePersistence;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.kernel.service.ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.kernel.service.UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.kernel.service.UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		persistedModelLocalServiceRegistry.register("com.aw.common.util.model.MediaStore",
			mediaStoreLocalService);
	}

	public void destroy() {
		persistedModelLocalServiceRegistry.unregister(
			"com.aw.common.util.model.MediaStore");
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return MediaStoreLocalService.class.getName();
	}

	protected Class<?> getModelClass() {
		return MediaStore.class;
	}

	protected String getModelClassName() {
		return MediaStore.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = mediaStorePersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = com.aw.common.util.service.CommonUtilLocalService.class)
	protected com.aw.common.util.service.CommonUtilLocalService commonUtilLocalService;
	@BeanReference(type = com.aw.common.util.service.ErrorCodeLocalService.class)
	protected com.aw.common.util.service.ErrorCodeLocalService errorCodeLocalService;
	@BeanReference(type = com.aw.common.util.service.LogLocalService.class)
	protected com.aw.common.util.service.LogLocalService logLocalService;
	@BeanReference(type = MediaStoreLocalService.class)
	protected MediaStoreLocalService mediaStoreLocalService;
	@BeanReference(type = MediaStorePersistence.class)
	protected MediaStorePersistence mediaStorePersistence;
	@BeanReference(type = com.aw.common.util.service.PermissionCheckerLocalService.class)
	protected com.aw.common.util.service.PermissionCheckerLocalService permissionCheckerLocalService;
	@ServiceReference(type = com.liferay.counter.kernel.service.CounterLocalService.class)
	protected com.liferay.counter.kernel.service.CounterLocalService counterLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.ClassNameLocalService.class)
	protected com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService;
	@ServiceReference(type = ClassNamePersistence.class)
	protected ClassNamePersistence classNamePersistence;
	@ServiceReference(type = com.liferay.portal.kernel.service.ResourceLocalService.class)
	protected com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.UserLocalService.class)
	protected com.liferay.portal.kernel.service.UserLocalService userLocalService;
	@ServiceReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	@ServiceReference(type = PersistedModelLocalServiceRegistry.class)
	protected PersistedModelLocalServiceRegistry persistedModelLocalServiceRegistry;
}