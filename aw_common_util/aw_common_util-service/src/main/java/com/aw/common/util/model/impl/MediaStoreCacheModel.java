/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.common.util.model.MediaStore;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing MediaStore in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see MediaStore
 * @generated
 */
@ProviderType
public class MediaStoreCacheModel implements CacheModel<MediaStore>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MediaStoreCacheModel)) {
			return false;
		}

		MediaStoreCacheModel mediaStoreCacheModel = (MediaStoreCacheModel)obj;

		if (mediaUid.equals(mediaStoreCacheModel.mediaUid)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, mediaUid);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{mediaUid=");
		sb.append(mediaUid);
		sb.append(", isActive=");
		sb.append(isActive);
		sb.append(", ownerUid=");
		sb.append(ownerUid);
		sb.append(", publicFlag=");
		sb.append(publicFlag);
		sb.append(", fileType=");
		sb.append(fileType);
		sb.append(", fileExt=");
		sb.append(fileExt);
		sb.append(", fileSize=");
		sb.append(fileSize);
		sb.append(", addTime=");
		sb.append(addTime);
		sb.append(", updateTime=");
		sb.append(updateTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public MediaStore toEntityModel() {
		MediaStoreImpl mediaStoreImpl = new MediaStoreImpl();

		if (mediaUid == null) {
			mediaStoreImpl.setMediaUid(StringPool.BLANK);
		}
		else {
			mediaStoreImpl.setMediaUid(mediaUid);
		}

		if (isActive == null) {
			mediaStoreImpl.setIsActive(StringPool.BLANK);
		}
		else {
			mediaStoreImpl.setIsActive(isActive);
		}

		if (ownerUid == null) {
			mediaStoreImpl.setOwnerUid(StringPool.BLANK);
		}
		else {
			mediaStoreImpl.setOwnerUid(ownerUid);
		}

		if (publicFlag == null) {
			mediaStoreImpl.setPublicFlag(StringPool.BLANK);
		}
		else {
			mediaStoreImpl.setPublicFlag(publicFlag);
		}

		if (fileType == null) {
			mediaStoreImpl.setFileType(StringPool.BLANK);
		}
		else {
			mediaStoreImpl.setFileType(fileType);
		}

		if (fileExt == null) {
			mediaStoreImpl.setFileExt(StringPool.BLANK);
		}
		else {
			mediaStoreImpl.setFileExt(fileExt);
		}

		if (fileSize == null) {
			mediaStoreImpl.setFileSize(StringPool.BLANK);
		}
		else {
			mediaStoreImpl.setFileSize(fileSize);
		}

		if (addTime == Long.MIN_VALUE) {
			mediaStoreImpl.setAddTime(null);
		}
		else {
			mediaStoreImpl.setAddTime(new Date(addTime));
		}

		if (updateTime == Long.MIN_VALUE) {
			mediaStoreImpl.setUpdateTime(null);
		}
		else {
			mediaStoreImpl.setUpdateTime(new Date(updateTime));
		}

		mediaStoreImpl.resetOriginalValues();

		return mediaStoreImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		mediaUid = objectInput.readUTF();
		isActive = objectInput.readUTF();
		ownerUid = objectInput.readUTF();
		publicFlag = objectInput.readUTF();
		fileType = objectInput.readUTF();
		fileExt = objectInput.readUTF();
		fileSize = objectInput.readUTF();
		addTime = objectInput.readLong();
		updateTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (mediaUid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mediaUid);
		}

		if (isActive == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(isActive);
		}

		if (ownerUid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ownerUid);
		}

		if (publicFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(publicFlag);
		}

		if (fileType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(fileType);
		}

		if (fileExt == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(fileExt);
		}

		if (fileSize == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(fileSize);
		}

		objectOutput.writeLong(addTime);
		objectOutput.writeLong(updateTime);
	}

	public String mediaUid;
	public String isActive;
	public String ownerUid;
	public String publicFlag;
	public String fileType;
	public String fileExt;
	public String fileSize;
	public long addTime;
	public long updateTime;
}