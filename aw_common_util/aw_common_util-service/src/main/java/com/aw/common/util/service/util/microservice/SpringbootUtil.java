package com.aw.common.util.service.util.microservice;

import java.util.Map;

import com.aw.common.util.service.util.PropertyUtil;

//To call the local springboot services
public class SpringbootUtil{
	
	/*
	 * Here is asuming we only have one springboot servier, to support
	 * multiple, we should store path in a map as key with the number 
	 * of boot instance name as value, then use value to look up port
	 * */
	private static final String port = PropertyUtil.getProp("springboot");

	public static String callServices(String path, Map<String,String> input){
		return MicroServiceUtil.callService(port, path, input);
	}
}
