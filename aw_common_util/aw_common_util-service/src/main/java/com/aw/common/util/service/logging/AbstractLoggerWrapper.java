package com.aw.common.util.service.logging;
import org.apache.log4j.Logger;;
/**
 * Application logging abstract implementation for methods.
 * 
 * @author faisal.qadeer
 *
 */
public abstract class AbstractLoggerWrapper implements ILoggerWrapper {
	
	protected Logger logger;
	
	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}
}
