package com.aw.common.util.service.logging;

/**
 * Default logger implementation for application.
 * 
 * @author faisal.qadeer
 *
 */
public class LoggerWrapperImp extends AbstractLoggerWrapper {

	private String  currentUserID="Current Thread User ID:";
	@Override
	public void debug(String debug, String userId) {

		this.logger.debug(debug);		
	}

	@Override
	public void info(String info, String userId) {
		this.logger.info(info);		
	}

	@Override
	public void warn(String warn, String userId) {		
		this.logger.warn(warn);
	}
	
	@Override
	public void warn(String warn, Throwable ex, String userId) {
		this.logger.warn(warn, ex);
	}

	@Override
	public void error(String error, String userId) {
		if(userId != null)
		{
			this.logger.error(currentUserID.concat(userId));
		}
		this.logger.error(error);		
	}

	@Override
	public void error(Throwable throwable, String userId) {
		if(userId!=null)
		{
			this.logger.error(currentUserID.concat(userId));
		}
		this.logger.error(throwable);
	}

	@Override
	public void error(String error, Throwable throwable, String userId) {
		if(userId!=null)
		{
			this.logger.error(currentUserID.concat(userId));
		}
		this.logger.error(error, throwable);
	}

	@Override
	public void fatal(String error, String userId) {
		if(userId!=null)
		{
			this.logger.fatal(currentUserID.concat(userId));
		}
		this.logger.fatal(error);
		
	}

	@Override
	public void fatal(String error, Exception exc, String userId) {
		if(userId!=null)
		{
			this.logger.fatal(currentUserID.concat(userId));
		}
		this.logger.fatal(error,exc);
	}

	@Override
	public void infoPerformance(String info, String userId) {
		
		
	}

}
