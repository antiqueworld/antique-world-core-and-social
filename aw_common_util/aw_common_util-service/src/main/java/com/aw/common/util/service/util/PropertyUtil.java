package com.aw.common.util.service.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.liferay.portal.kernel.util.PropsUtil;

public class PropertyUtil {
	public static final String CASSANDRA_IP = PropsUtil.get("cassandraNode");
	public static final String ISNTANCE_NAME = PropsUtil.get("instanceName");
	public static final String DEFAULT_LOCALE = PropsUtil.get("defaultLocale");
	
	private static Properties prop = null;
	static{
		
		InputStream input = null;

		try {

			input = new FileInputStream("/apps/microservice.properties");
			prop = new Properties();
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String getProp(String key) {
		String s = prop.getProperty(key);
		return s;
	}

}
