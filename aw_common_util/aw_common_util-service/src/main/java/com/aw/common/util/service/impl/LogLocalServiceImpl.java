/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service.impl;

import com.aw.common.util.service.base.LogLocalServiceBaseImpl;
import com.aw.common.util.service.util.LogUtil;

/**
 * The implementation of the log local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.common.util.service.LogLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LogLocalServiceBaseImpl
 * @see com.aw.common.util.service.LogLocalServiceUtil
 */
public class LogLocalServiceImpl extends LogLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.common.util.service.LogLocalServiceUtil} to access the log local service.
	 */
	public String getError(){return LogUtil.ERROR;}
	
	public String getInfo(){return LogUtil.INFO;}
	
	public String getDebug(){return LogUtil.DEBUG;}
	
	public String getFatal(){return LogUtil.FATAL;}
	
	public String getWarn(){return LogUtil.WARN;}
	
	//log at the app level, will be logged into the file with the class name
	public boolean appLog(Class<? extends Object> clazz, String type, String msg, Exception e){
		try{
			LogUtil.log(clazz, type, msg, e);
			return true;
		}catch(Exception ex){
			return false;
		}
	}
	
	//common log, will be logged to a central file LogUtil
	public boolean commonLog(String type, String msg, Exception e){
		try{
			LogUtil.log(LogUtil.class, type, msg, e);
			return true;
		}catch(Exception ex){
			return false;
		}
	}
	
}