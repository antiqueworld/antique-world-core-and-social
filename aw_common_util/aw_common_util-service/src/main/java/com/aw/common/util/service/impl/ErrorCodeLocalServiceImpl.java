/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service.impl;

import com.aw.common.util.service.base.ErrorCodeLocalServiceBaseImpl;
import com.aw.common.util.service.constant.StatusCode;;

/**
 * The implementation of the error code local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.common.util.service.ErrorCodeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ErrorCodeLocalServiceBaseImpl
 * @see com.aw.common.util.service.ErrorCodeLocalServiceUtil
 */

//Add any global error code here, then generate getter method
public class ErrorCodeLocalServiceImpl extends ErrorCodeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.common.util.service.ErrorCodeLocalServiceUtil} to access the error code local service.
	 */
	
	
	//getters
	public String getSuccess() {return StatusCode.SUCCESS;}
	public String getPermission() {return StatusCode.PERMISSION;}
	
	public String getUserNotExist() {return StatusCode.USER_NOT_EXIST;}
	public  String getContactNotExist() {return StatusCode.CONTACT_NOT_EXIST;}
	public String getAddressNotFound() {return StatusCode.ADDRESS_NOT_FOUND;}
	public String getBirthFormatError() {return StatusCode.BIRTH_FORMAT_ERROR;}
	public String getImageFormatError() {return StatusCode.IMAGE_FORMAT_ERROR;}
	public String getPhoneFormatError() {return StatusCode.PHONE_FORMAT_ERROR;}
	public String getAddressFormatError() {return StatusCode.ADDRESS_FORMAT_ERROR;}
	
	public String getErrorMessage(String code, String locale){
		return StatusCode.getMsg(code, locale);
	}	

}