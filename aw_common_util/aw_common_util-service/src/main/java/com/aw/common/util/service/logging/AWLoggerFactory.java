package com.aw.common.util.service.logging;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;


//Generate Logger with path of the class name (log file seperation)
public class AWLoggerFactory {
	
	private static String PATH = System.getProperty("catalina.home") + "/logs";
	
	public static ILoggerWrapper getAppLoggerOnboard(Class<?> clazz){
		Logger rootImpl = getRootLogger(clazz);
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);
		
		return logger;
	}
	//In case even property file is not readable
	public static ILoggerWrapper getFailOverLoggerCommon(Class<?> clazz){
		
		Logger rootImpl = getRootLogger(clazz);		
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);		
		return logger;
	}
	
	//most common, for app level
    public static ILoggerWrapper getAppLoggerCommon(Class<?> clazz){
		
		Logger rootImpl = getRootLogger(clazz);		
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);		
		return logger;
	}
    
    public static ILoggerWrapper getAppLoggerCommon(String logFileName, Class<?> clazz){
		
		Logger rootImpl = getRootLogger(logFileName, clazz);		
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);		
		return logger;
	}
	
	public static ILoggerWrapper getAppLoggerCollect(Class<?> clazz){
		
		Logger rootImpl = getRootLogger(clazz);		
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);	
		return logger;
	}
	
	public static ILoggerWrapper getAppLoggerDeliver(Class<?> del){		
		Logger rootImpl = getRootLogger(del);			
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);			
		return logger;
	}
		
	public static ILoggerWrapper getAppLoggerValidate(Class<?> clazz){
		Logger rootImpl = getRootLogger(clazz);			
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);	
		return logger;
	}
	
	public static ILoggerWrapper getAppLoggerOcrDex(Class<?> clazz){			
		Logger rootImpl = getRootLogger(clazz);		
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);
		return logger;
	}
	
	public static ILoggerWrapper getScheduledJobsLogger(Class<?> clazz){		
		Logger rootImpl = getRootLogger(clazz);		
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);	
		return logger;
	}
	
	public static ILoggerWrapper getInstrumentationLogger(Class<?> clazz){	
		Logger rootImpl = getRootLogger("instrumentation",clazz);
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);
		return logger;
	}
	
	public static ILoggerWrapper getPolicyEngineLogger(Class<?> clazz){	
		Logger rootImpl = getRootLogger(clazz);
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);
		return logger;
	}
	
	
	public static ILoggerWrapper getAppLoggerAvox(Class<?> clazz){
		Logger rootImpl = getRootLogger(clazz);
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);
		return logger;
	}
		
	private static Logger getRootLogger(Class<?> clazz){
		return getRootLogger(clazz.getName(),clazz);
	}

	private static Logger getRootLogger(String fileName, Class<?> clazz){

		Logger rootLogger = null;
		
		try{
			rootLogger = Logger.getLogger(clazz); 
			Layout pattern = new PatternLayout("[%d{MM-dd-yyyy HH:mm:ss,SSS}] [%-5p] [%t]: %c - %m%n");
			String path=null;
			if (PATH != null && !PATH.trim().isEmpty()) {
				if(null!=fileName){
					path = PATH.concat("/applog/"+ fileName+  ".log");
				}else{
					path = PATH.concat("/applog/failover.log");
				}					
				
				FileAppender newAppender = new FileAppender(); 
				
				newAppender.setLayout(pattern);
				newAppender.setFile(path);
				
				newAppender.activateOptions();
				
				rootLogger.setAdditivity(false);
				rootLogger.setLevel(Level.DEBUG);
				rootLogger.removeAllAppenders();
				rootLogger.addAppender(newAppender);
			} else {
				//Unable to fetch JBoss environment. Use the console appender
				if (rootLogger.isEnabledFor(Level.WARN)) {
					rootLogger.warn("jboss.server.log.dir property not found while creating logs for " + fileName + ". Directing logs to console");
				}
								
				ConsoleAppender appender = new ConsoleAppender(pattern);
				
				rootLogger.setAdditivity(false);
				rootLogger.setLevel(Level.ALL);
				rootLogger.addAppender(appender);
			}
		}catch(Exception ex){
			rootLogger.error(ex.getStackTrace().toString());
		}
		return rootLogger;		
	}
	
	public static ILoggerWrapper getAppLoggerDashboard(Class<?> clazz) {			
		Logger rootImpl = getRootLogger(clazz);		
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);		
		return logger;
	}
	
	/*public static Logger getAppLoggerDashboard(Class<?> clazz) {
		return getRootLogger(DAS_PATH_KEY,clazz);
	}*/
	
	public static ILoggerWrapper getAppProfilerLogger(Class<?> clazz) {		
		Logger rootImpl = getRootLogger("profiler",clazz);		
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);		
		return logger;
	}
		
	
		/**
		 * This method for getting documents logger
		 */
	public static ILoggerWrapper getAppLoggerDocuments(Class<?> clazz) {			
			Logger rootImpl = getRootLogger(clazz);			
			ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);			
			return logger;
		}
	
	
	public static ILoggerWrapper getAppBPMSLogger(Class<?> clazz) {	
		Logger rootImpl = getRootLogger(clazz);	
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);
		return logger;
	}
	
	public static ILoggerWrapper getAppOcrLogger(Class<?> clazz) {		
		Logger rootImpl = getRootLogger(clazz);	
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);
		return logger;
	}
	
	
	public static ILoggerWrapper getAppLoggerReqPolicy(Class<?> clazz) {		
		Logger rootImpl = getRootLogger(clazz);
		ILoggerWrapper logger = LoggerFactory.getInstance().getLoggger(LoggerWrapperImp.class.getName(), rootImpl);	
		return logger;
	}
	
	//private static Level determineLogLevel(){
	//	String dbStrLogLevel = ApplicationProperties.getLogProperty(ApplicationConstants.LOG_LEVEL_CONFIG_KEY);
	//	Level logLevel = null;
	//	if(null!=dbStrLogLevel){
	//		switch (dbStrLogLevel)
	//		{
	//			case ApplicationConstants.LOG_LEVEL_DEBUG:
	//				logLevel = 	Level.DEBUG;
	//				break;
	//			case ApplicationConstants.LOG_LEVEL_ERROR:
	//				logLevel = Level.ERROR;
	//				break;
	//			case ApplicationConstants.LOG_LEVEL_FATAL:
	//				logLevel = Level.FATAL;
	//				break;
	//			case ApplicationConstants.LOG_LEVEL_INFO:
	//				logLevel = Level.INFO;
	//				break;
	//			case ApplicationConstants.LOG_LEVEL_WARN:
	//				logLevel = Level.WARN;
	//				break;
	//			default: 
	//				logLevel = Level.ERROR;
	//		}
	//	}else{
	//		logLevel = Level.ERROR;
	//	}
	//	return logLevel;
	//}
//	public static void reloadLogLevel(){
//		Logger root = Logger.getRootLogger();
//		Enumeration allLoggers = root.getLoggerRepository().getCurrentCategories();
//		Level logLeve = determineLogLevel();
//			while (allLoggers.hasMoreElements()){
//		       Category tmpLogger = (Category) allLoggers.nextElement();
//		       if(null!=tmpLogger && null!=tmpLogger.getName() && tmpLogger.getName().startsWith("com.cred")){
//		    	   tmpLogger.setLevel(logLeve);
//		       }
//			}
//	}
}
