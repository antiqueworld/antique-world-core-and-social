package com.aw.common.util.service.logging;

/**
 * Application logging interface to hide the implementation details of application logging.
 * Interface only contains method & properties need to expose external classes. 
 * 
 * @author faisal.qadeer
 *
 */
public interface ILoggerWrapper {

	public abstract void debug(String debug, String userId);

	public abstract void info(String info, String userId);

	public abstract void warn(String warn, String userId);

	public abstract void warn(String warn, Throwable ex, String userId);

	public abstract void error(String error, String userId);

	public abstract void error(Throwable throwable, String userId);

	public abstract void error(String error, Throwable throwable, String userId);
	
	public abstract void fatal(String error, String userId);

	public abstract void fatal(String error, Exception exc, String userId);
	
	public abstract void infoPerformance(String info, String userId);
}