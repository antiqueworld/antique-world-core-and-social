package com.aw.common.util.service.util.microservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

//To call the local micro services
//Never call this directly!!!!!!!!
public class MicroServiceUtil {
	
	public static String callService(String port, String path, Map<String,String> input){
		
		StringBuffer result = new StringBuffer("");
		try {

			String address = "http://localhost:"+port+"/"+path;
			//add params
			if(input != null && !input.isEmpty()){
				StringBuffer sb = new StringBuffer(address+"?");
				for(String s : input.keySet()){
					sb.append(s+"="+input.get(s)+"&&");
				}
				sb.append("remove");
				address = sb.toString().replace("&&remove", "");
				
			}
			URL url = new URL(address);
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

			String output;
	
			while ((output = br.readLine()) != null) {
				result.append(output);
			}			
			conn.disconnect();
		  } catch (MalformedURLException e) {
			e.printStackTrace();
		  } catch (IOException e) {
			e.printStackTrace();
		  }
		
		return result.toString();
	}
}
