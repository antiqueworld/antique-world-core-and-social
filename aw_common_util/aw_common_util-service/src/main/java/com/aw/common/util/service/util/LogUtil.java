package com.aw.common.util.service.util;

import com.aw.common.util.service.logging.AWLoggerFactory;
import com.aw.common.util.service.logging.ILoggerWrapper;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;

public class LogUtil {
	//private static final Logger l = Logger.getLogger(new LogUtil().getClass().getName());
	public static final String ERROR = "error";
	public static final String INFO = "info";
	public static final String DEBUG = "debug";
	public static final String FATAL = "fatal";
	public static final String WARN = "warn";
	
	public static void log(Class<? extends Object> clazz, String type, String msg, Exception e){
		
		String userId = null;
		try{
			userId = String.valueOf(PermissionThreadLocal.getPermissionChecker().getUser().getUserId());
		}catch(Exception ex){
			//do nothing
		}
		
		ILoggerWrapper l;
		if(clazz == null){
			l =  AWLoggerFactory.getAppLoggerCommon(LogUtil.class);//for principle errors
		}else{
			l = AWLoggerFactory.getAppLoggerCommon(clazz);
		}
		 
		
		if(e == null){
			switch(type){
			case ERROR : l.error(msg, userId);
						 break;
						 
			case INFO : l.info(msg, userId);
			 			 break;
			 			 
			case DEBUG : l.debug(msg, userId);
					 	 break;
					 	 
			case FATAL : l.fatal(msg, userId);
			 			 break;
			 			 
			case WARN : l.warn(msg, userId);
					  	break;
			}
		}else{
			switch(type){
			case ERROR : l.error(msg, e, userId);
						 break;
						 
			case INFO : l.info(msg, userId);
			 			 break;
			 			 
			case DEBUG : l.debug(msg, userId);
					 	 break;
					 	 
			case FATAL : l.fatal(msg, e, userId);
			 			 break;
			 			 
			case WARN : l.warn(msg, e, userId);
					  	break;
			}
		}	
	}
	
//	//This is how to use the log
//	public static void appLog(Class<? extends Object> clazz, String msg){
//		String userId = null;
//		try{
//			userId = String.valueOf(PermissionThreadLocal.getPermissionChecker().getUser().getUserId());
//		}catch(Exception e){
//			//do nothing
//		}
//			
//		ILoggerWrapper logger = AWLoggerFactory.getAppLoggerCommon(clazz);
//		logger.error(msg, userId);
//		
//	}
}
