/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import com.aw.common.util.service.PermissionCheckerLocalServiceUtil;
import com.aw.common.util.service.base.CommonUtilLocalServiceBaseImpl;
import com.aw.common.util.service.util.DataFormatUtil;
import com.aw.common.util.service.util.MediaDaoUtil;
import com.aw.common.util.service.util.PropertyUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

/**
 * The implementation of the common util local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.common.util.service.CommonUtilLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CommonUtilLocalServiceBaseImpl
 * @see com.aw.common.util.service.CommonUtilLocalServiceUtil
 */
public class CommonUtilLocalServiceImpl extends CommonUtilLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.common.util.service.CommonUtilLocalServiceUtil} to access the common util local service.
	 */
	public static final String PORTRAIT_IMG = "portrait";
	public static final String ID_IMG = "id";
	public static final String ITEM_IMG = "item";
	
	public JSONObject getResponse(JSONObject jo, JSONArray ja, String code, String locale){		
		return DataFormatUtil.getResponse(jo, ja, code, locale);
	}
	
	public  JSONObject getJson (@SuppressWarnings("rawtypes") BaseModel model){
		return DataFormatUtil.xmlToJson(model.toXmlString());
	}
	
	@SuppressWarnings("rawtypes")
	public JSONArray getJsonArray(List<? extends BaseModel> modelList){
		JSONArray ja = JSONFactoryUtil.createJSONArray();
		
		if( modelList == null || modelList.isEmpty()) return ja;
		
		for(BaseModel bm : modelList){
			ja.put(getJson(bm));
		}
		return ja;
	}
	
	//                             must         must          optional      optional         optional
	public boolean uploadMedia(File file, String type, String userUid, String itemId, String isPublic){
		if(type == null){
			return false;
		}
		
		String resUid = "";
		
		byte[] bArray = {1};
		try {
			bArray = Files.readAllBytes(file.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		User u = PermissionCheckerLocalServiceUtil.getCurrentUser();
		if(bArray.length < 3 || u == null){
			return false;
		}
		
		
		//if potrait  update for self, or if userUid not null and is customer Service then insert to customer
		if(type.equalsIgnoreCase(PORTRAIT_IMG)){
			try {
				User tempU = UserLocalServiceUtil.updatePortrait(u.getUserId(), bArray);
				resUid = String.valueOf(tempU.getPortraitId());		
			} catch (PortalException e) {
				e.printStackTrace();
			}			
		}
		//if IDCopy then insert for self, or if userUid not null and is customer Service then insert to customer
		if(type.equalsIgnoreCase(ID_IMG)){
			if(userUid != null && !userUid.isEmpty()){//TODO change roles
				if(PermissionCheckerLocalServiceUtil.isAdmin()!=null)
				resUid = MediaDaoUtil.saveIdImage(bArray, userUid);
			}else{
				resUid = MediaDaoUtil.saveIdImage(bArray, u.getUserUuid());				
			}
			
		}
		
		//if item then get item by id then check if is item owner and if item media reach limit
		if(type.equalsIgnoreCase(ITEM_IMG)){
			if(userUid != null && itemId != null)				
			resUid = MediaDaoUtil.saveItemImage(bArray, userUid, itemId, isPublic);
		}
		System.out.println(resUid);
		
		return !resUid.isEmpty();
	}
	
	//Only used for retrieving non-portrait image
	public byte[] getMedia(String mediaId){
		return MediaDaoUtil.getMedia(mediaId);
	}
	
	public String getCassandraNode(){
		return PropertyUtil.CASSANDRA_IP;
	}
	
	public String getInstanceName(){
		return PropertyUtil.ISNTANCE_NAME;
	}
	
	public String getDefaultLocale(){
		return PropertyUtil.DEFAULT_LOCALE;
	}
}