/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service.impl;

import java.util.Date;

import com.aw.common.util.model.MediaStore;
import com.aw.common.util.service.base.MediaStoreLocalServiceBaseImpl;
import com.aw.common.util.service.persistence.MediaStoreUtil;
import com.liferay.portal.kernel.dao.jdbc.OutputBlob;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

/**
 * The implementation of the media store local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.common.util.service.MediaStoreLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MediaStoreLocalServiceBaseImpl
 * @see com.aw.common.util.service.MediaStoreLocalServiceUtil
 */
public class MediaStoreLocalServiceImpl extends MediaStoreLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.common.util.service.MediaStoreLocalServiceUtil} to access the media store local service.
	 */
	public MediaStore createMedia(byte[] file, String uid, String isPublic){
		MediaStore ms = null;
		String publicFlag = ""+isPublic;
		if(publicFlag.equals("N") || publicFlag.equals("Y")){
			//it's good
		}else{
			publicFlag = "N";
		}
		
		UnsyncByteArrayInputStream inputStream =new UnsyncByteArrayInputStream(file);
		OutputBlob imageBlob = new OutputBlob(inputStream, file.length);
		
		String uuid =  PortalUUIDUtil.generate();
		ms = MediaStoreUtil.create(uuid);
		ms.setContent(imageBlob);
		ms.setPublicFlag(publicFlag);
		
		//TODO add the file detail infor later
		
		Date d = new Date();
		ms.setAddTime(d);
		ms.setUpdateTime(d);
		ms.persist();
		
		return ms;
	}
}