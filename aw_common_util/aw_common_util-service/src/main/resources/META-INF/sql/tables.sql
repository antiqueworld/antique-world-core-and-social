create table aw_util_MediaStore (
	mediaUid VARCHAR(75) not null primary key,
	content BLOB,
	isActive VARCHAR(75) null,
	ownerUid VARCHAR(75) null,
	publicFlag VARCHAR(75) null,
	fileType VARCHAR(75) null,
	fileExt VARCHAR(75) null,
	fileSize VARCHAR(75) null,
	addTime DATE null,
	updateTime DATE null
);