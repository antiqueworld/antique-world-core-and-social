/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ErrorCodeService}.
 *
 * @author Brian Wing Shun Chan
 * @see ErrorCodeService
 * @generated
 */
@ProviderType
public class ErrorCodeServiceWrapper implements ErrorCodeService,
	ServiceWrapper<ErrorCodeService> {
	public ErrorCodeServiceWrapper(ErrorCodeService errorCodeService) {
		_errorCodeService = errorCodeService;
	}

	@Override
	public java.lang.String getErrorMessage(java.lang.String code,
		java.lang.String locale) {
		return _errorCodeService.getErrorMessage(code, locale);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _errorCodeService.getOSGiServiceIdentifier();
	}

	@Override
	public ErrorCodeService getWrappedService() {
		return _errorCodeService;
	}

	@Override
	public void setWrappedService(ErrorCodeService errorCodeService) {
		_errorCodeService = errorCodeService;
	}

	private ErrorCodeService _errorCodeService;
}