/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Log. This utility wraps
 * {@link com.aw.common.util.service.impl.LogLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see LogLocalService
 * @see com.aw.common.util.service.base.LogLocalServiceBaseImpl
 * @see com.aw.common.util.service.impl.LogLocalServiceImpl
 * @generated
 */
@ProviderType
public class LogLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.common.util.service.impl.LogLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static boolean appLog(
		java.lang.Class<?extends java.lang.Object> clazz,
		java.lang.String type, java.lang.String msg, java.lang.Exception e) {
		return getService().appLog(clazz, type, msg, e);
	}

	public static boolean commonLog(java.lang.String type,
		java.lang.String msg, java.lang.Exception e) {
		return getService().commonLog(type, msg, e);
	}

	public static java.lang.String getDebug() {
		return getService().getDebug();
	}

	public static java.lang.String getError() {
		return getService().getError();
	}

	public static java.lang.String getFatal() {
		return getService().getFatal();
	}

	public static java.lang.String getInfo() {
		return getService().getInfo();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static java.lang.String getWarn() {
		return getService().getWarn();
	}

	public static LogLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LogLocalService, LogLocalService> _serviceTracker =
		ServiceTrackerFactory.open(LogLocalService.class);
}