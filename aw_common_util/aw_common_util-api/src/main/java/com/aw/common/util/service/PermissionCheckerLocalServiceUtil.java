/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for PermissionChecker. This utility wraps
 * {@link com.aw.common.util.service.impl.PermissionCheckerLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see PermissionCheckerLocalService
 * @see com.aw.common.util.service.base.PermissionCheckerLocalServiceBaseImpl
 * @see com.aw.common.util.service.impl.PermissionCheckerLocalServiceImpl
 * @generated
 */
@ProviderType
public class PermissionCheckerLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.common.util.service.impl.PermissionCheckerLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.model.User getCurrentUser() {
		return getService().getCurrentUser();
	}

	public static com.liferay.portal.kernel.model.User isAdmin() {
		return getService().isAdmin();
	}

	public static com.liferay.portal.kernel.model.User permissionCheck(
		java.util.Set<java.lang.String> permitedRoles) {
		return getService().permissionCheck(permitedRoles);
	}

	public static java.lang.String getADMIN() {
		return getService().getADMIN();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static java.util.Set<java.lang.String> getRolesForUser() {
		return getService().getRolesForUser();
	}

	public static PermissionCheckerLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<PermissionCheckerLocalService, PermissionCheckerLocalService> _serviceTracker =
		ServiceTrackerFactory.open(PermissionCheckerLocalService.class);
}