/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

import java.io.File;

import java.util.List;

/**
 * Provides the local service interface for CommonUtil. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see CommonUtilLocalServiceUtil
 * @see com.aw.common.util.service.base.CommonUtilLocalServiceBaseImpl
 * @see com.aw.common.util.service.impl.CommonUtilLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface CommonUtilLocalService extends BaseLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CommonUtilLocalServiceUtil} to access the common util local service. Add custom service methods to {@link com.aw.common.util.service.impl.CommonUtilLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public boolean uploadMedia(File file, java.lang.String type,
		java.lang.String userUid, java.lang.String itemId,
		java.lang.String isPublic);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public byte[] getMedia(java.lang.String mediaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONArray getJsonArray(List<?extends BaseModel> modelList);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getJson(BaseModel model);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getResponse(JSONObject jo, JSONArray ja,
		java.lang.String code, java.lang.String locale);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getCassandraNode();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getDefaultLocale();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getInstanceName();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();
}