/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for CommonUtil. This utility wraps
 * {@link com.aw.common.util.service.impl.CommonUtilServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see CommonUtilService
 * @see com.aw.common.util.service.base.CommonUtilServiceBaseImpl
 * @see com.aw.common.util.service.impl.CommonUtilServiceImpl
 * @generated
 */
@ProviderType
public class CommonUtilServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.common.util.service.impl.CommonUtilServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static java.lang.String getBalance(java.lang.String address,
		java.lang.String tokenAddress) {
		return getService().getBalance(address, tokenAddress);
	}

	public static java.lang.String getEthBalance(java.lang.String address) {
		return getService().getEthBalance(address);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static CommonUtilService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<CommonUtilService, CommonUtilService> _serviceTracker =
		ServiceTrackerFactory.open(CommonUtilService.class);
}