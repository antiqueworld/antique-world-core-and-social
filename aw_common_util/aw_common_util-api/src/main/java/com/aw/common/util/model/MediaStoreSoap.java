/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.sql.Blob;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class MediaStoreSoap implements Serializable {
	public static MediaStoreSoap toSoapModel(MediaStore model) {
		MediaStoreSoap soapModel = new MediaStoreSoap();

		soapModel.setMediaUid(model.getMediaUid());
		soapModel.setContent(model.getContent());
		soapModel.setIsActive(model.getIsActive());
		soapModel.setOwnerUid(model.getOwnerUid());
		soapModel.setPublicFlag(model.getPublicFlag());
		soapModel.setFileType(model.getFileType());
		soapModel.setFileExt(model.getFileExt());
		soapModel.setFileSize(model.getFileSize());
		soapModel.setAddTime(model.getAddTime());
		soapModel.setUpdateTime(model.getUpdateTime());

		return soapModel;
	}

	public static MediaStoreSoap[] toSoapModels(MediaStore[] models) {
		MediaStoreSoap[] soapModels = new MediaStoreSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MediaStoreSoap[][] toSoapModels(MediaStore[][] models) {
		MediaStoreSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MediaStoreSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MediaStoreSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MediaStoreSoap[] toSoapModels(List<MediaStore> models) {
		List<MediaStoreSoap> soapModels = new ArrayList<MediaStoreSoap>(models.size());

		for (MediaStore model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MediaStoreSoap[soapModels.size()]);
	}

	public MediaStoreSoap() {
	}

	public String getPrimaryKey() {
		return _mediaUid;
	}

	public void setPrimaryKey(String pk) {
		setMediaUid(pk);
	}

	public String getMediaUid() {
		return _mediaUid;
	}

	public void setMediaUid(String mediaUid) {
		_mediaUid = mediaUid;
	}

	public Blob getContent() {
		return _content;
	}

	public void setContent(Blob content) {
		_content = content;
	}

	public String getIsActive() {
		return _isActive;
	}

	public void setIsActive(String isActive) {
		_isActive = isActive;
	}

	public String getOwnerUid() {
		return _ownerUid;
	}

	public void setOwnerUid(String ownerUid) {
		_ownerUid = ownerUid;
	}

	public String getPublicFlag() {
		return _publicFlag;
	}

	public void setPublicFlag(String publicFlag) {
		_publicFlag = publicFlag;
	}

	public String getFileType() {
		return _fileType;
	}

	public void setFileType(String fileType) {
		_fileType = fileType;
	}

	public String getFileExt() {
		return _fileExt;
	}

	public void setFileExt(String fileExt) {
		_fileExt = fileExt;
	}

	public String getFileSize() {
		return _fileSize;
	}

	public void setFileSize(String fileSize) {
		_fileSize = fileSize;
	}

	public Date getAddTime() {
		return _addTime;
	}

	public void setAddTime(Date addTime) {
		_addTime = addTime;
	}

	public Date getUpdateTime() {
		return _updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		_updateTime = updateTime;
	}

	private String _mediaUid;
	private Blob _content;
	private String _isActive;
	private String _ownerUid;
	private String _publicFlag;
	private String _fileType;
	private String _fileExt;
	private String _fileSize;
	private Date _addTime;
	private Date _updateTime;
}