/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for MediaStore. This utility wraps
 * {@link com.aw.common.util.service.impl.MediaStoreLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see MediaStoreLocalService
 * @see com.aw.common.util.service.base.MediaStoreLocalServiceBaseImpl
 * @see com.aw.common.util.service.impl.MediaStoreLocalServiceImpl
 * @generated
 */
@ProviderType
public class MediaStoreLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.common.util.service.impl.MediaStoreLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the media store to the database. Also notifies the appropriate model listeners.
	*
	* @param mediaStore the media store
	* @return the media store that was added
	*/
	public static com.aw.common.util.model.MediaStore addMediaStore(
		com.aw.common.util.model.MediaStore mediaStore) {
		return getService().addMediaStore(mediaStore);
	}

	public static com.aw.common.util.model.MediaStore createMedia(byte[] file,
		java.lang.String uid, java.lang.String isPublic) {
		return getService().createMedia(file, uid, isPublic);
	}

	/**
	* Creates a new media store with the primary key. Does not add the media store to the database.
	*
	* @param mediaUid the primary key for the new media store
	* @return the new media store
	*/
	public static com.aw.common.util.model.MediaStore createMediaStore(
		java.lang.String mediaUid) {
		return getService().createMediaStore(mediaUid);
	}

	/**
	* Deletes the media store from the database. Also notifies the appropriate model listeners.
	*
	* @param mediaStore the media store
	* @return the media store that was removed
	*/
	public static com.aw.common.util.model.MediaStore deleteMediaStore(
		com.aw.common.util.model.MediaStore mediaStore) {
		return getService().deleteMediaStore(mediaStore);
	}

	/**
	* Deletes the media store with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mediaUid the primary key of the media store
	* @return the media store that was removed
	* @throws PortalException if a media store with the primary key could not be found
	*/
	public static com.aw.common.util.model.MediaStore deleteMediaStore(
		java.lang.String mediaUid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteMediaStore(mediaUid);
	}

	public static com.aw.common.util.model.MediaStore fetchMediaStore(
		java.lang.String mediaUid) {
		return getService().fetchMediaStore(mediaUid);
	}

	/**
	* Returns the media store with the primary key.
	*
	* @param mediaUid the primary key of the media store
	* @return the media store
	* @throws PortalException if a media store with the primary key could not be found
	*/
	public static com.aw.common.util.model.MediaStore getMediaStore(
		java.lang.String mediaUid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getMediaStore(mediaUid);
	}

	/**
	* Updates the media store in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param mediaStore the media store
	* @return the media store that was updated
	*/
	public static com.aw.common.util.model.MediaStore updateMediaStore(
		com.aw.common.util.model.MediaStore mediaStore) {
		return getService().updateMediaStore(mediaStore);
	}

	public static com.aw.common.util.model.MediaStoreContentBlobModel getContentBlobModel(
		java.io.Serializable primaryKey) {
		return getService().getContentBlobModel(primaryKey);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of media stores.
	*
	* @return the number of media stores
	*/
	public static int getMediaStoresCount() {
		return getService().getMediaStoresCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.common.util.model.impl.MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.common.util.model.impl.MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the media stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.common.util.model.impl.MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of media stores
	* @param end the upper bound of the range of media stores (not inclusive)
	* @return the range of media stores
	*/
	public static java.util.List<com.aw.common.util.model.MediaStore> getMediaStores(
		int start, int end) {
		return getService().getMediaStores(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static MediaStoreLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<MediaStoreLocalService, MediaStoreLocalService> _serviceTracker =
		ServiceTrackerFactory.open(MediaStoreLocalService.class);
}