/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ErrorCode. This utility wraps
 * {@link com.aw.common.util.service.impl.ErrorCodeLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ErrorCodeLocalService
 * @see com.aw.common.util.service.base.ErrorCodeLocalServiceBaseImpl
 * @see com.aw.common.util.service.impl.ErrorCodeLocalServiceImpl
 * @generated
 */
@ProviderType
public class ErrorCodeLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.common.util.service.impl.ErrorCodeLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static java.lang.String getAddressFormatError() {
		return getService().getAddressFormatError();
	}

	public static java.lang.String getAddressNotFound() {
		return getService().getAddressNotFound();
	}

	public static java.lang.String getBirthFormatError() {
		return getService().getBirthFormatError();
	}

	public static java.lang.String getContactNotExist() {
		return getService().getContactNotExist();
	}

	public static java.lang.String getErrorMessage(java.lang.String code,
		java.lang.String locale) {
		return getService().getErrorMessage(code, locale);
	}

	public static java.lang.String getImageFormatError() {
		return getService().getImageFormatError();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static java.lang.String getPermission() {
		return getService().getPermission();
	}

	public static java.lang.String getPhoneFormatError() {
		return getService().getPhoneFormatError();
	}

	public static java.lang.String getSuccess() {
		return getService().getSuccess();
	}

	public static java.lang.String getUserNotExist() {
		return getService().getUserNotExist();
	}

	public static ErrorCodeLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ErrorCodeLocalService, ErrorCodeLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ErrorCodeLocalService.class);
}