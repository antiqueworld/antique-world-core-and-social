/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.common.util.model.MediaStore;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the media store service. This utility wraps {@link com.aw.common.util.service.persistence.impl.MediaStorePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MediaStorePersistence
 * @see com.aw.common.util.service.persistence.impl.MediaStorePersistenceImpl
 * @generated
 */
@ProviderType
public class MediaStoreUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(MediaStore mediaStore) {
		getPersistence().clearCache(mediaStore);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<MediaStore> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<MediaStore> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<MediaStore> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<MediaStore> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static MediaStore update(MediaStore mediaStore) {
		return getPersistence().update(mediaStore);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static MediaStore update(MediaStore mediaStore,
		ServiceContext serviceContext) {
		return getPersistence().update(mediaStore, serviceContext);
	}

	/**
	* Caches the media store in the entity cache if it is enabled.
	*
	* @param mediaStore the media store
	*/
	public static void cacheResult(MediaStore mediaStore) {
		getPersistence().cacheResult(mediaStore);
	}

	/**
	* Caches the media stores in the entity cache if it is enabled.
	*
	* @param mediaStores the media stores
	*/
	public static void cacheResult(List<MediaStore> mediaStores) {
		getPersistence().cacheResult(mediaStores);
	}

	/**
	* Creates a new media store with the primary key. Does not add the media store to the database.
	*
	* @param mediaUid the primary key for the new media store
	* @return the new media store
	*/
	public static MediaStore create(java.lang.String mediaUid) {
		return getPersistence().create(mediaUid);
	}

	/**
	* Removes the media store with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mediaUid the primary key of the media store
	* @return the media store that was removed
	* @throws NoSuchMediaStoreException if a media store with the primary key could not be found
	*/
	public static MediaStore remove(java.lang.String mediaUid)
		throws com.aw.common.util.exception.NoSuchMediaStoreException {
		return getPersistence().remove(mediaUid);
	}

	public static MediaStore updateImpl(MediaStore mediaStore) {
		return getPersistence().updateImpl(mediaStore);
	}

	/**
	* Returns the media store with the primary key or throws a {@link NoSuchMediaStoreException} if it could not be found.
	*
	* @param mediaUid the primary key of the media store
	* @return the media store
	* @throws NoSuchMediaStoreException if a media store with the primary key could not be found
	*/
	public static MediaStore findByPrimaryKey(java.lang.String mediaUid)
		throws com.aw.common.util.exception.NoSuchMediaStoreException {
		return getPersistence().findByPrimaryKey(mediaUid);
	}

	/**
	* Returns the media store with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param mediaUid the primary key of the media store
	* @return the media store, or <code>null</code> if a media store with the primary key could not be found
	*/
	public static MediaStore fetchByPrimaryKey(java.lang.String mediaUid) {
		return getPersistence().fetchByPrimaryKey(mediaUid);
	}

	public static java.util.Map<java.io.Serializable, MediaStore> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the media stores.
	*
	* @return the media stores
	*/
	public static List<MediaStore> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the media stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of media stores
	* @param end the upper bound of the range of media stores (not inclusive)
	* @return the range of media stores
	*/
	public static List<MediaStore> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the media stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of media stores
	* @param end the upper bound of the range of media stores (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of media stores
	*/
	public static List<MediaStore> findAll(int start, int end,
		OrderByComparator<MediaStore> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the media stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of media stores
	* @param end the upper bound of the range of media stores (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of media stores
	*/
	public static List<MediaStore> findAll(int start, int end,
		OrderByComparator<MediaStore> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the media stores from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of media stores.
	*
	* @return the number of media stores
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static MediaStorePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<MediaStorePersistence, MediaStorePersistence> _serviceTracker =
		ServiceTrackerFactory.open(MediaStorePersistence.class);
}