/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * Provides the local service interface for Log. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see LogLocalServiceUtil
 * @see com.aw.common.util.service.base.LogLocalServiceBaseImpl
 * @see com.aw.common.util.service.impl.LogLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface LogLocalService extends BaseLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LogLocalServiceUtil} to access the log local service. Add custom service methods to {@link com.aw.common.util.service.impl.LogLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public boolean appLog(java.lang.Class<?extends java.lang.Object> clazz,
		java.lang.String type, java.lang.String msg, java.lang.Exception e);

	public boolean commonLog(java.lang.String type, java.lang.String msg,
		java.lang.Exception e);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getDebug();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getError();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getFatal();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getInfo();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getWarn();
}