/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ErrorCodeLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ErrorCodeLocalService
 * @generated
 */
@ProviderType
public class ErrorCodeLocalServiceWrapper implements ErrorCodeLocalService,
	ServiceWrapper<ErrorCodeLocalService> {
	public ErrorCodeLocalServiceWrapper(
		ErrorCodeLocalService errorCodeLocalService) {
		_errorCodeLocalService = errorCodeLocalService;
	}

	@Override
	public java.lang.String getAddressFormatError() {
		return _errorCodeLocalService.getAddressFormatError();
	}

	@Override
	public java.lang.String getAddressNotFound() {
		return _errorCodeLocalService.getAddressNotFound();
	}

	@Override
	public java.lang.String getBirthFormatError() {
		return _errorCodeLocalService.getBirthFormatError();
	}

	@Override
	public java.lang.String getContactNotExist() {
		return _errorCodeLocalService.getContactNotExist();
	}

	@Override
	public java.lang.String getErrorMessage(java.lang.String code,
		java.lang.String locale) {
		return _errorCodeLocalService.getErrorMessage(code, locale);
	}

	@Override
	public java.lang.String getImageFormatError() {
		return _errorCodeLocalService.getImageFormatError();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _errorCodeLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public java.lang.String getPermission() {
		return _errorCodeLocalService.getPermission();
	}

	@Override
	public java.lang.String getPhoneFormatError() {
		return _errorCodeLocalService.getPhoneFormatError();
	}

	@Override
	public java.lang.String getSuccess() {
		return _errorCodeLocalService.getSuccess();
	}

	@Override
	public java.lang.String getUserNotExist() {
		return _errorCodeLocalService.getUserNotExist();
	}

	@Override
	public ErrorCodeLocalService getWrappedService() {
		return _errorCodeLocalService;
	}

	@Override
	public void setWrappedService(ErrorCodeLocalService errorCodeLocalService) {
		_errorCodeLocalService = errorCodeLocalService;
	}

	private ErrorCodeLocalService _errorCodeLocalService;
}