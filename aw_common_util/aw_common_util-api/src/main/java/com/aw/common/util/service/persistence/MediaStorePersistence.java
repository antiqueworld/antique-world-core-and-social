/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.common.util.exception.NoSuchMediaStoreException;
import com.aw.common.util.model.MediaStore;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the media store service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.aw.common.util.service.persistence.impl.MediaStorePersistenceImpl
 * @see MediaStoreUtil
 * @generated
 */
@ProviderType
public interface MediaStorePersistence extends BasePersistence<MediaStore> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MediaStoreUtil} to access the media store persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the media store in the entity cache if it is enabled.
	*
	* @param mediaStore the media store
	*/
	public void cacheResult(MediaStore mediaStore);

	/**
	* Caches the media stores in the entity cache if it is enabled.
	*
	* @param mediaStores the media stores
	*/
	public void cacheResult(java.util.List<MediaStore> mediaStores);

	/**
	* Creates a new media store with the primary key. Does not add the media store to the database.
	*
	* @param mediaUid the primary key for the new media store
	* @return the new media store
	*/
	public MediaStore create(java.lang.String mediaUid);

	/**
	* Removes the media store with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mediaUid the primary key of the media store
	* @return the media store that was removed
	* @throws NoSuchMediaStoreException if a media store with the primary key could not be found
	*/
	public MediaStore remove(java.lang.String mediaUid)
		throws NoSuchMediaStoreException;

	public MediaStore updateImpl(MediaStore mediaStore);

	/**
	* Returns the media store with the primary key or throws a {@link NoSuchMediaStoreException} if it could not be found.
	*
	* @param mediaUid the primary key of the media store
	* @return the media store
	* @throws NoSuchMediaStoreException if a media store with the primary key could not be found
	*/
	public MediaStore findByPrimaryKey(java.lang.String mediaUid)
		throws NoSuchMediaStoreException;

	/**
	* Returns the media store with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param mediaUid the primary key of the media store
	* @return the media store, or <code>null</code> if a media store with the primary key could not be found
	*/
	public MediaStore fetchByPrimaryKey(java.lang.String mediaUid);

	@Override
	public java.util.Map<java.io.Serializable, MediaStore> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the media stores.
	*
	* @return the media stores
	*/
	public java.util.List<MediaStore> findAll();

	/**
	* Returns a range of all the media stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of media stores
	* @param end the upper bound of the range of media stores (not inclusive)
	* @return the range of media stores
	*/
	public java.util.List<MediaStore> findAll(int start, int end);

	/**
	* Returns an ordered range of all the media stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of media stores
	* @param end the upper bound of the range of media stores (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of media stores
	*/
	public java.util.List<MediaStore> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<MediaStore> orderByComparator);

	/**
	* Returns an ordered range of all the media stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of media stores
	* @param end the upper bound of the range of media stores (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of media stores
	*/
	public java.util.List<MediaStore> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<MediaStore> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the media stores from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of media stores.
	*
	* @return the number of media stores
	*/
	public int countAll();
}