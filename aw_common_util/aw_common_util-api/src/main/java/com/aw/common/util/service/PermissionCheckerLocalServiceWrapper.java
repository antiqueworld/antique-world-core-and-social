/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PermissionCheckerLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PermissionCheckerLocalService
 * @generated
 */
@ProviderType
public class PermissionCheckerLocalServiceWrapper
	implements PermissionCheckerLocalService,
		ServiceWrapper<PermissionCheckerLocalService> {
	public PermissionCheckerLocalServiceWrapper(
		PermissionCheckerLocalService permissionCheckerLocalService) {
		_permissionCheckerLocalService = permissionCheckerLocalService;
	}

	@Override
	public com.liferay.portal.kernel.model.User getCurrentUser() {
		return _permissionCheckerLocalService.getCurrentUser();
	}

	@Override
	public com.liferay.portal.kernel.model.User isAdmin() {
		return _permissionCheckerLocalService.isAdmin();
	}

	@Override
	public com.liferay.portal.kernel.model.User permissionCheck(
		java.util.Set<java.lang.String> permitedRoles) {
		return _permissionCheckerLocalService.permissionCheck(permitedRoles);
	}

	@Override
	public java.lang.String getADMIN() {
		return _permissionCheckerLocalService.getADMIN();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _permissionCheckerLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.Set<java.lang.String> getRolesForUser() {
		return _permissionCheckerLocalService.getRolesForUser();
	}

	@Override
	public PermissionCheckerLocalService getWrappedService() {
		return _permissionCheckerLocalService;
	}

	@Override
	public void setWrappedService(
		PermissionCheckerLocalService permissionCheckerLocalService) {
		_permissionCheckerLocalService = permissionCheckerLocalService;
	}

	private PermissionCheckerLocalService _permissionCheckerLocalService;
}