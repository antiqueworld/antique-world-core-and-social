/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CommonUtilService}.
 *
 * @author Brian Wing Shun Chan
 * @see CommonUtilService
 * @generated
 */
@ProviderType
public class CommonUtilServiceWrapper implements CommonUtilService,
	ServiceWrapper<CommonUtilService> {
	public CommonUtilServiceWrapper(CommonUtilService commonUtilService) {
		_commonUtilService = commonUtilService;
	}

	@Override
	public java.lang.String getBalance(java.lang.String address,
		java.lang.String tokenAddress) {
		return _commonUtilService.getBalance(address, tokenAddress);
	}

	@Override
	public java.lang.String getEthBalance(java.lang.String address) {
		return _commonUtilService.getEthBalance(address);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _commonUtilService.getOSGiServiceIdentifier();
	}

	@Override
	public CommonUtilService getWrappedService() {
		return _commonUtilService;
	}

	@Override
	public void setWrappedService(CommonUtilService commonUtilService) {
		_commonUtilService = commonUtilService;
	}

	private CommonUtilService _commonUtilService;
}