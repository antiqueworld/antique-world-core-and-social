/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.model;

import aQute.bnd.annotation.ProviderType;

import java.sql.Blob;

/**
 * The Blob model class for lazy loading the content column in MediaStore.
 *
 * @author Brian Wing Shun Chan
 * @see MediaStore
 * @generated
 */
@ProviderType
public class MediaStoreContentBlobModel {
	public MediaStoreContentBlobModel() {
	}

	public MediaStoreContentBlobModel(String mediaUid) {
		_mediaUid = mediaUid;
	}

	public MediaStoreContentBlobModel(String mediaUid, Blob contentBlob) {
		_mediaUid = mediaUid;
		_contentBlob = contentBlob;
	}

	public String getMediaUid() {
		return _mediaUid;
	}

	public void setMediaUid(String mediaUid) {
		_mediaUid = mediaUid;
	}

	public Blob getContentBlob() {
		return _contentBlob;
	}

	public void setContentBlob(Blob contentBlob) {
		_contentBlob = contentBlob;
	}

	private String _mediaUid;
	private Blob _contentBlob;
}