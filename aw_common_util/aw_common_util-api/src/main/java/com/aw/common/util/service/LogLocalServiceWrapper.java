/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link LogLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see LogLocalService
 * @generated
 */
@ProviderType
public class LogLocalServiceWrapper implements LogLocalService,
	ServiceWrapper<LogLocalService> {
	public LogLocalServiceWrapper(LogLocalService logLocalService) {
		_logLocalService = logLocalService;
	}

	@Override
	public boolean appLog(java.lang.Class<?extends java.lang.Object> clazz,
		java.lang.String type, java.lang.String msg, java.lang.Exception e) {
		return _logLocalService.appLog(clazz, type, msg, e);
	}

	@Override
	public boolean commonLog(java.lang.String type, java.lang.String msg,
		java.lang.Exception e) {
		return _logLocalService.commonLog(type, msg, e);
	}

	@Override
	public java.lang.String getDebug() {
		return _logLocalService.getDebug();
	}

	@Override
	public java.lang.String getError() {
		return _logLocalService.getError();
	}

	@Override
	public java.lang.String getFatal() {
		return _logLocalService.getFatal();
	}

	@Override
	public java.lang.String getInfo() {
		return _logLocalService.getInfo();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _logLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public java.lang.String getWarn() {
		return _logLocalService.getWarn();
	}

	@Override
	public LogLocalService getWrappedService() {
		return _logLocalService;
	}

	@Override
	public void setWrappedService(LogLocalService logLocalService) {
		_logLocalService = logLocalService;
	}

	private LogLocalService _logLocalService;
}