/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.sql.Blob;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link MediaStore}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MediaStore
 * @generated
 */
@ProviderType
public class MediaStoreWrapper implements MediaStore, ModelWrapper<MediaStore> {
	public MediaStoreWrapper(MediaStore mediaStore) {
		_mediaStore = mediaStore;
	}

	@Override
	public Class<?> getModelClass() {
		return MediaStore.class;
	}

	@Override
	public String getModelClassName() {
		return MediaStore.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("mediaUid", getMediaUid());
		attributes.put("content", getContent());
		attributes.put("isActive", getIsActive());
		attributes.put("ownerUid", getOwnerUid());
		attributes.put("publicFlag", getPublicFlag());
		attributes.put("fileType", getFileType());
		attributes.put("fileExt", getFileExt());
		attributes.put("fileSize", getFileSize());
		attributes.put("addTime", getAddTime());
		attributes.put("updateTime", getUpdateTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String mediaUid = (String)attributes.get("mediaUid");

		if (mediaUid != null) {
			setMediaUid(mediaUid);
		}

		Blob content = (Blob)attributes.get("content");

		if (content != null) {
			setContent(content);
		}

		String isActive = (String)attributes.get("isActive");

		if (isActive != null) {
			setIsActive(isActive);
		}

		String ownerUid = (String)attributes.get("ownerUid");

		if (ownerUid != null) {
			setOwnerUid(ownerUid);
		}

		String publicFlag = (String)attributes.get("publicFlag");

		if (publicFlag != null) {
			setPublicFlag(publicFlag);
		}

		String fileType = (String)attributes.get("fileType");

		if (fileType != null) {
			setFileType(fileType);
		}

		String fileExt = (String)attributes.get("fileExt");

		if (fileExt != null) {
			setFileExt(fileExt);
		}

		String fileSize = (String)attributes.get("fileSize");

		if (fileSize != null) {
			setFileSize(fileSize);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}

		Date updateTime = (Date)attributes.get("updateTime");

		if (updateTime != null) {
			setUpdateTime(updateTime);
		}
	}

	@Override
	public MediaStore toEscapedModel() {
		return new MediaStoreWrapper(_mediaStore.toEscapedModel());
	}

	@Override
	public MediaStore toUnescapedModel() {
		return new MediaStoreWrapper(_mediaStore.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _mediaStore.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _mediaStore.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _mediaStore.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _mediaStore.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<MediaStore> toCacheModel() {
		return _mediaStore.toCacheModel();
	}

	@Override
	public int compareTo(MediaStore mediaStore) {
		return _mediaStore.compareTo(mediaStore);
	}

	@Override
	public int hashCode() {
		return _mediaStore.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _mediaStore.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new MediaStoreWrapper((MediaStore)_mediaStore.clone());
	}

	/**
	* Returns the file ext of this media store.
	*
	* @return the file ext of this media store
	*/
	@Override
	public java.lang.String getFileExt() {
		return _mediaStore.getFileExt();
	}

	/**
	* Returns the file size of this media store.
	*
	* @return the file size of this media store
	*/
	@Override
	public java.lang.String getFileSize() {
		return _mediaStore.getFileSize();
	}

	/**
	* Returns the file type of this media store.
	*
	* @return the file type of this media store
	*/
	@Override
	public java.lang.String getFileType() {
		return _mediaStore.getFileType();
	}

	/**
	* Returns the is active of this media store.
	*
	* @return the is active of this media store
	*/
	@Override
	public java.lang.String getIsActive() {
		return _mediaStore.getIsActive();
	}

	/**
	* Returns the media uid of this media store.
	*
	* @return the media uid of this media store
	*/
	@Override
	public java.lang.String getMediaUid() {
		return _mediaStore.getMediaUid();
	}

	/**
	* Returns the owner uid of this media store.
	*
	* @return the owner uid of this media store
	*/
	@Override
	public java.lang.String getOwnerUid() {
		return _mediaStore.getOwnerUid();
	}

	/**
	* Returns the primary key of this media store.
	*
	* @return the primary key of this media store
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _mediaStore.getPrimaryKey();
	}

	/**
	* Returns the public flag of this media store.
	*
	* @return the public flag of this media store
	*/
	@Override
	public java.lang.String getPublicFlag() {
		return _mediaStore.getPublicFlag();
	}

	@Override
	public java.lang.String toString() {
		return _mediaStore.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _mediaStore.toXmlString();
	}

	/**
	* Returns the content of this media store.
	*
	* @return the content of this media store
	*/
	@Override
	public Blob getContent() {
		return _mediaStore.getContent();
	}

	/**
	* Returns the add time of this media store.
	*
	* @return the add time of this media store
	*/
	@Override
	public Date getAddTime() {
		return _mediaStore.getAddTime();
	}

	/**
	* Returns the update time of this media store.
	*
	* @return the update time of this media store
	*/
	@Override
	public Date getUpdateTime() {
		return _mediaStore.getUpdateTime();
	}

	@Override
	public void persist() {
		_mediaStore.persist();
	}

	/**
	* Sets the add time of this media store.
	*
	* @param addTime the add time of this media store
	*/
	@Override
	public void setAddTime(Date addTime) {
		_mediaStore.setAddTime(addTime);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_mediaStore.setCachedModel(cachedModel);
	}

	/**
	* Sets the content of this media store.
	*
	* @param content the content of this media store
	*/
	@Override
	public void setContent(Blob content) {
		_mediaStore.setContent(content);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_mediaStore.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_mediaStore.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_mediaStore.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the file ext of this media store.
	*
	* @param fileExt the file ext of this media store
	*/
	@Override
	public void setFileExt(java.lang.String fileExt) {
		_mediaStore.setFileExt(fileExt);
	}

	/**
	* Sets the file size of this media store.
	*
	* @param fileSize the file size of this media store
	*/
	@Override
	public void setFileSize(java.lang.String fileSize) {
		_mediaStore.setFileSize(fileSize);
	}

	/**
	* Sets the file type of this media store.
	*
	* @param fileType the file type of this media store
	*/
	@Override
	public void setFileType(java.lang.String fileType) {
		_mediaStore.setFileType(fileType);
	}

	/**
	* Sets the is active of this media store.
	*
	* @param isActive the is active of this media store
	*/
	@Override
	public void setIsActive(java.lang.String isActive) {
		_mediaStore.setIsActive(isActive);
	}

	/**
	* Sets the media uid of this media store.
	*
	* @param mediaUid the media uid of this media store
	*/
	@Override
	public void setMediaUid(java.lang.String mediaUid) {
		_mediaStore.setMediaUid(mediaUid);
	}

	@Override
	public void setNew(boolean n) {
		_mediaStore.setNew(n);
	}

	/**
	* Sets the owner uid of this media store.
	*
	* @param ownerUid the owner uid of this media store
	*/
	@Override
	public void setOwnerUid(java.lang.String ownerUid) {
		_mediaStore.setOwnerUid(ownerUid);
	}

	/**
	* Sets the primary key of this media store.
	*
	* @param primaryKey the primary key of this media store
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_mediaStore.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_mediaStore.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the public flag of this media store.
	*
	* @param publicFlag the public flag of this media store
	*/
	@Override
	public void setPublicFlag(java.lang.String publicFlag) {
		_mediaStore.setPublicFlag(publicFlag);
	}

	/**
	* Sets the update time of this media store.
	*
	* @param updateTime the update time of this media store
	*/
	@Override
	public void setUpdateTime(Date updateTime) {
		_mediaStore.setUpdateTime(updateTime);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MediaStoreWrapper)) {
			return false;
		}

		MediaStoreWrapper mediaStoreWrapper = (MediaStoreWrapper)obj;

		if (Objects.equals(_mediaStore, mediaStoreWrapper._mediaStore)) {
			return true;
		}

		return false;
	}

	@Override
	public MediaStore getWrappedModel() {
		return _mediaStore;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _mediaStore.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _mediaStore.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_mediaStore.resetOriginalValues();
	}

	private final MediaStore _mediaStore;
}