/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CommonUtilLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see CommonUtilLocalService
 * @generated
 */
@ProviderType
public class CommonUtilLocalServiceWrapper implements CommonUtilLocalService,
	ServiceWrapper<CommonUtilLocalService> {
	public CommonUtilLocalServiceWrapper(
		CommonUtilLocalService commonUtilLocalService) {
		_commonUtilLocalService = commonUtilLocalService;
	}

	@Override
	public boolean uploadMedia(java.io.File file, java.lang.String type,
		java.lang.String userUid, java.lang.String itemId,
		java.lang.String isPublic) {
		return _commonUtilLocalService.uploadMedia(file, type, userUid, itemId,
			isPublic);
	}

	@Override
	public byte[] getMedia(java.lang.String mediaId) {
		return _commonUtilLocalService.getMedia(mediaId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray getJsonArray(
		java.util.List<?extends com.liferay.portal.kernel.model.BaseModel> modelList) {
		return _commonUtilLocalService.getJsonArray(modelList);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getJson(
		com.liferay.portal.kernel.model.BaseModel model) {
		return _commonUtilLocalService.getJson(model);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getResponse(
		com.liferay.portal.kernel.json.JSONObject jo,
		com.liferay.portal.kernel.json.JSONArray ja, java.lang.String code,
		java.lang.String locale) {
		return _commonUtilLocalService.getResponse(jo, ja, code, locale);
	}

	@Override
	public java.lang.String getCassandraNode() {
		return _commonUtilLocalService.getCassandraNode();
	}

	@Override
	public java.lang.String getDefaultLocale() {
		return _commonUtilLocalService.getDefaultLocale();
	}

	@Override
	public java.lang.String getInstanceName() {
		return _commonUtilLocalService.getInstanceName();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _commonUtilLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public CommonUtilLocalService getWrappedService() {
		return _commonUtilLocalService;
	}

	@Override
	public void setWrappedService(CommonUtilLocalService commonUtilLocalService) {
		_commonUtilLocalService = commonUtilLocalService;
	}

	private CommonUtilLocalService _commonUtilLocalService;
}