/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.common.util.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MediaStoreLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see MediaStoreLocalService
 * @generated
 */
@ProviderType
public class MediaStoreLocalServiceWrapper implements MediaStoreLocalService,
	ServiceWrapper<MediaStoreLocalService> {
	public MediaStoreLocalServiceWrapper(
		MediaStoreLocalService mediaStoreLocalService) {
		_mediaStoreLocalService = mediaStoreLocalService;
	}

	/**
	* Adds the media store to the database. Also notifies the appropriate model listeners.
	*
	* @param mediaStore the media store
	* @return the media store that was added
	*/
	@Override
	public com.aw.common.util.model.MediaStore addMediaStore(
		com.aw.common.util.model.MediaStore mediaStore) {
		return _mediaStoreLocalService.addMediaStore(mediaStore);
	}

	@Override
	public com.aw.common.util.model.MediaStore createMedia(byte[] file,
		java.lang.String uid, java.lang.String isPublic) {
		return _mediaStoreLocalService.createMedia(file, uid, isPublic);
	}

	/**
	* Creates a new media store with the primary key. Does not add the media store to the database.
	*
	* @param mediaUid the primary key for the new media store
	* @return the new media store
	*/
	@Override
	public com.aw.common.util.model.MediaStore createMediaStore(
		java.lang.String mediaUid) {
		return _mediaStoreLocalService.createMediaStore(mediaUid);
	}

	/**
	* Deletes the media store from the database. Also notifies the appropriate model listeners.
	*
	* @param mediaStore the media store
	* @return the media store that was removed
	*/
	@Override
	public com.aw.common.util.model.MediaStore deleteMediaStore(
		com.aw.common.util.model.MediaStore mediaStore) {
		return _mediaStoreLocalService.deleteMediaStore(mediaStore);
	}

	/**
	* Deletes the media store with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mediaUid the primary key of the media store
	* @return the media store that was removed
	* @throws PortalException if a media store with the primary key could not be found
	*/
	@Override
	public com.aw.common.util.model.MediaStore deleteMediaStore(
		java.lang.String mediaUid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _mediaStoreLocalService.deleteMediaStore(mediaUid);
	}

	@Override
	public com.aw.common.util.model.MediaStore fetchMediaStore(
		java.lang.String mediaUid) {
		return _mediaStoreLocalService.fetchMediaStore(mediaUid);
	}

	/**
	* Returns the media store with the primary key.
	*
	* @param mediaUid the primary key of the media store
	* @return the media store
	* @throws PortalException if a media store with the primary key could not be found
	*/
	@Override
	public com.aw.common.util.model.MediaStore getMediaStore(
		java.lang.String mediaUid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _mediaStoreLocalService.getMediaStore(mediaUid);
	}

	/**
	* Updates the media store in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param mediaStore the media store
	* @return the media store that was updated
	*/
	@Override
	public com.aw.common.util.model.MediaStore updateMediaStore(
		com.aw.common.util.model.MediaStore mediaStore) {
		return _mediaStoreLocalService.updateMediaStore(mediaStore);
	}

	@Override
	public com.aw.common.util.model.MediaStoreContentBlobModel getContentBlobModel(
		java.io.Serializable primaryKey) {
		return _mediaStoreLocalService.getContentBlobModel(primaryKey);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _mediaStoreLocalService.dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _mediaStoreLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _mediaStoreLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of media stores.
	*
	* @return the number of media stores
	*/
	@Override
	public int getMediaStoresCount() {
		return _mediaStoreLocalService.getMediaStoresCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _mediaStoreLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _mediaStoreLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.common.util.model.impl.MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _mediaStoreLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.common.util.model.impl.MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _mediaStoreLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the media stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.common.util.model.impl.MediaStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of media stores
	* @param end the upper bound of the range of media stores (not inclusive)
	* @return the range of media stores
	*/
	@Override
	public java.util.List<com.aw.common.util.model.MediaStore> getMediaStores(
		int start, int end) {
		return _mediaStoreLocalService.getMediaStores(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _mediaStoreLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _mediaStoreLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public MediaStoreLocalService getWrappedService() {
		return _mediaStoreLocalService;
	}

	@Override
	public void setWrappedService(MediaStoreLocalService mediaStoreLocalService) {
		_mediaStoreLocalService = mediaStoreLocalService;
	}

	private MediaStoreLocalService _mediaStoreLocalService;
}