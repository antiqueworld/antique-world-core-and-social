/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.AddressExt;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the address ext service. This utility wraps {@link com.aw.service.social.service.persistence.impl.AddressExtPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AddressExtPersistence
 * @see com.aw.service.social.service.persistence.impl.AddressExtPersistenceImpl
 * @generated
 */
@ProviderType
public class AddressExtUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(AddressExt addressExt) {
		getPersistence().clearCache(addressExt);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AddressExt> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AddressExt> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AddressExt> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<AddressExt> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static AddressExt update(AddressExt addressExt) {
		return getPersistence().update(addressExt);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static AddressExt update(AddressExt addressExt,
		ServiceContext serviceContext) {
		return getPersistence().update(addressExt, serviceContext);
	}

	/**
	* Returns all the address exts where userUid = &#63;.
	*
	* @param userUid the user uid
	* @return the matching address exts
	*/
	public static List<AddressExt> findByuser(java.lang.String userUid) {
		return getPersistence().findByuser(userUid);
	}

	/**
	* Returns a range of all the address exts where userUid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userUid the user uid
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @return the range of matching address exts
	*/
	public static List<AddressExt> findByuser(java.lang.String userUid,
		int start, int end) {
		return getPersistence().findByuser(userUid, start, end);
	}

	/**
	* Returns an ordered range of all the address exts where userUid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userUid the user uid
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching address exts
	*/
	public static List<AddressExt> findByuser(java.lang.String userUid,
		int start, int end, OrderByComparator<AddressExt> orderByComparator) {
		return getPersistence()
				   .findByuser(userUid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the address exts where userUid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userUid the user uid
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching address exts
	*/
	public static List<AddressExt> findByuser(java.lang.String userUid,
		int start, int end, OrderByComparator<AddressExt> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByuser(userUid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first address ext in the ordered set where userUid = &#63;.
	*
	* @param userUid the user uid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching address ext
	* @throws NoSuchAddressExtException if a matching address ext could not be found
	*/
	public static AddressExt findByuser_First(java.lang.String userUid,
		OrderByComparator<AddressExt> orderByComparator)
		throws com.aw.service.social.exception.NoSuchAddressExtException {
		return getPersistence().findByuser_First(userUid, orderByComparator);
	}

	/**
	* Returns the first address ext in the ordered set where userUid = &#63;.
	*
	* @param userUid the user uid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching address ext, or <code>null</code> if a matching address ext could not be found
	*/
	public static AddressExt fetchByuser_First(java.lang.String userUid,
		OrderByComparator<AddressExt> orderByComparator) {
		return getPersistence().fetchByuser_First(userUid, orderByComparator);
	}

	/**
	* Returns the last address ext in the ordered set where userUid = &#63;.
	*
	* @param userUid the user uid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching address ext
	* @throws NoSuchAddressExtException if a matching address ext could not be found
	*/
	public static AddressExt findByuser_Last(java.lang.String userUid,
		OrderByComparator<AddressExt> orderByComparator)
		throws com.aw.service.social.exception.NoSuchAddressExtException {
		return getPersistence().findByuser_Last(userUid, orderByComparator);
	}

	/**
	* Returns the last address ext in the ordered set where userUid = &#63;.
	*
	* @param userUid the user uid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching address ext, or <code>null</code> if a matching address ext could not be found
	*/
	public static AddressExt fetchByuser_Last(java.lang.String userUid,
		OrderByComparator<AddressExt> orderByComparator) {
		return getPersistence().fetchByuser_Last(userUid, orderByComparator);
	}

	/**
	* Returns the address exts before and after the current address ext in the ordered set where userUid = &#63;.
	*
	* @param addressExtPK the primary key of the current address ext
	* @param userUid the user uid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next address ext
	* @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	*/
	public static AddressExt[] findByuser_PrevAndNext(
		AddressExtPK addressExtPK, java.lang.String userUid,
		OrderByComparator<AddressExt> orderByComparator)
		throws com.aw.service.social.exception.NoSuchAddressExtException {
		return getPersistence()
				   .findByuser_PrevAndNext(addressExtPK, userUid,
			orderByComparator);
	}

	/**
	* Removes all the address exts where userUid = &#63; from the database.
	*
	* @param userUid the user uid
	*/
	public static void removeByuser(java.lang.String userUid) {
		getPersistence().removeByuser(userUid);
	}

	/**
	* Returns the number of address exts where userUid = &#63;.
	*
	* @param userUid the user uid
	* @return the number of matching address exts
	*/
	public static int countByuser(java.lang.String userUid) {
		return getPersistence().countByuser(userUid);
	}

	/**
	* Caches the address ext in the entity cache if it is enabled.
	*
	* @param addressExt the address ext
	*/
	public static void cacheResult(AddressExt addressExt) {
		getPersistence().cacheResult(addressExt);
	}

	/**
	* Caches the address exts in the entity cache if it is enabled.
	*
	* @param addressExts the address exts
	*/
	public static void cacheResult(List<AddressExt> addressExts) {
		getPersistence().cacheResult(addressExts);
	}

	/**
	* Creates a new address ext with the primary key. Does not add the address ext to the database.
	*
	* @param addressExtPK the primary key for the new address ext
	* @return the new address ext
	*/
	public static AddressExt create(AddressExtPK addressExtPK) {
		return getPersistence().create(addressExtPK);
	}

	/**
	* Removes the address ext with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param addressExtPK the primary key of the address ext
	* @return the address ext that was removed
	* @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	*/
	public static AddressExt remove(AddressExtPK addressExtPK)
		throws com.aw.service.social.exception.NoSuchAddressExtException {
		return getPersistence().remove(addressExtPK);
	}

	public static AddressExt updateImpl(AddressExt addressExt) {
		return getPersistence().updateImpl(addressExt);
	}

	/**
	* Returns the address ext with the primary key or throws a {@link NoSuchAddressExtException} if it could not be found.
	*
	* @param addressExtPK the primary key of the address ext
	* @return the address ext
	* @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	*/
	public static AddressExt findByPrimaryKey(AddressExtPK addressExtPK)
		throws com.aw.service.social.exception.NoSuchAddressExtException {
		return getPersistence().findByPrimaryKey(addressExtPK);
	}

	/**
	* Returns the address ext with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param addressExtPK the primary key of the address ext
	* @return the address ext, or <code>null</code> if a address ext with the primary key could not be found
	*/
	public static AddressExt fetchByPrimaryKey(AddressExtPK addressExtPK) {
		return getPersistence().fetchByPrimaryKey(addressExtPK);
	}

	public static java.util.Map<java.io.Serializable, AddressExt> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the address exts.
	*
	* @return the address exts
	*/
	public static List<AddressExt> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the address exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @return the range of address exts
	*/
	public static List<AddressExt> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the address exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of address exts
	*/
	public static List<AddressExt> findAll(int start, int end,
		OrderByComparator<AddressExt> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the address exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of address exts
	*/
	public static List<AddressExt> findAll(int start, int end,
		OrderByComparator<AddressExt> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the address exts from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of address exts.
	*
	* @return the number of address exts
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static AddressExtPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AddressExtPersistence, AddressExtPersistence> _serviceTracker =
		ServiceTrackerFactory.open(AddressExtPersistence.class);
}