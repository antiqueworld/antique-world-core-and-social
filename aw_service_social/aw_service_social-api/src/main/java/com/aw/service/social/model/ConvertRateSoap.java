/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ConvertRateSoap implements Serializable {
	public static ConvertRateSoap toSoapModel(ConvertRate model) {
		ConvertRateSoap soapModel = new ConvertRateSoap();

		soapModel.setRateId(model.getRateId());
		soapModel.setFromCurrency(model.getFromCurrency());
		soapModel.setToCurrency(model.getToCurrency());
		soapModel.setIsActive(model.getIsActive());
		soapModel.setRate(model.getRate());
		soapModel.setStartTime(model.getStartTime());
		soapModel.setExpireTime(model.getExpireTime());

		return soapModel;
	}

	public static ConvertRateSoap[] toSoapModels(ConvertRate[] models) {
		ConvertRateSoap[] soapModels = new ConvertRateSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ConvertRateSoap[][] toSoapModels(ConvertRate[][] models) {
		ConvertRateSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ConvertRateSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ConvertRateSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ConvertRateSoap[] toSoapModels(List<ConvertRate> models) {
		List<ConvertRateSoap> soapModels = new ArrayList<ConvertRateSoap>(models.size());

		for (ConvertRate model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ConvertRateSoap[soapModels.size()]);
	}

	public ConvertRateSoap() {
	}

	public String getPrimaryKey() {
		return _rateId;
	}

	public void setPrimaryKey(String pk) {
		setRateId(pk);
	}

	public String getRateId() {
		return _rateId;
	}

	public void setRateId(String rateId) {
		_rateId = rateId;
	}

	public String getFromCurrency() {
		return _fromCurrency;
	}

	public void setFromCurrency(String fromCurrency) {
		_fromCurrency = fromCurrency;
	}

	public String getToCurrency() {
		return _toCurrency;
	}

	public void setToCurrency(String toCurrency) {
		_toCurrency = toCurrency;
	}

	public String getIsActive() {
		return _isActive;
	}

	public void setIsActive(String isActive) {
		_isActive = isActive;
	}

	public double getRate() {
		return _rate;
	}

	public void setRate(double rate) {
		_rate = rate;
	}

	public Date getStartTime() {
		return _startTime;
	}

	public void setStartTime(Date startTime) {
		_startTime = startTime;
	}

	public Date getExpireTime() {
		return _expireTime;
	}

	public void setExpireTime(Date expireTime) {
		_expireTime = expireTime;
	}

	private String _rateId;
	private String _fromCurrency;
	private String _toCurrency;
	private String _isActive;
	private double _rate;
	private Date _startTime;
	private Date _expireTime;
}