/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserInfoService}.
 *
 * @author Brian Wing Shun Chan
 * @see UserInfoService
 * @generated
 */
@ProviderType
public class UserInfoServiceWrapper implements UserInfoService,
	ServiceWrapper<UserInfoService> {
	public UserInfoServiceWrapper(UserInfoService userInfoService) {
		_userInfoService = userInfoService;
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getCountryList(
		java.lang.String locale) {
		return _userInfoService.getCountryList(locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getRegionList(
		java.lang.String countryId, java.lang.String locale) {
		return _userInfoService.getRegionList(countryId, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getUserInfo(
		java.lang.String locale) {
		return _userInfoService.getUserInfo(locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateBasicUserInfo(
		java.lang.String birthday, java.lang.String firstName,
		java.lang.String middleName, java.lang.String lastName,
		java.lang.String title, java.lang.String selfDesc,
		java.lang.String secEmail, java.lang.String phone,
		java.lang.String extension, java.lang.String cellPhone,
		java.lang.String locale) {
		return _userInfoService.updateBasicUserInfo(birthday, firstName,
			middleName, lastName, title, selfDesc, secEmail, phone, extension,
			cellPhone, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateIdImage(
		java.lang.String fileType, byte[] IdImage, java.lang.String isActive,
		java.lang.String publicFlag, java.lang.String locale) {
		return _userInfoService.updateIdImage(fileType, IdImage, isActive,
			publicFlag, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateSensitiveInfo(
		java.lang.String IdIssueCountryId, java.lang.String IdType,
		java.lang.String IdNumber, java.lang.String ssn, java.lang.String locale) {
		return _userInfoService.updateSensitiveInfo(IdIssueCountryId, IdType,
			IdNumber, ssn, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateUserAddressInfo(
		java.lang.String addressType, java.lang.String street1,
		java.lang.String street2, java.lang.String street3,
		java.lang.String city, java.lang.String regionId,
		java.lang.String countryId, java.lang.String zipCode,
		java.lang.String isMailing, java.lang.String locale) {
		return _userInfoService.updateUserAddressInfo(addressType, street1,
			street2, street3, city, regionId, countryId, zipCode, isMailing,
			locale);
	}

	@Override
	public java.lang.String getCountryName(long countryId) {
		return _userInfoService.getCountryName(countryId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _userInfoService.getOSGiServiceIdentifier();
	}

	@Override
	public java.lang.String getRegionName(long regionId) {
		return _userInfoService.getRegionName(regionId);
	}

	@Override
	public UserInfoService getWrappedService() {
		return _userInfoService;
	}

	@Override
	public void setWrappedService(UserInfoService userInfoService) {
		_userInfoService = userInfoService;
	}

	private UserInfoService _userInfoService;
}