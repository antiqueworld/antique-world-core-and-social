/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for UserInfo. This utility wraps
 * {@link com.aw.service.social.service.impl.UserInfoServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see UserInfoService
 * @see com.aw.service.social.service.base.UserInfoServiceBaseImpl
 * @see com.aw.service.social.service.impl.UserInfoServiceImpl
 * @generated
 */
@ProviderType
public class UserInfoServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.service.social.service.impl.UserInfoServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.json.JSONObject getCountryList(
		java.lang.String locale) {
		return getService().getCountryList(locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject getRegionList(
		java.lang.String countryId, java.lang.String locale) {
		return getService().getRegionList(countryId, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject getUserInfo(
		java.lang.String locale) {
		return getService().getUserInfo(locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateBasicUserInfo(
		java.lang.String birthday, java.lang.String firstName,
		java.lang.String middleName, java.lang.String lastName,
		java.lang.String title, java.lang.String selfDesc,
		java.lang.String secEmail, java.lang.String phone,
		java.lang.String extension, java.lang.String cellPhone,
		java.lang.String locale) {
		return getService()
				   .updateBasicUserInfo(birthday, firstName, middleName,
			lastName, title, selfDesc, secEmail, phone, extension, cellPhone,
			locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateIdImage(
		java.lang.String fileType, byte[] IdImage, java.lang.String isActive,
		java.lang.String publicFlag, java.lang.String locale) {
		return getService()
				   .updateIdImage(fileType, IdImage, isActive, publicFlag,
			locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateSensitiveInfo(
		java.lang.String IdIssueCountryId, java.lang.String IdType,
		java.lang.String IdNumber, java.lang.String ssn, java.lang.String locale) {
		return getService()
				   .updateSensitiveInfo(IdIssueCountryId, IdType, IdNumber,
			ssn, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateUserAddressInfo(
		java.lang.String addressType, java.lang.String street1,
		java.lang.String street2, java.lang.String street3,
		java.lang.String city, java.lang.String regionId,
		java.lang.String countryId, java.lang.String zipCode,
		java.lang.String isMailing, java.lang.String locale) {
		return getService()
				   .updateUserAddressInfo(addressType, street1, street2,
			street3, city, regionId, countryId, zipCode, isMailing, locale);
	}

	public static java.lang.String getCountryName(long countryId) {
		return getService().getCountryName(countryId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static java.lang.String getRegionName(long regionId) {
		return getService().getRegionName(regionId);
	}

	public static UserInfoService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UserInfoService, UserInfoService> _serviceTracker =
		ServiceTrackerFactory.open(UserInfoService.class);
}