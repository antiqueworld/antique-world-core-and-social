/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ContactExt}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ContactExt
 * @generated
 */
@ProviderType
public class ContactExtWrapper implements ContactExt, ModelWrapper<ContactExt> {
	public ContactExtWrapper(ContactExt contactExt) {
		_contactExt = contactExt;
	}

	@Override
	public Class<?> getModelClass() {
		return ContactExt.class;
	}

	@Override
	public String getModelClassName() {
		return ContactExt.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userUid", getUserUid());
		attributes.put("primEmail", getPrimEmail());
		attributes.put("secEmail", getSecEmail());
		attributes.put("phone", getPhone());
		attributes.put("extension", getExtension());
		attributes.put("cellPhone", getCellPhone());
		attributes.put("facebook", getFacebook());
		attributes.put("wechat", getWechat());
		attributes.put("twitter", getTwitter());
		attributes.put("weibo", getWeibo());
		attributes.put("addTime", getAddTime());
		attributes.put("updateTime", getUpdateTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String userUid = (String)attributes.get("userUid");

		if (userUid != null) {
			setUserUid(userUid);
		}

		String primEmail = (String)attributes.get("primEmail");

		if (primEmail != null) {
			setPrimEmail(primEmail);
		}

		String secEmail = (String)attributes.get("secEmail");

		if (secEmail != null) {
			setSecEmail(secEmail);
		}

		String phone = (String)attributes.get("phone");

		if (phone != null) {
			setPhone(phone);
		}

		String extension = (String)attributes.get("extension");

		if (extension != null) {
			setExtension(extension);
		}

		String cellPhone = (String)attributes.get("cellPhone");

		if (cellPhone != null) {
			setCellPhone(cellPhone);
		}

		String facebook = (String)attributes.get("facebook");

		if (facebook != null) {
			setFacebook(facebook);
		}

		String wechat = (String)attributes.get("wechat");

		if (wechat != null) {
			setWechat(wechat);
		}

		String twitter = (String)attributes.get("twitter");

		if (twitter != null) {
			setTwitter(twitter);
		}

		String weibo = (String)attributes.get("weibo");

		if (weibo != null) {
			setWeibo(weibo);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}

		Date updateTime = (Date)attributes.get("updateTime");

		if (updateTime != null) {
			setUpdateTime(updateTime);
		}
	}

	@Override
	public ContactExt toEscapedModel() {
		return new ContactExtWrapper(_contactExt.toEscapedModel());
	}

	@Override
	public ContactExt toUnescapedModel() {
		return new ContactExtWrapper(_contactExt.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _contactExt.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _contactExt.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _contactExt.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _contactExt.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ContactExt> toCacheModel() {
		return _contactExt.toCacheModel();
	}

	@Override
	public int compareTo(ContactExt contactExt) {
		return _contactExt.compareTo(contactExt);
	}

	@Override
	public int hashCode() {
		return _contactExt.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _contactExt.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ContactExtWrapper((ContactExt)_contactExt.clone());
	}

	/**
	* Returns the cell phone of this contact ext.
	*
	* @return the cell phone of this contact ext
	*/
	@Override
	public java.lang.String getCellPhone() {
		return _contactExt.getCellPhone();
	}

	/**
	* Returns the extension of this contact ext.
	*
	* @return the extension of this contact ext
	*/
	@Override
	public java.lang.String getExtension() {
		return _contactExt.getExtension();
	}

	/**
	* Returns the facebook of this contact ext.
	*
	* @return the facebook of this contact ext
	*/
	@Override
	public java.lang.String getFacebook() {
		return _contactExt.getFacebook();
	}

	/**
	* Returns the phone of this contact ext.
	*
	* @return the phone of this contact ext
	*/
	@Override
	public java.lang.String getPhone() {
		return _contactExt.getPhone();
	}

	/**
	* Returns the prim email of this contact ext.
	*
	* @return the prim email of this contact ext
	*/
	@Override
	public java.lang.String getPrimEmail() {
		return _contactExt.getPrimEmail();
	}

	/**
	* Returns the primary key of this contact ext.
	*
	* @return the primary key of this contact ext
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _contactExt.getPrimaryKey();
	}

	/**
	* Returns the sec email of this contact ext.
	*
	* @return the sec email of this contact ext
	*/
	@Override
	public java.lang.String getSecEmail() {
		return _contactExt.getSecEmail();
	}

	/**
	* Returns the twitter of this contact ext.
	*
	* @return the twitter of this contact ext
	*/
	@Override
	public java.lang.String getTwitter() {
		return _contactExt.getTwitter();
	}

	/**
	* Returns the user uid of this contact ext.
	*
	* @return the user uid of this contact ext
	*/
	@Override
	public java.lang.String getUserUid() {
		return _contactExt.getUserUid();
	}

	/**
	* Returns the wechat of this contact ext.
	*
	* @return the wechat of this contact ext
	*/
	@Override
	public java.lang.String getWechat() {
		return _contactExt.getWechat();
	}

	/**
	* Returns the weibo of this contact ext.
	*
	* @return the weibo of this contact ext
	*/
	@Override
	public java.lang.String getWeibo() {
		return _contactExt.getWeibo();
	}

	@Override
	public java.lang.String toString() {
		return _contactExt.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _contactExt.toXmlString();
	}

	/**
	* Returns the add time of this contact ext.
	*
	* @return the add time of this contact ext
	*/
	@Override
	public Date getAddTime() {
		return _contactExt.getAddTime();
	}

	/**
	* Returns the update time of this contact ext.
	*
	* @return the update time of this contact ext
	*/
	@Override
	public Date getUpdateTime() {
		return _contactExt.getUpdateTime();
	}

	@Override
	public void persist() {
		_contactExt.persist();
	}

	/**
	* Sets the add time of this contact ext.
	*
	* @param addTime the add time of this contact ext
	*/
	@Override
	public void setAddTime(Date addTime) {
		_contactExt.setAddTime(addTime);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_contactExt.setCachedModel(cachedModel);
	}

	/**
	* Sets the cell phone of this contact ext.
	*
	* @param cellPhone the cell phone of this contact ext
	*/
	@Override
	public void setCellPhone(java.lang.String cellPhone) {
		_contactExt.setCellPhone(cellPhone);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_contactExt.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_contactExt.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_contactExt.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the extension of this contact ext.
	*
	* @param extension the extension of this contact ext
	*/
	@Override
	public void setExtension(java.lang.String extension) {
		_contactExt.setExtension(extension);
	}

	/**
	* Sets the facebook of this contact ext.
	*
	* @param facebook the facebook of this contact ext
	*/
	@Override
	public void setFacebook(java.lang.String facebook) {
		_contactExt.setFacebook(facebook);
	}

	@Override
	public void setNew(boolean n) {
		_contactExt.setNew(n);
	}

	/**
	* Sets the phone of this contact ext.
	*
	* @param phone the phone of this contact ext
	*/
	@Override
	public void setPhone(java.lang.String phone) {
		_contactExt.setPhone(phone);
	}

	/**
	* Sets the prim email of this contact ext.
	*
	* @param primEmail the prim email of this contact ext
	*/
	@Override
	public void setPrimEmail(java.lang.String primEmail) {
		_contactExt.setPrimEmail(primEmail);
	}

	/**
	* Sets the primary key of this contact ext.
	*
	* @param primaryKey the primary key of this contact ext
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_contactExt.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_contactExt.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the sec email of this contact ext.
	*
	* @param secEmail the sec email of this contact ext
	*/
	@Override
	public void setSecEmail(java.lang.String secEmail) {
		_contactExt.setSecEmail(secEmail);
	}

	/**
	* Sets the twitter of this contact ext.
	*
	* @param twitter the twitter of this contact ext
	*/
	@Override
	public void setTwitter(java.lang.String twitter) {
		_contactExt.setTwitter(twitter);
	}

	/**
	* Sets the update time of this contact ext.
	*
	* @param updateTime the update time of this contact ext
	*/
	@Override
	public void setUpdateTime(Date updateTime) {
		_contactExt.setUpdateTime(updateTime);
	}

	/**
	* Sets the user uid of this contact ext.
	*
	* @param userUid the user uid of this contact ext
	*/
	@Override
	public void setUserUid(java.lang.String userUid) {
		_contactExt.setUserUid(userUid);
	}

	/**
	* Sets the wechat of this contact ext.
	*
	* @param wechat the wechat of this contact ext
	*/
	@Override
	public void setWechat(java.lang.String wechat) {
		_contactExt.setWechat(wechat);
	}

	/**
	* Sets the weibo of this contact ext.
	*
	* @param weibo the weibo of this contact ext
	*/
	@Override
	public void setWeibo(java.lang.String weibo) {
		_contactExt.setWeibo(weibo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ContactExtWrapper)) {
			return false;
		}

		ContactExtWrapper contactExtWrapper = (ContactExtWrapper)obj;

		if (Objects.equals(_contactExt, contactExtWrapper._contactExt)) {
			return true;
		}

		return false;
	}

	@Override
	public ContactExt getWrappedModel() {
		return _contactExt;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _contactExt.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _contactExt.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_contactExt.resetOriginalValues();
	}

	private final ContactExt _contactExt;
}