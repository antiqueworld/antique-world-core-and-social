/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Order}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Order
 * @generated
 */
@ProviderType
public class OrderWrapper implements Order, ModelWrapper<Order> {
	public OrderWrapper(Order order) {
		_order = order;
	}

	@Override
	public Class<?> getModelClass() {
		return Order.class;
	}

	@Override
	public String getModelClassName() {
		return Order.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("OrderId", getOrderId());
		attributes.put("userUid", getUserUid());
		attributes.put("userWalletAddr", getUserWalletAddr());
		attributes.put("fromType", getFromType());
		attributes.put("fromCurrency", getFromCurrency());
		attributes.put("fromAmount", getFromAmount());
		attributes.put("rateId", getRateId());
		attributes.put("toCurrency", getToCurrency());
		attributes.put("toAmount", getToAmount());
		attributes.put("chargedFlag", getChargedFlag());
		attributes.put("sentFlag", getSentFlag());
		attributes.put("completedFlag", getCompletedFlag());
		attributes.put("addTime", getAddTime());
		attributes.put("updateTime", getUpdateTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String OrderId = (String)attributes.get("OrderId");

		if (OrderId != null) {
			setOrderId(OrderId);
		}

		String userUid = (String)attributes.get("userUid");

		if (userUid != null) {
			setUserUid(userUid);
		}

		String userWalletAddr = (String)attributes.get("userWalletAddr");

		if (userWalletAddr != null) {
			setUserWalletAddr(userWalletAddr);
		}

		String fromType = (String)attributes.get("fromType");

		if (fromType != null) {
			setFromType(fromType);
		}

		String fromCurrency = (String)attributes.get("fromCurrency");

		if (fromCurrency != null) {
			setFromCurrency(fromCurrency);
		}

		Double fromAmount = (Double)attributes.get("fromAmount");

		if (fromAmount != null) {
			setFromAmount(fromAmount);
		}

		String rateId = (String)attributes.get("rateId");

		if (rateId != null) {
			setRateId(rateId);
		}

		String toCurrency = (String)attributes.get("toCurrency");

		if (toCurrency != null) {
			setToCurrency(toCurrency);
		}

		Double toAmount = (Double)attributes.get("toAmount");

		if (toAmount != null) {
			setToAmount(toAmount);
		}

		String chargedFlag = (String)attributes.get("chargedFlag");

		if (chargedFlag != null) {
			setChargedFlag(chargedFlag);
		}

		String sentFlag = (String)attributes.get("sentFlag");

		if (sentFlag != null) {
			setSentFlag(sentFlag);
		}

		String completedFlag = (String)attributes.get("completedFlag");

		if (completedFlag != null) {
			setCompletedFlag(completedFlag);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}

		Date updateTime = (Date)attributes.get("updateTime");

		if (updateTime != null) {
			setUpdateTime(updateTime);
		}
	}

	@Override
	public Order toEscapedModel() {
		return new OrderWrapper(_order.toEscapedModel());
	}

	@Override
	public Order toUnescapedModel() {
		return new OrderWrapper(_order.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _order.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _order.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _order.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _order.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Order> toCacheModel() {
		return _order.toCacheModel();
	}

	/**
	* Returns the from amount of this order.
	*
	* @return the from amount of this order
	*/
	@Override
	public double getFromAmount() {
		return _order.getFromAmount();
	}

	/**
	* Returns the to amount of this order.
	*
	* @return the to amount of this order
	*/
	@Override
	public double getToAmount() {
		return _order.getToAmount();
	}

	@Override
	public int compareTo(Order order) {
		return _order.compareTo(order);
	}

	@Override
	public int hashCode() {
		return _order.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _order.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new OrderWrapper((Order)_order.clone());
	}

	/**
	* Returns the charged flag of this order.
	*
	* @return the charged flag of this order
	*/
	@Override
	public java.lang.String getChargedFlag() {
		return _order.getChargedFlag();
	}

	/**
	* Returns the completed flag of this order.
	*
	* @return the completed flag of this order
	*/
	@Override
	public java.lang.String getCompletedFlag() {
		return _order.getCompletedFlag();
	}

	/**
	* Returns the from currency of this order.
	*
	* @return the from currency of this order
	*/
	@Override
	public java.lang.String getFromCurrency() {
		return _order.getFromCurrency();
	}

	/**
	* Returns the from type of this order.
	*
	* @return the from type of this order
	*/
	@Override
	public java.lang.String getFromType() {
		return _order.getFromType();
	}

	/**
	* Returns the order ID of this order.
	*
	* @return the order ID of this order
	*/
	@Override
	public java.lang.String getOrderId() {
		return _order.getOrderId();
	}

	/**
	* Returns the primary key of this order.
	*
	* @return the primary key of this order
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _order.getPrimaryKey();
	}

	/**
	* Returns the rate ID of this order.
	*
	* @return the rate ID of this order
	*/
	@Override
	public java.lang.String getRateId() {
		return _order.getRateId();
	}

	/**
	* Returns the sent flag of this order.
	*
	* @return the sent flag of this order
	*/
	@Override
	public java.lang.String getSentFlag() {
		return _order.getSentFlag();
	}

	/**
	* Returns the to currency of this order.
	*
	* @return the to currency of this order
	*/
	@Override
	public java.lang.String getToCurrency() {
		return _order.getToCurrency();
	}

	/**
	* Returns the user uid of this order.
	*
	* @return the user uid of this order
	*/
	@Override
	public java.lang.String getUserUid() {
		return _order.getUserUid();
	}

	/**
	* Returns the user wallet addr of this order.
	*
	* @return the user wallet addr of this order
	*/
	@Override
	public java.lang.String getUserWalletAddr() {
		return _order.getUserWalletAddr();
	}

	@Override
	public java.lang.String toString() {
		return _order.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _order.toXmlString();
	}

	/**
	* Returns the add time of this order.
	*
	* @return the add time of this order
	*/
	@Override
	public Date getAddTime() {
		return _order.getAddTime();
	}

	/**
	* Returns the update time of this order.
	*
	* @return the update time of this order
	*/
	@Override
	public Date getUpdateTime() {
		return _order.getUpdateTime();
	}

	@Override
	public void persist() {
		_order.persist();
	}

	/**
	* Sets the add time of this order.
	*
	* @param addTime the add time of this order
	*/
	@Override
	public void setAddTime(Date addTime) {
		_order.setAddTime(addTime);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_order.setCachedModel(cachedModel);
	}

	/**
	* Sets the charged flag of this order.
	*
	* @param chargedFlag the charged flag of this order
	*/
	@Override
	public void setChargedFlag(java.lang.String chargedFlag) {
		_order.setChargedFlag(chargedFlag);
	}

	/**
	* Sets the completed flag of this order.
	*
	* @param completedFlag the completed flag of this order
	*/
	@Override
	public void setCompletedFlag(java.lang.String completedFlag) {
		_order.setCompletedFlag(completedFlag);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_order.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_order.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_order.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the from amount of this order.
	*
	* @param fromAmount the from amount of this order
	*/
	@Override
	public void setFromAmount(double fromAmount) {
		_order.setFromAmount(fromAmount);
	}

	/**
	* Sets the from currency of this order.
	*
	* @param fromCurrency the from currency of this order
	*/
	@Override
	public void setFromCurrency(java.lang.String fromCurrency) {
		_order.setFromCurrency(fromCurrency);
	}

	/**
	* Sets the from type of this order.
	*
	* @param fromType the from type of this order
	*/
	@Override
	public void setFromType(java.lang.String fromType) {
		_order.setFromType(fromType);
	}

	@Override
	public void setNew(boolean n) {
		_order.setNew(n);
	}

	/**
	* Sets the order ID of this order.
	*
	* @param OrderId the order ID of this order
	*/
	@Override
	public void setOrderId(java.lang.String OrderId) {
		_order.setOrderId(OrderId);
	}

	/**
	* Sets the primary key of this order.
	*
	* @param primaryKey the primary key of this order
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_order.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_order.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the rate ID of this order.
	*
	* @param rateId the rate ID of this order
	*/
	@Override
	public void setRateId(java.lang.String rateId) {
		_order.setRateId(rateId);
	}

	/**
	* Sets the sent flag of this order.
	*
	* @param sentFlag the sent flag of this order
	*/
	@Override
	public void setSentFlag(java.lang.String sentFlag) {
		_order.setSentFlag(sentFlag);
	}

	/**
	* Sets the to amount of this order.
	*
	* @param toAmount the to amount of this order
	*/
	@Override
	public void setToAmount(double toAmount) {
		_order.setToAmount(toAmount);
	}

	/**
	* Sets the to currency of this order.
	*
	* @param toCurrency the to currency of this order
	*/
	@Override
	public void setToCurrency(java.lang.String toCurrency) {
		_order.setToCurrency(toCurrency);
	}

	/**
	* Sets the update time of this order.
	*
	* @param updateTime the update time of this order
	*/
	@Override
	public void setUpdateTime(Date updateTime) {
		_order.setUpdateTime(updateTime);
	}

	/**
	* Sets the user uid of this order.
	*
	* @param userUid the user uid of this order
	*/
	@Override
	public void setUserUid(java.lang.String userUid) {
		_order.setUserUid(userUid);
	}

	/**
	* Sets the user wallet addr of this order.
	*
	* @param userWalletAddr the user wallet addr of this order
	*/
	@Override
	public void setUserWalletAddr(java.lang.String userWalletAddr) {
		_order.setUserWalletAddr(userWalletAddr);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OrderWrapper)) {
			return false;
		}

		OrderWrapper orderWrapper = (OrderWrapper)obj;

		if (Objects.equals(_order, orderWrapper._order)) {
			return true;
		}

		return false;
	}

	@Override
	public Order getWrappedModel() {
		return _order;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _order.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _order.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_order.resetOriginalValues();
	}

	private final Order _order;
}