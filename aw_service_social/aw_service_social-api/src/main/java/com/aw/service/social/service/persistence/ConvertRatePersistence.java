/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.exception.NoSuchConvertRateException;
import com.aw.service.social.model.ConvertRate;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the convert rate service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.aw.service.social.service.persistence.impl.ConvertRatePersistenceImpl
 * @see ConvertRateUtil
 * @generated
 */
@ProviderType
public interface ConvertRatePersistence extends BasePersistence<ConvertRate> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ConvertRateUtil} to access the convert rate persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @return the matching convert rates
	*/
	public java.util.List<ConvertRate> findBypair(
		java.lang.String fromCurrency, java.lang.String toCurrency);

	/**
	* Returns a range of all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @return the range of matching convert rates
	*/
	public java.util.List<ConvertRate> findBypair(
		java.lang.String fromCurrency, java.lang.String toCurrency, int start,
		int end);

	/**
	* Returns an ordered range of all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching convert rates
	*/
	public java.util.List<ConvertRate> findBypair(
		java.lang.String fromCurrency, java.lang.String toCurrency, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator);

	/**
	* Returns an ordered range of all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching convert rates
	*/
	public java.util.List<ConvertRate> findBypair(
		java.lang.String fromCurrency, java.lang.String toCurrency, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching convert rate
	* @throws NoSuchConvertRateException if a matching convert rate could not be found
	*/
	public ConvertRate findBypair_First(java.lang.String fromCurrency,
		java.lang.String toCurrency,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException;

	/**
	* Returns the first convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching convert rate, or <code>null</code> if a matching convert rate could not be found
	*/
	public ConvertRate fetchBypair_First(java.lang.String fromCurrency,
		java.lang.String toCurrency,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator);

	/**
	* Returns the last convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching convert rate
	* @throws NoSuchConvertRateException if a matching convert rate could not be found
	*/
	public ConvertRate findBypair_Last(java.lang.String fromCurrency,
		java.lang.String toCurrency,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException;

	/**
	* Returns the last convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching convert rate, or <code>null</code> if a matching convert rate could not be found
	*/
	public ConvertRate fetchBypair_Last(java.lang.String fromCurrency,
		java.lang.String toCurrency,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator);

	/**
	* Returns the convert rates before and after the current convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param rateId the primary key of the current convert rate
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next convert rate
	* @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	*/
	public ConvertRate[] findBypair_PrevAndNext(java.lang.String rateId,
		java.lang.String fromCurrency, java.lang.String toCurrency,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException;

	/**
	* Removes all the convert rates where fromCurrency = &#63; and toCurrency = &#63; from the database.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	*/
	public void removeBypair(java.lang.String fromCurrency,
		java.lang.String toCurrency);

	/**
	* Returns the number of convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @return the number of matching convert rates
	*/
	public int countBypair(java.lang.String fromCurrency,
		java.lang.String toCurrency);

	/**
	* Returns all the convert rates where isActive = &#63;.
	*
	* @param isActive the is active
	* @return the matching convert rates
	*/
	public java.util.List<ConvertRate> findByactive(java.lang.String isActive);

	/**
	* Returns a range of all the convert rates where isActive = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isActive the is active
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @return the range of matching convert rates
	*/
	public java.util.List<ConvertRate> findByactive(java.lang.String isActive,
		int start, int end);

	/**
	* Returns an ordered range of all the convert rates where isActive = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isActive the is active
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching convert rates
	*/
	public java.util.List<ConvertRate> findByactive(java.lang.String isActive,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator);

	/**
	* Returns an ordered range of all the convert rates where isActive = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isActive the is active
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching convert rates
	*/
	public java.util.List<ConvertRate> findByactive(java.lang.String isActive,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first convert rate in the ordered set where isActive = &#63;.
	*
	* @param isActive the is active
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching convert rate
	* @throws NoSuchConvertRateException if a matching convert rate could not be found
	*/
	public ConvertRate findByactive_First(java.lang.String isActive,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException;

	/**
	* Returns the first convert rate in the ordered set where isActive = &#63;.
	*
	* @param isActive the is active
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching convert rate, or <code>null</code> if a matching convert rate could not be found
	*/
	public ConvertRate fetchByactive_First(java.lang.String isActive,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator);

	/**
	* Returns the last convert rate in the ordered set where isActive = &#63;.
	*
	* @param isActive the is active
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching convert rate
	* @throws NoSuchConvertRateException if a matching convert rate could not be found
	*/
	public ConvertRate findByactive_Last(java.lang.String isActive,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException;

	/**
	* Returns the last convert rate in the ordered set where isActive = &#63;.
	*
	* @param isActive the is active
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching convert rate, or <code>null</code> if a matching convert rate could not be found
	*/
	public ConvertRate fetchByactive_Last(java.lang.String isActive,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator);

	/**
	* Returns the convert rates before and after the current convert rate in the ordered set where isActive = &#63;.
	*
	* @param rateId the primary key of the current convert rate
	* @param isActive the is active
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next convert rate
	* @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	*/
	public ConvertRate[] findByactive_PrevAndNext(java.lang.String rateId,
		java.lang.String isActive,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException;

	/**
	* Removes all the convert rates where isActive = &#63; from the database.
	*
	* @param isActive the is active
	*/
	public void removeByactive(java.lang.String isActive);

	/**
	* Returns the number of convert rates where isActive = &#63;.
	*
	* @param isActive the is active
	* @return the number of matching convert rates
	*/
	public int countByactive(java.lang.String isActive);

	/**
	* Caches the convert rate in the entity cache if it is enabled.
	*
	* @param convertRate the convert rate
	*/
	public void cacheResult(ConvertRate convertRate);

	/**
	* Caches the convert rates in the entity cache if it is enabled.
	*
	* @param convertRates the convert rates
	*/
	public void cacheResult(java.util.List<ConvertRate> convertRates);

	/**
	* Creates a new convert rate with the primary key. Does not add the convert rate to the database.
	*
	* @param rateId the primary key for the new convert rate
	* @return the new convert rate
	*/
	public ConvertRate create(java.lang.String rateId);

	/**
	* Removes the convert rate with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param rateId the primary key of the convert rate
	* @return the convert rate that was removed
	* @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	*/
	public ConvertRate remove(java.lang.String rateId)
		throws NoSuchConvertRateException;

	public ConvertRate updateImpl(ConvertRate convertRate);

	/**
	* Returns the convert rate with the primary key or throws a {@link NoSuchConvertRateException} if it could not be found.
	*
	* @param rateId the primary key of the convert rate
	* @return the convert rate
	* @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	*/
	public ConvertRate findByPrimaryKey(java.lang.String rateId)
		throws NoSuchConvertRateException;

	/**
	* Returns the convert rate with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param rateId the primary key of the convert rate
	* @return the convert rate, or <code>null</code> if a convert rate with the primary key could not be found
	*/
	public ConvertRate fetchByPrimaryKey(java.lang.String rateId);

	@Override
	public java.util.Map<java.io.Serializable, ConvertRate> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the convert rates.
	*
	* @return the convert rates
	*/
	public java.util.List<ConvertRate> findAll();

	/**
	* Returns a range of all the convert rates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @return the range of convert rates
	*/
	public java.util.List<ConvertRate> findAll(int start, int end);

	/**
	* Returns an ordered range of all the convert rates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of convert rates
	*/
	public java.util.List<ConvertRate> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator);

	/**
	* Returns an ordered range of all the convert rates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of convert rates
	*/
	public java.util.List<ConvertRate> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ConvertRate> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the convert rates from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of convert rates.
	*
	* @return the number of convert rates
	*/
	public int countAll();
}