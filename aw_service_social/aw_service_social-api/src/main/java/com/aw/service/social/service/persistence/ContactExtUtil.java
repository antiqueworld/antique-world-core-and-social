/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.ContactExt;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the contact ext service. This utility wraps {@link com.aw.service.social.service.persistence.impl.ContactExtPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ContactExtPersistence
 * @see com.aw.service.social.service.persistence.impl.ContactExtPersistenceImpl
 * @generated
 */
@ProviderType
public class ContactExtUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ContactExt contactExt) {
		getPersistence().clearCache(contactExt);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ContactExt> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ContactExt> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ContactExt> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ContactExt> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ContactExt update(ContactExt contactExt) {
		return getPersistence().update(contactExt);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ContactExt update(ContactExt contactExt,
		ServiceContext serviceContext) {
		return getPersistence().update(contactExt, serviceContext);
	}

	/**
	* Caches the contact ext in the entity cache if it is enabled.
	*
	* @param contactExt the contact ext
	*/
	public static void cacheResult(ContactExt contactExt) {
		getPersistence().cacheResult(contactExt);
	}

	/**
	* Caches the contact exts in the entity cache if it is enabled.
	*
	* @param contactExts the contact exts
	*/
	public static void cacheResult(List<ContactExt> contactExts) {
		getPersistence().cacheResult(contactExts);
	}

	/**
	* Creates a new contact ext with the primary key. Does not add the contact ext to the database.
	*
	* @param userUid the primary key for the new contact ext
	* @return the new contact ext
	*/
	public static ContactExt create(java.lang.String userUid) {
		return getPersistence().create(userUid);
	}

	/**
	* Removes the contact ext with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userUid the primary key of the contact ext
	* @return the contact ext that was removed
	* @throws NoSuchContactExtException if a contact ext with the primary key could not be found
	*/
	public static ContactExt remove(java.lang.String userUid)
		throws com.aw.service.social.exception.NoSuchContactExtException {
		return getPersistence().remove(userUid);
	}

	public static ContactExt updateImpl(ContactExt contactExt) {
		return getPersistence().updateImpl(contactExt);
	}

	/**
	* Returns the contact ext with the primary key or throws a {@link NoSuchContactExtException} if it could not be found.
	*
	* @param userUid the primary key of the contact ext
	* @return the contact ext
	* @throws NoSuchContactExtException if a contact ext with the primary key could not be found
	*/
	public static ContactExt findByPrimaryKey(java.lang.String userUid)
		throws com.aw.service.social.exception.NoSuchContactExtException {
		return getPersistence().findByPrimaryKey(userUid);
	}

	/**
	* Returns the contact ext with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userUid the primary key of the contact ext
	* @return the contact ext, or <code>null</code> if a contact ext with the primary key could not be found
	*/
	public static ContactExt fetchByPrimaryKey(java.lang.String userUid) {
		return getPersistence().fetchByPrimaryKey(userUid);
	}

	public static java.util.Map<java.io.Serializable, ContactExt> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the contact exts.
	*
	* @return the contact exts
	*/
	public static List<ContactExt> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the contact exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact exts
	* @param end the upper bound of the range of contact exts (not inclusive)
	* @return the range of contact exts
	*/
	public static List<ContactExt> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the contact exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact exts
	* @param end the upper bound of the range of contact exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of contact exts
	*/
	public static List<ContactExt> findAll(int start, int end,
		OrderByComparator<ContactExt> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the contact exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact exts
	* @param end the upper bound of the range of contact exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of contact exts
	*/
	public static List<ContactExt> findAll(int start, int end,
		OrderByComparator<ContactExt> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the contact exts from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of contact exts.
	*
	* @return the number of contact exts
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ContactExtPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ContactExtPersistence, ContactExtPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ContactExtPersistence.class);
}