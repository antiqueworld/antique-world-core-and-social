/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ContactExtSoap implements Serializable {
	public static ContactExtSoap toSoapModel(ContactExt model) {
		ContactExtSoap soapModel = new ContactExtSoap();

		soapModel.setUserUid(model.getUserUid());
		soapModel.setPrimEmail(model.getPrimEmail());
		soapModel.setSecEmail(model.getSecEmail());
		soapModel.setPhone(model.getPhone());
		soapModel.setExtension(model.getExtension());
		soapModel.setCellPhone(model.getCellPhone());
		soapModel.setFacebook(model.getFacebook());
		soapModel.setWechat(model.getWechat());
		soapModel.setTwitter(model.getTwitter());
		soapModel.setWeibo(model.getWeibo());
		soapModel.setAddTime(model.getAddTime());
		soapModel.setUpdateTime(model.getUpdateTime());

		return soapModel;
	}

	public static ContactExtSoap[] toSoapModels(ContactExt[] models) {
		ContactExtSoap[] soapModels = new ContactExtSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ContactExtSoap[][] toSoapModels(ContactExt[][] models) {
		ContactExtSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ContactExtSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ContactExtSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ContactExtSoap[] toSoapModels(List<ContactExt> models) {
		List<ContactExtSoap> soapModels = new ArrayList<ContactExtSoap>(models.size());

		for (ContactExt model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ContactExtSoap[soapModels.size()]);
	}

	public ContactExtSoap() {
	}

	public String getPrimaryKey() {
		return _userUid;
	}

	public void setPrimaryKey(String pk) {
		setUserUid(pk);
	}

	public String getUserUid() {
		return _userUid;
	}

	public void setUserUid(String userUid) {
		_userUid = userUid;
	}

	public String getPrimEmail() {
		return _primEmail;
	}

	public void setPrimEmail(String primEmail) {
		_primEmail = primEmail;
	}

	public String getSecEmail() {
		return _secEmail;
	}

	public void setSecEmail(String secEmail) {
		_secEmail = secEmail;
	}

	public String getPhone() {
		return _phone;
	}

	public void setPhone(String phone) {
		_phone = phone;
	}

	public String getExtension() {
		return _extension;
	}

	public void setExtension(String extension) {
		_extension = extension;
	}

	public String getCellPhone() {
		return _cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		_cellPhone = cellPhone;
	}

	public String getFacebook() {
		return _facebook;
	}

	public void setFacebook(String facebook) {
		_facebook = facebook;
	}

	public String getWechat() {
		return _wechat;
	}

	public void setWechat(String wechat) {
		_wechat = wechat;
	}

	public String getTwitter() {
		return _twitter;
	}

	public void setTwitter(String twitter) {
		_twitter = twitter;
	}

	public String getWeibo() {
		return _weibo;
	}

	public void setWeibo(String weibo) {
		_weibo = weibo;
	}

	public Date getAddTime() {
		return _addTime;
	}

	public void setAddTime(Date addTime) {
		_addTime = addTime;
	}

	public Date getUpdateTime() {
		return _updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		_updateTime = updateTime;
	}

	private String _userUid;
	private String _primEmail;
	private String _secEmail;
	private String _phone;
	private String _extension;
	private String _cellPhone;
	private String _facebook;
	private String _wechat;
	private String _twitter;
	private String _weibo;
	private Date _addTime;
	private Date _updateTime;
}