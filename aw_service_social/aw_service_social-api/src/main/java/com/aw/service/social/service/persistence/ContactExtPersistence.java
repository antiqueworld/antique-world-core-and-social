/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.exception.NoSuchContactExtException;
import com.aw.service.social.model.ContactExt;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the contact ext service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.aw.service.social.service.persistence.impl.ContactExtPersistenceImpl
 * @see ContactExtUtil
 * @generated
 */
@ProviderType
public interface ContactExtPersistence extends BasePersistence<ContactExt> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ContactExtUtil} to access the contact ext persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the contact ext in the entity cache if it is enabled.
	*
	* @param contactExt the contact ext
	*/
	public void cacheResult(ContactExt contactExt);

	/**
	* Caches the contact exts in the entity cache if it is enabled.
	*
	* @param contactExts the contact exts
	*/
	public void cacheResult(java.util.List<ContactExt> contactExts);

	/**
	* Creates a new contact ext with the primary key. Does not add the contact ext to the database.
	*
	* @param userUid the primary key for the new contact ext
	* @return the new contact ext
	*/
	public ContactExt create(java.lang.String userUid);

	/**
	* Removes the contact ext with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userUid the primary key of the contact ext
	* @return the contact ext that was removed
	* @throws NoSuchContactExtException if a contact ext with the primary key could not be found
	*/
	public ContactExt remove(java.lang.String userUid)
		throws NoSuchContactExtException;

	public ContactExt updateImpl(ContactExt contactExt);

	/**
	* Returns the contact ext with the primary key or throws a {@link NoSuchContactExtException} if it could not be found.
	*
	* @param userUid the primary key of the contact ext
	* @return the contact ext
	* @throws NoSuchContactExtException if a contact ext with the primary key could not be found
	*/
	public ContactExt findByPrimaryKey(java.lang.String userUid)
		throws NoSuchContactExtException;

	/**
	* Returns the contact ext with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userUid the primary key of the contact ext
	* @return the contact ext, or <code>null</code> if a contact ext with the primary key could not be found
	*/
	public ContactExt fetchByPrimaryKey(java.lang.String userUid);

	@Override
	public java.util.Map<java.io.Serializable, ContactExt> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the contact exts.
	*
	* @return the contact exts
	*/
	public java.util.List<ContactExt> findAll();

	/**
	* Returns a range of all the contact exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact exts
	* @param end the upper bound of the range of contact exts (not inclusive)
	* @return the range of contact exts
	*/
	public java.util.List<ContactExt> findAll(int start, int end);

	/**
	* Returns an ordered range of all the contact exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact exts
	* @param end the upper bound of the range of contact exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of contact exts
	*/
	public java.util.List<ContactExt> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactExt> orderByComparator);

	/**
	* Returns an ordered range of all the contact exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact exts
	* @param end the upper bound of the range of contact exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of contact exts
	*/
	public java.util.List<ContactExt> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactExt> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the contact exts from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of contact exts.
	*
	* @return the number of contact exts
	*/
	public int countAll();
}