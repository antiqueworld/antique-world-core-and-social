/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ConvertRateLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ConvertRateLocalService
 * @generated
 */
@ProviderType
public class ConvertRateLocalServiceWrapper implements ConvertRateLocalService,
	ServiceWrapper<ConvertRateLocalService> {
	public ConvertRateLocalServiceWrapper(
		ConvertRateLocalService convertRateLocalService) {
		_convertRateLocalService = convertRateLocalService;
	}

	/**
	* Adds the convert rate to the database. Also notifies the appropriate model listeners.
	*
	* @param convertRate the convert rate
	* @return the convert rate that was added
	*/
	@Override
	public com.aw.service.social.model.ConvertRate addConvertRate(
		com.aw.service.social.model.ConvertRate convertRate) {
		return _convertRateLocalService.addConvertRate(convertRate);
	}

	/**
	* Creates a new convert rate with the primary key. Does not add the convert rate to the database.
	*
	* @param rateId the primary key for the new convert rate
	* @return the new convert rate
	*/
	@Override
	public com.aw.service.social.model.ConvertRate createConvertRate(
		java.lang.String rateId) {
		return _convertRateLocalService.createConvertRate(rateId);
	}

	/**
	* Deletes the convert rate from the database. Also notifies the appropriate model listeners.
	*
	* @param convertRate the convert rate
	* @return the convert rate that was removed
	*/
	@Override
	public com.aw.service.social.model.ConvertRate deleteConvertRate(
		com.aw.service.social.model.ConvertRate convertRate) {
		return _convertRateLocalService.deleteConvertRate(convertRate);
	}

	/**
	* Deletes the convert rate with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param rateId the primary key of the convert rate
	* @return the convert rate that was removed
	* @throws PortalException if a convert rate with the primary key could not be found
	*/
	@Override
	public com.aw.service.social.model.ConvertRate deleteConvertRate(
		java.lang.String rateId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _convertRateLocalService.deleteConvertRate(rateId);
	}

	@Override
	public com.aw.service.social.model.ConvertRate fetchConvertRate(
		java.lang.String rateId) {
		return _convertRateLocalService.fetchConvertRate(rateId);
	}

	@Override
	public com.aw.service.social.model.ConvertRate getActiveRate(
		java.lang.String from, java.lang.String to) {
		return _convertRateLocalService.getActiveRate(from, to);
	}

	/**
	* Returns the convert rate with the primary key.
	*
	* @param rateId the primary key of the convert rate
	* @return the convert rate
	* @throws PortalException if a convert rate with the primary key could not be found
	*/
	@Override
	public com.aw.service.social.model.ConvertRate getConvertRate(
		java.lang.String rateId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _convertRateLocalService.getConvertRate(rateId);
	}

	/**
	* Updates the convert rate in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param convertRate the convert rate
	* @return the convert rate that was updated
	*/
	@Override
	public com.aw.service.social.model.ConvertRate updateConvertRate(
		com.aw.service.social.model.ConvertRate convertRate) {
		return _convertRateLocalService.updateConvertRate(convertRate);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _convertRateLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject allActiveRates(
		java.lang.String locale) {
		return _convertRateLocalService.allActiveRates(locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject changeRate(
		java.lang.String from, java.lang.String to, double newRate,
		java.lang.String locale) {
		return _convertRateLocalService.changeRate(from, to, newRate, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getActiveRate(
		java.lang.String from, java.lang.String to, java.lang.String locale) {
		return _convertRateLocalService.getActiveRate(from, to, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getRates(
		java.lang.String from, java.lang.String to, java.lang.String locale) {
		return _convertRateLocalService.getRates(from, to, locale);
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _convertRateLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _convertRateLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of convert rates.
	*
	* @return the number of convert rates
	*/
	@Override
	public int getConvertRatesCount() {
		return _convertRateLocalService.getConvertRatesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _convertRateLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _convertRateLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _convertRateLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _convertRateLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the convert rates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @return the range of convert rates
	*/
	@Override
	public java.util.List<com.aw.service.social.model.ConvertRate> getConvertRates(
		int start, int end) {
		return _convertRateLocalService.getConvertRates(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _convertRateLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _convertRateLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public ConvertRateLocalService getWrappedService() {
		return _convertRateLocalService;
	}

	@Override
	public void setWrappedService(
		ConvertRateLocalService convertRateLocalService) {
		_convertRateLocalService = convertRateLocalService;
	}

	private ConvertRateLocalService _convertRateLocalService;
}