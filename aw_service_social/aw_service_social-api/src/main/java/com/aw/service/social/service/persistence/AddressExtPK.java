/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class AddressExtPK implements Comparable<AddressExtPK>, Serializable {
	public String userUid;
	public String addressType;

	public AddressExtPK() {
	}

	public AddressExtPK(String userUid, String addressType) {
		this.userUid = userUid;
		this.addressType = addressType;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	@Override
	public int compareTo(AddressExtPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		value = userUid.compareTo(pk.userUid);

		if (value != 0) {
			return value;
		}

		value = addressType.compareTo(pk.addressType);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AddressExtPK)) {
			return false;
		}

		AddressExtPK pk = (AddressExtPK)obj;

		if ((userUid.equals(pk.userUid)) &&
				(addressType.equals(pk.addressType))) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, userUid);
		hashCode = HashUtil.hash(hashCode, addressType);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("userUid");
		sb.append(StringPool.EQUAL);
		sb.append(userUid);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("addressType");
		sb.append(StringPool.EQUAL);
		sb.append(addressType);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}