/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.service.persistence.AddressExtPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class AddressExtSoap implements Serializable {
	public static AddressExtSoap toSoapModel(AddressExt model) {
		AddressExtSoap soapModel = new AddressExtSoap();

		soapModel.setUserUid(model.getUserUid());
		soapModel.setAddressType(model.getAddressType());
		soapModel.setStreet1(model.getStreet1());
		soapModel.setStreet2(model.getStreet2());
		soapModel.setStreet3(model.getStreet3());
		soapModel.setCity(model.getCity());
		soapModel.setRegionId(model.getRegionId());
		soapModel.setCountryId(model.getCountryId());
		soapModel.setZipCode(model.getZipCode());
		soapModel.setIsMailing(model.getIsMailing());

		return soapModel;
	}

	public static AddressExtSoap[] toSoapModels(AddressExt[] models) {
		AddressExtSoap[] soapModels = new AddressExtSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AddressExtSoap[][] toSoapModels(AddressExt[][] models) {
		AddressExtSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AddressExtSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AddressExtSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AddressExtSoap[] toSoapModels(List<AddressExt> models) {
		List<AddressExtSoap> soapModels = new ArrayList<AddressExtSoap>(models.size());

		for (AddressExt model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AddressExtSoap[soapModels.size()]);
	}

	public AddressExtSoap() {
	}

	public AddressExtPK getPrimaryKey() {
		return new AddressExtPK(_userUid, _addressType);
	}

	public void setPrimaryKey(AddressExtPK pk) {
		setUserUid(pk.userUid);
		setAddressType(pk.addressType);
	}

	public String getUserUid() {
		return _userUid;
	}

	public void setUserUid(String userUid) {
		_userUid = userUid;
	}

	public String getAddressType() {
		return _addressType;
	}

	public void setAddressType(String addressType) {
		_addressType = addressType;
	}

	public String getStreet1() {
		return _street1;
	}

	public void setStreet1(String street1) {
		_street1 = street1;
	}

	public String getStreet2() {
		return _street2;
	}

	public void setStreet2(String street2) {
		_street2 = street2;
	}

	public String getStreet3() {
		return _street3;
	}

	public void setStreet3(String street3) {
		_street3 = street3;
	}

	public String getCity() {
		return _city;
	}

	public void setCity(String city) {
		_city = city;
	}

	public String getRegionId() {
		return _regionId;
	}

	public void setRegionId(String regionId) {
		_regionId = regionId;
	}

	public String getCountryId() {
		return _countryId;
	}

	public void setCountryId(String countryId) {
		_countryId = countryId;
	}

	public String getZipCode() {
		return _zipCode;
	}

	public void setZipCode(String zipCode) {
		_zipCode = zipCode;
	}

	public String getIsMailing() {
		return _isMailing;
	}

	public void setIsMailing(String isMailing) {
		_isMailing = isMailing;
	}

	private String _userUid;
	private String _addressType;
	private String _street1;
	private String _street2;
	private String _street3;
	private String _city;
	private String _regionId;
	private String _countryId;
	private String _zipCode;
	private String _isMailing;
}