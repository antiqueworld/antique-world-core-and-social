/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class TransactionSoap implements Serializable {
	public static TransactionSoap toSoapModel(Transaction model) {
		TransactionSoap soapModel = new TransactionSoap();

		soapModel.setTransactionId(model.getTransactionId());
		soapModel.setOrderId(model.getOrderId());
		soapModel.setUserWalletAddr(model.getUserWalletAddr());
		soapModel.setType(model.getType());
		soapModel.setCurrency(model.getCurrency());
		soapModel.setAmount(model.getAmount());
		soapModel.setStatus(model.getStatus());
		soapModel.setAddTime(model.getAddTime());
		soapModel.setUpdateTime(model.getUpdateTime());

		return soapModel;
	}

	public static TransactionSoap[] toSoapModels(Transaction[] models) {
		TransactionSoap[] soapModels = new TransactionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TransactionSoap[][] toSoapModels(Transaction[][] models) {
		TransactionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TransactionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TransactionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TransactionSoap[] toSoapModels(List<Transaction> models) {
		List<TransactionSoap> soapModels = new ArrayList<TransactionSoap>(models.size());

		for (Transaction model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TransactionSoap[soapModels.size()]);
	}

	public TransactionSoap() {
	}

	public String getPrimaryKey() {
		return _transactionId;
	}

	public void setPrimaryKey(String pk) {
		setTransactionId(pk);
	}

	public String getTransactionId() {
		return _transactionId;
	}

	public void setTransactionId(String transactionId) {
		_transactionId = transactionId;
	}

	public String getOrderId() {
		return _orderId;
	}

	public void setOrderId(String orderId) {
		_orderId = orderId;
	}

	public String getUserWalletAddr() {
		return _userWalletAddr;
	}

	public void setUserWalletAddr(String userWalletAddr) {
		_userWalletAddr = userWalletAddr;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	public String getCurrency() {
		return _currency;
	}

	public void setCurrency(String currency) {
		_currency = currency;
	}

	public double getAmount() {
		return _amount;
	}

	public void setAmount(double amount) {
		_amount = amount;
	}

	public String getStatus() {
		return _status;
	}

	public void setStatus(String status) {
		_status = status;
	}

	public Date getAddTime() {
		return _addTime;
	}

	public void setAddTime(Date addTime) {
		_addTime = addTime;
	}

	public Date getUpdateTime() {
		return _updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		_updateTime = updateTime;
	}

	private String _transactionId;
	private String _orderId;
	private String _userWalletAddr;
	private String _type;
	private String _currency;
	private double _amount;
	private String _status;
	private Date _addTime;
	private Date _updateTime;
}