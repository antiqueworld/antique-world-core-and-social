/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CommerceService}.
 *
 * @author Brian Wing Shun Chan
 * @see CommerceService
 * @generated
 */
@ProviderType
public class CommerceServiceWrapper implements CommerceService,
	ServiceWrapper<CommerceService> {
	public CommerceServiceWrapper(CommerceService commerceService) {
		_commerceService = commerceService;
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject allActiveRates(
		String from, String to, String locale) {
		return _commerceService.allActiveRates(from, to, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject cancelOrder(
		String orderId, String locale) {
		return _commerceService.cancelOrder(orderId, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject changeRate(String from,
		String to, double newRate, String locale) {
		return _commerceService.changeRate(from, to, newRate, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject createOrder(
		String payType, String payCurrency, String getCurrency, double amount,
		String walletAddress, String locale) {
		return _commerceService.createOrder(payType, payCurrency, getCurrency,
			amount, walletAddress, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject createOrderForUser(
		String userUid, String payType, String payCurrency, String getCurrency,
		double amount, String walletAddress, String locale) {
		return _commerceService.createOrderForUser(userUid, payType,
			payCurrency, getCurrency, amount, walletAddress, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getActiveRate(
		String from, String to, String locale) {
		return _commerceService.getActiveRate(from, to, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getAllOrders(
		String chargeFlag, String sentFlag, String locale) {
		return _commerceService.getAllOrders(chargeFlag, sentFlag, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getCoinIssued(
		String toCurrency, String locale) {
		return _commerceService.getCoinIssued(toCurrency, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getMoneyRaise(
		String fromCurrency, String locale) {
		return _commerceService.getMoneyRaise(fromCurrency, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getOrderDetail(
		String orderId, String locale) {
		return _commerceService.getOrderDetail(orderId, locale);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _commerceService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getRates(String from,
		String to, String locale) {
		return _commerceService.getRates(from, to, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject orderComplete(
		String orderId, String locale) {
		return _commerceService.orderComplete(orderId, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject orderPaid(String orderId,
		String locale) {
		return _commerceService.orderPaid(orderId, locale);
	}

	@Override
	public CommerceService getWrappedService() {
		return _commerceService;
	}

	@Override
	public void setWrappedService(CommerceService commerceService) {
		_commerceService = commerceService;
	}

	private CommerceService _commerceService;
}