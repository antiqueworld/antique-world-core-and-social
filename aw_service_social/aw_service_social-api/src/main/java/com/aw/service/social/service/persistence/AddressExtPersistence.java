/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.exception.NoSuchAddressExtException;
import com.aw.service.social.model.AddressExt;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the address ext service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.aw.service.social.service.persistence.impl.AddressExtPersistenceImpl
 * @see AddressExtUtil
 * @generated
 */
@ProviderType
public interface AddressExtPersistence extends BasePersistence<AddressExt> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AddressExtUtil} to access the address ext persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the address exts where userUid = &#63;.
	*
	* @param userUid the user uid
	* @return the matching address exts
	*/
	public java.util.List<AddressExt> findByuser(java.lang.String userUid);

	/**
	* Returns a range of all the address exts where userUid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userUid the user uid
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @return the range of matching address exts
	*/
	public java.util.List<AddressExt> findByuser(java.lang.String userUid,
		int start, int end);

	/**
	* Returns an ordered range of all the address exts where userUid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userUid the user uid
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching address exts
	*/
	public java.util.List<AddressExt> findByuser(java.lang.String userUid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AddressExt> orderByComparator);

	/**
	* Returns an ordered range of all the address exts where userUid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userUid the user uid
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching address exts
	*/
	public java.util.List<AddressExt> findByuser(java.lang.String userUid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AddressExt> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first address ext in the ordered set where userUid = &#63;.
	*
	* @param userUid the user uid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching address ext
	* @throws NoSuchAddressExtException if a matching address ext could not be found
	*/
	public AddressExt findByuser_First(java.lang.String userUid,
		com.liferay.portal.kernel.util.OrderByComparator<AddressExt> orderByComparator)
		throws NoSuchAddressExtException;

	/**
	* Returns the first address ext in the ordered set where userUid = &#63;.
	*
	* @param userUid the user uid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching address ext, or <code>null</code> if a matching address ext could not be found
	*/
	public AddressExt fetchByuser_First(java.lang.String userUid,
		com.liferay.portal.kernel.util.OrderByComparator<AddressExt> orderByComparator);

	/**
	* Returns the last address ext in the ordered set where userUid = &#63;.
	*
	* @param userUid the user uid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching address ext
	* @throws NoSuchAddressExtException if a matching address ext could not be found
	*/
	public AddressExt findByuser_Last(java.lang.String userUid,
		com.liferay.portal.kernel.util.OrderByComparator<AddressExt> orderByComparator)
		throws NoSuchAddressExtException;

	/**
	* Returns the last address ext in the ordered set where userUid = &#63;.
	*
	* @param userUid the user uid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching address ext, or <code>null</code> if a matching address ext could not be found
	*/
	public AddressExt fetchByuser_Last(java.lang.String userUid,
		com.liferay.portal.kernel.util.OrderByComparator<AddressExt> orderByComparator);

	/**
	* Returns the address exts before and after the current address ext in the ordered set where userUid = &#63;.
	*
	* @param addressExtPK the primary key of the current address ext
	* @param userUid the user uid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next address ext
	* @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	*/
	public AddressExt[] findByuser_PrevAndNext(AddressExtPK addressExtPK,
		java.lang.String userUid,
		com.liferay.portal.kernel.util.OrderByComparator<AddressExt> orderByComparator)
		throws NoSuchAddressExtException;

	/**
	* Removes all the address exts where userUid = &#63; from the database.
	*
	* @param userUid the user uid
	*/
	public void removeByuser(java.lang.String userUid);

	/**
	* Returns the number of address exts where userUid = &#63;.
	*
	* @param userUid the user uid
	* @return the number of matching address exts
	*/
	public int countByuser(java.lang.String userUid);

	/**
	* Caches the address ext in the entity cache if it is enabled.
	*
	* @param addressExt the address ext
	*/
	public void cacheResult(AddressExt addressExt);

	/**
	* Caches the address exts in the entity cache if it is enabled.
	*
	* @param addressExts the address exts
	*/
	public void cacheResult(java.util.List<AddressExt> addressExts);

	/**
	* Creates a new address ext with the primary key. Does not add the address ext to the database.
	*
	* @param addressExtPK the primary key for the new address ext
	* @return the new address ext
	*/
	public AddressExt create(AddressExtPK addressExtPK);

	/**
	* Removes the address ext with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param addressExtPK the primary key of the address ext
	* @return the address ext that was removed
	* @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	*/
	public AddressExt remove(AddressExtPK addressExtPK)
		throws NoSuchAddressExtException;

	public AddressExt updateImpl(AddressExt addressExt);

	/**
	* Returns the address ext with the primary key or throws a {@link NoSuchAddressExtException} if it could not be found.
	*
	* @param addressExtPK the primary key of the address ext
	* @return the address ext
	* @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	*/
	public AddressExt findByPrimaryKey(AddressExtPK addressExtPK)
		throws NoSuchAddressExtException;

	/**
	* Returns the address ext with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param addressExtPK the primary key of the address ext
	* @return the address ext, or <code>null</code> if a address ext with the primary key could not be found
	*/
	public AddressExt fetchByPrimaryKey(AddressExtPK addressExtPK);

	@Override
	public java.util.Map<java.io.Serializable, AddressExt> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the address exts.
	*
	* @return the address exts
	*/
	public java.util.List<AddressExt> findAll();

	/**
	* Returns a range of all the address exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @return the range of address exts
	*/
	public java.util.List<AddressExt> findAll(int start, int end);

	/**
	* Returns an ordered range of all the address exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of address exts
	*/
	public java.util.List<AddressExt> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AddressExt> orderByComparator);

	/**
	* Returns an ordered range of all the address exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of address exts
	* @param end the upper bound of the range of address exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of address exts
	*/
	public java.util.List<AddressExt> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AddressExt> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the address exts from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of address exts.
	*
	* @return the number of address exts
	*/
	public int countAll();
}