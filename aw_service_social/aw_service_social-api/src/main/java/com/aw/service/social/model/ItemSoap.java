/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ItemSoap implements Serializable {
	public static ItemSoap toSoapModel(Item model) {
		ItemSoap soapModel = new ItemSoap();

		soapModel.setItemId(model.getItemId());
		soapModel.setOwnerUid(model.getOwnerUid());
		soapModel.setErc20Addr(model.getErc20Addr());
		soapModel.setTokenCurrency(model.getTokenCurrency());
		soapModel.setErc721Addr(model.getErc721Addr());
		soapModel.setTitle(model.getTitle());
		soapModel.setDescription(model.getDescription());
		soapModel.setAddTime(model.getAddTime());
		soapModel.setUpdateTime(model.getUpdateTime());

		return soapModel;
	}

	public static ItemSoap[] toSoapModels(Item[] models) {
		ItemSoap[] soapModels = new ItemSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ItemSoap[][] toSoapModels(Item[][] models) {
		ItemSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ItemSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ItemSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ItemSoap[] toSoapModels(List<Item> models) {
		List<ItemSoap> soapModels = new ArrayList<ItemSoap>(models.size());

		for (Item model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ItemSoap[soapModels.size()]);
	}

	public ItemSoap() {
	}

	public String getPrimaryKey() {
		return _itemId;
	}

	public void setPrimaryKey(String pk) {
		setItemId(pk);
	}

	public String getItemId() {
		return _itemId;
	}

	public void setItemId(String itemId) {
		_itemId = itemId;
	}

	public String getOwnerUid() {
		return _ownerUid;
	}

	public void setOwnerUid(String ownerUid) {
		_ownerUid = ownerUid;
	}

	public String getErc20Addr() {
		return _erc20Addr;
	}

	public void setErc20Addr(String erc20Addr) {
		_erc20Addr = erc20Addr;
	}

	public String getTokenCurrency() {
		return _tokenCurrency;
	}

	public void setTokenCurrency(String tokenCurrency) {
		_tokenCurrency = tokenCurrency;
	}

	public String getErc721Addr() {
		return _erc721Addr;
	}

	public void setErc721Addr(String erc721Addr) {
		_erc721Addr = erc721Addr;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public Date getAddTime() {
		return _addTime;
	}

	public void setAddTime(Date addTime) {
		_addTime = addTime;
	}

	public Date getUpdateTime() {
		return _updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		_updateTime = updateTime;
	}

	private String _itemId;
	private String _ownerUid;
	private String _erc20Addr;
	private String _tokenCurrency;
	private String _erc721Addr;
	private String _title;
	private String _description;
	private Date _addTime;
	private Date _updateTime;
}