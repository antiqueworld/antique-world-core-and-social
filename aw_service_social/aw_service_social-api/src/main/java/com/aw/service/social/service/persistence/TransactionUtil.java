/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.Transaction;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the transaction service. This utility wraps {@link com.aw.service.social.service.persistence.impl.TransactionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TransactionPersistence
 * @see com.aw.service.social.service.persistence.impl.TransactionPersistenceImpl
 * @generated
 */
@ProviderType
public class TransactionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Transaction transaction) {
		getPersistence().clearCache(transaction);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Transaction> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Transaction> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Transaction> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Transaction> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Transaction update(Transaction transaction) {
		return getPersistence().update(transaction);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Transaction update(Transaction transaction,
		ServiceContext serviceContext) {
		return getPersistence().update(transaction, serviceContext);
	}

	/**
	* Returns all the transactions where orderId = &#63;.
	*
	* @param orderId the order ID
	* @return the matching transactions
	*/
	public static List<Transaction> findByorder(java.lang.String orderId) {
		return getPersistence().findByorder(orderId);
	}

	/**
	* Returns a range of all the transactions where orderId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param orderId the order ID
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @return the range of matching transactions
	*/
	public static List<Transaction> findByorder(java.lang.String orderId,
		int start, int end) {
		return getPersistence().findByorder(orderId, start, end);
	}

	/**
	* Returns an ordered range of all the transactions where orderId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param orderId the order ID
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching transactions
	*/
	public static List<Transaction> findByorder(java.lang.String orderId,
		int start, int end, OrderByComparator<Transaction> orderByComparator) {
		return getPersistence()
				   .findByorder(orderId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the transactions where orderId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param orderId the order ID
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching transactions
	*/
	public static List<Transaction> findByorder(java.lang.String orderId,
		int start, int end, OrderByComparator<Transaction> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByorder(orderId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first transaction in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching transaction
	* @throws NoSuchTransactionException if a matching transaction could not be found
	*/
	public static Transaction findByorder_First(java.lang.String orderId,
		OrderByComparator<Transaction> orderByComparator)
		throws com.aw.service.social.exception.NoSuchTransactionException {
		return getPersistence().findByorder_First(orderId, orderByComparator);
	}

	/**
	* Returns the first transaction in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching transaction, or <code>null</code> if a matching transaction could not be found
	*/
	public static Transaction fetchByorder_First(java.lang.String orderId,
		OrderByComparator<Transaction> orderByComparator) {
		return getPersistence().fetchByorder_First(orderId, orderByComparator);
	}

	/**
	* Returns the last transaction in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching transaction
	* @throws NoSuchTransactionException if a matching transaction could not be found
	*/
	public static Transaction findByorder_Last(java.lang.String orderId,
		OrderByComparator<Transaction> orderByComparator)
		throws com.aw.service.social.exception.NoSuchTransactionException {
		return getPersistence().findByorder_Last(orderId, orderByComparator);
	}

	/**
	* Returns the last transaction in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching transaction, or <code>null</code> if a matching transaction could not be found
	*/
	public static Transaction fetchByorder_Last(java.lang.String orderId,
		OrderByComparator<Transaction> orderByComparator) {
		return getPersistence().fetchByorder_Last(orderId, orderByComparator);
	}

	/**
	* Returns the transactions before and after the current transaction in the ordered set where orderId = &#63;.
	*
	* @param transactionId the primary key of the current transaction
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next transaction
	* @throws NoSuchTransactionException if a transaction with the primary key could not be found
	*/
	public static Transaction[] findByorder_PrevAndNext(
		java.lang.String transactionId, java.lang.String orderId,
		OrderByComparator<Transaction> orderByComparator)
		throws com.aw.service.social.exception.NoSuchTransactionException {
		return getPersistence()
				   .findByorder_PrevAndNext(transactionId, orderId,
			orderByComparator);
	}

	/**
	* Removes all the transactions where orderId = &#63; from the database.
	*
	* @param orderId the order ID
	*/
	public static void removeByorder(java.lang.String orderId) {
		getPersistence().removeByorder(orderId);
	}

	/**
	* Returns the number of transactions where orderId = &#63;.
	*
	* @param orderId the order ID
	* @return the number of matching transactions
	*/
	public static int countByorder(java.lang.String orderId) {
		return getPersistence().countByorder(orderId);
	}

	/**
	* Caches the transaction in the entity cache if it is enabled.
	*
	* @param transaction the transaction
	*/
	public static void cacheResult(Transaction transaction) {
		getPersistence().cacheResult(transaction);
	}

	/**
	* Caches the transactions in the entity cache if it is enabled.
	*
	* @param transactions the transactions
	*/
	public static void cacheResult(List<Transaction> transactions) {
		getPersistence().cacheResult(transactions);
	}

	/**
	* Creates a new transaction with the primary key. Does not add the transaction to the database.
	*
	* @param transactionId the primary key for the new transaction
	* @return the new transaction
	*/
	public static Transaction create(java.lang.String transactionId) {
		return getPersistence().create(transactionId);
	}

	/**
	* Removes the transaction with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param transactionId the primary key of the transaction
	* @return the transaction that was removed
	* @throws NoSuchTransactionException if a transaction with the primary key could not be found
	*/
	public static Transaction remove(java.lang.String transactionId)
		throws com.aw.service.social.exception.NoSuchTransactionException {
		return getPersistence().remove(transactionId);
	}

	public static Transaction updateImpl(Transaction transaction) {
		return getPersistence().updateImpl(transaction);
	}

	/**
	* Returns the transaction with the primary key or throws a {@link NoSuchTransactionException} if it could not be found.
	*
	* @param transactionId the primary key of the transaction
	* @return the transaction
	* @throws NoSuchTransactionException if a transaction with the primary key could not be found
	*/
	public static Transaction findByPrimaryKey(java.lang.String transactionId)
		throws com.aw.service.social.exception.NoSuchTransactionException {
		return getPersistence().findByPrimaryKey(transactionId);
	}

	/**
	* Returns the transaction with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param transactionId the primary key of the transaction
	* @return the transaction, or <code>null</code> if a transaction with the primary key could not be found
	*/
	public static Transaction fetchByPrimaryKey(java.lang.String transactionId) {
		return getPersistence().fetchByPrimaryKey(transactionId);
	}

	public static java.util.Map<java.io.Serializable, Transaction> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the transactions.
	*
	* @return the transactions
	*/
	public static List<Transaction> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the transactions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @return the range of transactions
	*/
	public static List<Transaction> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the transactions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of transactions
	*/
	public static List<Transaction> findAll(int start, int end,
		OrderByComparator<Transaction> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the transactions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of transactions
	*/
	public static List<Transaction> findAll(int start, int end,
		OrderByComparator<Transaction> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the transactions from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of transactions.
	*
	* @return the number of transactions
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static TransactionPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<TransactionPersistence, TransactionPersistence> _serviceTracker =
		ServiceTrackerFactory.open(TransactionPersistence.class);
}