/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ContactExtLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ContactExtLocalService
 * @generated
 */
@ProviderType
public class ContactExtLocalServiceWrapper implements ContactExtLocalService,
	ServiceWrapper<ContactExtLocalService> {
	public ContactExtLocalServiceWrapper(
		ContactExtLocalService contactExtLocalService) {
		_contactExtLocalService = contactExtLocalService;
	}

	/**
	* Adds the contact ext to the database. Also notifies the appropriate model listeners.
	*
	* @param contactExt the contact ext
	* @return the contact ext that was added
	*/
	@Override
	public com.aw.service.social.model.ContactExt addContactExt(
		com.aw.service.social.model.ContactExt contactExt) {
		return _contactExtLocalService.addContactExt(contactExt);
	}

	/**
	* Creates a new contact ext with the primary key. Does not add the contact ext to the database.
	*
	* @param userUid the primary key for the new contact ext
	* @return the new contact ext
	*/
	@Override
	public com.aw.service.social.model.ContactExt createContactExt(
		java.lang.String userUid) {
		return _contactExtLocalService.createContactExt(userUid);
	}

	/**
	* Deletes the contact ext from the database. Also notifies the appropriate model listeners.
	*
	* @param contactExt the contact ext
	* @return the contact ext that was removed
	*/
	@Override
	public com.aw.service.social.model.ContactExt deleteContactExt(
		com.aw.service.social.model.ContactExt contactExt) {
		return _contactExtLocalService.deleteContactExt(contactExt);
	}

	/**
	* Deletes the contact ext with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userUid the primary key of the contact ext
	* @return the contact ext that was removed
	* @throws PortalException if a contact ext with the primary key could not be found
	*/
	@Override
	public com.aw.service.social.model.ContactExt deleteContactExt(
		java.lang.String userUid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _contactExtLocalService.deleteContactExt(userUid);
	}

	@Override
	public com.aw.service.social.model.ContactExt fetchContactExt(
		java.lang.String userUid) {
		return _contactExtLocalService.fetchContactExt(userUid);
	}

	/**
	* Returns the contact ext with the primary key.
	*
	* @param userUid the primary key of the contact ext
	* @return the contact ext
	* @throws PortalException if a contact ext with the primary key could not be found
	*/
	@Override
	public com.aw.service.social.model.ContactExt getContactExt(
		java.lang.String userUid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _contactExtLocalService.getContactExt(userUid);
	}

	/**
	* Updates the contact ext in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param contactExt the contact ext
	* @return the contact ext that was updated
	*/
	@Override
	public com.aw.service.social.model.ContactExt updateContactExt(
		com.aw.service.social.model.ContactExt contactExt) {
		return _contactExtLocalService.updateContactExt(contactExt);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _contactExtLocalService.dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _contactExtLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _contactExtLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of contact exts.
	*
	* @return the number of contact exts
	*/
	@Override
	public int getContactExtsCount() {
		return _contactExtLocalService.getContactExtsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _contactExtLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _contactExtLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _contactExtLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _contactExtLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the contact exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact exts
	* @param end the upper bound of the range of contact exts (not inclusive)
	* @return the range of contact exts
	*/
	@Override
	public java.util.List<com.aw.service.social.model.ContactExt> getContactExts(
		int start, int end) {
		return _contactExtLocalService.getContactExts(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _contactExtLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _contactExtLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public ContactExtLocalService getWrappedService() {
		return _contactExtLocalService;
	}

	@Override
	public void setWrappedService(ContactExtLocalService contactExtLocalService) {
		_contactExtLocalService = contactExtLocalService;
	}

	private ContactExtLocalService _contactExtLocalService;
}