/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * Provides the local service interface for UserInfo. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see UserInfoLocalServiceUtil
 * @see com.aw.service.social.service.base.UserInfoLocalServiceBaseImpl
 * @see com.aw.service.social.service.impl.UserInfoLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface UserInfoLocalService extends BaseLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserInfoLocalServiceUtil} to access the user info local service. Add custom service methods to {@link com.aw.service.social.service.impl.UserInfoLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getUserDetail(User user, java.lang.String locale);

	public JSONObject updateBasicUserInfo(User user, java.lang.String birthday,
		java.lang.String firstName, java.lang.String middleName,
		java.lang.String lastName, java.lang.String title,
		java.lang.String selfDesc, java.lang.String secEmail,
		java.lang.String phone, java.lang.String extension,
		java.lang.String cellPhone, java.lang.String locale);

	public JSONObject updateIdImage(User user, java.lang.String fileType,
		byte[] IdImage, java.lang.String isActive, java.lang.String publicFlag,
		java.lang.String locale);

	public JSONObject updateSensitiveInfo(User user,
		java.lang.String IdIssueCountryId, java.lang.String IdType,
		java.lang.String IdNumber, java.lang.String ssn, java.lang.String locale);

	public JSONObject updateUserAddressInfo(User user,
		java.lang.String addressType, java.lang.String street1,
		java.lang.String street2, java.lang.String street3,
		java.lang.String city, java.lang.String regionId,
		java.lang.String countryId, java.lang.String zipCode,
		java.lang.String isMailing, java.lang.String locale);

	public JSONObject updateUserPortrait(User user, byte[] portrait,
		java.lang.String locale);

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();
}