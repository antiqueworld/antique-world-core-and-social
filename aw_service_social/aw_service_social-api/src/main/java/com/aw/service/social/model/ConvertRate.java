/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the ConvertRate service. Represents a row in the &quot;aw_social_ConvertRate&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ConvertRateModel
 * @see com.aw.service.social.model.impl.ConvertRateImpl
 * @see com.aw.service.social.model.impl.ConvertRateModelImpl
 * @generated
 */
@ImplementationClassName("com.aw.service.social.model.impl.ConvertRateImpl")
@ProviderType
public interface ConvertRate extends ConvertRateModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link com.aw.service.social.model.impl.ConvertRateImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<ConvertRate, String> RATE_ID_ACCESSOR = new Accessor<ConvertRate, String>() {
			@Override
			public String get(ConvertRate convertRate) {
				return convertRate.getRateId();
			}

			@Override
			public Class<String> getAttributeClass() {
				return String.class;
			}

			@Override
			public Class<ConvertRate> getTypeClass() {
				return ConvertRate.class;
			}
		};
}