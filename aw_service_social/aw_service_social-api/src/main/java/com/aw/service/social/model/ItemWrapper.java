/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Item}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Item
 * @generated
 */
@ProviderType
public class ItemWrapper implements Item, ModelWrapper<Item> {
	public ItemWrapper(Item item) {
		_item = item;
	}

	@Override
	public Class<?> getModelClass() {
		return Item.class;
	}

	@Override
	public String getModelClassName() {
		return Item.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("itemId", getItemId());
		attributes.put("ownerUid", getOwnerUid());
		attributes.put("erc20Addr", getErc20Addr());
		attributes.put("tokenCurrency", getTokenCurrency());
		attributes.put("erc721Addr", getErc721Addr());
		attributes.put("title", getTitle());
		attributes.put("description", getDescription());
		attributes.put("addTime", getAddTime());
		attributes.put("updateTime", getUpdateTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String itemId = (String)attributes.get("itemId");

		if (itemId != null) {
			setItemId(itemId);
		}

		String ownerUid = (String)attributes.get("ownerUid");

		if (ownerUid != null) {
			setOwnerUid(ownerUid);
		}

		String erc20Addr = (String)attributes.get("erc20Addr");

		if (erc20Addr != null) {
			setErc20Addr(erc20Addr);
		}

		String tokenCurrency = (String)attributes.get("tokenCurrency");

		if (tokenCurrency != null) {
			setTokenCurrency(tokenCurrency);
		}

		String erc721Addr = (String)attributes.get("erc721Addr");

		if (erc721Addr != null) {
			setErc721Addr(erc721Addr);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}

		Date updateTime = (Date)attributes.get("updateTime");

		if (updateTime != null) {
			setUpdateTime(updateTime);
		}
	}

	@Override
	public Item toEscapedModel() {
		return new ItemWrapper(_item.toEscapedModel());
	}

	@Override
	public Item toUnescapedModel() {
		return new ItemWrapper(_item.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _item.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _item.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _item.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _item.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Item> toCacheModel() {
		return _item.toCacheModel();
	}

	@Override
	public int compareTo(Item item) {
		return _item.compareTo(item);
	}

	@Override
	public int hashCode() {
		return _item.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _item.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ItemWrapper((Item)_item.clone());
	}

	/**
	* Returns the description of this item.
	*
	* @return the description of this item
	*/
	@Override
	public java.lang.String getDescription() {
		return _item.getDescription();
	}

	/**
	* Returns the erc20 addr of this item.
	*
	* @return the erc20 addr of this item
	*/
	@Override
	public java.lang.String getErc20Addr() {
		return _item.getErc20Addr();
	}

	/**
	* Returns the erc721 addr of this item.
	*
	* @return the erc721 addr of this item
	*/
	@Override
	public java.lang.String getErc721Addr() {
		return _item.getErc721Addr();
	}

	/**
	* Returns the item ID of this item.
	*
	* @return the item ID of this item
	*/
	@Override
	public java.lang.String getItemId() {
		return _item.getItemId();
	}

	/**
	* Returns the owner uid of this item.
	*
	* @return the owner uid of this item
	*/
	@Override
	public java.lang.String getOwnerUid() {
		return _item.getOwnerUid();
	}

	/**
	* Returns the primary key of this item.
	*
	* @return the primary key of this item
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _item.getPrimaryKey();
	}

	/**
	* Returns the title of this item.
	*
	* @return the title of this item
	*/
	@Override
	public java.lang.String getTitle() {
		return _item.getTitle();
	}

	/**
	* Returns the token currency of this item.
	*
	* @return the token currency of this item
	*/
	@Override
	public java.lang.String getTokenCurrency() {
		return _item.getTokenCurrency();
	}

	@Override
	public java.lang.String toString() {
		return _item.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _item.toXmlString();
	}

	/**
	* Returns the add time of this item.
	*
	* @return the add time of this item
	*/
	@Override
	public Date getAddTime() {
		return _item.getAddTime();
	}

	/**
	* Returns the update time of this item.
	*
	* @return the update time of this item
	*/
	@Override
	public Date getUpdateTime() {
		return _item.getUpdateTime();
	}

	@Override
	public void persist() {
		_item.persist();
	}

	/**
	* Sets the add time of this item.
	*
	* @param addTime the add time of this item
	*/
	@Override
	public void setAddTime(Date addTime) {
		_item.setAddTime(addTime);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_item.setCachedModel(cachedModel);
	}

	/**
	* Sets the description of this item.
	*
	* @param description the description of this item
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_item.setDescription(description);
	}

	/**
	* Sets the erc20 addr of this item.
	*
	* @param erc20Addr the erc20 addr of this item
	*/
	@Override
	public void setErc20Addr(java.lang.String erc20Addr) {
		_item.setErc20Addr(erc20Addr);
	}

	/**
	* Sets the erc721 addr of this item.
	*
	* @param erc721Addr the erc721 addr of this item
	*/
	@Override
	public void setErc721Addr(java.lang.String erc721Addr) {
		_item.setErc721Addr(erc721Addr);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_item.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_item.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_item.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the item ID of this item.
	*
	* @param itemId the item ID of this item
	*/
	@Override
	public void setItemId(java.lang.String itemId) {
		_item.setItemId(itemId);
	}

	@Override
	public void setNew(boolean n) {
		_item.setNew(n);
	}

	/**
	* Sets the owner uid of this item.
	*
	* @param ownerUid the owner uid of this item
	*/
	@Override
	public void setOwnerUid(java.lang.String ownerUid) {
		_item.setOwnerUid(ownerUid);
	}

	/**
	* Sets the primary key of this item.
	*
	* @param primaryKey the primary key of this item
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_item.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_item.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the title of this item.
	*
	* @param title the title of this item
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_item.setTitle(title);
	}

	/**
	* Sets the token currency of this item.
	*
	* @param tokenCurrency the token currency of this item
	*/
	@Override
	public void setTokenCurrency(java.lang.String tokenCurrency) {
		_item.setTokenCurrency(tokenCurrency);
	}

	/**
	* Sets the update time of this item.
	*
	* @param updateTime the update time of this item
	*/
	@Override
	public void setUpdateTime(Date updateTime) {
		_item.setUpdateTime(updateTime);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ItemWrapper)) {
			return false;
		}

		ItemWrapper itemWrapper = (ItemWrapper)obj;

		if (Objects.equals(_item, itemWrapper._item)) {
			return true;
		}

		return false;
	}

	@Override
	public Item getWrappedModel() {
		return _item;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _item.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _item.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_item.resetOriginalValues();
	}

	private final Item _item;
}