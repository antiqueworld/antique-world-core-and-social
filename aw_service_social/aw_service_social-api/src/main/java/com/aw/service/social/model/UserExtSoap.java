/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class UserExtSoap implements Serializable {
	public static UserExtSoap toSoapModel(UserExt model) {
		UserExtSoap soapModel = new UserExtSoap();

		soapModel.setUserUid(model.getUserUid());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserInstance(model.getUserInstance());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setMiddleName(model.getMiddleName());
		soapModel.setLastName(model.getLastName());
		soapModel.setBirthday(model.getBirthday());
		soapModel.setPortraitId(model.getPortraitId());
		soapModel.setTitle(model.getTitle());
		soapModel.setSelfDesc(model.getSelfDesc());
		soapModel.setSsn(model.getSsn());
		soapModel.setIdIssueCountry(model.getIdIssueCountry());
		soapModel.setIdType(model.getIdType());
		soapModel.setIdImageId(model.getIdImageId());
		soapModel.setIdNumber(model.getIdNumber());
		soapModel.setAddTime(model.getAddTime());
		soapModel.setUpdateTime(model.getUpdateTime());

		return soapModel;
	}

	public static UserExtSoap[] toSoapModels(UserExt[] models) {
		UserExtSoap[] soapModels = new UserExtSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserExtSoap[][] toSoapModels(UserExt[][] models) {
		UserExtSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserExtSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserExtSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserExtSoap[] toSoapModels(List<UserExt> models) {
		List<UserExtSoap> soapModels = new ArrayList<UserExtSoap>(models.size());

		for (UserExt model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserExtSoap[soapModels.size()]);
	}

	public UserExtSoap() {
	}

	public String getPrimaryKey() {
		return _userUid;
	}

	public void setPrimaryKey(String pk) {
		setUserUid(pk);
	}

	public String getUserUid() {
		return _userUid;
	}

	public void setUserUid(String userUid) {
		_userUid = userUid;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserInstance() {
		return _userInstance;
	}

	public void setUserInstance(String userInstance) {
		_userInstance = userInstance;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	public String getMiddleName() {
		return _middleName;
	}

	public void setMiddleName(String middleName) {
		_middleName = middleName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	public String getBirthday() {
		return _birthday;
	}

	public void setBirthday(String birthday) {
		_birthday = birthday;
	}

	public String getPortraitId() {
		return _portraitId;
	}

	public void setPortraitId(String portraitId) {
		_portraitId = portraitId;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getSelfDesc() {
		return _selfDesc;
	}

	public void setSelfDesc(String selfDesc) {
		_selfDesc = selfDesc;
	}

	public String getSsn() {
		return _ssn;
	}

	public void setSsn(String ssn) {
		_ssn = ssn;
	}

	public String getIdIssueCountry() {
		return _IdIssueCountry;
	}

	public void setIdIssueCountry(String IdIssueCountry) {
		_IdIssueCountry = IdIssueCountry;
	}

	public String getIdType() {
		return _IdType;
	}

	public void setIdType(String IdType) {
		_IdType = IdType;
	}

	public String getIdImageId() {
		return _IdImageId;
	}

	public void setIdImageId(String IdImageId) {
		_IdImageId = IdImageId;
	}

	public String getIdNumber() {
		return _IdNumber;
	}

	public void setIdNumber(String IdNumber) {
		_IdNumber = IdNumber;
	}

	public Date getAddTime() {
		return _addTime;
	}

	public void setAddTime(Date addTime) {
		_addTime = addTime;
	}

	public Date getUpdateTime() {
		return _updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		_updateTime = updateTime;
	}

	private String _userUid;
	private long _userId;
	private String _userInstance;
	private String _firstName;
	private String _middleName;
	private String _lastName;
	private String _birthday;
	private String _portraitId;
	private String _title;
	private String _selfDesc;
	private String _ssn;
	private String _IdIssueCountry;
	private String _IdType;
	private String _IdImageId;
	private String _IdNumber;
	private Date _addTime;
	private Date _updateTime;
}