/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.ConvertRate;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the convert rate service. This utility wraps {@link com.aw.service.social.service.persistence.impl.ConvertRatePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ConvertRatePersistence
 * @see com.aw.service.social.service.persistence.impl.ConvertRatePersistenceImpl
 * @generated
 */
@ProviderType
public class ConvertRateUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ConvertRate convertRate) {
		getPersistence().clearCache(convertRate);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ConvertRate> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ConvertRate> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ConvertRate> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ConvertRate> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ConvertRate update(ConvertRate convertRate) {
		return getPersistence().update(convertRate);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ConvertRate update(ConvertRate convertRate,
		ServiceContext serviceContext) {
		return getPersistence().update(convertRate, serviceContext);
	}

	/**
	* Returns all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @return the matching convert rates
	*/
	public static List<ConvertRate> findBypair(java.lang.String fromCurrency,
		java.lang.String toCurrency) {
		return getPersistence().findBypair(fromCurrency, toCurrency);
	}

	/**
	* Returns a range of all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @return the range of matching convert rates
	*/
	public static List<ConvertRate> findBypair(java.lang.String fromCurrency,
		java.lang.String toCurrency, int start, int end) {
		return getPersistence().findBypair(fromCurrency, toCurrency, start, end);
	}

	/**
	* Returns an ordered range of all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching convert rates
	*/
	public static List<ConvertRate> findBypair(java.lang.String fromCurrency,
		java.lang.String toCurrency, int start, int end,
		OrderByComparator<ConvertRate> orderByComparator) {
		return getPersistence()
				   .findBypair(fromCurrency, toCurrency, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching convert rates
	*/
	public static List<ConvertRate> findBypair(java.lang.String fromCurrency,
		java.lang.String toCurrency, int start, int end,
		OrderByComparator<ConvertRate> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBypair(fromCurrency, toCurrency, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching convert rate
	* @throws NoSuchConvertRateException if a matching convert rate could not be found
	*/
	public static ConvertRate findBypair_First(java.lang.String fromCurrency,
		java.lang.String toCurrency,
		OrderByComparator<ConvertRate> orderByComparator)
		throws com.aw.service.social.exception.NoSuchConvertRateException {
		return getPersistence()
				   .findBypair_First(fromCurrency, toCurrency, orderByComparator);
	}

	/**
	* Returns the first convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching convert rate, or <code>null</code> if a matching convert rate could not be found
	*/
	public static ConvertRate fetchBypair_First(java.lang.String fromCurrency,
		java.lang.String toCurrency,
		OrderByComparator<ConvertRate> orderByComparator) {
		return getPersistence()
				   .fetchBypair_First(fromCurrency, toCurrency,
			orderByComparator);
	}

	/**
	* Returns the last convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching convert rate
	* @throws NoSuchConvertRateException if a matching convert rate could not be found
	*/
	public static ConvertRate findBypair_Last(java.lang.String fromCurrency,
		java.lang.String toCurrency,
		OrderByComparator<ConvertRate> orderByComparator)
		throws com.aw.service.social.exception.NoSuchConvertRateException {
		return getPersistence()
				   .findBypair_Last(fromCurrency, toCurrency, orderByComparator);
	}

	/**
	* Returns the last convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching convert rate, or <code>null</code> if a matching convert rate could not be found
	*/
	public static ConvertRate fetchBypair_Last(java.lang.String fromCurrency,
		java.lang.String toCurrency,
		OrderByComparator<ConvertRate> orderByComparator) {
		return getPersistence()
				   .fetchBypair_Last(fromCurrency, toCurrency, orderByComparator);
	}

	/**
	* Returns the convert rates before and after the current convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param rateId the primary key of the current convert rate
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next convert rate
	* @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	*/
	public static ConvertRate[] findBypair_PrevAndNext(
		java.lang.String rateId, java.lang.String fromCurrency,
		java.lang.String toCurrency,
		OrderByComparator<ConvertRate> orderByComparator)
		throws com.aw.service.social.exception.NoSuchConvertRateException {
		return getPersistence()
				   .findBypair_PrevAndNext(rateId, fromCurrency, toCurrency,
			orderByComparator);
	}

	/**
	* Removes all the convert rates where fromCurrency = &#63; and toCurrency = &#63; from the database.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	*/
	public static void removeBypair(java.lang.String fromCurrency,
		java.lang.String toCurrency) {
		getPersistence().removeBypair(fromCurrency, toCurrency);
	}

	/**
	* Returns the number of convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	*
	* @param fromCurrency the from currency
	* @param toCurrency the to currency
	* @return the number of matching convert rates
	*/
	public static int countBypair(java.lang.String fromCurrency,
		java.lang.String toCurrency) {
		return getPersistence().countBypair(fromCurrency, toCurrency);
	}

	/**
	* Returns all the convert rates where isActive = &#63;.
	*
	* @param isActive the is active
	* @return the matching convert rates
	*/
	public static List<ConvertRate> findByactive(java.lang.String isActive) {
		return getPersistence().findByactive(isActive);
	}

	/**
	* Returns a range of all the convert rates where isActive = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isActive the is active
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @return the range of matching convert rates
	*/
	public static List<ConvertRate> findByactive(java.lang.String isActive,
		int start, int end) {
		return getPersistence().findByactive(isActive, start, end);
	}

	/**
	* Returns an ordered range of all the convert rates where isActive = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isActive the is active
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching convert rates
	*/
	public static List<ConvertRate> findByactive(java.lang.String isActive,
		int start, int end, OrderByComparator<ConvertRate> orderByComparator) {
		return getPersistence()
				   .findByactive(isActive, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the convert rates where isActive = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isActive the is active
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching convert rates
	*/
	public static List<ConvertRate> findByactive(java.lang.String isActive,
		int start, int end, OrderByComparator<ConvertRate> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByactive(isActive, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first convert rate in the ordered set where isActive = &#63;.
	*
	* @param isActive the is active
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching convert rate
	* @throws NoSuchConvertRateException if a matching convert rate could not be found
	*/
	public static ConvertRate findByactive_First(java.lang.String isActive,
		OrderByComparator<ConvertRate> orderByComparator)
		throws com.aw.service.social.exception.NoSuchConvertRateException {
		return getPersistence().findByactive_First(isActive, orderByComparator);
	}

	/**
	* Returns the first convert rate in the ordered set where isActive = &#63;.
	*
	* @param isActive the is active
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching convert rate, or <code>null</code> if a matching convert rate could not be found
	*/
	public static ConvertRate fetchByactive_First(java.lang.String isActive,
		OrderByComparator<ConvertRate> orderByComparator) {
		return getPersistence().fetchByactive_First(isActive, orderByComparator);
	}

	/**
	* Returns the last convert rate in the ordered set where isActive = &#63;.
	*
	* @param isActive the is active
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching convert rate
	* @throws NoSuchConvertRateException if a matching convert rate could not be found
	*/
	public static ConvertRate findByactive_Last(java.lang.String isActive,
		OrderByComparator<ConvertRate> orderByComparator)
		throws com.aw.service.social.exception.NoSuchConvertRateException {
		return getPersistence().findByactive_Last(isActive, orderByComparator);
	}

	/**
	* Returns the last convert rate in the ordered set where isActive = &#63;.
	*
	* @param isActive the is active
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching convert rate, or <code>null</code> if a matching convert rate could not be found
	*/
	public static ConvertRate fetchByactive_Last(java.lang.String isActive,
		OrderByComparator<ConvertRate> orderByComparator) {
		return getPersistence().fetchByactive_Last(isActive, orderByComparator);
	}

	/**
	* Returns the convert rates before and after the current convert rate in the ordered set where isActive = &#63;.
	*
	* @param rateId the primary key of the current convert rate
	* @param isActive the is active
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next convert rate
	* @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	*/
	public static ConvertRate[] findByactive_PrevAndNext(
		java.lang.String rateId, java.lang.String isActive,
		OrderByComparator<ConvertRate> orderByComparator)
		throws com.aw.service.social.exception.NoSuchConvertRateException {
		return getPersistence()
				   .findByactive_PrevAndNext(rateId, isActive, orderByComparator);
	}

	/**
	* Removes all the convert rates where isActive = &#63; from the database.
	*
	* @param isActive the is active
	*/
	public static void removeByactive(java.lang.String isActive) {
		getPersistence().removeByactive(isActive);
	}

	/**
	* Returns the number of convert rates where isActive = &#63;.
	*
	* @param isActive the is active
	* @return the number of matching convert rates
	*/
	public static int countByactive(java.lang.String isActive) {
		return getPersistence().countByactive(isActive);
	}

	/**
	* Caches the convert rate in the entity cache if it is enabled.
	*
	* @param convertRate the convert rate
	*/
	public static void cacheResult(ConvertRate convertRate) {
		getPersistence().cacheResult(convertRate);
	}

	/**
	* Caches the convert rates in the entity cache if it is enabled.
	*
	* @param convertRates the convert rates
	*/
	public static void cacheResult(List<ConvertRate> convertRates) {
		getPersistence().cacheResult(convertRates);
	}

	/**
	* Creates a new convert rate with the primary key. Does not add the convert rate to the database.
	*
	* @param rateId the primary key for the new convert rate
	* @return the new convert rate
	*/
	public static ConvertRate create(java.lang.String rateId) {
		return getPersistence().create(rateId);
	}

	/**
	* Removes the convert rate with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param rateId the primary key of the convert rate
	* @return the convert rate that was removed
	* @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	*/
	public static ConvertRate remove(java.lang.String rateId)
		throws com.aw.service.social.exception.NoSuchConvertRateException {
		return getPersistence().remove(rateId);
	}

	public static ConvertRate updateImpl(ConvertRate convertRate) {
		return getPersistence().updateImpl(convertRate);
	}

	/**
	* Returns the convert rate with the primary key or throws a {@link NoSuchConvertRateException} if it could not be found.
	*
	* @param rateId the primary key of the convert rate
	* @return the convert rate
	* @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	*/
	public static ConvertRate findByPrimaryKey(java.lang.String rateId)
		throws com.aw.service.social.exception.NoSuchConvertRateException {
		return getPersistence().findByPrimaryKey(rateId);
	}

	/**
	* Returns the convert rate with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param rateId the primary key of the convert rate
	* @return the convert rate, or <code>null</code> if a convert rate with the primary key could not be found
	*/
	public static ConvertRate fetchByPrimaryKey(java.lang.String rateId) {
		return getPersistence().fetchByPrimaryKey(rateId);
	}

	public static java.util.Map<java.io.Serializable, ConvertRate> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the convert rates.
	*
	* @return the convert rates
	*/
	public static List<ConvertRate> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the convert rates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @return the range of convert rates
	*/
	public static List<ConvertRate> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the convert rates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of convert rates
	*/
	public static List<ConvertRate> findAll(int start, int end,
		OrderByComparator<ConvertRate> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the convert rates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of convert rates
	*/
	public static List<ConvertRate> findAll(int start, int end,
		OrderByComparator<ConvertRate> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the convert rates from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of convert rates.
	*
	* @return the number of convert rates
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ConvertRatePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ConvertRatePersistence, ConvertRatePersistence> _serviceTracker =
		ServiceTrackerFactory.open(ConvertRatePersistence.class);
}