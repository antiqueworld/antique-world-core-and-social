/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link AddressExt}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AddressExt
 * @generated
 */
@ProviderType
public class AddressExtWrapper implements AddressExt, ModelWrapper<AddressExt> {
	public AddressExtWrapper(AddressExt addressExt) {
		_addressExt = addressExt;
	}

	@Override
	public Class<?> getModelClass() {
		return AddressExt.class;
	}

	@Override
	public String getModelClassName() {
		return AddressExt.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userUid", getUserUid());
		attributes.put("addressType", getAddressType());
		attributes.put("street1", getStreet1());
		attributes.put("street2", getStreet2());
		attributes.put("street3", getStreet3());
		attributes.put("city", getCity());
		attributes.put("regionId", getRegionId());
		attributes.put("countryId", getCountryId());
		attributes.put("zipCode", getZipCode());
		attributes.put("isMailing", getIsMailing());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String userUid = (String)attributes.get("userUid");

		if (userUid != null) {
			setUserUid(userUid);
		}

		String addressType = (String)attributes.get("addressType");

		if (addressType != null) {
			setAddressType(addressType);
		}

		String street1 = (String)attributes.get("street1");

		if (street1 != null) {
			setStreet1(street1);
		}

		String street2 = (String)attributes.get("street2");

		if (street2 != null) {
			setStreet2(street2);
		}

		String street3 = (String)attributes.get("street3");

		if (street3 != null) {
			setStreet3(street3);
		}

		String city = (String)attributes.get("city");

		if (city != null) {
			setCity(city);
		}

		String regionId = (String)attributes.get("regionId");

		if (regionId != null) {
			setRegionId(regionId);
		}

		String countryId = (String)attributes.get("countryId");

		if (countryId != null) {
			setCountryId(countryId);
		}

		String zipCode = (String)attributes.get("zipCode");

		if (zipCode != null) {
			setZipCode(zipCode);
		}

		String isMailing = (String)attributes.get("isMailing");

		if (isMailing != null) {
			setIsMailing(isMailing);
		}
	}

	@Override
	public AddressExt toEscapedModel() {
		return new AddressExtWrapper(_addressExt.toEscapedModel());
	}

	@Override
	public AddressExt toUnescapedModel() {
		return new AddressExtWrapper(_addressExt.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _addressExt.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _addressExt.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _addressExt.isNew();
	}

	/**
	* Returns the primary key of this address ext.
	*
	* @return the primary key of this address ext
	*/
	@Override
	public com.aw.service.social.service.persistence.AddressExtPK getPrimaryKey() {
		return _addressExt.getPrimaryKey();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _addressExt.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<AddressExt> toCacheModel() {
		return _addressExt.toCacheModel();
	}

	@Override
	public int compareTo(AddressExt addressExt) {
		return _addressExt.compareTo(addressExt);
	}

	@Override
	public int hashCode() {
		return _addressExt.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _addressExt.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new AddressExtWrapper((AddressExt)_addressExt.clone());
	}

	/**
	* Returns the address type of this address ext.
	*
	* @return the address type of this address ext
	*/
	@Override
	public java.lang.String getAddressType() {
		return _addressExt.getAddressType();
	}

	/**
	* Returns the city of this address ext.
	*
	* @return the city of this address ext
	*/
	@Override
	public java.lang.String getCity() {
		return _addressExt.getCity();
	}

	/**
	* Returns the country ID of this address ext.
	*
	* @return the country ID of this address ext
	*/
	@Override
	public java.lang.String getCountryId() {
		return _addressExt.getCountryId();
	}

	/**
	* Returns the is mailing of this address ext.
	*
	* @return the is mailing of this address ext
	*/
	@Override
	public java.lang.String getIsMailing() {
		return _addressExt.getIsMailing();
	}

	/**
	* Returns the region ID of this address ext.
	*
	* @return the region ID of this address ext
	*/
	@Override
	public java.lang.String getRegionId() {
		return _addressExt.getRegionId();
	}

	/**
	* Returns the street1 of this address ext.
	*
	* @return the street1 of this address ext
	*/
	@Override
	public java.lang.String getStreet1() {
		return _addressExt.getStreet1();
	}

	/**
	* Returns the street2 of this address ext.
	*
	* @return the street2 of this address ext
	*/
	@Override
	public java.lang.String getStreet2() {
		return _addressExt.getStreet2();
	}

	/**
	* Returns the street3 of this address ext.
	*
	* @return the street3 of this address ext
	*/
	@Override
	public java.lang.String getStreet3() {
		return _addressExt.getStreet3();
	}

	/**
	* Returns the user uid of this address ext.
	*
	* @return the user uid of this address ext
	*/
	@Override
	public java.lang.String getUserUid() {
		return _addressExt.getUserUid();
	}

	/**
	* Returns the zip code of this address ext.
	*
	* @return the zip code of this address ext
	*/
	@Override
	public java.lang.String getZipCode() {
		return _addressExt.getZipCode();
	}

	@Override
	public java.lang.String toString() {
		return _addressExt.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _addressExt.toXmlString();
	}

	@Override
	public void persist() {
		_addressExt.persist();
	}

	/**
	* Sets the address type of this address ext.
	*
	* @param addressType the address type of this address ext
	*/
	@Override
	public void setAddressType(java.lang.String addressType) {
		_addressExt.setAddressType(addressType);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_addressExt.setCachedModel(cachedModel);
	}

	/**
	* Sets the city of this address ext.
	*
	* @param city the city of this address ext
	*/
	@Override
	public void setCity(java.lang.String city) {
		_addressExt.setCity(city);
	}

	/**
	* Sets the country ID of this address ext.
	*
	* @param countryId the country ID of this address ext
	*/
	@Override
	public void setCountryId(java.lang.String countryId) {
		_addressExt.setCountryId(countryId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_addressExt.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_addressExt.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_addressExt.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the is mailing of this address ext.
	*
	* @param isMailing the is mailing of this address ext
	*/
	@Override
	public void setIsMailing(java.lang.String isMailing) {
		_addressExt.setIsMailing(isMailing);
	}

	@Override
	public void setNew(boolean n) {
		_addressExt.setNew(n);
	}

	/**
	* Sets the primary key of this address ext.
	*
	* @param primaryKey the primary key of this address ext
	*/
	@Override
	public void setPrimaryKey(
		com.aw.service.social.service.persistence.AddressExtPK primaryKey) {
		_addressExt.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_addressExt.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the region ID of this address ext.
	*
	* @param regionId the region ID of this address ext
	*/
	@Override
	public void setRegionId(java.lang.String regionId) {
		_addressExt.setRegionId(regionId);
	}

	/**
	* Sets the street1 of this address ext.
	*
	* @param street1 the street1 of this address ext
	*/
	@Override
	public void setStreet1(java.lang.String street1) {
		_addressExt.setStreet1(street1);
	}

	/**
	* Sets the street2 of this address ext.
	*
	* @param street2 the street2 of this address ext
	*/
	@Override
	public void setStreet2(java.lang.String street2) {
		_addressExt.setStreet2(street2);
	}

	/**
	* Sets the street3 of this address ext.
	*
	* @param street3 the street3 of this address ext
	*/
	@Override
	public void setStreet3(java.lang.String street3) {
		_addressExt.setStreet3(street3);
	}

	/**
	* Sets the user uid of this address ext.
	*
	* @param userUid the user uid of this address ext
	*/
	@Override
	public void setUserUid(java.lang.String userUid) {
		_addressExt.setUserUid(userUid);
	}

	/**
	* Sets the zip code of this address ext.
	*
	* @param zipCode the zip code of this address ext
	*/
	@Override
	public void setZipCode(java.lang.String zipCode) {
		_addressExt.setZipCode(zipCode);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AddressExtWrapper)) {
			return false;
		}

		AddressExtWrapper addressExtWrapper = (AddressExtWrapper)obj;

		if (Objects.equals(_addressExt, addressExtWrapper._addressExt)) {
			return true;
		}

		return false;
	}

	@Override
	public AddressExt getWrappedModel() {
		return _addressExt;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _addressExt.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _addressExt.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_addressExt.resetOriginalValues();
	}

	private final AddressExt _addressExt;
}