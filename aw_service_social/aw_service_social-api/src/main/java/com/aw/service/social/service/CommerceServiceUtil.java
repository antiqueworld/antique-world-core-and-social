/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Commerce. This utility wraps
 * {@link com.aw.service.social.service.impl.CommerceServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see CommerceService
 * @see com.aw.service.social.service.base.CommerceServiceBaseImpl
 * @see com.aw.service.social.service.impl.CommerceServiceImpl
 * @generated
 */
@ProviderType
public class CommerceServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.service.social.service.impl.CommerceServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.json.JSONObject allActiveRates(
		String from, String to, String locale) {
		return getService().allActiveRates(from, to, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject cancelOrder(
		String orderId, String locale) {
		return getService().cancelOrder(orderId, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject changeRate(
		String from, String to, double newRate, String locale) {
		return getService().changeRate(from, to, newRate, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject createOrder(
		String payType, String payCurrency, String getCurrency, double amount,
		String walletAddress, String locale) {
		return getService()
				   .createOrder(payType, payCurrency, getCurrency, amount,
			walletAddress, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject createOrderForUser(
		String userUid, String payType, String payCurrency, String getCurrency,
		double amount, String walletAddress, String locale) {
		return getService()
				   .createOrderForUser(userUid, payType, payCurrency,
			getCurrency, amount, walletAddress, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject getActiveRate(
		String from, String to, String locale) {
		return getService().getActiveRate(from, to, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject getAllOrders(
		String chargeFlag, String sentFlag, String locale) {
		return getService().getAllOrders(chargeFlag, sentFlag, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject getCoinIssued(
		String toCurrency, String locale) {
		return getService().getCoinIssued(toCurrency, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject getMoneyRaise(
		String fromCurrency, String locale) {
		return getService().getMoneyRaise(fromCurrency, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject getOrderDetail(
		String orderId, String locale) {
		return getService().getOrderDetail(orderId, locale);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.json.JSONObject getRates(
		String from, String to, String locale) {
		return getService().getRates(from, to, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject orderComplete(
		String orderId, String locale) {
		return getService().orderComplete(orderId, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject orderPaid(
		String orderId, String locale) {
		return getService().orderPaid(orderId, locale);
	}

	public static CommerceService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<CommerceService, CommerceService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(CommerceService.class);

		ServiceTracker<CommerceService, CommerceService> serviceTracker = new ServiceTracker<CommerceService, CommerceService>(bundle.getBundleContext(),
				CommerceService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}