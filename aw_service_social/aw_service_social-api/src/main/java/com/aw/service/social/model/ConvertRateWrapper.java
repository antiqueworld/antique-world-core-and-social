/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ConvertRate}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ConvertRate
 * @generated
 */
@ProviderType
public class ConvertRateWrapper implements ConvertRate,
	ModelWrapper<ConvertRate> {
	public ConvertRateWrapper(ConvertRate convertRate) {
		_convertRate = convertRate;
	}

	@Override
	public Class<?> getModelClass() {
		return ConvertRate.class;
	}

	@Override
	public String getModelClassName() {
		return ConvertRate.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("rateId", getRateId());
		attributes.put("fromCurrency", getFromCurrency());
		attributes.put("toCurrency", getToCurrency());
		attributes.put("isActive", getIsActive());
		attributes.put("rate", getRate());
		attributes.put("startTime", getStartTime());
		attributes.put("expireTime", getExpireTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String rateId = (String)attributes.get("rateId");

		if (rateId != null) {
			setRateId(rateId);
		}

		String fromCurrency = (String)attributes.get("fromCurrency");

		if (fromCurrency != null) {
			setFromCurrency(fromCurrency);
		}

		String toCurrency = (String)attributes.get("toCurrency");

		if (toCurrency != null) {
			setToCurrency(toCurrency);
		}

		String isActive = (String)attributes.get("isActive");

		if (isActive != null) {
			setIsActive(isActive);
		}

		Double rate = (Double)attributes.get("rate");

		if (rate != null) {
			setRate(rate);
		}

		Date startTime = (Date)attributes.get("startTime");

		if (startTime != null) {
			setStartTime(startTime);
		}

		Date expireTime = (Date)attributes.get("expireTime");

		if (expireTime != null) {
			setExpireTime(expireTime);
		}
	}

	@Override
	public ConvertRate toEscapedModel() {
		return new ConvertRateWrapper(_convertRate.toEscapedModel());
	}

	@Override
	public ConvertRate toUnescapedModel() {
		return new ConvertRateWrapper(_convertRate.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _convertRate.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _convertRate.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _convertRate.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _convertRate.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ConvertRate> toCacheModel() {
		return _convertRate.toCacheModel();
	}

	/**
	* Returns the rate of this convert rate.
	*
	* @return the rate of this convert rate
	*/
	@Override
	public double getRate() {
		return _convertRate.getRate();
	}

	@Override
	public int compareTo(ConvertRate convertRate) {
		return _convertRate.compareTo(convertRate);
	}

	@Override
	public int hashCode() {
		return _convertRate.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _convertRate.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ConvertRateWrapper((ConvertRate)_convertRate.clone());
	}

	/**
	* Returns the from currency of this convert rate.
	*
	* @return the from currency of this convert rate
	*/
	@Override
	public java.lang.String getFromCurrency() {
		return _convertRate.getFromCurrency();
	}

	/**
	* Returns the is active of this convert rate.
	*
	* @return the is active of this convert rate
	*/
	@Override
	public java.lang.String getIsActive() {
		return _convertRate.getIsActive();
	}

	/**
	* Returns the primary key of this convert rate.
	*
	* @return the primary key of this convert rate
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _convertRate.getPrimaryKey();
	}

	/**
	* Returns the rate ID of this convert rate.
	*
	* @return the rate ID of this convert rate
	*/
	@Override
	public java.lang.String getRateId() {
		return _convertRate.getRateId();
	}

	/**
	* Returns the to currency of this convert rate.
	*
	* @return the to currency of this convert rate
	*/
	@Override
	public java.lang.String getToCurrency() {
		return _convertRate.getToCurrency();
	}

	@Override
	public java.lang.String toString() {
		return _convertRate.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _convertRate.toXmlString();
	}

	/**
	* Returns the expire time of this convert rate.
	*
	* @return the expire time of this convert rate
	*/
	@Override
	public Date getExpireTime() {
		return _convertRate.getExpireTime();
	}

	/**
	* Returns the start time of this convert rate.
	*
	* @return the start time of this convert rate
	*/
	@Override
	public Date getStartTime() {
		return _convertRate.getStartTime();
	}

	@Override
	public void persist() {
		_convertRate.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_convertRate.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_convertRate.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_convertRate.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_convertRate.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the expire time of this convert rate.
	*
	* @param expireTime the expire time of this convert rate
	*/
	@Override
	public void setExpireTime(Date expireTime) {
		_convertRate.setExpireTime(expireTime);
	}

	/**
	* Sets the from currency of this convert rate.
	*
	* @param fromCurrency the from currency of this convert rate
	*/
	@Override
	public void setFromCurrency(java.lang.String fromCurrency) {
		_convertRate.setFromCurrency(fromCurrency);
	}

	/**
	* Sets the is active of this convert rate.
	*
	* @param isActive the is active of this convert rate
	*/
	@Override
	public void setIsActive(java.lang.String isActive) {
		_convertRate.setIsActive(isActive);
	}

	@Override
	public void setNew(boolean n) {
		_convertRate.setNew(n);
	}

	/**
	* Sets the primary key of this convert rate.
	*
	* @param primaryKey the primary key of this convert rate
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_convertRate.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_convertRate.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the rate of this convert rate.
	*
	* @param rate the rate of this convert rate
	*/
	@Override
	public void setRate(double rate) {
		_convertRate.setRate(rate);
	}

	/**
	* Sets the rate ID of this convert rate.
	*
	* @param rateId the rate ID of this convert rate
	*/
	@Override
	public void setRateId(java.lang.String rateId) {
		_convertRate.setRateId(rateId);
	}

	/**
	* Sets the start time of this convert rate.
	*
	* @param startTime the start time of this convert rate
	*/
	@Override
	public void setStartTime(Date startTime) {
		_convertRate.setStartTime(startTime);
	}

	/**
	* Sets the to currency of this convert rate.
	*
	* @param toCurrency the to currency of this convert rate
	*/
	@Override
	public void setToCurrency(java.lang.String toCurrency) {
		_convertRate.setToCurrency(toCurrency);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ConvertRateWrapper)) {
			return false;
		}

		ConvertRateWrapper convertRateWrapper = (ConvertRateWrapper)obj;

		if (Objects.equals(_convertRate, convertRateWrapper._convertRate)) {
			return true;
		}

		return false;
	}

	@Override
	public ConvertRate getWrappedModel() {
		return _convertRate;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _convertRate.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _convertRate.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_convertRate.resetOriginalValues();
	}

	private final ConvertRate _convertRate;
}