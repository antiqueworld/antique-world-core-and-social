/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class OrderSoap implements Serializable {
	public static OrderSoap toSoapModel(Order model) {
		OrderSoap soapModel = new OrderSoap();

		soapModel.setOrderId(model.getOrderId());
		soapModel.setUserUid(model.getUserUid());
		soapModel.setUserWalletAddr(model.getUserWalletAddr());
		soapModel.setFromType(model.getFromType());
		soapModel.setFromCurrency(model.getFromCurrency());
		soapModel.setFromAmount(model.getFromAmount());
		soapModel.setRateId(model.getRateId());
		soapModel.setToCurrency(model.getToCurrency());
		soapModel.setToAmount(model.getToAmount());
		soapModel.setChargedFlag(model.getChargedFlag());
		soapModel.setSentFlag(model.getSentFlag());
		soapModel.setCompletedFlag(model.getCompletedFlag());
		soapModel.setAddTime(model.getAddTime());
		soapModel.setUpdateTime(model.getUpdateTime());

		return soapModel;
	}

	public static OrderSoap[] toSoapModels(Order[] models) {
		OrderSoap[] soapModels = new OrderSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static OrderSoap[][] toSoapModels(Order[][] models) {
		OrderSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new OrderSoap[models.length][models[0].length];
		}
		else {
			soapModels = new OrderSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static OrderSoap[] toSoapModels(List<Order> models) {
		List<OrderSoap> soapModels = new ArrayList<OrderSoap>(models.size());

		for (Order model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new OrderSoap[soapModels.size()]);
	}

	public OrderSoap() {
	}

	public String getPrimaryKey() {
		return _OrderId;
	}

	public void setPrimaryKey(String pk) {
		setOrderId(pk);
	}

	public String getOrderId() {
		return _OrderId;
	}

	public void setOrderId(String OrderId) {
		_OrderId = OrderId;
	}

	public String getUserUid() {
		return _userUid;
	}

	public void setUserUid(String userUid) {
		_userUid = userUid;
	}

	public String getUserWalletAddr() {
		return _userWalletAddr;
	}

	public void setUserWalletAddr(String userWalletAddr) {
		_userWalletAddr = userWalletAddr;
	}

	public String getFromType() {
		return _fromType;
	}

	public void setFromType(String fromType) {
		_fromType = fromType;
	}

	public String getFromCurrency() {
		return _fromCurrency;
	}

	public void setFromCurrency(String fromCurrency) {
		_fromCurrency = fromCurrency;
	}

	public double getFromAmount() {
		return _fromAmount;
	}

	public void setFromAmount(double fromAmount) {
		_fromAmount = fromAmount;
	}

	public String getRateId() {
		return _rateId;
	}

	public void setRateId(String rateId) {
		_rateId = rateId;
	}

	public String getToCurrency() {
		return _toCurrency;
	}

	public void setToCurrency(String toCurrency) {
		_toCurrency = toCurrency;
	}

	public double getToAmount() {
		return _toAmount;
	}

	public void setToAmount(double toAmount) {
		_toAmount = toAmount;
	}

	public String getChargedFlag() {
		return _chargedFlag;
	}

	public void setChargedFlag(String chargedFlag) {
		_chargedFlag = chargedFlag;
	}

	public String getSentFlag() {
		return _sentFlag;
	}

	public void setSentFlag(String sentFlag) {
		_sentFlag = sentFlag;
	}

	public String getCompletedFlag() {
		return _completedFlag;
	}

	public void setCompletedFlag(String completedFlag) {
		_completedFlag = completedFlag;
	}

	public Date getAddTime() {
		return _addTime;
	}

	public void setAddTime(Date addTime) {
		_addTime = addTime;
	}

	public Date getUpdateTime() {
		return _updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		_updateTime = updateTime;
	}

	private String _OrderId;
	private String _userUid;
	private String _userWalletAddr;
	private String _fromType;
	private String _fromCurrency;
	private double _fromAmount;
	private String _rateId;
	private String _toCurrency;
	private double _toAmount;
	private String _chargedFlag;
	private String _sentFlag;
	private String _completedFlag;
	private Date _addTime;
	private Date _updateTime;
}