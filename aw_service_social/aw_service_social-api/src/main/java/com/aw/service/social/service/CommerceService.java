/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.liferay.portal.kernel.service.BaseService;
import com.liferay.portal.kernel.spring.osgi.OSGiBeanProperties;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * Provides the remote service interface for Commerce. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see CommerceServiceUtil
 * @see com.aw.service.social.service.base.CommerceServiceBaseImpl
 * @see com.aw.service.social.service.impl.CommerceServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@OSGiBeanProperties(property =  {
	"json.web.service.context.name=aw_social", "json.web.service.context.path=Commerce"}, service = CommerceService.class)
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface CommerceService extends BaseService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CommerceServiceUtil} to access the commerce remote service. Add custom service methods to {@link com.aw.service.social.service.impl.CommerceServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public JSONObject allActiveRates(String from, String to, String locale);

	public JSONObject cancelOrder(String orderId, String locale);

	public JSONObject changeRate(String from, String to, double newRate,
		String locale);

	public JSONObject createOrder(String payType, String payCurrency,
		String getCurrency, double amount, String walletAddress, String locale);

	public JSONObject createOrderForUser(String userUid, String payType,
		String payCurrency, String getCurrency, double amount,
		String walletAddress, String locale);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getActiveRate(String from, String to, String locale);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getAllOrders(String chargeFlag, String sentFlag,
		String locale);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getCoinIssued(String toCurrency, String locale);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getMoneyRaise(String fromCurrency, String locale);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getOrderDetail(String orderId, String locale);

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public String getOSGiServiceIdentifier();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getRates(String from, String to, String locale);

	public JSONObject orderComplete(String orderId, String locale);

	public JSONObject orderPaid(String orderId, String locale);
}