/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.exception.NoSuchTransactionException;
import com.aw.service.social.model.Transaction;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the transaction service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.aw.service.social.service.persistence.impl.TransactionPersistenceImpl
 * @see TransactionUtil
 * @generated
 */
@ProviderType
public interface TransactionPersistence extends BasePersistence<Transaction> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TransactionUtil} to access the transaction persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the transactions where orderId = &#63;.
	*
	* @param orderId the order ID
	* @return the matching transactions
	*/
	public java.util.List<Transaction> findByorder(java.lang.String orderId);

	/**
	* Returns a range of all the transactions where orderId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param orderId the order ID
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @return the range of matching transactions
	*/
	public java.util.List<Transaction> findByorder(java.lang.String orderId,
		int start, int end);

	/**
	* Returns an ordered range of all the transactions where orderId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param orderId the order ID
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching transactions
	*/
	public java.util.List<Transaction> findByorder(java.lang.String orderId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Transaction> orderByComparator);

	/**
	* Returns an ordered range of all the transactions where orderId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param orderId the order ID
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching transactions
	*/
	public java.util.List<Transaction> findByorder(java.lang.String orderId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Transaction> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first transaction in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching transaction
	* @throws NoSuchTransactionException if a matching transaction could not be found
	*/
	public Transaction findByorder_First(java.lang.String orderId,
		com.liferay.portal.kernel.util.OrderByComparator<Transaction> orderByComparator)
		throws NoSuchTransactionException;

	/**
	* Returns the first transaction in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching transaction, or <code>null</code> if a matching transaction could not be found
	*/
	public Transaction fetchByorder_First(java.lang.String orderId,
		com.liferay.portal.kernel.util.OrderByComparator<Transaction> orderByComparator);

	/**
	* Returns the last transaction in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching transaction
	* @throws NoSuchTransactionException if a matching transaction could not be found
	*/
	public Transaction findByorder_Last(java.lang.String orderId,
		com.liferay.portal.kernel.util.OrderByComparator<Transaction> orderByComparator)
		throws NoSuchTransactionException;

	/**
	* Returns the last transaction in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching transaction, or <code>null</code> if a matching transaction could not be found
	*/
	public Transaction fetchByorder_Last(java.lang.String orderId,
		com.liferay.portal.kernel.util.OrderByComparator<Transaction> orderByComparator);

	/**
	* Returns the transactions before and after the current transaction in the ordered set where orderId = &#63;.
	*
	* @param transactionId the primary key of the current transaction
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next transaction
	* @throws NoSuchTransactionException if a transaction with the primary key could not be found
	*/
	public Transaction[] findByorder_PrevAndNext(
		java.lang.String transactionId, java.lang.String orderId,
		com.liferay.portal.kernel.util.OrderByComparator<Transaction> orderByComparator)
		throws NoSuchTransactionException;

	/**
	* Removes all the transactions where orderId = &#63; from the database.
	*
	* @param orderId the order ID
	*/
	public void removeByorder(java.lang.String orderId);

	/**
	* Returns the number of transactions where orderId = &#63;.
	*
	* @param orderId the order ID
	* @return the number of matching transactions
	*/
	public int countByorder(java.lang.String orderId);

	/**
	* Caches the transaction in the entity cache if it is enabled.
	*
	* @param transaction the transaction
	*/
	public void cacheResult(Transaction transaction);

	/**
	* Caches the transactions in the entity cache if it is enabled.
	*
	* @param transactions the transactions
	*/
	public void cacheResult(java.util.List<Transaction> transactions);

	/**
	* Creates a new transaction with the primary key. Does not add the transaction to the database.
	*
	* @param transactionId the primary key for the new transaction
	* @return the new transaction
	*/
	public Transaction create(java.lang.String transactionId);

	/**
	* Removes the transaction with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param transactionId the primary key of the transaction
	* @return the transaction that was removed
	* @throws NoSuchTransactionException if a transaction with the primary key could not be found
	*/
	public Transaction remove(java.lang.String transactionId)
		throws NoSuchTransactionException;

	public Transaction updateImpl(Transaction transaction);

	/**
	* Returns the transaction with the primary key or throws a {@link NoSuchTransactionException} if it could not be found.
	*
	* @param transactionId the primary key of the transaction
	* @return the transaction
	* @throws NoSuchTransactionException if a transaction with the primary key could not be found
	*/
	public Transaction findByPrimaryKey(java.lang.String transactionId)
		throws NoSuchTransactionException;

	/**
	* Returns the transaction with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param transactionId the primary key of the transaction
	* @return the transaction, or <code>null</code> if a transaction with the primary key could not be found
	*/
	public Transaction fetchByPrimaryKey(java.lang.String transactionId);

	@Override
	public java.util.Map<java.io.Serializable, Transaction> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the transactions.
	*
	* @return the transactions
	*/
	public java.util.List<Transaction> findAll();

	/**
	* Returns a range of all the transactions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @return the range of transactions
	*/
	public java.util.List<Transaction> findAll(int start, int end);

	/**
	* Returns an ordered range of all the transactions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of transactions
	*/
	public java.util.List<Transaction> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Transaction> orderByComparator);

	/**
	* Returns an ordered range of all the transactions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of transactions
	* @param end the upper bound of the range of transactions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of transactions
	*/
	public java.util.List<Transaction> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Transaction> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the transactions from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of transactions.
	*
	* @return the number of transactions
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}