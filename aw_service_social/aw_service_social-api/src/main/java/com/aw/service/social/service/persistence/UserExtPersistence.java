/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.exception.NoSuchUserExtException;
import com.aw.service.social.model.UserExt;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the user ext service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.aw.service.social.service.persistence.impl.UserExtPersistenceImpl
 * @see UserExtUtil
 * @generated
 */
@ProviderType
public interface UserExtPersistence extends BasePersistence<UserExt> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserExtUtil} to access the user ext persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the user ext in the entity cache if it is enabled.
	*
	* @param userExt the user ext
	*/
	public void cacheResult(UserExt userExt);

	/**
	* Caches the user exts in the entity cache if it is enabled.
	*
	* @param userExts the user exts
	*/
	public void cacheResult(java.util.List<UserExt> userExts);

	/**
	* Creates a new user ext with the primary key. Does not add the user ext to the database.
	*
	* @param userUid the primary key for the new user ext
	* @return the new user ext
	*/
	public UserExt create(java.lang.String userUid);

	/**
	* Removes the user ext with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userUid the primary key of the user ext
	* @return the user ext that was removed
	* @throws NoSuchUserExtException if a user ext with the primary key could not be found
	*/
	public UserExt remove(java.lang.String userUid)
		throws NoSuchUserExtException;

	public UserExt updateImpl(UserExt userExt);

	/**
	* Returns the user ext with the primary key or throws a {@link NoSuchUserExtException} if it could not be found.
	*
	* @param userUid the primary key of the user ext
	* @return the user ext
	* @throws NoSuchUserExtException if a user ext with the primary key could not be found
	*/
	public UserExt findByPrimaryKey(java.lang.String userUid)
		throws NoSuchUserExtException;

	/**
	* Returns the user ext with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userUid the primary key of the user ext
	* @return the user ext, or <code>null</code> if a user ext with the primary key could not be found
	*/
	public UserExt fetchByPrimaryKey(java.lang.String userUid);

	@Override
	public java.util.Map<java.io.Serializable, UserExt> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the user exts.
	*
	* @return the user exts
	*/
	public java.util.List<UserExt> findAll();

	/**
	* Returns a range of all the user exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user exts
	* @param end the upper bound of the range of user exts (not inclusive)
	* @return the range of user exts
	*/
	public java.util.List<UserExt> findAll(int start, int end);

	/**
	* Returns an ordered range of all the user exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user exts
	* @param end the upper bound of the range of user exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user exts
	*/
	public java.util.List<UserExt> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserExt> orderByComparator);

	/**
	* Returns an ordered range of all the user exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user exts
	* @param end the upper bound of the range of user exts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of user exts
	*/
	public java.util.List<UserExt> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserExt> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the user exts from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of user exts.
	*
	* @return the number of user exts
	*/
	public int countAll();
}