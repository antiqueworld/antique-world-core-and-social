/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ConvertRate. This utility wraps
 * {@link com.aw.service.social.service.impl.ConvertRateLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ConvertRateLocalService
 * @see com.aw.service.social.service.base.ConvertRateLocalServiceBaseImpl
 * @see com.aw.service.social.service.impl.ConvertRateLocalServiceImpl
 * @generated
 */
@ProviderType
public class ConvertRateLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.service.social.service.impl.ConvertRateLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the convert rate to the database. Also notifies the appropriate model listeners.
	*
	* @param convertRate the convert rate
	* @return the convert rate that was added
	*/
	public static com.aw.service.social.model.ConvertRate addConvertRate(
		com.aw.service.social.model.ConvertRate convertRate) {
		return getService().addConvertRate(convertRate);
	}

	/**
	* Creates a new convert rate with the primary key. Does not add the convert rate to the database.
	*
	* @param rateId the primary key for the new convert rate
	* @return the new convert rate
	*/
	public static com.aw.service.social.model.ConvertRate createConvertRate(
		java.lang.String rateId) {
		return getService().createConvertRate(rateId);
	}

	/**
	* Deletes the convert rate from the database. Also notifies the appropriate model listeners.
	*
	* @param convertRate the convert rate
	* @return the convert rate that was removed
	*/
	public static com.aw.service.social.model.ConvertRate deleteConvertRate(
		com.aw.service.social.model.ConvertRate convertRate) {
		return getService().deleteConvertRate(convertRate);
	}

	/**
	* Deletes the convert rate with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param rateId the primary key of the convert rate
	* @return the convert rate that was removed
	* @throws PortalException if a convert rate with the primary key could not be found
	*/
	public static com.aw.service.social.model.ConvertRate deleteConvertRate(
		java.lang.String rateId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteConvertRate(rateId);
	}

	public static com.aw.service.social.model.ConvertRate fetchConvertRate(
		java.lang.String rateId) {
		return getService().fetchConvertRate(rateId);
	}

	public static com.aw.service.social.model.ConvertRate getActiveRate(
		java.lang.String from, java.lang.String to) {
		return getService().getActiveRate(from, to);
	}

	/**
	* Returns the convert rate with the primary key.
	*
	* @param rateId the primary key of the convert rate
	* @return the convert rate
	* @throws PortalException if a convert rate with the primary key could not be found
	*/
	public static com.aw.service.social.model.ConvertRate getConvertRate(
		java.lang.String rateId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getConvertRate(rateId);
	}

	/**
	* Updates the convert rate in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param convertRate the convert rate
	* @return the convert rate that was updated
	*/
	public static com.aw.service.social.model.ConvertRate updateConvertRate(
		com.aw.service.social.model.ConvertRate convertRate) {
		return getService().updateConvertRate(convertRate);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.json.JSONObject allActiveRates(
		java.lang.String locale) {
		return getService().allActiveRates(locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject changeRate(
		java.lang.String from, java.lang.String to, double newRate,
		java.lang.String locale) {
		return getService().changeRate(from, to, newRate, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject getActiveRate(
		java.lang.String from, java.lang.String to, java.lang.String locale) {
		return getService().getActiveRate(from, to, locale);
	}

	public static com.liferay.portal.kernel.json.JSONObject getRates(
		java.lang.String from, java.lang.String to, java.lang.String locale) {
		return getService().getRates(from, to, locale);
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of convert rates.
	*
	* @return the number of convert rates
	*/
	public static int getConvertRatesCount() {
		return getService().getConvertRatesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the convert rates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of convert rates
	* @param end the upper bound of the range of convert rates (not inclusive)
	* @return the range of convert rates
	*/
	public static java.util.List<com.aw.service.social.model.ConvertRate> getConvertRates(
		int start, int end) {
		return getService().getConvertRates(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ConvertRateLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ConvertRateLocalService, ConvertRateLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ConvertRateLocalService.class);
}