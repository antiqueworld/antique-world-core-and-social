/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserExtLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see UserExtLocalService
 * @generated
 */
@ProviderType
public class UserExtLocalServiceWrapper implements UserExtLocalService,
	ServiceWrapper<UserExtLocalService> {
	public UserExtLocalServiceWrapper(UserExtLocalService userExtLocalService) {
		_userExtLocalService = userExtLocalService;
	}

	/**
	* Adds the user ext to the database. Also notifies the appropriate model listeners.
	*
	* @param userExt the user ext
	* @return the user ext that was added
	*/
	@Override
	public com.aw.service.social.model.UserExt addUserExt(
		com.aw.service.social.model.UserExt userExt) {
		return _userExtLocalService.addUserExt(userExt);
	}

	/**
	* Creates a new user ext with the primary key. Does not add the user ext to the database.
	*
	* @param userUid the primary key for the new user ext
	* @return the new user ext
	*/
	@Override
	public com.aw.service.social.model.UserExt createUserExt(
		java.lang.String userUid) {
		return _userExtLocalService.createUserExt(userUid);
	}

	/**
	* Deletes the user ext from the database. Also notifies the appropriate model listeners.
	*
	* @param userExt the user ext
	* @return the user ext that was removed
	*/
	@Override
	public com.aw.service.social.model.UserExt deleteUserExt(
		com.aw.service.social.model.UserExt userExt) {
		return _userExtLocalService.deleteUserExt(userExt);
	}

	/**
	* Deletes the user ext with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userUid the primary key of the user ext
	* @return the user ext that was removed
	* @throws PortalException if a user ext with the primary key could not be found
	*/
	@Override
	public com.aw.service.social.model.UserExt deleteUserExt(
		java.lang.String userUid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userExtLocalService.deleteUserExt(userUid);
	}

	@Override
	public com.aw.service.social.model.UserExt fetchUserExt(
		java.lang.String userUid) {
		return _userExtLocalService.fetchUserExt(userUid);
	}

	/**
	* Returns the user ext with the primary key.
	*
	* @param userUid the primary key of the user ext
	* @return the user ext
	* @throws PortalException if a user ext with the primary key could not be found
	*/
	@Override
	public com.aw.service.social.model.UserExt getUserExt(
		java.lang.String userUid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userExtLocalService.getUserExt(userUid);
	}

	/**
	* Updates the user ext in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userExt the user ext
	* @return the user ext that was updated
	*/
	@Override
	public com.aw.service.social.model.UserExt updateUserExt(
		com.aw.service.social.model.UserExt userExt) {
		return _userExtLocalService.updateUserExt(userExt);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _userExtLocalService.dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userExtLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userExtLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of user exts.
	*
	* @return the number of user exts
	*/
	@Override
	public int getUserExtsCount() {
		return _userExtLocalService.getUserExtsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _userExtLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _userExtLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.UserExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _userExtLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.UserExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _userExtLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the user exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.UserExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user exts
	* @param end the upper bound of the range of user exts (not inclusive)
	* @return the range of user exts
	*/
	@Override
	public java.util.List<com.aw.service.social.model.UserExt> getUserExts(
		int start, int end) {
		return _userExtLocalService.getUserExts(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _userExtLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _userExtLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public UserExtLocalService getWrappedService() {
		return _userExtLocalService;
	}

	@Override
	public void setWrappedService(UserExtLocalService userExtLocalService) {
		_userExtLocalService = userExtLocalService;
	}

	private UserExtLocalService _userExtLocalService;
}