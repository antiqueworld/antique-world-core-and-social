/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Transaction}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Transaction
 * @generated
 */
@ProviderType
public class TransactionWrapper implements Transaction,
	ModelWrapper<Transaction> {
	public TransactionWrapper(Transaction transaction) {
		_transaction = transaction;
	}

	@Override
	public Class<?> getModelClass() {
		return Transaction.class;
	}

	@Override
	public String getModelClassName() {
		return Transaction.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("transactionId", getTransactionId());
		attributes.put("orderId", getOrderId());
		attributes.put("userWalletAddr", getUserWalletAddr());
		attributes.put("type", getType());
		attributes.put("currency", getCurrency());
		attributes.put("amount", getAmount());
		attributes.put("status", getStatus());
		attributes.put("addTime", getAddTime());
		attributes.put("updateTime", getUpdateTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String transactionId = (String)attributes.get("transactionId");

		if (transactionId != null) {
			setTransactionId(transactionId);
		}

		String orderId = (String)attributes.get("orderId");

		if (orderId != null) {
			setOrderId(orderId);
		}

		String userWalletAddr = (String)attributes.get("userWalletAddr");

		if (userWalletAddr != null) {
			setUserWalletAddr(userWalletAddr);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String currency = (String)attributes.get("currency");

		if (currency != null) {
			setCurrency(currency);
		}

		Double amount = (Double)attributes.get("amount");

		if (amount != null) {
			setAmount(amount);
		}

		String status = (String)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}

		Date updateTime = (Date)attributes.get("updateTime");

		if (updateTime != null) {
			setUpdateTime(updateTime);
		}
	}

	@Override
	public Transaction toEscapedModel() {
		return new TransactionWrapper(_transaction.toEscapedModel());
	}

	@Override
	public Transaction toUnescapedModel() {
		return new TransactionWrapper(_transaction.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _transaction.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _transaction.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _transaction.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _transaction.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Transaction> toCacheModel() {
		return _transaction.toCacheModel();
	}

	/**
	* Returns the amount of this transaction.
	*
	* @return the amount of this transaction
	*/
	@Override
	public double getAmount() {
		return _transaction.getAmount();
	}

	@Override
	public int compareTo(Transaction transaction) {
		return _transaction.compareTo(transaction);
	}

	@Override
	public int hashCode() {
		return _transaction.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _transaction.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new TransactionWrapper((Transaction)_transaction.clone());
	}

	/**
	* Returns the currency of this transaction.
	*
	* @return the currency of this transaction
	*/
	@Override
	public java.lang.String getCurrency() {
		return _transaction.getCurrency();
	}

	/**
	* Returns the order ID of this transaction.
	*
	* @return the order ID of this transaction
	*/
	@Override
	public java.lang.String getOrderId() {
		return _transaction.getOrderId();
	}

	/**
	* Returns the primary key of this transaction.
	*
	* @return the primary key of this transaction
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _transaction.getPrimaryKey();
	}

	/**
	* Returns the status of this transaction.
	*
	* @return the status of this transaction
	*/
	@Override
	public java.lang.String getStatus() {
		return _transaction.getStatus();
	}

	/**
	* Returns the transaction ID of this transaction.
	*
	* @return the transaction ID of this transaction
	*/
	@Override
	public java.lang.String getTransactionId() {
		return _transaction.getTransactionId();
	}

	/**
	* Returns the type of this transaction.
	*
	* @return the type of this transaction
	*/
	@Override
	public java.lang.String getType() {
		return _transaction.getType();
	}

	/**
	* Returns the user wallet addr of this transaction.
	*
	* @return the user wallet addr of this transaction
	*/
	@Override
	public java.lang.String getUserWalletAddr() {
		return _transaction.getUserWalletAddr();
	}

	@Override
	public java.lang.String toString() {
		return _transaction.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _transaction.toXmlString();
	}

	/**
	* Returns the add time of this transaction.
	*
	* @return the add time of this transaction
	*/
	@Override
	public Date getAddTime() {
		return _transaction.getAddTime();
	}

	/**
	* Returns the update time of this transaction.
	*
	* @return the update time of this transaction
	*/
	@Override
	public Date getUpdateTime() {
		return _transaction.getUpdateTime();
	}

	@Override
	public void persist() {
		_transaction.persist();
	}

	/**
	* Sets the add time of this transaction.
	*
	* @param addTime the add time of this transaction
	*/
	@Override
	public void setAddTime(Date addTime) {
		_transaction.setAddTime(addTime);
	}

	/**
	* Sets the amount of this transaction.
	*
	* @param amount the amount of this transaction
	*/
	@Override
	public void setAmount(double amount) {
		_transaction.setAmount(amount);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_transaction.setCachedModel(cachedModel);
	}

	/**
	* Sets the currency of this transaction.
	*
	* @param currency the currency of this transaction
	*/
	@Override
	public void setCurrency(java.lang.String currency) {
		_transaction.setCurrency(currency);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_transaction.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_transaction.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_transaction.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_transaction.setNew(n);
	}

	/**
	* Sets the order ID of this transaction.
	*
	* @param orderId the order ID of this transaction
	*/
	@Override
	public void setOrderId(java.lang.String orderId) {
		_transaction.setOrderId(orderId);
	}

	/**
	* Sets the primary key of this transaction.
	*
	* @param primaryKey the primary key of this transaction
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_transaction.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_transaction.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the status of this transaction.
	*
	* @param status the status of this transaction
	*/
	@Override
	public void setStatus(java.lang.String status) {
		_transaction.setStatus(status);
	}

	/**
	* Sets the transaction ID of this transaction.
	*
	* @param transactionId the transaction ID of this transaction
	*/
	@Override
	public void setTransactionId(java.lang.String transactionId) {
		_transaction.setTransactionId(transactionId);
	}

	/**
	* Sets the type of this transaction.
	*
	* @param type the type of this transaction
	*/
	@Override
	public void setType(java.lang.String type) {
		_transaction.setType(type);
	}

	/**
	* Sets the update time of this transaction.
	*
	* @param updateTime the update time of this transaction
	*/
	@Override
	public void setUpdateTime(Date updateTime) {
		_transaction.setUpdateTime(updateTime);
	}

	/**
	* Sets the user wallet addr of this transaction.
	*
	* @param userWalletAddr the user wallet addr of this transaction
	*/
	@Override
	public void setUserWalletAddr(java.lang.String userWalletAddr) {
		_transaction.setUserWalletAddr(userWalletAddr);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TransactionWrapper)) {
			return false;
		}

		TransactionWrapper transactionWrapper = (TransactionWrapper)obj;

		if (Objects.equals(_transaction, transactionWrapper._transaction)) {
			return true;
		}

		return false;
	}

	@Override
	public Transaction getWrappedModel() {
		return _transaction;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _transaction.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _transaction.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_transaction.resetOriginalValues();
	}

	private final Transaction _transaction;
}