/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ContactExt. This utility wraps
 * {@link com.aw.service.social.service.impl.ContactExtLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ContactExtLocalService
 * @see com.aw.service.social.service.base.ContactExtLocalServiceBaseImpl
 * @see com.aw.service.social.service.impl.ContactExtLocalServiceImpl
 * @generated
 */
@ProviderType
public class ContactExtLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.service.social.service.impl.ContactExtLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the contact ext to the database. Also notifies the appropriate model listeners.
	*
	* @param contactExt the contact ext
	* @return the contact ext that was added
	*/
	public static com.aw.service.social.model.ContactExt addContactExt(
		com.aw.service.social.model.ContactExt contactExt) {
		return getService().addContactExt(contactExt);
	}

	/**
	* Creates a new contact ext with the primary key. Does not add the contact ext to the database.
	*
	* @param userUid the primary key for the new contact ext
	* @return the new contact ext
	*/
	public static com.aw.service.social.model.ContactExt createContactExt(
		java.lang.String userUid) {
		return getService().createContactExt(userUid);
	}

	/**
	* Deletes the contact ext from the database. Also notifies the appropriate model listeners.
	*
	* @param contactExt the contact ext
	* @return the contact ext that was removed
	*/
	public static com.aw.service.social.model.ContactExt deleteContactExt(
		com.aw.service.social.model.ContactExt contactExt) {
		return getService().deleteContactExt(contactExt);
	}

	/**
	* Deletes the contact ext with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userUid the primary key of the contact ext
	* @return the contact ext that was removed
	* @throws PortalException if a contact ext with the primary key could not be found
	*/
	public static com.aw.service.social.model.ContactExt deleteContactExt(
		java.lang.String userUid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteContactExt(userUid);
	}

	public static com.aw.service.social.model.ContactExt fetchContactExt(
		java.lang.String userUid) {
		return getService().fetchContactExt(userUid);
	}

	/**
	* Returns the contact ext with the primary key.
	*
	* @param userUid the primary key of the contact ext
	* @return the contact ext
	* @throws PortalException if a contact ext with the primary key could not be found
	*/
	public static com.aw.service.social.model.ContactExt getContactExt(
		java.lang.String userUid)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getContactExt(userUid);
	}

	/**
	* Updates the contact ext in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param contactExt the contact ext
	* @return the contact ext that was updated
	*/
	public static com.aw.service.social.model.ContactExt updateContactExt(
		com.aw.service.social.model.ContactExt contactExt) {
		return getService().updateContactExt(contactExt);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of contact exts.
	*
	* @return the number of contact exts
	*/
	public static int getContactExtsCount() {
		return getService().getContactExtsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the contact exts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.service.social.model.impl.ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact exts
	* @param end the upper bound of the range of contact exts (not inclusive)
	* @return the range of contact exts
	*/
	public static java.util.List<com.aw.service.social.model.ContactExt> getContactExts(
		int start, int end) {
		return getService().getContactExts(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ContactExtLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ContactExtLocalService, ContactExtLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ContactExtLocalService.class);
}