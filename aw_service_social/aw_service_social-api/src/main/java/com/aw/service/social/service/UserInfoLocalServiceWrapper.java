/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserInfoLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see UserInfoLocalService
 * @generated
 */
@ProviderType
public class UserInfoLocalServiceWrapper implements UserInfoLocalService,
	ServiceWrapper<UserInfoLocalService> {
	public UserInfoLocalServiceWrapper(
		UserInfoLocalService userInfoLocalService) {
		_userInfoLocalService = userInfoLocalService;
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getUserDetail(
		com.liferay.portal.kernel.model.User user, java.lang.String locale) {
		return _userInfoLocalService.getUserDetail(user, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateBasicUserInfo(
		com.liferay.portal.kernel.model.User user, java.lang.String birthday,
		java.lang.String firstName, java.lang.String middleName,
		java.lang.String lastName, java.lang.String title,
		java.lang.String selfDesc, java.lang.String secEmail,
		java.lang.String phone, java.lang.String extension,
		java.lang.String cellPhone, java.lang.String locale) {
		return _userInfoLocalService.updateBasicUserInfo(user, birthday,
			firstName, middleName, lastName, title, selfDesc, secEmail, phone,
			extension, cellPhone, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateIdImage(
		com.liferay.portal.kernel.model.User user, java.lang.String fileType,
		byte[] IdImage, java.lang.String isActive, java.lang.String publicFlag,
		java.lang.String locale) {
		return _userInfoLocalService.updateIdImage(user, fileType, IdImage,
			isActive, publicFlag, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateSensitiveInfo(
		com.liferay.portal.kernel.model.User user,
		java.lang.String IdIssueCountryId, java.lang.String IdType,
		java.lang.String IdNumber, java.lang.String ssn, java.lang.String locale) {
		return _userInfoLocalService.updateSensitiveInfo(user,
			IdIssueCountryId, IdType, IdNumber, ssn, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateUserAddressInfo(
		com.liferay.portal.kernel.model.User user,
		java.lang.String addressType, java.lang.String street1,
		java.lang.String street2, java.lang.String street3,
		java.lang.String city, java.lang.String regionId,
		java.lang.String countryId, java.lang.String zipCode,
		java.lang.String isMailing, java.lang.String locale) {
		return _userInfoLocalService.updateUserAddressInfo(user, addressType,
			street1, street2, street3, city, regionId, countryId, zipCode,
			isMailing, locale);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateUserPortrait(
		com.liferay.portal.kernel.model.User user, byte[] portrait,
		java.lang.String locale) {
		return _userInfoLocalService.updateUserPortrait(user, portrait, locale);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _userInfoLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public UserInfoLocalService getWrappedService() {
		return _userInfoLocalService;
	}

	@Override
	public void setWrappedService(UserInfoLocalService userInfoLocalService) {
		_userInfoLocalService = userInfoLocalService;
	}

	private UserInfoLocalService _userInfoLocalService;
}