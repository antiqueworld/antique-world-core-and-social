/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.liferay.portal.kernel.service.BaseService;
import com.liferay.portal.kernel.spring.osgi.OSGiBeanProperties;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * Provides the remote service interface for UserInfo. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see UserInfoServiceUtil
 * @see com.aw.service.social.service.base.UserInfoServiceBaseImpl
 * @see com.aw.service.social.service.impl.UserInfoServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@OSGiBeanProperties(property =  {
	"json.web.service.context.name=aw_social", "json.web.service.context.path=UserInfo"}, service = UserInfoService.class)
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface UserInfoService extends BaseService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserInfoServiceUtil} to access the user info remote service. Add custom service methods to {@link com.aw.service.social.service.impl.UserInfoServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getCountryList(java.lang.String locale);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getRegionList(java.lang.String countryId,
		java.lang.String locale);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getUserInfo(java.lang.String locale);

	public JSONObject updateBasicUserInfo(java.lang.String birthday,
		java.lang.String firstName, java.lang.String middleName,
		java.lang.String lastName, java.lang.String title,
		java.lang.String selfDesc, java.lang.String secEmail,
		java.lang.String phone, java.lang.String extension,
		java.lang.String cellPhone, java.lang.String locale);

	public JSONObject updateIdImage(java.lang.String fileType, byte[] IdImage,
		java.lang.String isActive, java.lang.String publicFlag,
		java.lang.String locale);

	public JSONObject updateSensitiveInfo(java.lang.String IdIssueCountryId,
		java.lang.String IdType, java.lang.String IdNumber,
		java.lang.String ssn, java.lang.String locale);

	public JSONObject updateUserAddressInfo(java.lang.String addressType,
		java.lang.String street1, java.lang.String street2,
		java.lang.String street3, java.lang.String city,
		java.lang.String regionId, java.lang.String countryId,
		java.lang.String zipCode, java.lang.String isMailing,
		java.lang.String locale);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getCountryName(long countryId);

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getRegionName(long regionId);
}