/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the UserExt service. Represents a row in the &quot;aw_social_UserExt&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see UserExtModel
 * @see com.aw.service.social.model.impl.UserExtImpl
 * @see com.aw.service.social.model.impl.UserExtModelImpl
 * @generated
 */
@ImplementationClassName("com.aw.service.social.model.impl.UserExtImpl")
@ProviderType
public interface UserExt extends UserExtModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link com.aw.service.social.model.impl.UserExtImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<UserExt, String> USER_UID_ACCESSOR = new Accessor<UserExt, String>() {
			@Override
			public String get(UserExt userExt) {
				return userExt.getUserUid();
			}

			@Override
			public Class<String> getAttributeClass() {
				return String.class;
			}

			@Override
			public Class<UserExt> getTypeClass() {
				return UserExt.class;
			}
		};
}