/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link UserExt}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserExt
 * @generated
 */
@ProviderType
public class UserExtWrapper implements UserExt, ModelWrapper<UserExt> {
	public UserExtWrapper(UserExt userExt) {
		_userExt = userExt;
	}

	@Override
	public Class<?> getModelClass() {
		return UserExt.class;
	}

	@Override
	public String getModelClassName() {
		return UserExt.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userUid", getUserUid());
		attributes.put("userId", getUserId());
		attributes.put("userInstance", getUserInstance());
		attributes.put("firstName", getFirstName());
		attributes.put("middleName", getMiddleName());
		attributes.put("lastName", getLastName());
		attributes.put("birthday", getBirthday());
		attributes.put("portraitId", getPortraitId());
		attributes.put("title", getTitle());
		attributes.put("selfDesc", getSelfDesc());
		attributes.put("ssn", getSsn());
		attributes.put("IdIssueCountry", getIdIssueCountry());
		attributes.put("IdType", getIdType());
		attributes.put("IdImageId", getIdImageId());
		attributes.put("IdNumber", getIdNumber());
		attributes.put("addTime", getAddTime());
		attributes.put("updateTime", getUpdateTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String userUid = (String)attributes.get("userUid");

		if (userUid != null) {
			setUserUid(userUid);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userInstance = (String)attributes.get("userInstance");

		if (userInstance != null) {
			setUserInstance(userInstance);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String middleName = (String)attributes.get("middleName");

		if (middleName != null) {
			setMiddleName(middleName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		String birthday = (String)attributes.get("birthday");

		if (birthday != null) {
			setBirthday(birthday);
		}

		String portraitId = (String)attributes.get("portraitId");

		if (portraitId != null) {
			setPortraitId(portraitId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String selfDesc = (String)attributes.get("selfDesc");

		if (selfDesc != null) {
			setSelfDesc(selfDesc);
		}

		String ssn = (String)attributes.get("ssn");

		if (ssn != null) {
			setSsn(ssn);
		}

		String IdIssueCountry = (String)attributes.get("IdIssueCountry");

		if (IdIssueCountry != null) {
			setIdIssueCountry(IdIssueCountry);
		}

		String IdType = (String)attributes.get("IdType");

		if (IdType != null) {
			setIdType(IdType);
		}

		String IdImageId = (String)attributes.get("IdImageId");

		if (IdImageId != null) {
			setIdImageId(IdImageId);
		}

		String IdNumber = (String)attributes.get("IdNumber");

		if (IdNumber != null) {
			setIdNumber(IdNumber);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}

		Date updateTime = (Date)attributes.get("updateTime");

		if (updateTime != null) {
			setUpdateTime(updateTime);
		}
	}

	@Override
	public UserExt toEscapedModel() {
		return new UserExtWrapper(_userExt.toEscapedModel());
	}

	@Override
	public UserExt toUnescapedModel() {
		return new UserExtWrapper(_userExt.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _userExt.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _userExt.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _userExt.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _userExt.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<UserExt> toCacheModel() {
		return _userExt.toCacheModel();
	}

	@Override
	public int compareTo(UserExt userExt) {
		return _userExt.compareTo(userExt);
	}

	@Override
	public int hashCode() {
		return _userExt.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _userExt.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new UserExtWrapper((UserExt)_userExt.clone());
	}

	/**
	* Returns the birthday of this user ext.
	*
	* @return the birthday of this user ext
	*/
	@Override
	public java.lang.String getBirthday() {
		return _userExt.getBirthday();
	}

	/**
	* Returns the first name of this user ext.
	*
	* @return the first name of this user ext
	*/
	@Override
	public java.lang.String getFirstName() {
		return _userExt.getFirstName();
	}

	/**
	* Returns the id image ID of this user ext.
	*
	* @return the id image ID of this user ext
	*/
	@Override
	public java.lang.String getIdImageId() {
		return _userExt.getIdImageId();
	}

	/**
	* Returns the id issue country of this user ext.
	*
	* @return the id issue country of this user ext
	*/
	@Override
	public java.lang.String getIdIssueCountry() {
		return _userExt.getIdIssueCountry();
	}

	/**
	* Returns the id number of this user ext.
	*
	* @return the id number of this user ext
	*/
	@Override
	public java.lang.String getIdNumber() {
		return _userExt.getIdNumber();
	}

	/**
	* Returns the id type of this user ext.
	*
	* @return the id type of this user ext
	*/
	@Override
	public java.lang.String getIdType() {
		return _userExt.getIdType();
	}

	/**
	* Returns the last name of this user ext.
	*
	* @return the last name of this user ext
	*/
	@Override
	public java.lang.String getLastName() {
		return _userExt.getLastName();
	}

	/**
	* Returns the middle name of this user ext.
	*
	* @return the middle name of this user ext
	*/
	@Override
	public java.lang.String getMiddleName() {
		return _userExt.getMiddleName();
	}

	/**
	* Returns the portrait ID of this user ext.
	*
	* @return the portrait ID of this user ext
	*/
	@Override
	public java.lang.String getPortraitId() {
		return _userExt.getPortraitId();
	}

	/**
	* Returns the primary key of this user ext.
	*
	* @return the primary key of this user ext
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _userExt.getPrimaryKey();
	}

	/**
	* Returns the self desc of this user ext.
	*
	* @return the self desc of this user ext
	*/
	@Override
	public java.lang.String getSelfDesc() {
		return _userExt.getSelfDesc();
	}

	/**
	* Returns the ssn of this user ext.
	*
	* @return the ssn of this user ext
	*/
	@Override
	public java.lang.String getSsn() {
		return _userExt.getSsn();
	}

	/**
	* Returns the title of this user ext.
	*
	* @return the title of this user ext
	*/
	@Override
	public java.lang.String getTitle() {
		return _userExt.getTitle();
	}

	/**
	* Returns the user instance of this user ext.
	*
	* @return the user instance of this user ext
	*/
	@Override
	public java.lang.String getUserInstance() {
		return _userExt.getUserInstance();
	}

	/**
	* Returns the user uid of this user ext.
	*
	* @return the user uid of this user ext
	*/
	@Override
	public java.lang.String getUserUid() {
		return _userExt.getUserUid();
	}

	/**
	* Returns the user uuid of this user ext.
	*
	* @return the user uuid of this user ext
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _userExt.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _userExt.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userExt.toXmlString();
	}

	/**
	* Returns the add time of this user ext.
	*
	* @return the add time of this user ext
	*/
	@Override
	public Date getAddTime() {
		return _userExt.getAddTime();
	}

	/**
	* Returns the update time of this user ext.
	*
	* @return the update time of this user ext
	*/
	@Override
	public Date getUpdateTime() {
		return _userExt.getUpdateTime();
	}

	/**
	* Returns the user ID of this user ext.
	*
	* @return the user ID of this user ext
	*/
	@Override
	public long getUserId() {
		return _userExt.getUserId();
	}

	@Override
	public void persist() {
		_userExt.persist();
	}

	/**
	* Sets the add time of this user ext.
	*
	* @param addTime the add time of this user ext
	*/
	@Override
	public void setAddTime(Date addTime) {
		_userExt.setAddTime(addTime);
	}

	/**
	* Sets the birthday of this user ext.
	*
	* @param birthday the birthday of this user ext
	*/
	@Override
	public void setBirthday(java.lang.String birthday) {
		_userExt.setBirthday(birthday);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userExt.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_userExt.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_userExt.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_userExt.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the first name of this user ext.
	*
	* @param firstName the first name of this user ext
	*/
	@Override
	public void setFirstName(java.lang.String firstName) {
		_userExt.setFirstName(firstName);
	}

	/**
	* Sets the id image ID of this user ext.
	*
	* @param IdImageId the id image ID of this user ext
	*/
	@Override
	public void setIdImageId(java.lang.String IdImageId) {
		_userExt.setIdImageId(IdImageId);
	}

	/**
	* Sets the id issue country of this user ext.
	*
	* @param IdIssueCountry the id issue country of this user ext
	*/
	@Override
	public void setIdIssueCountry(java.lang.String IdIssueCountry) {
		_userExt.setIdIssueCountry(IdIssueCountry);
	}

	/**
	* Sets the id number of this user ext.
	*
	* @param IdNumber the id number of this user ext
	*/
	@Override
	public void setIdNumber(java.lang.String IdNumber) {
		_userExt.setIdNumber(IdNumber);
	}

	/**
	* Sets the id type of this user ext.
	*
	* @param IdType the id type of this user ext
	*/
	@Override
	public void setIdType(java.lang.String IdType) {
		_userExt.setIdType(IdType);
	}

	/**
	* Sets the last name of this user ext.
	*
	* @param lastName the last name of this user ext
	*/
	@Override
	public void setLastName(java.lang.String lastName) {
		_userExt.setLastName(lastName);
	}

	/**
	* Sets the middle name of this user ext.
	*
	* @param middleName the middle name of this user ext
	*/
	@Override
	public void setMiddleName(java.lang.String middleName) {
		_userExt.setMiddleName(middleName);
	}

	@Override
	public void setNew(boolean n) {
		_userExt.setNew(n);
	}

	/**
	* Sets the portrait ID of this user ext.
	*
	* @param portraitId the portrait ID of this user ext
	*/
	@Override
	public void setPortraitId(java.lang.String portraitId) {
		_userExt.setPortraitId(portraitId);
	}

	/**
	* Sets the primary key of this user ext.
	*
	* @param primaryKey the primary key of this user ext
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_userExt.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_userExt.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the self desc of this user ext.
	*
	* @param selfDesc the self desc of this user ext
	*/
	@Override
	public void setSelfDesc(java.lang.String selfDesc) {
		_userExt.setSelfDesc(selfDesc);
	}

	/**
	* Sets the ssn of this user ext.
	*
	* @param ssn the ssn of this user ext
	*/
	@Override
	public void setSsn(java.lang.String ssn) {
		_userExt.setSsn(ssn);
	}

	/**
	* Sets the title of this user ext.
	*
	* @param title the title of this user ext
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_userExt.setTitle(title);
	}

	/**
	* Sets the update time of this user ext.
	*
	* @param updateTime the update time of this user ext
	*/
	@Override
	public void setUpdateTime(Date updateTime) {
		_userExt.setUpdateTime(updateTime);
	}

	/**
	* Sets the user ID of this user ext.
	*
	* @param userId the user ID of this user ext
	*/
	@Override
	public void setUserId(long userId) {
		_userExt.setUserId(userId);
	}

	/**
	* Sets the user instance of this user ext.
	*
	* @param userInstance the user instance of this user ext
	*/
	@Override
	public void setUserInstance(java.lang.String userInstance) {
		_userExt.setUserInstance(userInstance);
	}

	/**
	* Sets the user uid of this user ext.
	*
	* @param userUid the user uid of this user ext
	*/
	@Override
	public void setUserUid(java.lang.String userUid) {
		_userExt.setUserUid(userUid);
	}

	/**
	* Sets the user uuid of this user ext.
	*
	* @param userUuid the user uuid of this user ext
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_userExt.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserExtWrapper)) {
			return false;
		}

		UserExtWrapper userExtWrapper = (UserExtWrapper)obj;

		if (Objects.equals(_userExt, userExtWrapper._userExt)) {
			return true;
		}

		return false;
	}

	@Override
	public UserExt getWrappedModel() {
		return _userExt;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _userExt.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _userExt.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_userExt.resetOriginalValues();
	}

	private final UserExt _userExt;
}