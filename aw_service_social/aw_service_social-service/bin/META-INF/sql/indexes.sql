create index IX_5643D5B9 on aw_social_AddressExt (userUid[$COLUMN_LENGTH:75$]);

create index IX_56C248DF on aw_social_ConvertRate (fromCurrency[$COLUMN_LENGTH:75$], toCurrency[$COLUMN_LENGTH:75$]);
create index IX_2498AEF4 on aw_social_ConvertRate (isActive[$COLUMN_LENGTH:75$]);

create index IX_2A029A3E on aw_social_Transaction (orderId[$COLUMN_LENGTH:75$]);