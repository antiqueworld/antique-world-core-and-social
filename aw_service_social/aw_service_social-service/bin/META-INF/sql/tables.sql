create table aw_social_AddressExt (
	userUid VARCHAR(75) not null,
	addressType VARCHAR(75) not null,
	street1 VARCHAR(75) null,
	street2 VARCHAR(75) null,
	street3 VARCHAR(75) null,
	city VARCHAR(75) null,
	regionId VARCHAR(75) null,
	countryId VARCHAR(75) null,
	zipCode VARCHAR(75) null,
	isMailing VARCHAR(75) null,
	primary key (userUid, addressType)
);

create table aw_social_ContactExt (
	userUid VARCHAR(75) not null primary key,
	primEmail VARCHAR(75) null,
	secEmail VARCHAR(75) null,
	phone VARCHAR(75) null,
	extension VARCHAR(75) null,
	cellPhone VARCHAR(75) null,
	facebook VARCHAR(75) null,
	wechat VARCHAR(75) null,
	twitter VARCHAR(75) null,
	weibo VARCHAR(75) null,
	addTime DATE null,
	updateTime DATE null
);

create table aw_social_ConvertRate (
	rateId VARCHAR(75) not null primary key,
	fromCurrency VARCHAR(75) null,
	toCurrency VARCHAR(75) null,
	isActive VARCHAR(75) null,
	rate DOUBLE,
	startTime DATE null,
	expireTime DATE null
);

create table aw_social_Item (
	itemId VARCHAR(75) not null primary key,
	ownerUid VARCHAR(75) null,
	erc20Addr VARCHAR(75) null,
	tokenCurrency VARCHAR(75) null,
	erc721Addr VARCHAR(75) null,
	title VARCHAR(75) null,
	description VARCHAR(75) null,
	addTime DATE null,
	updateTime DATE null
);

create table aw_social_MediaStore (
	id_ VARCHAR(75) not null primary key,
	content BLOB,
	isActive VARCHAR(75) null,
	ownerUid VARCHAR(75) null,
	publicFlag VARCHAR(75) null,
	fileType VARCHAR(75) null,
	fileExt VARCHAR(75) null,
	fileSize VARCHAR(75) null,
	addTime DATE null,
	updateTime DATE null
);

create table aw_social_Order (
	OrderId VARCHAR(75) not null primary key,
	userUid VARCHAR(75) null,
	userWalletAddr VARCHAR(75) null,
	fromType VARCHAR(75) null,
	fromCurrency VARCHAR(75) null,
	fromAmount DOUBLE,
	rateId VARCHAR(75) null,
	toCurrency VARCHAR(75) null,
	toAmount DOUBLE,
	chargedFlag VARCHAR(75) null,
	sentFlag VARCHAR(75) null,
	completedFlag VARCHAR(75) null,
	addTime DATE null,
	updateTime DATE null
);

create table aw_social_Transaction (
	transactionId VARCHAR(75) not null primary key,
	orderId VARCHAR(75) null,
	userWalletAddr VARCHAR(75) null,
	type_ VARCHAR(75) null,
	currency_ VARCHAR(75) null,
	amount DOUBLE,
	status VARCHAR(75) null,
	addTime DATE null,
	updateTime DATE null
);

create table aw_social_UserExt (
	userUid VARCHAR(75) not null primary key,
	userId LONG,
	userInstance VARCHAR(75) null,
	firstName VARCHAR(75) null,
	middleName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	birthday VARCHAR(75) null,
	portraitId VARCHAR(75) null,
	title VARCHAR(75) null,
	selfDesc VARCHAR(75) null,
	ssn VARCHAR(75) null,
	IdIssueCountry VARCHAR(75) null,
	IdType VARCHAR(75) null,
	IdImageId VARCHAR(75) null,
	IdNumber VARCHAR(75) null,
	addTime DATE null,
	updateTime DATE null
);