/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.impl;

import com.aw.service.social.service.base.UserInfoLocalServiceBaseImpl;
import com.aw.service.social.service.util.UserGetUtil;
import com.aw.service.social.service.util.UserUpdateUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;

/**
 * The implementation of the user info local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.service.social.service.UserInfoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserInfoLocalServiceBaseImpl
 * @see com.aw.service.social.service.UserInfoLocalServiceUtil
 */
public class UserInfoLocalServiceImpl extends UserInfoLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.service.social.service.UserInfoLocalServiceUtil} to access the user info local service.
	 */
	public JSONObject getUserDetail(User user, String locale) {
		return new UserGetUtil().getUserDetail(user, locale);
	}
	
//	public JSONObject getMediaDetail(String mediaUid, String locale){
//		return new UserGetUtil().getUserMedia(mediaUid, locale);
//	}
	
	public JSONObject updateBasicUserInfo(User user, String birthday, String firstName, String middleName,
			String lastName, String title, String selfDesc, String secEmail, String phone, String extension, String cellPhone, String locale) {
	
		return new UserUpdateUtil().updateBasicUserInfo(user, birthday, firstName, middleName, lastName, title,
				selfDesc, secEmail, phone, extension, cellPhone, locale);
	
	}
	
	public JSONObject updateUserPortrait(User user, byte[] portrait, String locale) {
		return new UserUpdateUtil().updateUserPortrait(user, portrait, locale);
	}
	
	public JSONObject updateUserAddressInfo(User user, String addressType, String street1, String street2,
			String street3, String city, String regionId, String countryId, String zipCode, String isMailing,
			String locale) {
	
		return new UserUpdateUtil().updateUserAddressInfo(user, addressType, street1, street2, street3, city, regionId,
				countryId, zipCode, isMailing, locale);
	}
	
	public JSONObject updateSensitiveInfo(User user, String IdIssueCountryId, String IdType, String IdNumber,
			String ssn, String locale) {
	
		return new UserUpdateUtil().updateSensitiveInfo(user, IdIssueCountryId, IdType, IdNumber, ssn, locale);
	
	}
	
	public JSONObject updateIdImage(User user, String fileType, byte[] IdImage, String isActive, String publicFlag,
			String locale) {
		return new UserUpdateUtil().updateIdImage(user, fileType, IdImage, isActive, publicFlag, locale);
	}
}