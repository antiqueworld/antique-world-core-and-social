/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.impl;

import com.aw.common.util.service.CommonUtilLocalServiceUtil;
import com.aw.common.util.service.ErrorCodeLocalServiceUtil;
import com.aw.service.social.service.base.UserInfoServiceBaseImpl;
import com.aw.service.social.service.UserInfoLocalServiceUtil;
import com.aw.service.social.service.util.UserGetUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.persistence.CountryUtil;
import com.liferay.portal.kernel.service.persistence.RegionUtil;

/**
 * The implementation of the user info remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.service.social.service.UserInfoService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserInfoServiceBaseImpl
 * @see com.aw.service.social.service.UserInfoServiceUtil
 */
public class UserInfoServiceImpl extends UserInfoServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.service.social.service.UserInfoServiceUtil} to access the user info remote service.
	 */
	public JSONObject getUserInfo(String locale) {

		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
		return UserInfoLocalServiceUtil.getUserDetail(user, locale);
	}
//	public JSONObject getUserMedia(String mediaUid, String locale){
//		return UserInfoLocalServiceUtil.getMediaDetail(mediaUid, locale);
//	}

	public JSONObject updateBasicUserInfo(String birthday, String firstName, String middleName, String lastName,
			String title, String selfDesc, String secEmail, String phone, String extension, String cellPhone, String locale) {

		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();

		if (user != null) {
			// byte[] bytes = Base64.decodeBase64(potrait);
			return UserInfoLocalServiceUtil.updateBasicUserInfo(user, birthday, firstName, middleName, lastName, title,
					selfDesc, secEmail, phone, extension, cellPhone, locale);
		}

		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);

	}

	public JSONObject updateUserAddressInfo(String addressType, String street1, String street2, String street3,
			String city, String regionId, String countryId, String zipCode, String isMailing, String locale) {

		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();

		if (user != null) {
			return UserInfoLocalServiceUtil.updateUserAddressInfo(user, addressType, street1, street2, street3, city,
					regionId, countryId, zipCode, isMailing, locale);
		}

		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);

	}

	public JSONObject updateSensitiveInfo(String IdIssueCountryId, String IdType, String IdNumber, String ssn,
			String locale) {

		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();

		if (user != null) {
			// byte[] bytes = Base64.decodeBase64(IdImage);
			return UserInfoLocalServiceUtil.updateSensitiveInfo(user, IdIssueCountryId, IdType, IdNumber, ssn, locale);
		}

		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);

	}

//	public JSONObject updateUserPortrait(String fileType, byte[] portrait, String isActive, String publicFlag,
//			String locale) {
//		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
//		User user = pc.getUser();
//
//		if (user != null) {
//			// byte[] bytes = Base64.decodeBase64(IdImage);
//			return UserInfoLocalServiceUtil.updateUserPortrait(user, fileType, portrait, isActive, publicFlag, locale);
//		}
//		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
//	}

	public JSONObject updateIdImage(String fileType, byte[] IdImage, String isActive, String publicFlag,
			String locale) {
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();

		if (user != null) {
			// byte[] bytes = Base64.decodeBase64(IdImage);
			return UserInfoLocalServiceUtil.updateIdImage(user, fileType, IdImage, isActive, publicFlag, locale);
		}
		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
	}
	
	public String getCountryName(long countryId){
		return CountryUtil.fetchByPrimaryKey(countryId).getName();
	}
	
	public String getRegionName(long regionId){
		return RegionUtil.fetchByPrimaryKey(regionId).getName();
	}

	public JSONObject getCountryList(String locale) {
		return new UserGetUtil().getCountryList(locale);
	}

	public JSONObject getRegionList(String countryId, String locale) {
		return new UserGetUtil().getRegionList(countryId, locale);
	}
}