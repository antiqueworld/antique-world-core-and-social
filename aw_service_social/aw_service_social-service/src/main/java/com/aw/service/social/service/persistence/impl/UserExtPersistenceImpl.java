/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.exception.NoSuchUserExtException;
import com.aw.service.social.model.UserExt;
import com.aw.service.social.model.impl.UserExtImpl;
import com.aw.service.social.model.impl.UserExtModelImpl;
import com.aw.service.social.service.persistence.UserExtPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the user ext service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserExtPersistence
 * @see com.aw.service.social.service.persistence.UserExtUtil
 * @generated
 */
@ProviderType
public class UserExtPersistenceImpl extends BasePersistenceImpl<UserExt>
	implements UserExtPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserExtUtil} to access the user ext persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserExtImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserExtModelImpl.ENTITY_CACHE_ENABLED,
			UserExtModelImpl.FINDER_CACHE_ENABLED, UserExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserExtModelImpl.ENTITY_CACHE_ENABLED,
			UserExtModelImpl.FINDER_CACHE_ENABLED, UserExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserExtModelImpl.ENTITY_CACHE_ENABLED,
			UserExtModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public UserExtPersistenceImpl() {
		setModelClass(UserExt.class);
	}

	/**
	 * Caches the user ext in the entity cache if it is enabled.
	 *
	 * @param userExt the user ext
	 */
	@Override
	public void cacheResult(UserExt userExt) {
		entityCache.putResult(UserExtModelImpl.ENTITY_CACHE_ENABLED,
			UserExtImpl.class, userExt.getPrimaryKey(), userExt);

		userExt.resetOriginalValues();
	}

	/**
	 * Caches the user exts in the entity cache if it is enabled.
	 *
	 * @param userExts the user exts
	 */
	@Override
	public void cacheResult(List<UserExt> userExts) {
		for (UserExt userExt : userExts) {
			if (entityCache.getResult(UserExtModelImpl.ENTITY_CACHE_ENABLED,
						UserExtImpl.class, userExt.getPrimaryKey()) == null) {
				cacheResult(userExt);
			}
			else {
				userExt.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user exts.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(UserExtImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user ext.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserExt userExt) {
		entityCache.removeResult(UserExtModelImpl.ENTITY_CACHE_ENABLED,
			UserExtImpl.class, userExt.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserExt> userExts) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserExt userExt : userExts) {
			entityCache.removeResult(UserExtModelImpl.ENTITY_CACHE_ENABLED,
				UserExtImpl.class, userExt.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user ext with the primary key. Does not add the user ext to the database.
	 *
	 * @param userUid the primary key for the new user ext
	 * @return the new user ext
	 */
	@Override
	public UserExt create(String userUid) {
		UserExt userExt = new UserExtImpl();

		userExt.setNew(true);
		userExt.setPrimaryKey(userUid);

		return userExt;
	}

	/**
	 * Removes the user ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userUid the primary key of the user ext
	 * @return the user ext that was removed
	 * @throws NoSuchUserExtException if a user ext with the primary key could not be found
	 */
	@Override
	public UserExt remove(String userUid) throws NoSuchUserExtException {
		return remove((Serializable)userUid);
	}

	/**
	 * Removes the user ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user ext
	 * @return the user ext that was removed
	 * @throws NoSuchUserExtException if a user ext with the primary key could not be found
	 */
	@Override
	public UserExt remove(Serializable primaryKey)
		throws NoSuchUserExtException {
		Session session = null;

		try {
			session = openSession();

			UserExt userExt = (UserExt)session.get(UserExtImpl.class, primaryKey);

			if (userExt == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserExtException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userExt);
		}
		catch (NoSuchUserExtException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserExt removeImpl(UserExt userExt) {
		userExt = toUnwrappedModel(userExt);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userExt)) {
				userExt = (UserExt)session.get(UserExtImpl.class,
						userExt.getPrimaryKeyObj());
			}

			if (userExt != null) {
				session.delete(userExt);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userExt != null) {
			clearCache(userExt);
		}

		return userExt;
	}

	@Override
	public UserExt updateImpl(UserExt userExt) {
		userExt = toUnwrappedModel(userExt);

		boolean isNew = userExt.isNew();

		Session session = null;

		try {
			session = openSession();

			if (userExt.isNew()) {
				session.save(userExt);

				userExt.setNew(false);
			}
			else {
				userExt = (UserExt)session.merge(userExt);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(UserExtModelImpl.ENTITY_CACHE_ENABLED,
			UserExtImpl.class, userExt.getPrimaryKey(), userExt, false);

		userExt.resetOriginalValues();

		return userExt;
	}

	protected UserExt toUnwrappedModel(UserExt userExt) {
		if (userExt instanceof UserExtImpl) {
			return userExt;
		}

		UserExtImpl userExtImpl = new UserExtImpl();

		userExtImpl.setNew(userExt.isNew());
		userExtImpl.setPrimaryKey(userExt.getPrimaryKey());

		userExtImpl.setUserUid(userExt.getUserUid());
		userExtImpl.setUserId(userExt.getUserId());
		userExtImpl.setUserInstance(userExt.getUserInstance());
		userExtImpl.setFirstName(userExt.getFirstName());
		userExtImpl.setMiddleName(userExt.getMiddleName());
		userExtImpl.setLastName(userExt.getLastName());
		userExtImpl.setBirthday(userExt.getBirthday());
		userExtImpl.setPortraitId(userExt.getPortraitId());
		userExtImpl.setTitle(userExt.getTitle());
		userExtImpl.setSelfDesc(userExt.getSelfDesc());
		userExtImpl.setSsn(userExt.getSsn());
		userExtImpl.setIdIssueCountry(userExt.getIdIssueCountry());
		userExtImpl.setIdType(userExt.getIdType());
		userExtImpl.setIdImageId(userExt.getIdImageId());
		userExtImpl.setIdNumber(userExt.getIdNumber());
		userExtImpl.setAddTime(userExt.getAddTime());
		userExtImpl.setUpdateTime(userExt.getUpdateTime());

		return userExtImpl;
	}

	/**
	 * Returns the user ext with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user ext
	 * @return the user ext
	 * @throws NoSuchUserExtException if a user ext with the primary key could not be found
	 */
	@Override
	public UserExt findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserExtException {
		UserExt userExt = fetchByPrimaryKey(primaryKey);

		if (userExt == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserExtException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userExt;
	}

	/**
	 * Returns the user ext with the primary key or throws a {@link NoSuchUserExtException} if it could not be found.
	 *
	 * @param userUid the primary key of the user ext
	 * @return the user ext
	 * @throws NoSuchUserExtException if a user ext with the primary key could not be found
	 */
	@Override
	public UserExt findByPrimaryKey(String userUid)
		throws NoSuchUserExtException {
		return findByPrimaryKey((Serializable)userUid);
	}

	/**
	 * Returns the user ext with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user ext
	 * @return the user ext, or <code>null</code> if a user ext with the primary key could not be found
	 */
	@Override
	public UserExt fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(UserExtModelImpl.ENTITY_CACHE_ENABLED,
				UserExtImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		UserExt userExt = (UserExt)serializable;

		if (userExt == null) {
			Session session = null;

			try {
				session = openSession();

				userExt = (UserExt)session.get(UserExtImpl.class, primaryKey);

				if (userExt != null) {
					cacheResult(userExt);
				}
				else {
					entityCache.putResult(UserExtModelImpl.ENTITY_CACHE_ENABLED,
						UserExtImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(UserExtModelImpl.ENTITY_CACHE_ENABLED,
					UserExtImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userExt;
	}

	/**
	 * Returns the user ext with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userUid the primary key of the user ext
	 * @return the user ext, or <code>null</code> if a user ext with the primary key could not be found
	 */
	@Override
	public UserExt fetchByPrimaryKey(String userUid) {
		return fetchByPrimaryKey((Serializable)userUid);
	}

	@Override
	public Map<Serializable, UserExt> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, UserExt> map = new HashMap<Serializable, UserExt>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			UserExt userExt = fetchByPrimaryKey(primaryKey);

			if (userExt != null) {
				map.put(primaryKey, userExt);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(UserExtModelImpl.ENTITY_CACHE_ENABLED,
					UserExtImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (UserExt)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_USEREXT_WHERE_PKS_IN);

		for (int i = 0; i < uncachedPrimaryKeys.size(); i++) {
			query.append(StringPool.QUESTION);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			QueryPos qPos = QueryPos.getInstance(q);

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				qPos.add((String)primaryKey);
			}

			for (UserExt userExt : (List<UserExt>)q.list()) {
				map.put(userExt.getPrimaryKeyObj(), userExt);

				cacheResult(userExt);

				uncachedPrimaryKeys.remove(userExt.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(UserExtModelImpl.ENTITY_CACHE_ENABLED,
					UserExtImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the user exts.
	 *
	 * @return the user exts
	 */
	@Override
	public List<UserExt> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user exts
	 * @param end the upper bound of the range of user exts (not inclusive)
	 * @return the range of user exts
	 */
	@Override
	public List<UserExt> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user exts
	 * @param end the upper bound of the range of user exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user exts
	 */
	@Override
	public List<UserExt> findAll(int start, int end,
		OrderByComparator<UserExt> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user exts
	 * @param end the upper bound of the range of user exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of user exts
	 */
	@Override
	public List<UserExt> findAll(int start, int end,
		OrderByComparator<UserExt> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserExt> list = null;

		if (retrieveFromCache) {
			list = (List<UserExt>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_USEREXT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USEREXT;

				if (pagination) {
					sql = sql.concat(UserExtModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserExt>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserExt>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user exts from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (UserExt userExt : findAll()) {
			remove(userExt);
		}
	}

	/**
	 * Returns the number of user exts.
	 *
	 * @return the number of user exts
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USEREXT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return UserExtModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the user ext persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(UserExtImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_USEREXT = "SELECT userExt FROM UserExt userExt";
	private static final String _SQL_SELECT_USEREXT_WHERE_PKS_IN = "SELECT userExt FROM UserExt userExt WHERE userUid IN (";
	private static final String _SQL_COUNT_USEREXT = "SELECT COUNT(userExt) FROM UserExt userExt";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userExt.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserExt exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(UserExtPersistenceImpl.class);
}