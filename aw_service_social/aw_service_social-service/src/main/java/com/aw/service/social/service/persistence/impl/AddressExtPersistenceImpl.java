/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.exception.NoSuchAddressExtException;
import com.aw.service.social.model.AddressExt;
import com.aw.service.social.model.impl.AddressExtImpl;
import com.aw.service.social.model.impl.AddressExtModelImpl;
import com.aw.service.social.service.persistence.AddressExtPK;
import com.aw.service.social.service.persistence.AddressExtPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the address ext service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AddressExtPersistence
 * @see com.aw.service.social.service.persistence.AddressExtUtil
 * @generated
 */
@ProviderType
public class AddressExtPersistenceImpl extends BasePersistenceImpl<AddressExt>
	implements AddressExtPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AddressExtUtil} to access the address ext persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AddressExtImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
			AddressExtModelImpl.FINDER_CACHE_ENABLED, AddressExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
			AddressExtModelImpl.FINDER_CACHE_ENABLED, AddressExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
			AddressExtModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USER = new FinderPath(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
			AddressExtModelImpl.FINDER_CACHE_ENABLED, AddressExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuser",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER = new FinderPath(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
			AddressExtModelImpl.FINDER_CACHE_ENABLED, AddressExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuser",
			new String[] { String.class.getName() },
			AddressExtModelImpl.USERUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USER = new FinderPath(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
			AddressExtModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuser",
			new String[] { String.class.getName() });

	/**
	 * Returns all the address exts where userUid = &#63;.
	 *
	 * @param userUid the user uid
	 * @return the matching address exts
	 */
	@Override
	public List<AddressExt> findByuser(String userUid) {
		return findByuser(userUid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the address exts where userUid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userUid the user uid
	 * @param start the lower bound of the range of address exts
	 * @param end the upper bound of the range of address exts (not inclusive)
	 * @return the range of matching address exts
	 */
	@Override
	public List<AddressExt> findByuser(String userUid, int start, int end) {
		return findByuser(userUid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the address exts where userUid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userUid the user uid
	 * @param start the lower bound of the range of address exts
	 * @param end the upper bound of the range of address exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching address exts
	 */
	@Override
	public List<AddressExt> findByuser(String userUid, int start, int end,
		OrderByComparator<AddressExt> orderByComparator) {
		return findByuser(userUid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the address exts where userUid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userUid the user uid
	 * @param start the lower bound of the range of address exts
	 * @param end the upper bound of the range of address exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching address exts
	 */
	@Override
	public List<AddressExt> findByuser(String userUid, int start, int end,
		OrderByComparator<AddressExt> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER;
			finderArgs = new Object[] { userUid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USER;
			finderArgs = new Object[] { userUid, start, end, orderByComparator };
		}

		List<AddressExt> list = null;

		if (retrieveFromCache) {
			list = (List<AddressExt>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (AddressExt addressExt : list) {
					if (!Objects.equals(userUid, addressExt.getUserUid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ADDRESSEXT_WHERE);

			boolean bindUserUid = false;

			if (userUid == null) {
				query.append(_FINDER_COLUMN_USER_USERUID_1);
			}
			else if (userUid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USER_USERUID_3);
			}
			else {
				bindUserUid = true;

				query.append(_FINDER_COLUMN_USER_USERUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AddressExtModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUserUid) {
					qPos.add(userUid);
				}

				if (!pagination) {
					list = (List<AddressExt>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<AddressExt>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first address ext in the ordered set where userUid = &#63;.
	 *
	 * @param userUid the user uid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching address ext
	 * @throws NoSuchAddressExtException if a matching address ext could not be found
	 */
	@Override
	public AddressExt findByuser_First(String userUid,
		OrderByComparator<AddressExt> orderByComparator)
		throws NoSuchAddressExtException {
		AddressExt addressExt = fetchByuser_First(userUid, orderByComparator);

		if (addressExt != null) {
			return addressExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userUid=");
		msg.append(userUid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAddressExtException(msg.toString());
	}

	/**
	 * Returns the first address ext in the ordered set where userUid = &#63;.
	 *
	 * @param userUid the user uid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching address ext, or <code>null</code> if a matching address ext could not be found
	 */
	@Override
	public AddressExt fetchByuser_First(String userUid,
		OrderByComparator<AddressExt> orderByComparator) {
		List<AddressExt> list = findByuser(userUid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last address ext in the ordered set where userUid = &#63;.
	 *
	 * @param userUid the user uid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching address ext
	 * @throws NoSuchAddressExtException if a matching address ext could not be found
	 */
	@Override
	public AddressExt findByuser_Last(String userUid,
		OrderByComparator<AddressExt> orderByComparator)
		throws NoSuchAddressExtException {
		AddressExt addressExt = fetchByuser_Last(userUid, orderByComparator);

		if (addressExt != null) {
			return addressExt;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userUid=");
		msg.append(userUid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAddressExtException(msg.toString());
	}

	/**
	 * Returns the last address ext in the ordered set where userUid = &#63;.
	 *
	 * @param userUid the user uid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching address ext, or <code>null</code> if a matching address ext could not be found
	 */
	@Override
	public AddressExt fetchByuser_Last(String userUid,
		OrderByComparator<AddressExt> orderByComparator) {
		int count = countByuser(userUid);

		if (count == 0) {
			return null;
		}

		List<AddressExt> list = findByuser(userUid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the address exts before and after the current address ext in the ordered set where userUid = &#63;.
	 *
	 * @param addressExtPK the primary key of the current address ext
	 * @param userUid the user uid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next address ext
	 * @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	 */
	@Override
	public AddressExt[] findByuser_PrevAndNext(AddressExtPK addressExtPK,
		String userUid, OrderByComparator<AddressExt> orderByComparator)
		throws NoSuchAddressExtException {
		AddressExt addressExt = findByPrimaryKey(addressExtPK);

		Session session = null;

		try {
			session = openSession();

			AddressExt[] array = new AddressExtImpl[3];

			array[0] = getByuser_PrevAndNext(session, addressExt, userUid,
					orderByComparator, true);

			array[1] = addressExt;

			array[2] = getByuser_PrevAndNext(session, addressExt, userUid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AddressExt getByuser_PrevAndNext(Session session,
		AddressExt addressExt, String userUid,
		OrderByComparator<AddressExt> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ADDRESSEXT_WHERE);

		boolean bindUserUid = false;

		if (userUid == null) {
			query.append(_FINDER_COLUMN_USER_USERUID_1);
		}
		else if (userUid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_USER_USERUID_3);
		}
		else {
			bindUserUid = true;

			query.append(_FINDER_COLUMN_USER_USERUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AddressExtModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUserUid) {
			qPos.add(userUid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(addressExt);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AddressExt> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the address exts where userUid = &#63; from the database.
	 *
	 * @param userUid the user uid
	 */
	@Override
	public void removeByuser(String userUid) {
		for (AddressExt addressExt : findByuser(userUid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(addressExt);
		}
	}

	/**
	 * Returns the number of address exts where userUid = &#63;.
	 *
	 * @param userUid the user uid
	 * @return the number of matching address exts
	 */
	@Override
	public int countByuser(String userUid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USER;

		Object[] finderArgs = new Object[] { userUid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ADDRESSEXT_WHERE);

			boolean bindUserUid = false;

			if (userUid == null) {
				query.append(_FINDER_COLUMN_USER_USERUID_1);
			}
			else if (userUid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USER_USERUID_3);
			}
			else {
				bindUserUid = true;

				query.append(_FINDER_COLUMN_USER_USERUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUserUid) {
					qPos.add(userUid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USER_USERUID_1 = "addressExt.id.userUid IS NULL";
	private static final String _FINDER_COLUMN_USER_USERUID_2 = "addressExt.id.userUid = ?";
	private static final String _FINDER_COLUMN_USER_USERUID_3 = "(addressExt.id.userUid IS NULL OR addressExt.id.userUid = '')";

	public AddressExtPersistenceImpl() {
		setModelClass(AddressExt.class);
	}

	/**
	 * Caches the address ext in the entity cache if it is enabled.
	 *
	 * @param addressExt the address ext
	 */
	@Override
	public void cacheResult(AddressExt addressExt) {
		entityCache.putResult(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
			AddressExtImpl.class, addressExt.getPrimaryKey(), addressExt);

		addressExt.resetOriginalValues();
	}

	/**
	 * Caches the address exts in the entity cache if it is enabled.
	 *
	 * @param addressExts the address exts
	 */
	@Override
	public void cacheResult(List<AddressExt> addressExts) {
		for (AddressExt addressExt : addressExts) {
			if (entityCache.getResult(
						AddressExtModelImpl.ENTITY_CACHE_ENABLED,
						AddressExtImpl.class, addressExt.getPrimaryKey()) == null) {
				cacheResult(addressExt);
			}
			else {
				addressExt.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all address exts.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(AddressExtImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the address ext.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AddressExt addressExt) {
		entityCache.removeResult(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
			AddressExtImpl.class, addressExt.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AddressExt> addressExts) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AddressExt addressExt : addressExts) {
			entityCache.removeResult(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
				AddressExtImpl.class, addressExt.getPrimaryKey());
		}
	}

	/**
	 * Creates a new address ext with the primary key. Does not add the address ext to the database.
	 *
	 * @param addressExtPK the primary key for the new address ext
	 * @return the new address ext
	 */
	@Override
	public AddressExt create(AddressExtPK addressExtPK) {
		AddressExt addressExt = new AddressExtImpl();

		addressExt.setNew(true);
		addressExt.setPrimaryKey(addressExtPK);

		return addressExt;
	}

	/**
	 * Removes the address ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param addressExtPK the primary key of the address ext
	 * @return the address ext that was removed
	 * @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	 */
	@Override
	public AddressExt remove(AddressExtPK addressExtPK)
		throws NoSuchAddressExtException {
		return remove((Serializable)addressExtPK);
	}

	/**
	 * Removes the address ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the address ext
	 * @return the address ext that was removed
	 * @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	 */
	@Override
	public AddressExt remove(Serializable primaryKey)
		throws NoSuchAddressExtException {
		Session session = null;

		try {
			session = openSession();

			AddressExt addressExt = (AddressExt)session.get(AddressExtImpl.class,
					primaryKey);

			if (addressExt == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAddressExtException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(addressExt);
		}
		catch (NoSuchAddressExtException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AddressExt removeImpl(AddressExt addressExt) {
		addressExt = toUnwrappedModel(addressExt);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(addressExt)) {
				addressExt = (AddressExt)session.get(AddressExtImpl.class,
						addressExt.getPrimaryKeyObj());
			}

			if (addressExt != null) {
				session.delete(addressExt);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (addressExt != null) {
			clearCache(addressExt);
		}

		return addressExt;
	}

	@Override
	public AddressExt updateImpl(AddressExt addressExt) {
		addressExt = toUnwrappedModel(addressExt);

		boolean isNew = addressExt.isNew();

		AddressExtModelImpl addressExtModelImpl = (AddressExtModelImpl)addressExt;

		Session session = null;

		try {
			session = openSession();

			if (addressExt.isNew()) {
				session.save(addressExt);

				addressExt.setNew(false);
			}
			else {
				addressExt = (AddressExt)session.merge(addressExt);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!AddressExtModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { addressExtModelImpl.getUserUid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_USER, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((addressExtModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						addressExtModelImpl.getOriginalUserUid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USER, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER,
					args);

				args = new Object[] { addressExtModelImpl.getUserUid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USER, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER,
					args);
			}
		}

		entityCache.putResult(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
			AddressExtImpl.class, addressExt.getPrimaryKey(), addressExt, false);

		addressExt.resetOriginalValues();

		return addressExt;
	}

	protected AddressExt toUnwrappedModel(AddressExt addressExt) {
		if (addressExt instanceof AddressExtImpl) {
			return addressExt;
		}

		AddressExtImpl addressExtImpl = new AddressExtImpl();

		addressExtImpl.setNew(addressExt.isNew());
		addressExtImpl.setPrimaryKey(addressExt.getPrimaryKey());

		addressExtImpl.setUserUid(addressExt.getUserUid());
		addressExtImpl.setAddressType(addressExt.getAddressType());
		addressExtImpl.setStreet1(addressExt.getStreet1());
		addressExtImpl.setStreet2(addressExt.getStreet2());
		addressExtImpl.setStreet3(addressExt.getStreet3());
		addressExtImpl.setCity(addressExt.getCity());
		addressExtImpl.setRegionId(addressExt.getRegionId());
		addressExtImpl.setCountryId(addressExt.getCountryId());
		addressExtImpl.setZipCode(addressExt.getZipCode());
		addressExtImpl.setIsMailing(addressExt.getIsMailing());

		return addressExtImpl;
	}

	/**
	 * Returns the address ext with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the address ext
	 * @return the address ext
	 * @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	 */
	@Override
	public AddressExt findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAddressExtException {
		AddressExt addressExt = fetchByPrimaryKey(primaryKey);

		if (addressExt == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAddressExtException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return addressExt;
	}

	/**
	 * Returns the address ext with the primary key or throws a {@link NoSuchAddressExtException} if it could not be found.
	 *
	 * @param addressExtPK the primary key of the address ext
	 * @return the address ext
	 * @throws NoSuchAddressExtException if a address ext with the primary key could not be found
	 */
	@Override
	public AddressExt findByPrimaryKey(AddressExtPK addressExtPK)
		throws NoSuchAddressExtException {
		return findByPrimaryKey((Serializable)addressExtPK);
	}

	/**
	 * Returns the address ext with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the address ext
	 * @return the address ext, or <code>null</code> if a address ext with the primary key could not be found
	 */
	@Override
	public AddressExt fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
				AddressExtImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		AddressExt addressExt = (AddressExt)serializable;

		if (addressExt == null) {
			Session session = null;

			try {
				session = openSession();

				addressExt = (AddressExt)session.get(AddressExtImpl.class,
						primaryKey);

				if (addressExt != null) {
					cacheResult(addressExt);
				}
				else {
					entityCache.putResult(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
						AddressExtImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(AddressExtModelImpl.ENTITY_CACHE_ENABLED,
					AddressExtImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return addressExt;
	}

	/**
	 * Returns the address ext with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param addressExtPK the primary key of the address ext
	 * @return the address ext, or <code>null</code> if a address ext with the primary key could not be found
	 */
	@Override
	public AddressExt fetchByPrimaryKey(AddressExtPK addressExtPK) {
		return fetchByPrimaryKey((Serializable)addressExtPK);
	}

	@Override
	public Map<Serializable, AddressExt> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, AddressExt> map = new HashMap<Serializable, AddressExt>();

		for (Serializable primaryKey : primaryKeys) {
			AddressExt addressExt = fetchByPrimaryKey(primaryKey);

			if (addressExt != null) {
				map.put(primaryKey, addressExt);
			}
		}

		return map;
	}

	/**
	 * Returns all the address exts.
	 *
	 * @return the address exts
	 */
	@Override
	public List<AddressExt> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the address exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of address exts
	 * @param end the upper bound of the range of address exts (not inclusive)
	 * @return the range of address exts
	 */
	@Override
	public List<AddressExt> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the address exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of address exts
	 * @param end the upper bound of the range of address exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of address exts
	 */
	@Override
	public List<AddressExt> findAll(int start, int end,
		OrderByComparator<AddressExt> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the address exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AddressExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of address exts
	 * @param end the upper bound of the range of address exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of address exts
	 */
	@Override
	public List<AddressExt> findAll(int start, int end,
		OrderByComparator<AddressExt> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AddressExt> list = null;

		if (retrieveFromCache) {
			list = (List<AddressExt>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_ADDRESSEXT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ADDRESSEXT;

				if (pagination) {
					sql = sql.concat(AddressExtModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<AddressExt>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<AddressExt>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the address exts from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (AddressExt addressExt : findAll()) {
			remove(addressExt);
		}
	}

	/**
	 * Returns the number of address exts.
	 *
	 * @return the number of address exts
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ADDRESSEXT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return AddressExtModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the address ext persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(AddressExtImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_ADDRESSEXT = "SELECT addressExt FROM AddressExt addressExt";
	private static final String _SQL_SELECT_ADDRESSEXT_WHERE = "SELECT addressExt FROM AddressExt addressExt WHERE ";
	private static final String _SQL_COUNT_ADDRESSEXT = "SELECT COUNT(addressExt) FROM AddressExt addressExt";
	private static final String _SQL_COUNT_ADDRESSEXT_WHERE = "SELECT COUNT(addressExt) FROM AddressExt addressExt WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "addressExt.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AddressExt exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AddressExt exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(AddressExtPersistenceImpl.class);
}