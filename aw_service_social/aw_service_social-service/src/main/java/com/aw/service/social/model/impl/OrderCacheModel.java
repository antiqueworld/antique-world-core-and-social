/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.Order;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Order in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Order
 * @generated
 */
@ProviderType
public class OrderCacheModel implements CacheModel<Order>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OrderCacheModel)) {
			return false;
		}

		OrderCacheModel orderCacheModel = (OrderCacheModel)obj;

		if (OrderId.equals(orderCacheModel.OrderId)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, OrderId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{OrderId=");
		sb.append(OrderId);
		sb.append(", userUid=");
		sb.append(userUid);
		sb.append(", userWalletAddr=");
		sb.append(userWalletAddr);
		sb.append(", fromType=");
		sb.append(fromType);
		sb.append(", fromCurrency=");
		sb.append(fromCurrency);
		sb.append(", fromAmount=");
		sb.append(fromAmount);
		sb.append(", rateId=");
		sb.append(rateId);
		sb.append(", toCurrency=");
		sb.append(toCurrency);
		sb.append(", toAmount=");
		sb.append(toAmount);
		sb.append(", chargedFlag=");
		sb.append(chargedFlag);
		sb.append(", sentFlag=");
		sb.append(sentFlag);
		sb.append(", completedFlag=");
		sb.append(completedFlag);
		sb.append(", addTime=");
		sb.append(addTime);
		sb.append(", updateTime=");
		sb.append(updateTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Order toEntityModel() {
		OrderImpl orderImpl = new OrderImpl();

		if (OrderId == null) {
			orderImpl.setOrderId(StringPool.BLANK);
		}
		else {
			orderImpl.setOrderId(OrderId);
		}

		if (userUid == null) {
			orderImpl.setUserUid(StringPool.BLANK);
		}
		else {
			orderImpl.setUserUid(userUid);
		}

		if (userWalletAddr == null) {
			orderImpl.setUserWalletAddr(StringPool.BLANK);
		}
		else {
			orderImpl.setUserWalletAddr(userWalletAddr);
		}

		if (fromType == null) {
			orderImpl.setFromType(StringPool.BLANK);
		}
		else {
			orderImpl.setFromType(fromType);
		}

		if (fromCurrency == null) {
			orderImpl.setFromCurrency(StringPool.BLANK);
		}
		else {
			orderImpl.setFromCurrency(fromCurrency);
		}

		orderImpl.setFromAmount(fromAmount);

		if (rateId == null) {
			orderImpl.setRateId(StringPool.BLANK);
		}
		else {
			orderImpl.setRateId(rateId);
		}

		if (toCurrency == null) {
			orderImpl.setToCurrency(StringPool.BLANK);
		}
		else {
			orderImpl.setToCurrency(toCurrency);
		}

		orderImpl.setToAmount(toAmount);

		if (chargedFlag == null) {
			orderImpl.setChargedFlag(StringPool.BLANK);
		}
		else {
			orderImpl.setChargedFlag(chargedFlag);
		}

		if (sentFlag == null) {
			orderImpl.setSentFlag(StringPool.BLANK);
		}
		else {
			orderImpl.setSentFlag(sentFlag);
		}

		if (completedFlag == null) {
			orderImpl.setCompletedFlag(StringPool.BLANK);
		}
		else {
			orderImpl.setCompletedFlag(completedFlag);
		}

		if (addTime == Long.MIN_VALUE) {
			orderImpl.setAddTime(null);
		}
		else {
			orderImpl.setAddTime(new Date(addTime));
		}

		if (updateTime == Long.MIN_VALUE) {
			orderImpl.setUpdateTime(null);
		}
		else {
			orderImpl.setUpdateTime(new Date(updateTime));
		}

		orderImpl.resetOriginalValues();

		return orderImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		OrderId = objectInput.readUTF();
		userUid = objectInput.readUTF();
		userWalletAddr = objectInput.readUTF();
		fromType = objectInput.readUTF();
		fromCurrency = objectInput.readUTF();

		fromAmount = objectInput.readDouble();
		rateId = objectInput.readUTF();
		toCurrency = objectInput.readUTF();

		toAmount = objectInput.readDouble();
		chargedFlag = objectInput.readUTF();
		sentFlag = objectInput.readUTF();
		completedFlag = objectInput.readUTF();
		addTime = objectInput.readLong();
		updateTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (OrderId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(OrderId);
		}

		if (userUid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userUid);
		}

		if (userWalletAddr == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userWalletAddr);
		}

		if (fromType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(fromType);
		}

		if (fromCurrency == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(fromCurrency);
		}

		objectOutput.writeDouble(fromAmount);

		if (rateId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(rateId);
		}

		if (toCurrency == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(toCurrency);
		}

		objectOutput.writeDouble(toAmount);

		if (chargedFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(chargedFlag);
		}

		if (sentFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(sentFlag);
		}

		if (completedFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(completedFlag);
		}

		objectOutput.writeLong(addTime);
		objectOutput.writeLong(updateTime);
	}

	public String OrderId;
	public String userUid;
	public String userWalletAddr;
	public String fromType;
	public String fromCurrency;
	public double fromAmount;
	public String rateId;
	public String toCurrency;
	public double toAmount;
	public String chargedFlag;
	public String sentFlag;
	public String completedFlag;
	public long addTime;
	public long updateTime;
}