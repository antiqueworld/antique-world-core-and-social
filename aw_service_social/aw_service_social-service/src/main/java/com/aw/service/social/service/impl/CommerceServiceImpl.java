/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.impl;

import com.aw.common.util.service.CommonUtilLocalServiceUtil;
import com.aw.common.util.service.ErrorCodeLocalServiceUtil;
import com.aw.common.util.service.PermissionCheckerLocalServiceUtil;
import com.aw.service.social.service.ConvertRateLocalServiceUtil;
import com.aw.service.social.service.OrderLocalServiceUtil;
import com.aw.service.social.service.base.CommerceServiceBaseImpl;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;

/**
 * The implementation of the commerce remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.service.social.service.CommerceService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CommerceServiceBaseImpl
 * @see com.aw.service.social.service.CommerceServiceUtil
 */
public class CommerceServiceImpl extends CommerceServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.service.social.service.CommerceServiceUtil} to access the commerce remote service.
	 */
	
	//create an order to purchase erc-20, user create by them self
	public JSONObject createOrder(String payType, String payCurrency, String getCurrency, 
			double amount, String walletAddress, String locale){
		User u = PermissionCheckerLocalServiceUtil.getCurrentUser();
		if(u != null){//TODO add KYC verify
			return OrderLocalServiceUtil.createOrder(u.getUserUuid(), payType, payCurrency
					, getCurrency, amount, walletAddress, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
		
	}
	
	//create an order to purchase erc-20 for user, just for recording manually
	public JSONObject createOrderForUser(String userUid, String payType, String payCurrency, String getCurrency, 
			double amount, String walletAddress, String locale){
		
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return OrderLocalServiceUtil.createOrder(userUid, payType, payCurrency, getCurrency, amount, walletAddress, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
		
	}
	
	//change order status to paid if payment success
	public JSONObject orderPaid(String orderId, String locale){	
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return OrderLocalServiceUtil.orderPaid(orderId, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
		
	}
	
	//change order status to sent and complete if sent out erc-20 successfully
	public JSONObject orderComplete(String orderId, String locale){
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return OrderLocalServiceUtil.orderComplete(orderId, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
		
	}
	
	//TODO
	public JSONObject cancelOrder(String orderId, String locale){
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return OrderLocalServiceUtil.cancelOrder(orderId);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
		
	}
	
	//list all orders, can input filter flag, default will be all if null of flag
	public JSONObject getAllOrders(String chargeFlag, String sentFlag, String locale){//admin
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return OrderLocalServiceUtil.getAllOrders(chargeFlag, sentFlag, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
	}
	
	//get order details (includes transaction info)
	public JSONObject getOrderDetail(String orderId, String locale){//admin
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return OrderLocalServiceUtil.getOrderDetail(orderId, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
	}

	// get money raise total in certain currency
	public JSONObject getMoneyRaise(String fromCurrency, String locale) {
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return OrderLocalServiceUtil.getMoneyRaise(fromCurrency, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}		
	}

	// get issued coin and its total
	public JSONObject getCoinIssued(String toCurrency, String locale) {
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return OrderLocalServiceUtil.getCoinIssued(toCurrency, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}		
	}
//	//get detail of one transaction
//	public JSONObject getTransactionDetail(String trxId, String locale){//admin
//		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
//		if(u != null){
//			return OrderLocalServiceUtil.orderPaid(orderId, locale);
//		}else{
//			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
//		}
//		return null;
//	}
	
	//get the current active rate
	public JSONObject getActiveRate(String from, String to, String locale){
		User u = PermissionCheckerLocalServiceUtil.getCurrentUser();//TODO add other role later
		if(u != null){
			return ConvertRateLocalServiceUtil.getActiveRate(from, to, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
	}
	
	//get all rate history
	public JSONObject getRates(String from, String to, String locale){
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return ConvertRateLocalServiceUtil.getRates(from, to, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
	}
	
	//list all active rates
	public JSONObject allActiveRates(String from, String to, String locale){
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return ConvertRateLocalServiceUtil.allActiveRates(locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
	}
	
	//admin inactivate current one and create a new one
	public JSONObject changeRate(String from, String to, double newRate, String locale){
		User u = PermissionCheckerLocalServiceUtil.isAdmin();//TODO add other role later
		if(u != null){
			return ConvertRateLocalServiceUtil.changeRate(from, to, newRate, locale);
		}else{
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getPermission(), locale);
		}
	}
}