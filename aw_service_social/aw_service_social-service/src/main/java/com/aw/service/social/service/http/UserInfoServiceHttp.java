/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.http;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.service.UserInfoServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

/**
 * Provides the HTTP utility for the
 * {@link UserInfoServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * {@link HttpPrincipal} parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserInfoServiceSoap
 * @see HttpPrincipal
 * @see UserInfoServiceUtil
 * @generated
 */
@ProviderType
public class UserInfoServiceHttp {
	public static com.liferay.portal.kernel.json.JSONObject getUserInfo(
		HttpPrincipal httpPrincipal, java.lang.String locale) {
		try {
			MethodKey methodKey = new MethodKey(UserInfoServiceUtil.class,
					"getUserInfo", _getUserInfoParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject updateBasicUserInfo(
		HttpPrincipal httpPrincipal, java.lang.String birthday,
		java.lang.String firstName, java.lang.String middleName,
		java.lang.String lastName, java.lang.String title,
		java.lang.String selfDesc, java.lang.String secEmail,
		java.lang.String phone, java.lang.String extension,
		java.lang.String cellPhone, java.lang.String locale) {
		try {
			MethodKey methodKey = new MethodKey(UserInfoServiceUtil.class,
					"updateBasicUserInfo", _updateBasicUserInfoParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					birthday, firstName, middleName, lastName, title, selfDesc,
					secEmail, phone, extension, cellPhone, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject updateUserAddressInfo(
		HttpPrincipal httpPrincipal, java.lang.String addressType,
		java.lang.String street1, java.lang.String street2,
		java.lang.String street3, java.lang.String city,
		java.lang.String regionId, java.lang.String countryId,
		java.lang.String zipCode, java.lang.String isMailing,
		java.lang.String locale) {
		try {
			MethodKey methodKey = new MethodKey(UserInfoServiceUtil.class,
					"updateUserAddressInfo",
					_updateUserAddressInfoParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					addressType, street1, street2, street3, city, regionId,
					countryId, zipCode, isMailing, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject updateSensitiveInfo(
		HttpPrincipal httpPrincipal, java.lang.String IdIssueCountryId,
		java.lang.String IdType, java.lang.String IdNumber,
		java.lang.String ssn, java.lang.String locale) {
		try {
			MethodKey methodKey = new MethodKey(UserInfoServiceUtil.class,
					"updateSensitiveInfo", _updateSensitiveInfoParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					IdIssueCountryId, IdType, IdNumber, ssn, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject updateIdImage(
		HttpPrincipal httpPrincipal, java.lang.String fileType, byte[] IdImage,
		java.lang.String isActive, java.lang.String publicFlag,
		java.lang.String locale) {
		try {
			MethodKey methodKey = new MethodKey(UserInfoServiceUtil.class,
					"updateIdImage", _updateIdImageParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					fileType, IdImage, isActive, publicFlag, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getCountryName(HttpPrincipal httpPrincipal,
		long countryId) {
		try {
			MethodKey methodKey = new MethodKey(UserInfoServiceUtil.class,
					"getCountryName", _getCountryNameParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(methodKey, countryId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getRegionName(HttpPrincipal httpPrincipal,
		long regionId) {
		try {
			MethodKey methodKey = new MethodKey(UserInfoServiceUtil.class,
					"getRegionName", _getRegionNameParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(methodKey, regionId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject getCountryList(
		HttpPrincipal httpPrincipal, java.lang.String locale) {
		try {
			MethodKey methodKey = new MethodKey(UserInfoServiceUtil.class,
					"getCountryList", _getCountryListParameterTypes7);

			MethodHandler methodHandler = new MethodHandler(methodKey, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject getRegionList(
		HttpPrincipal httpPrincipal, java.lang.String countryId,
		java.lang.String locale) {
		try {
			MethodKey methodKey = new MethodKey(UserInfoServiceUtil.class,
					"getRegionList", _getRegionListParameterTypes8);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					countryId, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(UserInfoServiceHttp.class);
	private static final Class<?>[] _getUserInfoParameterTypes0 = new Class[] {
			java.lang.String.class
		};
	private static final Class<?>[] _updateBasicUserInfoParameterTypes1 = new Class[] {
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class
		};
	private static final Class<?>[] _updateUserAddressInfoParameterTypes2 = new Class[] {
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class
		};
	private static final Class<?>[] _updateSensitiveInfoParameterTypes3 = new Class[] {
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class
		};
	private static final Class<?>[] _updateIdImageParameterTypes4 = new Class[] {
			java.lang.String.class, byte[].class, java.lang.String.class,
			java.lang.String.class, java.lang.String.class
		};
	private static final Class<?>[] _getCountryNameParameterTypes5 = new Class[] {
			long.class
		};
	private static final Class<?>[] _getRegionNameParameterTypes6 = new Class[] {
			long.class
		};
	private static final Class<?>[] _getCountryListParameterTypes7 = new Class[] {
			java.lang.String.class
		};
	private static final Class<?>[] _getRegionListParameterTypes8 = new Class[] {
			java.lang.String.class, java.lang.String.class
		};
}