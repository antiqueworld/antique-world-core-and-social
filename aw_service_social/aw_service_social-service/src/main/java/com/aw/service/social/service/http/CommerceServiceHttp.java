/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.http;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.service.CommerceServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

/**
 * Provides the HTTP utility for the
 * {@link CommerceServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * {@link HttpPrincipal} parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CommerceServiceSoap
 * @see HttpPrincipal
 * @see CommerceServiceUtil
 * @generated
 */
@ProviderType
public class CommerceServiceHttp {
	public static com.liferay.portal.kernel.json.JSONObject createOrder(
		HttpPrincipal httpPrincipal, String payType, String payCurrency,
		String getCurrency, double amount, String walletAddress, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"createOrder", _createOrderParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey, payType,
					payCurrency, getCurrency, amount, walletAddress, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject createOrderForUser(
		HttpPrincipal httpPrincipal, String userUid, String payType,
		String payCurrency, String getCurrency, double amount,
		String walletAddress, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"createOrderForUser", _createOrderForUserParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey, userUid,
					payType, payCurrency, getCurrency, amount, walletAddress,
					locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject orderPaid(
		HttpPrincipal httpPrincipal, String orderId, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"orderPaid", _orderPaidParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey, orderId,
					locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject orderComplete(
		HttpPrincipal httpPrincipal, String orderId, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"orderComplete", _orderCompleteParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(methodKey, orderId,
					locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject cancelOrder(
		HttpPrincipal httpPrincipal, String orderId, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"cancelOrder", _cancelOrderParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(methodKey, orderId,
					locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject getAllOrders(
		HttpPrincipal httpPrincipal, String chargeFlag, String sentFlag,
		String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"getAllOrders", _getAllOrdersParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					chargeFlag, sentFlag, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject getOrderDetail(
		HttpPrincipal httpPrincipal, String orderId, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"getOrderDetail", _getOrderDetailParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(methodKey, orderId,
					locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject getMoneyRaise(
		HttpPrincipal httpPrincipal, String fromCurrency, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"getMoneyRaise", _getMoneyRaiseParameterTypes7);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					fromCurrency, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject getCoinIssued(
		HttpPrincipal httpPrincipal, String toCurrency, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"getCoinIssued", _getCoinIssuedParameterTypes8);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					toCurrency, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject getActiveRate(
		HttpPrincipal httpPrincipal, String from, String to, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"getActiveRate", _getActiveRateParameterTypes9);

			MethodHandler methodHandler = new MethodHandler(methodKey, from,
					to, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject getRates(
		HttpPrincipal httpPrincipal, String from, String to, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"getRates", _getRatesParameterTypes10);

			MethodHandler methodHandler = new MethodHandler(methodKey, from,
					to, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject allActiveRates(
		HttpPrincipal httpPrincipal, String from, String to, String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"allActiveRates", _allActiveRatesParameterTypes11);

			MethodHandler methodHandler = new MethodHandler(methodKey, from,
					to, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static com.liferay.portal.kernel.json.JSONObject changeRate(
		HttpPrincipal httpPrincipal, String from, String to, double newRate,
		String locale) {
		try {
			MethodKey methodKey = new MethodKey(CommerceServiceUtil.class,
					"changeRate", _changeRateParameterTypes12);

			MethodHandler methodHandler = new MethodHandler(methodKey, from,
					to, newRate, locale);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (com.liferay.portal.kernel.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(CommerceServiceHttp.class);
	private static final Class<?>[] _createOrderParameterTypes0 = new Class[] {
			String.class, String.class, String.class, double.class, String.class,
			String.class
		};
	private static final Class<?>[] _createOrderForUserParameterTypes1 = new Class[] {
			String.class, String.class, String.class, String.class, double.class,
			String.class, String.class
		};
	private static final Class<?>[] _orderPaidParameterTypes2 = new Class[] {
			String.class, String.class
		};
	private static final Class<?>[] _orderCompleteParameterTypes3 = new Class[] {
			String.class, String.class
		};
	private static final Class<?>[] _cancelOrderParameterTypes4 = new Class[] {
			String.class, String.class
		};
	private static final Class<?>[] _getAllOrdersParameterTypes5 = new Class[] {
			String.class, String.class, String.class
		};
	private static final Class<?>[] _getOrderDetailParameterTypes6 = new Class[] {
			String.class, String.class
		};
	private static final Class<?>[] _getMoneyRaiseParameterTypes7 = new Class[] {
			String.class, String.class
		};
	private static final Class<?>[] _getCoinIssuedParameterTypes8 = new Class[] {
			String.class, String.class
		};
	private static final Class<?>[] _getActiveRateParameterTypes9 = new Class[] {
			String.class, String.class, String.class
		};
	private static final Class<?>[] _getRatesParameterTypes10 = new Class[] {
			String.class, String.class, String.class
		};
	private static final Class<?>[] _allActiveRatesParameterTypes11 = new Class[] {
			String.class, String.class, String.class
		};
	private static final Class<?>[] _changeRateParameterTypes12 = new Class[] {
			String.class, String.class, double.class, String.class
		};
}