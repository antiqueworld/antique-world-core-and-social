/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.http;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.service.CommerceServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link CommerceServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CommerceServiceHttp
 * @see CommerceServiceUtil
 * @generated
 */
@ProviderType
public class CommerceServiceSoap {
	public static String createOrder(String payType, String payCurrency,
		String getCurrency, double amount, String walletAddress, String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.createOrder(payType,
					payCurrency, getCurrency, amount, walletAddress, locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String createOrderForUser(String userUid, String payType,
		String payCurrency, String getCurrency, double amount,
		String walletAddress, String locale) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.createOrderForUser(userUid,
					payType, payCurrency, getCurrency, amount, walletAddress,
					locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String orderPaid(String orderId, String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.orderPaid(orderId,
					locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String orderComplete(String orderId, String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.orderComplete(orderId,
					locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String cancelOrder(String orderId, String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.cancelOrder(orderId,
					locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String getAllOrders(String chargeFlag, String sentFlag,
		String locale) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.getAllOrders(chargeFlag,
					sentFlag, locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String getOrderDetail(String orderId, String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.getOrderDetail(orderId,
					locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String getMoneyRaise(String fromCurrency, String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.getMoneyRaise(fromCurrency,
					locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String getCoinIssued(String toCurrency, String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.getCoinIssued(toCurrency,
					locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String getActiveRate(String from, String to, String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.getActiveRate(from,
					to, locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String getRates(String from, String to, String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.getRates(from,
					to, locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String allActiveRates(String from, String to, String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.allActiveRates(from,
					to, locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static String changeRate(String from, String to, double newRate,
		String locale) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommerceServiceUtil.changeRate(from,
					to, newRate, locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(CommerceServiceSoap.class);
}