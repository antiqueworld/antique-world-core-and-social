package com.aw.service.social.service.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import com.aw.common.util.service.CommonUtilLocalServiceUtil;
import com.aw.common.util.service.ErrorCodeLocalServiceUtil;
import com.aw.common.util.service.LogLocalServiceUtil;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.LocaleUtil;

public class UserUpdateUtil_bk {

	long COMPANY_ID = 20115;// TODO change it



	public JSONObject updateBasicUserInfo(User user, String birthday, String firstName, String middleName,
			String lastName, String title, String selfDesc, String primEmail, String secEmail, String phone,
			String extension, String cellPhone, String locale) {

		String userUid = user.getUserUuid();
		String selectUserStmt = StatementUtil.selectUserStmt;
		ArrayList<Object> inputList = new ArrayList<>();
		inputList.add(userUid);
		JSONObject userJSON = null;
//		try {
//			userJSON = GenericDaoUtilLocalServiceUtil.queryGet(selectUserStmt, inputList, 0);
//		} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//				| CQLInputDataTypeMisMatchException e1) {
//			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//					DataTypeConverterUtil.stackTraceToString(e1), null);
//		}

		JSONArray userArr = userJSON.getJSONArray("data_details");
		if (userArr == null || userArr.length() == 0) {
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getUserNotExist(),
					locale);
		}
		ArrayList<Object> updateUserInput = new ArrayList<>();

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date dateTime = new Date();
		String dateTimeStr = formatter.format(dateTime);

		String updateUserStmt = StatementUtil.updateUserBasicStmt;

		updateUserInput.add(firstName);
		updateUserInput.add(middleName);
		updateUserInput.add(lastName);
		updateUserInput.add(birthday);
		updateUserInput.add(title);
		updateUserInput.add(selfDesc);
		updateUserInput.add(dateTimeStr);
		updateUserInput.add(userUid);

//		try {
//			GenericDaoUtilLocalServiceUtil.runUpdate(updateUserStmt, updateUserInput);
//		} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//				| CQLInputDataTypeMisMatchException e) {
//			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//					DataTypeConverterUtil.stackTraceToString(e), null);
//		}
		String updateContactStmt = StatementUtil.updateUserContactStmt;
		ArrayList<Object> updateContactInput = new ArrayList<>();
		updateContactInput.add(primEmail);
		updateContactInput.add(secEmail);
		updateContactInput.add(phone);
		updateContactInput.add(extension);
		updateContactInput.add(cellPhone);
		updateContactInput.add(userUid);

//		try {
//			GenericDaoUtilLocalServiceUtil.runUpdate(updateContactStmt, updateContactInput);
//		} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//				| CQLInputDataTypeMisMatchException e) {
//			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//					DataTypeConverterUtil.stackTraceToString(e), null);
//		}

		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}

	public JSONObject updateUserAddressInfo(User user, String addressType, String street1, String street2,
			String street3, String city, String regionId, String countryId, String zipCode, String isMailing,
			String locale) {

		String userUid = user.getUserUuid();
		ArrayList<Object> input0 = new ArrayList<>();
		String updateStmt = StatementUtil.updateAddressStmt;
		input0.add(street1);
		input0.add(street2);
		input0.add(street3);
		input0.add(city);
		input0.add(regionId);
		input0.add(countryId);
		input0.add(zipCode);
		input0.add(isMailing);
		input0.add(userUid);
		input0.add(addressType);
		boolean res1 = false;
//		try {
//			res1 = GenericDaoUtilLocalServiceUtil.runUpdate(updateStmt, input0);
//		} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//				| CQLInputDataTypeMisMatchException e) {
//			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//					"Error happened when updating address info", e);
//		}
		if (!res1) {
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getUserNotExist(),
					locale);
		}
		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}

	public JSONObject updateIdImage(User user, String fileType, byte[] IdImage, String isActive, String publicFlag,
			String locale) {

		InputStream is = new ByteArrayInputStream(IdImage);
		String fileExt = "";
		String fileSize = "";
		// if (fileType == null || fileType == "") {
		// System.out.println("UserUpdateUtil : FileType is empty!!!");
		// }
		try {
			fileExt = URLConnection.guessContentTypeFromStream(is);
			fileSize = Integer.toString(IdImage.length);
		} catch (IOException e) {
			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
					"  FileExt cannot be guessed!!! /n" + DataTypeConverterUtil.stackTraceToString(e), null);
		}
		// TODO:following 2 line
		if (fileExt == null)
			fileExt = "";
		String userUid = user.getUserUuid();
		ArrayList<Object> input0 = new ArrayList<>();
		input0.add(userUid);
		String getImageIdStmt = StatementUtil.selectImageIdStmt;
		JSONObject resJSON = null;
//		try {
//			resJSON = GenericDaoUtilLocalServiceUtil.queryGet(getImageIdStmt, input0, 0);
//		} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//				| CQLInputDataTypeMisMatchException e1) {
//			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//					DataTypeConverterUtil.stackTraceToString(e1), null);
//		}
		JSONArray ImageArray = resJSON.getJSONArray("data_details");
		if (ImageArray == null) {
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getUserNotExist(),
					locale);
		}
		LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
				"----the ImageArray is:" + ImageArray, null);
		JSONObject ImageJSON = ImageArray.getJSONObject(0);
		String mediaId = "";
		if (ImageJSON.has("idimageid")) {
			mediaId = ImageJSON.getString("idimageid");
		}

		// System.out.println("----------------------The media ID is ====" +
		// mediaId);
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date dateTime = new Date();
		String dateTimeStr = formatter.format(dateTime);
		if (!mediaId.equals("") && !(mediaId == null)) {

			String updateMediaStmt = StatementUtil.updateMediaStmt;

			ArrayList<Object> updateMediaInput = new ArrayList<>();
			updateMediaInput.add(fileType);
			updateMediaInput.add(fileExt);
			updateMediaInput.add(fileSize);
			updateMediaInput.add(ByteBuffer.wrap(IdImage));
			updateMediaInput.add(isActive);
			updateMediaInput.add(publicFlag);
			updateMediaInput.add(dateTimeStr);
			updateMediaInput.add(mediaId);

//			try {
//				GenericDaoUtilLocalServiceUtil.runUpdate(updateMediaStmt, updateMediaInput);
//			} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//					| CQLInputDataTypeMisMatchException e) {
//				LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//						DataTypeConverterUtil.stackTraceToString(e), null);
//			}
			// }
		} else {
			// System.out.println("UserUpdateUtil : mediaId.equals('')");
			String insertMediaStmt = StatementUtil.insertMediaStmt;
			ArrayList<Object> insertMediaInput = new ArrayList<>();
			UUID uid = null;//UUIDs.timeBased();
			insertMediaInput.add(uid.toString());
			insertMediaInput.add(isActive);
			insertMediaInput.add(userUid);
			insertMediaInput.add(publicFlag);
			insertMediaInput.add(fileType);
			insertMediaInput.add(fileExt);
			insertMediaInput.add(fileSize);
			insertMediaInput.add(ByteBuffer.wrap(IdImage));
			insertMediaInput.add(dateTimeStr);
			insertMediaInput.add(dateTimeStr);
//			try {
//				GenericDaoUtilLocalServiceUtil.runInsert(insertMediaStmt, insertMediaInput);
//			} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//					| CQLInputDataTypeMisMatchException e1) {
//				LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//						DataTypeConverterUtil.stackTraceToString(e1), null);
//			}
			String updateUserStmt = StatementUtil.updateUserIdImageStmt;
			ArrayList<Object> updateUserInput = new ArrayList<>();
			updateUserInput.add(uid.toString());
			updateUserInput.add(dateTimeStr);
			updateUserInput.add(userUid);
//			try {
//				GenericDaoUtilLocalServiceUtil.runUpdate(updateUserStmt, updateUserInput);
//			} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//					| CQLInputDataTypeMisMatchException e) {
//				LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//						DataTypeConverterUtil.stackTraceToString(e), null);
//			}
		}
		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);

	}

	public JSONObject updateSensitiveInfo(User user, String IdIssueCountryId, String IdType, String IdNumber,
			String ssn, String locale) {
		String userUid = user.getUserUuid();
		String selectUserStmt = StatementUtil.selectUserStmt;
		ArrayList<Object> inputList = new ArrayList<>();
		inputList.add(userUid);
		JSONObject userJSON = null;
//		try {
//			userJSON = GenericDaoUtilLocalServiceUtil.queryGet(selectUserStmt, inputList, 0);
//		} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//				| CQLInputDataTypeMisMatchException e1) {
//			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//					DataTypeConverterUtil.stackTraceToString(e1), null);
//		}

		JSONArray userArr = userJSON.getJSONArray("data_details");
		if (userArr == null || userArr.length() == 0) {
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getUserNotExist(),
					locale);
		}

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date dateTime = new Date();
		String dateTimeStr = formatter.format(dateTime);
		String updateUserStmt = StatementUtil.updateUserSensiStmt;

		ArrayList<Object> updateUserInput = new ArrayList<>();
		updateUserInput.add(IdIssueCountryId);
		updateUserInput.add(IdType);
		updateUserInput.add(IdNumber);
		updateUserInput.add(ssn);
		updateUserInput.add(dateTimeStr);
		updateUserInput.add(userUid);
//		try {
//			GenericDaoUtilLocalServiceUtil.runUpdate(updateUserStmt, updateUserInput);
//		} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//				| CQLInputDataTypeMisMatchException e1) {
//			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//					DataTypeConverterUtil.stackTraceToString(e1), null);
//		}

		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);

	}

	// 测试用的。。。先放这儿
	// type option linemoney-reg-user / linemoney-agent
	// wait for the zhiyi's version of password1 password2
	public User addUser(String firstName, String middleName, String lastName, String emailAddress, String password1,
			String password2, int bY, int bM, int bD, String title, String selfDesc, String portraitId) {

		try {
			long creatorUserId = 0;// hard code

			boolean autoPassword = false;
			boolean autoScreenName = true;
			String screenName = "";
			long facebookId = 0;
			String openId = "";

			int prefixId = 0;
			int suffixId = 0;
			boolean male = true;

			String jobTitle = "";
			long[] groupIds = null;
			long[] organizationIds = null;
			long[] roleIds = null;
			long[] userGroupIds = null;
			boolean sendEmail = true;
			ServiceContext serviceContext = null;

			Locale locale = LocaleUtil.getDefault();

			User user = UserLocalServiceUtil.addUser(creatorUserId, COMPANY_ID, autoPassword, password1, password2,
					autoScreenName, screenName, emailAddress, facebookId, openId, locale, firstName, middleName,
					lastName, prefixId, suffixId, male, bM, bD, bY, jobTitle, groupIds, organizationIds, roleIds,
					userGroupIds, sendEmail, serviceContext);

			if (user != null) {
				// initial cassandra user records
				// userId userUUId birth instance.....
				// add userExt and contact, leave address empty
			}

			return user;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@SuppressWarnings("unused")
	private void junkYard() {
		/*
		 * update user basic //update birthday MM/dd/yyyy if(birthday != null &&
		 * !birthday.isEmpty()){
		 * 
		 * Contact userContact = null; try { userContact = user.getContact(); }
		 * catch (PortalException e1) { e1.printStackTrace(); return
		 * DataFormatUtil.getResponse(null, null, StatusCodes.CONTACT_NOT_EXIST,
		 * locale); }
		 * 
		 * //since we have default birthday, user always should has a contact if
		 * (userContact != null){ try{ int mth =
		 * Integer.parseInt(birthday.split("/")[0])-1; int day =
		 * Integer.parseInt(birthday.split("/")[1]); int yr =
		 * Integer.parseInt(birthday.split("/")[2]);
		 * 
		 * Calendar cal = Calendar.getInstance(); cal.set(yr, mth, day, 0, 0,
		 * 0); Date date = cal.getTime();
		 * 
		 * userContact.setBirthday(date); userContact.persist();
		 * }catch(NumberFormatException e){ return
		 * DataFormatUtil.getResponse(null, null,
		 * StatusCodes.BIRTH_FORMAT_ERROR, locale); } } }
		 * 
		 * 
		 * if(firstName != null && !firstName.isEmpty()){
		 * user.setFirstName(firstName); }
		 * 
		 * if(lastName != null && !lastName.isEmpty()){
		 * user.setLastName(lastName); }
		 * 
		 * UserExt userExt = UserExtUtil.fetchByPrimaryKey(user.getUuid()); //
		 * 
		 * if(title != null && !title.isEmpty()){
		 * 
		 * if(userExt == null){//then create for it userExt =
		 * UserExtLocalServiceUtil.createExt(user); userExt.setTitle(title);
		 * userExt.persist(); }else{ userExt.setTitle(title); userExt.persist();
		 * }
		 * 
		 * }
		 * 
		 * if(potrait != null && potrait.length > 0){ try {
		 * UserServiceUtil.updatePortrait(user.getUserId(), potrait); } catch
		 * (PortalException e) { return DataFormatUtil.getResponse(null, null,
		 * StatusCodes.IMAGE_FORMAT_ERROR, locale); } }
		 * 
		 * if(desc != null && !desc.isEmpty()){ if(userExt == null){//then
		 * create for it userExt = UserExtLocalServiceUtil.createExt(user);
		 * userExt.setSelfDesc(desc); userExt.persist(); }else{
		 * userExt.setSelfDesc(desc); userExt.persist(); } }
		 * 
		 * user.persist(); return DataFormatUtil.getResponse(null, null,
		 * StatusCodes.SUCCESS, locale);
		 * 
		 */

		/*
		 * update users contact Contact userContact = null; try { userContact =
		 * user.getContact(); if(userContact == null){ return
		 * DataFormatUtil.getResponse(null, null, StatusCodes.CONTACT_NOT_EXIST,
		 * locale); } } catch (PortalException e) { e.printStackTrace(); return
		 * DataFormatUtil.getResponse(null, null, StatusCodes.CONTACT_NOT_EXIST,
		 * locale); }
		 * 
		 * //update phone if(phoneNumber != null && !phoneNumber.isEmpty()){
		 * 
		 * String className = Contact.class.getName(); long classPK =
		 * userContact.getContactId();
		 * 
		 * List<Phone> phoneList = null; phoneList = user.getPhones();
		 * 
		 * if(phoneList !=null && phoneList.size() > 0 ){
		 * 
		 * Phone phone =(Phone) phoneList.get(0); phone.setNumber(phoneNumber);
		 * phone.setExtension(extension); phone.persist();
		 * 
		 * }else{ try { PhoneLocalServiceUtil.addPhone(user.getUserId(),
		 * className, classPK, phoneNumber, extension, typeId, true, null); }
		 * catch (PortalException e) { e.printStackTrace(); return
		 * DataFormatUtil.getResponse(null, null,
		 * StatusCodes.PHONE_FORMAT_ERROR, locale); } } }
		 * 
		 * if(str1 != null && !str2.isEmpty()){ String className =
		 * Contact.class.getName(); long classPK = userContact.getContactId();
		 * List<Address> addresses = null;
		 * 
		 * try { addresses = AddressServiceUtil.getAddresses(className,
		 * classPK); } catch (PortalException e) { e.printStackTrace(); return
		 * DataFormatUtil.getResponse(null, null, StatusCodes.ADDRESS_NOT_FOUND,
		 * locale); }
		 * 
		 * if (addresses != null && addresses.size() > 0){ for(int i=0; i
		 * <addresses.size(); ++i) { Address a =(Address) addresses.get(i); if(a
		 * !=null && a.isPrimary()){
		 * 
		 * a.setStreet1(str1); a.setStreet2(str2); a.setStreet3(str3);
		 * 
		 * a.setCity(city); a.setRegionId(regionId); a.setCountryId(countryId);
		 * a.setTypeId(typeId); a.setZip(zip);
		 * 
		 * if(1 == isMailing){ a.setMailing(true); }else{ a.setMailing(false); }
		 * 
		 * a.persist(); } } }else{//create for it
		 * 
		 * boolean isMail; if(1 == isMailing){ isMail = true; }else{ isMail =
		 * false; }
		 * 
		 * try { AddressServiceUtil.addAddress(className, classPK, str1, str2,
		 * str3, city, zip , regionId, countryId, typeId, isMail, true, null); }
		 * catch (PortalException e) { e.printStackTrace(); return
		 * DataFormatUtil.getResponse(null, null,
		 * StatusCodes.ADDRESS_FORMAT_ERROR, locale); }
		 * 
		 * } }
		 * 
		 */

		/*
		 * upate sensitive
		 * 
		 * UserExt ext = UserExtUtil.fetchByPrimaryKey(user.getUuid()); if(ext
		 * == null){ ext = UserExtLocalServiceUtil.createExt(user); }
		 * 
		 * ext.setSsn(ssn); ext.setIdIssueCountry(IdIssueCountry);
		 * ext.setIdType(IdType);
		 * 
		 * if(imageChanged){ UnsyncByteArrayInputStream inputStream =new
		 * UnsyncByteArrayInputStream(IdImage); OutputBlob imageBlob = new
		 * OutputBlob(inputStream, IdImage.length);
		 * ext.setIdImagePic(imageBlob); }
		 * 
		 * ext.setIdNumber(IdNumber); ext.persist();
		 * 
		 */
		// update address .several addresses / addresstype by XH
		/**
		 * String selectStmt = "select * from aw.address where userUid = ? and
		 * addressType = ?"; JSONObject queryResult =
		 * GenericDaoUtilLocalServiceUtil.queryGet(selectStmt, input0, 0);
		 * JSONArray dataDetails = queryResult.getJSONArray("data_details");
		 * ArrayList<Object> input = new ArrayList<>(); input.add(userUid);
		 * input.add(addressType); input.add(street1); input.add(street2);
		 * input.add(street3); input.add(city); input.add(regionId);
		 * input.add(countryId); input.add(zipCode); input.add(isMailing);
		 * 
		 * String insertStmt = "insert into aw.address (userUid, addressType,
		 * street1, street2, street3, city, regionId, countryId, zipCode,
		 * isMailling) values (?,?,?,?,?,?,?,?,?,?)";
		 * GenericDaoUtilLocalServiceUtil.runInsert(insertStmt, input); if
		 * (dataDetails.length() > ADDRESS_HISTORY_LIMIT) { JSONObject
		 * oldestRecord = dataDetails.getJSONObject(ADDRESS_HISTORY_LIMIT - 1);
		 * String oldestTimeStr = oldestRecord.getString("addTime");
		 * SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy
		 * HH:mm:ss"); try { Date oldestTime = formatter.parse(oldestTimeStr);
		 * input0.add(oldestTime); String deleteStmt = "delete from aw.address
		 * where userUid = ? and addressType = ? and addTime = ?";
		 * GenericDaoUtilLocalServiceUtil.runDeleteQuery(deleteStmt, input0); }
		 * catch (ParseException e) { e.printStackTrace(); }
		 * 
		 * }
		 */
	}
	public JSONObject updateUserPortrait(User user, String fileType, byte[] portrait, String isActive,
			String publicFlag, String locale) {
		InputStream is = new ByteArrayInputStream(portrait);
		String fileExt = "";
		String fileSize = "";
		try {
			fileExt = URLConnection.guessContentTypeFromStream(is);
			fileSize = Integer.toString(portrait.length);
		} catch (IOException e) {
			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
					DataTypeConverterUtil.stackTraceToString(e), null);
		}
		// TODO the following line is for testing.
		if (fileExt == null)
			fileExt = "";
		String userUid = user.getUserUuid();

		String getPtStmt = StatementUtil.selectPortraitIdStmt;
		ArrayList<Object> getPtInput = new ArrayList<>();
		ArrayList<Object> updateUserInput = new ArrayList<>();
		ArrayList<Object> updateMediaInput = new ArrayList<>();
		getPtInput.add(userUid);
		JSONObject uidJSON = null;
//		try {
//			uidJSON = GenericDaoUtilLocalServiceUtil.queryGet(getPtStmt, getPtInput, 0);
//		} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//				| CQLInputDataTypeMisMatchException e1) {
//			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//					"Error happened when selecting portraitId from userExt", e1);
//		}
		JSONArray rs = uidJSON.getJSONArray("data_details");
		if (rs == null) {
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getUserNotExist(),
					locale);
		}
		JSONObject res = rs.getJSONObject(0);
		String mediaId = "";
		if (res.has("portraitid")) {
			mediaId = res.getString("portraitid");
		}
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date dateTime = new Date();
		String dateTimeStr = formatter.format(dateTime);

		if (mediaId.equals("") || mediaId == null) {
			String updateUserStmt = StatementUtil.updateUserPortraitStmt;
			String insertMediaStmt = StatementUtil.insertMediaStmt;
			UUID uid = null;//UUIDs.timeBased();
			updateMediaInput.add(uid.toString());
			updateMediaInput.add(isActive);
			updateMediaInput.add(userUid);
			updateMediaInput.add(publicFlag);
			updateMediaInput.add(fileType);
			updateMediaInput.add(fileExt);
			updateMediaInput.add(fileSize);
			updateMediaInput.add(ByteBuffer.wrap(portrait));
			updateMediaInput.add(dateTimeStr);
			updateMediaInput.add(dateTimeStr);
//			try {
//				GenericDaoUtilLocalServiceUtil.runInsert(insertMediaStmt, updateMediaInput);
//			} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//					| CQLInputDataTypeMisMatchException e1) {
//				LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//						"Error happened when updating new portrait info", e1);
//			}
			// TODO:but if the update of media is success but update of user
			// input is not success, there would be inconsistency
			updateUserInput.add(uid.toString());
			updateUserInput.add(dateTimeStr);
			updateUserInput.add(userUid);
			boolean res1 = false;
//			try {
//				res1 = GenericDaoUtilLocalServiceUtil.runUpdate(updateUserStmt, updateUserInput);
//			} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//					| CQLInputDataTypeMisMatchException e) {
//				LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//						DataTypeConverterUtil.stackTraceToString(e), null);
//			}
			if (!res1) {
				return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getUserNotExist(),
						locale);
			}

		} else {
			String updateMediaStmt = StatementUtil.updateMediaStmt;

			updateMediaInput.add(fileType);
			updateMediaInput.add(fileExt);
			updateMediaInput.add(fileSize);
			updateMediaInput.add(ByteBuffer.wrap(portrait));
			updateMediaInput.add(isActive);
			updateMediaInput.add(publicFlag);
			updateMediaInput.add(dateTimeStr);
			updateMediaInput.add(mediaId);
//			try {
//				GenericDaoUtilLocalServiceUtil.runUpdate(updateMediaStmt, updateMediaInput);
//			} catch (CQLIllegalModifyTableStructureException | CQLIncorrectFormatException
//					| CQLInputDataTypeMisMatchException e1) {
//				LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
//						"Error happened when updating portrait info", e1);
//			}
		}
		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}

}