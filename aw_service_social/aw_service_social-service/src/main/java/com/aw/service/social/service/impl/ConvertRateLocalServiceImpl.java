/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.impl;

import java.util.Date;
import java.util.List;

import com.aw.common.util.service.CommonUtilLocalServiceUtil;
import com.aw.common.util.service.ErrorCodeLocalServiceUtil;
import com.aw.service.social.model.ConvertRate;
import com.aw.service.social.service.base.ConvertRateLocalServiceBaseImpl;
import com.aw.service.social.service.persistence.ConvertRateUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

/**
 * The implementation of the convert rate local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.service.social.service.ConvertRateLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ConvertRateLocalServiceBaseImpl
 * @see com.aw.service.social.service.ConvertRateLocalServiceUtil
 */
public class ConvertRateLocalServiceImpl extends ConvertRateLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.service.social.service.ConvertRateLocalServiceUtil} to access the convert rate local service.
	 */
	public JSONObject getActiveRate(String from, String to, String locale){
		JSONObject json = JSONFactoryUtil.createJSONObject();			
		
		ConvertRate activeRate = getActiveRate(from, to);
		
		//hide the other info since it's public
		json.put("rateId", activeRate.getRateId());
		json.put("rate", activeRate.getRate());
		
		return CommonUtilLocalServiceUtil.getResponse(json, null,
				ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	
	public ConvertRate getActiveRate(String from, String to){
				
		List<ConvertRate> list = ConvertRateUtil.findBypair(from, to);
		
		ConvertRate activeRate = null;
		if(list != null && !list.isEmpty()){
			for(ConvertRate cr : list){
				if( cr.getIsActive().equalsIgnoreCase("Y")){
					activeRate = cr;
					break;
				}
			}
		}
	
		return activeRate;
	}
	
	//get all rate history
	public JSONObject getRates(String from, String to, String locale){
		JSONArray json = JSONFactoryUtil.createJSONArray();
		
		List<ConvertRate> list = ConvertRateUtil.findBypair(from, to);
		
		if(list != null && !list.isEmpty()){
			for(ConvertRate cr : list){									
				json.put(CommonUtilLocalServiceUtil.getJson(cr));
			}
		}
		
		return CommonUtilLocalServiceUtil.getResponse(null, json,
				ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	
	//list all active rates
	public JSONObject allActiveRates(String locale){
		JSONArray json = JSONFactoryUtil.createJSONArray();
		
		List<ConvertRate> list = ConvertRateUtil.findByactive("Y");
		
		if(list != null && !list.isEmpty()){
			for(ConvertRate cr : list){									
				json.put(CommonUtilLocalServiceUtil.getJson(cr));
			}
		}
		
		return CommonUtilLocalServiceUtil.getResponse(null, json,
				ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	
	//admin inactivate current one and create a new one
	public JSONObject changeRate(String from, String to, double newRate, String locale){
		List<ConvertRate> list = ConvertRateUtil.findBypair(from, to);
		
		ConvertRate activeRate = null;
		if(list != null && !list.isEmpty()){
			for(ConvertRate cr : list){
				if( cr.getIsActive().equalsIgnoreCase("Y")){
					activeRate = cr;
					break;
				}
			}
		}
		Date date = new Date();
		
		//de-activate it
		if(activeRate != null){
			activeRate.setIsActive("N");
			activeRate.setExpireTime(date);
			activeRate.persist();
		}		
		
		//create new one
		ConvertRate newCRate = ConvertRateUtil.create("RATE"+date.getTime());
		newCRate.setFromCurrency(from);
		newCRate.setToCurrency(to);
		newCRate.setIsActive("Y");
		newCRate.setRate(newRate);
		newCRate.setStartTime(date);
		newCRate.persist();
		
		return CommonUtilLocalServiceUtil.getResponse(CommonUtilLocalServiceUtil.getJson(newCRate), null,
				ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
}