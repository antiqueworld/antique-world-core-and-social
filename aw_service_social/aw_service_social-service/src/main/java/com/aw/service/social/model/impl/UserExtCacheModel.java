/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.UserExt;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UserExt in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see UserExt
 * @generated
 */
@ProviderType
public class UserExtCacheModel implements CacheModel<UserExt>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserExtCacheModel)) {
			return false;
		}

		UserExtCacheModel userExtCacheModel = (UserExtCacheModel)obj;

		if (userUid.equals(userExtCacheModel.userUid)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, userUid);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{userUid=");
		sb.append(userUid);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userInstance=");
		sb.append(userInstance);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", middleName=");
		sb.append(middleName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", birthday=");
		sb.append(birthday);
		sb.append(", portraitId=");
		sb.append(portraitId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", selfDesc=");
		sb.append(selfDesc);
		sb.append(", ssn=");
		sb.append(ssn);
		sb.append(", IdIssueCountry=");
		sb.append(IdIssueCountry);
		sb.append(", IdType=");
		sb.append(IdType);
		sb.append(", IdImageId=");
		sb.append(IdImageId);
		sb.append(", IdNumber=");
		sb.append(IdNumber);
		sb.append(", addTime=");
		sb.append(addTime);
		sb.append(", updateTime=");
		sb.append(updateTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserExt toEntityModel() {
		UserExtImpl userExtImpl = new UserExtImpl();

		if (userUid == null) {
			userExtImpl.setUserUid(StringPool.BLANK);
		}
		else {
			userExtImpl.setUserUid(userUid);
		}

		userExtImpl.setUserId(userId);

		if (userInstance == null) {
			userExtImpl.setUserInstance(StringPool.BLANK);
		}
		else {
			userExtImpl.setUserInstance(userInstance);
		}

		if (firstName == null) {
			userExtImpl.setFirstName(StringPool.BLANK);
		}
		else {
			userExtImpl.setFirstName(firstName);
		}

		if (middleName == null) {
			userExtImpl.setMiddleName(StringPool.BLANK);
		}
		else {
			userExtImpl.setMiddleName(middleName);
		}

		if (lastName == null) {
			userExtImpl.setLastName(StringPool.BLANK);
		}
		else {
			userExtImpl.setLastName(lastName);
		}

		if (birthday == null) {
			userExtImpl.setBirthday(StringPool.BLANK);
		}
		else {
			userExtImpl.setBirthday(birthday);
		}

		if (portraitId == null) {
			userExtImpl.setPortraitId(StringPool.BLANK);
		}
		else {
			userExtImpl.setPortraitId(portraitId);
		}

		if (title == null) {
			userExtImpl.setTitle(StringPool.BLANK);
		}
		else {
			userExtImpl.setTitle(title);
		}

		if (selfDesc == null) {
			userExtImpl.setSelfDesc(StringPool.BLANK);
		}
		else {
			userExtImpl.setSelfDesc(selfDesc);
		}

		if (ssn == null) {
			userExtImpl.setSsn(StringPool.BLANK);
		}
		else {
			userExtImpl.setSsn(ssn);
		}

		if (IdIssueCountry == null) {
			userExtImpl.setIdIssueCountry(StringPool.BLANK);
		}
		else {
			userExtImpl.setIdIssueCountry(IdIssueCountry);
		}

		if (IdType == null) {
			userExtImpl.setIdType(StringPool.BLANK);
		}
		else {
			userExtImpl.setIdType(IdType);
		}

		if (IdImageId == null) {
			userExtImpl.setIdImageId(StringPool.BLANK);
		}
		else {
			userExtImpl.setIdImageId(IdImageId);
		}

		if (IdNumber == null) {
			userExtImpl.setIdNumber(StringPool.BLANK);
		}
		else {
			userExtImpl.setIdNumber(IdNumber);
		}

		if (addTime == Long.MIN_VALUE) {
			userExtImpl.setAddTime(null);
		}
		else {
			userExtImpl.setAddTime(new Date(addTime));
		}

		if (updateTime == Long.MIN_VALUE) {
			userExtImpl.setUpdateTime(null);
		}
		else {
			userExtImpl.setUpdateTime(new Date(updateTime));
		}

		userExtImpl.resetOriginalValues();

		return userExtImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		userUid = objectInput.readUTF();

		userId = objectInput.readLong();
		userInstance = objectInput.readUTF();
		firstName = objectInput.readUTF();
		middleName = objectInput.readUTF();
		lastName = objectInput.readUTF();
		birthday = objectInput.readUTF();
		portraitId = objectInput.readUTF();
		title = objectInput.readUTF();
		selfDesc = objectInput.readUTF();
		ssn = objectInput.readUTF();
		IdIssueCountry = objectInput.readUTF();
		IdType = objectInput.readUTF();
		IdImageId = objectInput.readUTF();
		IdNumber = objectInput.readUTF();
		addTime = objectInput.readLong();
		updateTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (userUid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userUid);
		}

		objectOutput.writeLong(userId);

		if (userInstance == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userInstance);
		}

		if (firstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(firstName);
		}

		if (middleName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(middleName);
		}

		if (lastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lastName);
		}

		if (birthday == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(birthday);
		}

		if (portraitId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(portraitId);
		}

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (selfDesc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(selfDesc);
		}

		if (ssn == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ssn);
		}

		if (IdIssueCountry == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(IdIssueCountry);
		}

		if (IdType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(IdType);
		}

		if (IdImageId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(IdImageId);
		}

		if (IdNumber == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(IdNumber);
		}

		objectOutput.writeLong(addTime);
		objectOutput.writeLong(updateTime);
	}

	public String userUid;
	public long userId;
	public String userInstance;
	public String firstName;
	public String middleName;
	public String lastName;
	public String birthday;
	public String portraitId;
	public String title;
	public String selfDesc;
	public String ssn;
	public String IdIssueCountry;
	public String IdType;
	public String IdImageId;
	public String IdNumber;
	public long addTime;
	public long updateTime;
}