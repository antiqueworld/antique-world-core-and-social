/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.Item;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Item in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Item
 * @generated
 */
@ProviderType
public class ItemCacheModel implements CacheModel<Item>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ItemCacheModel)) {
			return false;
		}

		ItemCacheModel itemCacheModel = (ItemCacheModel)obj;

		if (itemId.equals(itemCacheModel.itemId)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, itemId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{itemId=");
		sb.append(itemId);
		sb.append(", ownerUid=");
		sb.append(ownerUid);
		sb.append(", erc20Addr=");
		sb.append(erc20Addr);
		sb.append(", tokenCurrency=");
		sb.append(tokenCurrency);
		sb.append(", erc721Addr=");
		sb.append(erc721Addr);
		sb.append(", title=");
		sb.append(title);
		sb.append(", description=");
		sb.append(description);
		sb.append(", addTime=");
		sb.append(addTime);
		sb.append(", updateTime=");
		sb.append(updateTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Item toEntityModel() {
		ItemImpl itemImpl = new ItemImpl();

		if (itemId == null) {
			itemImpl.setItemId(StringPool.BLANK);
		}
		else {
			itemImpl.setItemId(itemId);
		}

		if (ownerUid == null) {
			itemImpl.setOwnerUid(StringPool.BLANK);
		}
		else {
			itemImpl.setOwnerUid(ownerUid);
		}

		if (erc20Addr == null) {
			itemImpl.setErc20Addr(StringPool.BLANK);
		}
		else {
			itemImpl.setErc20Addr(erc20Addr);
		}

		if (tokenCurrency == null) {
			itemImpl.setTokenCurrency(StringPool.BLANK);
		}
		else {
			itemImpl.setTokenCurrency(tokenCurrency);
		}

		if (erc721Addr == null) {
			itemImpl.setErc721Addr(StringPool.BLANK);
		}
		else {
			itemImpl.setErc721Addr(erc721Addr);
		}

		if (title == null) {
			itemImpl.setTitle(StringPool.BLANK);
		}
		else {
			itemImpl.setTitle(title);
		}

		if (description == null) {
			itemImpl.setDescription(StringPool.BLANK);
		}
		else {
			itemImpl.setDescription(description);
		}

		if (addTime == Long.MIN_VALUE) {
			itemImpl.setAddTime(null);
		}
		else {
			itemImpl.setAddTime(new Date(addTime));
		}

		if (updateTime == Long.MIN_VALUE) {
			itemImpl.setUpdateTime(null);
		}
		else {
			itemImpl.setUpdateTime(new Date(updateTime));
		}

		itemImpl.resetOriginalValues();

		return itemImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		itemId = objectInput.readUTF();
		ownerUid = objectInput.readUTF();
		erc20Addr = objectInput.readUTF();
		tokenCurrency = objectInput.readUTF();
		erc721Addr = objectInput.readUTF();
		title = objectInput.readUTF();
		description = objectInput.readUTF();
		addTime = objectInput.readLong();
		updateTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (itemId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(itemId);
		}

		if (ownerUid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ownerUid);
		}

		if (erc20Addr == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(erc20Addr);
		}

		if (tokenCurrency == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tokenCurrency);
		}

		if (erc721Addr == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(erc721Addr);
		}

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		objectOutput.writeLong(addTime);
		objectOutput.writeLong(updateTime);
	}

	public String itemId;
	public String ownerUid;
	public String erc20Addr;
	public String tokenCurrency;
	public String erc721Addr;
	public String title;
	public String description;
	public long addTime;
	public long updateTime;
}