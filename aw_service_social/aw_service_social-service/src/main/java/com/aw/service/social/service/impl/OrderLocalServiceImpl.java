/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.aw.common.util.service.CommonUtilLocalServiceUtil;
import com.aw.common.util.service.ErrorCodeLocalServiceUtil;
import com.aw.service.social.model.ConvertRate;
import com.aw.service.social.model.Order;
import com.aw.service.social.model.Transaction;
import com.aw.service.social.service.ConvertRateLocalServiceUtil;
import com.aw.service.social.service.TransactionLocalServiceUtil;
import com.aw.service.social.service.base.OrderLocalServiceBaseImpl;
import com.aw.service.social.service.persistence.OrderUtil;
import com.aw.service.social.service.persistence.TransactionUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

/**
 * The implementation of the order local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.service.social.service.OrderLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see OrderLocalServiceBaseImpl
 * @see com.aw.service.social.service.OrderLocalServiceUtil
 */
public class OrderLocalServiceImpl extends OrderLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.service.social.service.OrderLocalServiceUtil} to access the order local service.
	 */
	//create an order(with 2 transactions) to purchase erc-20
	public JSONObject createOrder(String userUid, String payType, String payCurrency, String getCurrency, 
			double amount, String walletAddress, String locale){
		
		Date date = new Date();
		
		//create order
		Order order = OrderUtil.create("ORD"+date.getTime());
		order.setUserUid(userUid);
		order.setUserWalletAddr(walletAddress);
		order.setFromType(payType);
		order.setFromCurrency(payCurrency);
		order.setToCurrency(getCurrency);
		order.setFromAmount(amount);
		order.setChargedFlag("N");
		order.setSentFlag("N");
		order.setCompletedFlag("N");
		order.setAddTime(date);
		order.setUpdateTime(date);
		
		//create charge transaction
		TransactionLocalServiceUtil.createTransaction("TIN"+date.getTime()
		, order.getOrderId(), walletAddress, "C", payCurrency, amount);
		
		//calculate rate and amount for payout transaction
		ConvertRate cr = ConvertRateLocalServiceUtil.getActiveRate(payCurrency, getCurrency);
		double toAmount = amount * cr.getRate();
		
		order.setToAmount(toAmount);
		order.setRateId(cr.getRateId());
		
		//create payout transaction
		TransactionLocalServiceUtil.createTransaction("TOUT"+date.getTime()
		, order.getOrderId(), walletAddress, "P", getCurrency, toAmount);
				
		//persist order
		order.persist();
		
		return CommonUtilLocalServiceUtil.getResponse(CommonUtilLocalServiceUtil.getJson(order)
				, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	
	//change order status to paid if payment success
	public JSONObject orderPaid(String orderId, String locale){
		
		Order order = OrderUtil.fetchByPrimaryKey(orderId);
		List<Transaction> list = TransactionUtil.findByorder(orderId);
		Transaction t = null;
		if(list != null && !list.isEmpty()){
			for(Transaction tr : list){
				if(tr.getType().equalsIgnoreCase("C")){
					t = tr;
					break;
				}
			}
		}
		if(order != null && t != null){
			t.setStatus("DONE");
			t.setUpdateTime(new Date());
			order.setChargedFlag("Y");
			order.setUpdateTime(new Date());
			t.persist();
			order.persist();
		}
		
		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	
	//change order status to sent and complete if sent out erc-20 successfully
	public JSONObject orderComplete(String orderId, String locale){
		
		Order order = OrderUtil.fetchByPrimaryKey(orderId);
		List<Transaction> list = TransactionUtil.findByorder(orderId);
		Transaction t = null;
		if(list != null && !list.isEmpty()){
			for(Transaction tr : list){
				if(tr.getType().equalsIgnoreCase("P")){
					t = tr;
					break;
				}
			}
		}
		if(order != null && t != null){
			t.setStatus("DONE");
			t.setUpdateTime(new Date());
			order.setSentFlag("Y");
			order.setCompletedFlag("Y");
			order.setUpdateTime(new Date());
			t.persist();
			order.persist();
		}
		
		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	
	//TODO
	public JSONObject cancelOrder(String orderId){
		//get order and check status
		//if completed, then can not cancle
		//if not paid, change order and 2 transactions
		//is paid but not sent, change order and "P" transaction, start refund for "C" transaction
		
		return null;
	}
	
	//list all orders, can input filter flag, default will be all if null of flag
	public JSONObject getAllOrders(String chargeFlag, String sentFlag, String locale){//admin
		JSONArray json = JSONFactoryUtil.createJSONArray();
		
		List<Order> list = OrderUtil.findAll();
		
		if(list != null && !list.isEmpty()){
			for(Order o : list){
				//don't put whole order considering about the data load
				JSONObject j = JSONFactoryUtil.createJSONObject();
				j.put("orderId", o.getOrderId());
				j.put("fromCurrency", o.getFromCurrency());
				j.put("toCurrency", o.getToCurrency());
				j.put("userUid", o.getUserUid());
				j.put("userWallet", o.getUserWalletAddr());
				j.put("fromAmt", o.getFromAmount());
				j.put("toAmt", o.getToAmount());
				j.put("charged", o.getChargedFlag());
				j.put("sent", o.getSentFlag());
				j.put("completed", o.getCompletedFlag());
				
				json.put(j);
			}
		}
		
		return CommonUtilLocalServiceUtil.getResponse(null, json,
				ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	
	//get order details (includes transaction info)
	public JSONObject getOrderDetail(String orderId, String locale){//admin
		JSONObject json = CommonUtilLocalServiceUtil.getJson(OrderUtil.fetchByPrimaryKey(orderId));//JSONFactoryUtil.createJSONObject();
		
		List<Transaction> list = TransactionUtil.findByorder(orderId);
		int count = 1;
		if(list != null && !list.isEmpty()){
			for(Transaction t : list){
				json.put("transaction"+count, CommonUtilLocalServiceUtil.getJson(t));
				count++;
			}
		}		
		
		return CommonUtilLocalServiceUtil.getResponse(json, null,
				ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	
	// get money raise total in certain currency
	public JSONObject getMoneyRaise(String fromCurrency, String locale) {
		List<Order> list = OrderUtil.findAll();
		
		if(fromCurrency == null || fromCurrency.isEmpty()) {
			JSONArray json = JSONFactoryUtil.createJSONArray();
			
			if(list != null && !list.isEmpty()){
				Map<String, Double>hm = new HashMap<>();
				
				for(Order o : list) {
					if(!hm.containsKey(o.getFromCurrency())) {
						hm.put(o.getFromCurrency(), o.getFromAmount());
					} else {
						hm.put(o.getFromCurrency(), hm.get(o.getFromCurrency()) + o.getFromAmount());
					}
				}
				
				for(String key : hm.keySet()) {
					JSONObject j = JSONFactoryUtil.createJSONObject();
					j.put(key, hm.get(key));
					
					json.put(j);
				}
			}
			
			return CommonUtilLocalServiceUtil.getResponse(null, json,
					ErrorCodeLocalServiceUtil.getSuccess(), locale);
		}
		

		JSONObject json = JSONFactoryUtil.createJSONObject();
		double amount = 0;
		for(Order o :list) {
			if(o.getFromCurrency().equals(fromCurrency)) {
				amount += o.getFromAmount();
			}
		}
		json.put(fromCurrency, amount);
		
		return CommonUtilLocalServiceUtil.getResponse(json, null,
				ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	// get issued coin and its total
	public JSONObject getCoinIssued(String toCurrency, String locale) {
		List<Order> list = OrderUtil.findAll();
		
		if(toCurrency == null || toCurrency.isEmpty()) {
			JSONArray json = JSONFactoryUtil.createJSONArray();
			
			if(list != null && !list.isEmpty()){
				Map<String, Double>hm = new HashMap<>();
				
				for(Order o : list) {
					if(!hm.containsKey(o.getToCurrency())) {
						hm.put(o.getToCurrency(), o.getToAmount());
					} else {
						hm.put(o.getToCurrency(), hm.get(o.getToCurrency()) + o.getToAmount());
					}
				}
				
				for(String key : hm.keySet()) {
					JSONObject j = JSONFactoryUtil.createJSONObject();
					j.put(key, hm.get(key));
					
					json.put(j);
				}
			}
			
			return CommonUtilLocalServiceUtil.getResponse(null, json,
					ErrorCodeLocalServiceUtil.getSuccess(), locale);
		}
		

		JSONObject json = JSONFactoryUtil.createJSONObject();
		double amount = 0;
		for(Order o :list) {
			if(o.getToCurrency().equals(toCurrency)) {
				amount += o.getToAmount();
			}
		}
		json.put(toCurrency, amount);
		
		return CommonUtilLocalServiceUtil.getResponse(json, null,
				ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
}