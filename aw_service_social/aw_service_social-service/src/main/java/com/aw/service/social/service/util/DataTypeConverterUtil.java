package com.aw.service.social.service.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class DataTypeConverterUtil {
	public static String stackTraceToString(Exception e){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String sStackTrace = sw.toString();
		return sStackTrace;
	}
}
