/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.http;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.service.UserInfoServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link UserInfoServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserInfoServiceHttp
 * @see UserInfoServiceUtil
 * @generated
 */
@ProviderType
public class UserInfoServiceSoap {
	public static java.lang.String getUserInfo(java.lang.String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = UserInfoServiceUtil.getUserInfo(locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String updateBasicUserInfo(
		java.lang.String birthday, java.lang.String firstName,
		java.lang.String middleName, java.lang.String lastName,
		java.lang.String title, java.lang.String selfDesc,
		java.lang.String secEmail, java.lang.String phone,
		java.lang.String extension, java.lang.String cellPhone,
		java.lang.String locale) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = UserInfoServiceUtil.updateBasicUserInfo(birthday,
					firstName, middleName, lastName, title, selfDesc, secEmail,
					phone, extension, cellPhone, locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String updateUserAddressInfo(
		java.lang.String addressType, java.lang.String street1,
		java.lang.String street2, java.lang.String street3,
		java.lang.String city, java.lang.String regionId,
		java.lang.String countryId, java.lang.String zipCode,
		java.lang.String isMailing, java.lang.String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = UserInfoServiceUtil.updateUserAddressInfo(addressType,
					street1, street2, street3, city, regionId, countryId,
					zipCode, isMailing, locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String updateSensitiveInfo(
		java.lang.String IdIssueCountryId, java.lang.String IdType,
		java.lang.String IdNumber, java.lang.String ssn, java.lang.String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = UserInfoServiceUtil.updateSensitiveInfo(IdIssueCountryId,
					IdType, IdNumber, ssn, locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String updateIdImage(java.lang.String fileType,
		byte[] IdImage, java.lang.String isActive, java.lang.String publicFlag,
		java.lang.String locale) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = UserInfoServiceUtil.updateIdImage(fileType,
					IdImage, isActive, publicFlag, locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getCountryName(long countryId)
		throws RemoteException {
		try {
			java.lang.String returnValue = UserInfoServiceUtil.getCountryName(countryId);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getRegionName(long regionId)
		throws RemoteException {
		try {
			java.lang.String returnValue = UserInfoServiceUtil.getRegionName(regionId);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getCountryList(java.lang.String locale)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = UserInfoServiceUtil.getCountryList(locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getRegionList(java.lang.String countryId,
		java.lang.String locale) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = UserInfoServiceUtil.getRegionList(countryId,
					locale);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(UserInfoServiceSoap.class);
}