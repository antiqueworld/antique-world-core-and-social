/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.exception.NoSuchConvertRateException;
import com.aw.service.social.model.ConvertRate;
import com.aw.service.social.model.impl.ConvertRateImpl;
import com.aw.service.social.model.impl.ConvertRateModelImpl;
import com.aw.service.social.service.persistence.ConvertRatePersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the convert rate service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ConvertRatePersistence
 * @see com.aw.service.social.service.persistence.ConvertRateUtil
 * @generated
 */
@ProviderType
public class ConvertRatePersistenceImpl extends BasePersistenceImpl<ConvertRate>
	implements ConvertRatePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ConvertRateUtil} to access the convert rate persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ConvertRateImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateModelImpl.FINDER_CACHE_ENABLED, ConvertRateImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateModelImpl.FINDER_CACHE_ENABLED, ConvertRateImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PAIR = new FinderPath(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateModelImpl.FINDER_CACHE_ENABLED, ConvertRateImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBypair",
			new String[] {
				String.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PAIR = new FinderPath(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateModelImpl.FINDER_CACHE_ENABLED, ConvertRateImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBypair",
			new String[] { String.class.getName(), String.class.getName() },
			ConvertRateModelImpl.FROMCURRENCY_COLUMN_BITMASK |
			ConvertRateModelImpl.TOCURRENCY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PAIR = new FinderPath(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBypair",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	 *
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 * @return the matching convert rates
	 */
	@Override
	public List<ConvertRate> findBypair(String fromCurrency, String toCurrency) {
		return findBypair(fromCurrency, toCurrency, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 * @param start the lower bound of the range of convert rates
	 * @param end the upper bound of the range of convert rates (not inclusive)
	 * @return the range of matching convert rates
	 */
	@Override
	public List<ConvertRate> findBypair(String fromCurrency, String toCurrency,
		int start, int end) {
		return findBypair(fromCurrency, toCurrency, start, end, null);
	}

	/**
	 * Returns an ordered range of all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 * @param start the lower bound of the range of convert rates
	 * @param end the upper bound of the range of convert rates (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching convert rates
	 */
	@Override
	public List<ConvertRate> findBypair(String fromCurrency, String toCurrency,
		int start, int end, OrderByComparator<ConvertRate> orderByComparator) {
		return findBypair(fromCurrency, toCurrency, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 * @param start the lower bound of the range of convert rates
	 * @param end the upper bound of the range of convert rates (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching convert rates
	 */
	@Override
	public List<ConvertRate> findBypair(String fromCurrency, String toCurrency,
		int start, int end, OrderByComparator<ConvertRate> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PAIR;
			finderArgs = new Object[] { fromCurrency, toCurrency };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PAIR;
			finderArgs = new Object[] {
					fromCurrency, toCurrency,
					
					start, end, orderByComparator
				};
		}

		List<ConvertRate> list = null;

		if (retrieveFromCache) {
			list = (List<ConvertRate>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ConvertRate convertRate : list) {
					if (!Objects.equals(fromCurrency,
								convertRate.getFromCurrency()) ||
							!Objects.equals(toCurrency,
								convertRate.getToCurrency())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CONVERTRATE_WHERE);

			boolean bindFromCurrency = false;

			if (fromCurrency == null) {
				query.append(_FINDER_COLUMN_PAIR_FROMCURRENCY_1);
			}
			else if (fromCurrency.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PAIR_FROMCURRENCY_3);
			}
			else {
				bindFromCurrency = true;

				query.append(_FINDER_COLUMN_PAIR_FROMCURRENCY_2);
			}

			boolean bindToCurrency = false;

			if (toCurrency == null) {
				query.append(_FINDER_COLUMN_PAIR_TOCURRENCY_1);
			}
			else if (toCurrency.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PAIR_TOCURRENCY_3);
			}
			else {
				bindToCurrency = true;

				query.append(_FINDER_COLUMN_PAIR_TOCURRENCY_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ConvertRateModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindFromCurrency) {
					qPos.add(fromCurrency);
				}

				if (bindToCurrency) {
					qPos.add(toCurrency);
				}

				if (!pagination) {
					list = (List<ConvertRate>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ConvertRate>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	 *
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching convert rate
	 * @throws NoSuchConvertRateException if a matching convert rate could not be found
	 */
	@Override
	public ConvertRate findBypair_First(String fromCurrency, String toCurrency,
		OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException {
		ConvertRate convertRate = fetchBypair_First(fromCurrency, toCurrency,
				orderByComparator);

		if (convertRate != null) {
			return convertRate;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("fromCurrency=");
		msg.append(fromCurrency);

		msg.append(", toCurrency=");
		msg.append(toCurrency);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchConvertRateException(msg.toString());
	}

	/**
	 * Returns the first convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	 *
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching convert rate, or <code>null</code> if a matching convert rate could not be found
	 */
	@Override
	public ConvertRate fetchBypair_First(String fromCurrency,
		String toCurrency, OrderByComparator<ConvertRate> orderByComparator) {
		List<ConvertRate> list = findBypair(fromCurrency, toCurrency, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	 *
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching convert rate
	 * @throws NoSuchConvertRateException if a matching convert rate could not be found
	 */
	@Override
	public ConvertRate findBypair_Last(String fromCurrency, String toCurrency,
		OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException {
		ConvertRate convertRate = fetchBypair_Last(fromCurrency, toCurrency,
				orderByComparator);

		if (convertRate != null) {
			return convertRate;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("fromCurrency=");
		msg.append(fromCurrency);

		msg.append(", toCurrency=");
		msg.append(toCurrency);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchConvertRateException(msg.toString());
	}

	/**
	 * Returns the last convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	 *
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching convert rate, or <code>null</code> if a matching convert rate could not be found
	 */
	@Override
	public ConvertRate fetchBypair_Last(String fromCurrency, String toCurrency,
		OrderByComparator<ConvertRate> orderByComparator) {
		int count = countBypair(fromCurrency, toCurrency);

		if (count == 0) {
			return null;
		}

		List<ConvertRate> list = findBypair(fromCurrency, toCurrency,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the convert rates before and after the current convert rate in the ordered set where fromCurrency = &#63; and toCurrency = &#63;.
	 *
	 * @param rateId the primary key of the current convert rate
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next convert rate
	 * @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	 */
	@Override
	public ConvertRate[] findBypair_PrevAndNext(String rateId,
		String fromCurrency, String toCurrency,
		OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException {
		ConvertRate convertRate = findByPrimaryKey(rateId);

		Session session = null;

		try {
			session = openSession();

			ConvertRate[] array = new ConvertRateImpl[3];

			array[0] = getBypair_PrevAndNext(session, convertRate,
					fromCurrency, toCurrency, orderByComparator, true);

			array[1] = convertRate;

			array[2] = getBypair_PrevAndNext(session, convertRate,
					fromCurrency, toCurrency, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ConvertRate getBypair_PrevAndNext(Session session,
		ConvertRate convertRate, String fromCurrency, String toCurrency,
		OrderByComparator<ConvertRate> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_CONVERTRATE_WHERE);

		boolean bindFromCurrency = false;

		if (fromCurrency == null) {
			query.append(_FINDER_COLUMN_PAIR_FROMCURRENCY_1);
		}
		else if (fromCurrency.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PAIR_FROMCURRENCY_3);
		}
		else {
			bindFromCurrency = true;

			query.append(_FINDER_COLUMN_PAIR_FROMCURRENCY_2);
		}

		boolean bindToCurrency = false;

		if (toCurrency == null) {
			query.append(_FINDER_COLUMN_PAIR_TOCURRENCY_1);
		}
		else if (toCurrency.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PAIR_TOCURRENCY_3);
		}
		else {
			bindToCurrency = true;

			query.append(_FINDER_COLUMN_PAIR_TOCURRENCY_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ConvertRateModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindFromCurrency) {
			qPos.add(fromCurrency);
		}

		if (bindToCurrency) {
			qPos.add(toCurrency);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(convertRate);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ConvertRate> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the convert rates where fromCurrency = &#63; and toCurrency = &#63; from the database.
	 *
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 */
	@Override
	public void removeBypair(String fromCurrency, String toCurrency) {
		for (ConvertRate convertRate : findBypair(fromCurrency, toCurrency,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(convertRate);
		}
	}

	/**
	 * Returns the number of convert rates where fromCurrency = &#63; and toCurrency = &#63;.
	 *
	 * @param fromCurrency the from currency
	 * @param toCurrency the to currency
	 * @return the number of matching convert rates
	 */
	@Override
	public int countBypair(String fromCurrency, String toCurrency) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PAIR;

		Object[] finderArgs = new Object[] { fromCurrency, toCurrency };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CONVERTRATE_WHERE);

			boolean bindFromCurrency = false;

			if (fromCurrency == null) {
				query.append(_FINDER_COLUMN_PAIR_FROMCURRENCY_1);
			}
			else if (fromCurrency.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PAIR_FROMCURRENCY_3);
			}
			else {
				bindFromCurrency = true;

				query.append(_FINDER_COLUMN_PAIR_FROMCURRENCY_2);
			}

			boolean bindToCurrency = false;

			if (toCurrency == null) {
				query.append(_FINDER_COLUMN_PAIR_TOCURRENCY_1);
			}
			else if (toCurrency.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PAIR_TOCURRENCY_3);
			}
			else {
				bindToCurrency = true;

				query.append(_FINDER_COLUMN_PAIR_TOCURRENCY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindFromCurrency) {
					qPos.add(fromCurrency);
				}

				if (bindToCurrency) {
					qPos.add(toCurrency);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PAIR_FROMCURRENCY_1 = "convertRate.fromCurrency IS NULL AND ";
	private static final String _FINDER_COLUMN_PAIR_FROMCURRENCY_2 = "convertRate.fromCurrency = ? AND ";
	private static final String _FINDER_COLUMN_PAIR_FROMCURRENCY_3 = "(convertRate.fromCurrency IS NULL OR convertRate.fromCurrency = '') AND ";
	private static final String _FINDER_COLUMN_PAIR_TOCURRENCY_1 = "convertRate.toCurrency IS NULL";
	private static final String _FINDER_COLUMN_PAIR_TOCURRENCY_2 = "convertRate.toCurrency = ?";
	private static final String _FINDER_COLUMN_PAIR_TOCURRENCY_3 = "(convertRate.toCurrency IS NULL OR convertRate.toCurrency = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ACTIVE = new FinderPath(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateModelImpl.FINDER_CACHE_ENABLED, ConvertRateImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByactive",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVE =
		new FinderPath(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateModelImpl.FINDER_CACHE_ENABLED, ConvertRateImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByactive",
			new String[] { String.class.getName() },
			ConvertRateModelImpl.ISACTIVE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ACTIVE = new FinderPath(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByactive",
			new String[] { String.class.getName() });

	/**
	 * Returns all the convert rates where isActive = &#63;.
	 *
	 * @param isActive the is active
	 * @return the matching convert rates
	 */
	@Override
	public List<ConvertRate> findByactive(String isActive) {
		return findByactive(isActive, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the convert rates where isActive = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param isActive the is active
	 * @param start the lower bound of the range of convert rates
	 * @param end the upper bound of the range of convert rates (not inclusive)
	 * @return the range of matching convert rates
	 */
	@Override
	public List<ConvertRate> findByactive(String isActive, int start, int end) {
		return findByactive(isActive, start, end, null);
	}

	/**
	 * Returns an ordered range of all the convert rates where isActive = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param isActive the is active
	 * @param start the lower bound of the range of convert rates
	 * @param end the upper bound of the range of convert rates (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching convert rates
	 */
	@Override
	public List<ConvertRate> findByactive(String isActive, int start, int end,
		OrderByComparator<ConvertRate> orderByComparator) {
		return findByactive(isActive, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the convert rates where isActive = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param isActive the is active
	 * @param start the lower bound of the range of convert rates
	 * @param end the upper bound of the range of convert rates (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching convert rates
	 */
	@Override
	public List<ConvertRate> findByactive(String isActive, int start, int end,
		OrderByComparator<ConvertRate> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVE;
			finderArgs = new Object[] { isActive };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ACTIVE;
			finderArgs = new Object[] { isActive, start, end, orderByComparator };
		}

		List<ConvertRate> list = null;

		if (retrieveFromCache) {
			list = (List<ConvertRate>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ConvertRate convertRate : list) {
					if (!Objects.equals(isActive, convertRate.getIsActive())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CONVERTRATE_WHERE);

			boolean bindIsActive = false;

			if (isActive == null) {
				query.append(_FINDER_COLUMN_ACTIVE_ISACTIVE_1);
			}
			else if (isActive.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ACTIVE_ISACTIVE_3);
			}
			else {
				bindIsActive = true;

				query.append(_FINDER_COLUMN_ACTIVE_ISACTIVE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ConvertRateModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIsActive) {
					qPos.add(isActive);
				}

				if (!pagination) {
					list = (List<ConvertRate>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ConvertRate>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first convert rate in the ordered set where isActive = &#63;.
	 *
	 * @param isActive the is active
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching convert rate
	 * @throws NoSuchConvertRateException if a matching convert rate could not be found
	 */
	@Override
	public ConvertRate findByactive_First(String isActive,
		OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException {
		ConvertRate convertRate = fetchByactive_First(isActive,
				orderByComparator);

		if (convertRate != null) {
			return convertRate;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("isActive=");
		msg.append(isActive);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchConvertRateException(msg.toString());
	}

	/**
	 * Returns the first convert rate in the ordered set where isActive = &#63;.
	 *
	 * @param isActive the is active
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching convert rate, or <code>null</code> if a matching convert rate could not be found
	 */
	@Override
	public ConvertRate fetchByactive_First(String isActive,
		OrderByComparator<ConvertRate> orderByComparator) {
		List<ConvertRate> list = findByactive(isActive, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last convert rate in the ordered set where isActive = &#63;.
	 *
	 * @param isActive the is active
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching convert rate
	 * @throws NoSuchConvertRateException if a matching convert rate could not be found
	 */
	@Override
	public ConvertRate findByactive_Last(String isActive,
		OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException {
		ConvertRate convertRate = fetchByactive_Last(isActive, orderByComparator);

		if (convertRate != null) {
			return convertRate;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("isActive=");
		msg.append(isActive);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchConvertRateException(msg.toString());
	}

	/**
	 * Returns the last convert rate in the ordered set where isActive = &#63;.
	 *
	 * @param isActive the is active
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching convert rate, or <code>null</code> if a matching convert rate could not be found
	 */
	@Override
	public ConvertRate fetchByactive_Last(String isActive,
		OrderByComparator<ConvertRate> orderByComparator) {
		int count = countByactive(isActive);

		if (count == 0) {
			return null;
		}

		List<ConvertRate> list = findByactive(isActive, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the convert rates before and after the current convert rate in the ordered set where isActive = &#63;.
	 *
	 * @param rateId the primary key of the current convert rate
	 * @param isActive the is active
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next convert rate
	 * @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	 */
	@Override
	public ConvertRate[] findByactive_PrevAndNext(String rateId,
		String isActive, OrderByComparator<ConvertRate> orderByComparator)
		throws NoSuchConvertRateException {
		ConvertRate convertRate = findByPrimaryKey(rateId);

		Session session = null;

		try {
			session = openSession();

			ConvertRate[] array = new ConvertRateImpl[3];

			array[0] = getByactive_PrevAndNext(session, convertRate, isActive,
					orderByComparator, true);

			array[1] = convertRate;

			array[2] = getByactive_PrevAndNext(session, convertRate, isActive,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ConvertRate getByactive_PrevAndNext(Session session,
		ConvertRate convertRate, String isActive,
		OrderByComparator<ConvertRate> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CONVERTRATE_WHERE);

		boolean bindIsActive = false;

		if (isActive == null) {
			query.append(_FINDER_COLUMN_ACTIVE_ISACTIVE_1);
		}
		else if (isActive.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ACTIVE_ISACTIVE_3);
		}
		else {
			bindIsActive = true;

			query.append(_FINDER_COLUMN_ACTIVE_ISACTIVE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ConvertRateModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindIsActive) {
			qPos.add(isActive);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(convertRate);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ConvertRate> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the convert rates where isActive = &#63; from the database.
	 *
	 * @param isActive the is active
	 */
	@Override
	public void removeByactive(String isActive) {
		for (ConvertRate convertRate : findByactive(isActive,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(convertRate);
		}
	}

	/**
	 * Returns the number of convert rates where isActive = &#63;.
	 *
	 * @param isActive the is active
	 * @return the number of matching convert rates
	 */
	@Override
	public int countByactive(String isActive) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ACTIVE;

		Object[] finderArgs = new Object[] { isActive };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CONVERTRATE_WHERE);

			boolean bindIsActive = false;

			if (isActive == null) {
				query.append(_FINDER_COLUMN_ACTIVE_ISACTIVE_1);
			}
			else if (isActive.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ACTIVE_ISACTIVE_3);
			}
			else {
				bindIsActive = true;

				query.append(_FINDER_COLUMN_ACTIVE_ISACTIVE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIsActive) {
					qPos.add(isActive);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ACTIVE_ISACTIVE_1 = "convertRate.isActive IS NULL";
	private static final String _FINDER_COLUMN_ACTIVE_ISACTIVE_2 = "convertRate.isActive = ?";
	private static final String _FINDER_COLUMN_ACTIVE_ISACTIVE_3 = "(convertRate.isActive IS NULL OR convertRate.isActive = '')";

	public ConvertRatePersistenceImpl() {
		setModelClass(ConvertRate.class);
	}

	/**
	 * Caches the convert rate in the entity cache if it is enabled.
	 *
	 * @param convertRate the convert rate
	 */
	@Override
	public void cacheResult(ConvertRate convertRate) {
		entityCache.putResult(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateImpl.class, convertRate.getPrimaryKey(), convertRate);

		convertRate.resetOriginalValues();
	}

	/**
	 * Caches the convert rates in the entity cache if it is enabled.
	 *
	 * @param convertRates the convert rates
	 */
	@Override
	public void cacheResult(List<ConvertRate> convertRates) {
		for (ConvertRate convertRate : convertRates) {
			if (entityCache.getResult(
						ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
						ConvertRateImpl.class, convertRate.getPrimaryKey()) == null) {
				cacheResult(convertRate);
			}
			else {
				convertRate.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all convert rates.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ConvertRateImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the convert rate.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ConvertRate convertRate) {
		entityCache.removeResult(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateImpl.class, convertRate.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ConvertRate> convertRates) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ConvertRate convertRate : convertRates) {
			entityCache.removeResult(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
				ConvertRateImpl.class, convertRate.getPrimaryKey());
		}
	}

	/**
	 * Creates a new convert rate with the primary key. Does not add the convert rate to the database.
	 *
	 * @param rateId the primary key for the new convert rate
	 * @return the new convert rate
	 */
	@Override
	public ConvertRate create(String rateId) {
		ConvertRate convertRate = new ConvertRateImpl();

		convertRate.setNew(true);
		convertRate.setPrimaryKey(rateId);

		return convertRate;
	}

	/**
	 * Removes the convert rate with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param rateId the primary key of the convert rate
	 * @return the convert rate that was removed
	 * @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	 */
	@Override
	public ConvertRate remove(String rateId) throws NoSuchConvertRateException {
		return remove((Serializable)rateId);
	}

	/**
	 * Removes the convert rate with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the convert rate
	 * @return the convert rate that was removed
	 * @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	 */
	@Override
	public ConvertRate remove(Serializable primaryKey)
		throws NoSuchConvertRateException {
		Session session = null;

		try {
			session = openSession();

			ConvertRate convertRate = (ConvertRate)session.get(ConvertRateImpl.class,
					primaryKey);

			if (convertRate == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchConvertRateException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(convertRate);
		}
		catch (NoSuchConvertRateException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ConvertRate removeImpl(ConvertRate convertRate) {
		convertRate = toUnwrappedModel(convertRate);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(convertRate)) {
				convertRate = (ConvertRate)session.get(ConvertRateImpl.class,
						convertRate.getPrimaryKeyObj());
			}

			if (convertRate != null) {
				session.delete(convertRate);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (convertRate != null) {
			clearCache(convertRate);
		}

		return convertRate;
	}

	@Override
	public ConvertRate updateImpl(ConvertRate convertRate) {
		convertRate = toUnwrappedModel(convertRate);

		boolean isNew = convertRate.isNew();

		ConvertRateModelImpl convertRateModelImpl = (ConvertRateModelImpl)convertRate;

		Session session = null;

		try {
			session = openSession();

			if (convertRate.isNew()) {
				session.save(convertRate);

				convertRate.setNew(false);
			}
			else {
				convertRate = (ConvertRate)session.merge(convertRate);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!ConvertRateModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] {
					convertRateModelImpl.getFromCurrency(),
					convertRateModelImpl.getToCurrency()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PAIR, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PAIR,
				args);

			args = new Object[] { convertRateModelImpl.getIsActive() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_ACTIVE, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVE,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((convertRateModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PAIR.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						convertRateModelImpl.getOriginalFromCurrency(),
						convertRateModelImpl.getOriginalToCurrency()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PAIR, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PAIR,
					args);

				args = new Object[] {
						convertRateModelImpl.getFromCurrency(),
						convertRateModelImpl.getToCurrency()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PAIR, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PAIR,
					args);
			}

			if ((convertRateModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						convertRateModelImpl.getOriginalIsActive()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_ACTIVE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVE,
					args);

				args = new Object[] { convertRateModelImpl.getIsActive() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_ACTIVE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVE,
					args);
			}
		}

		entityCache.putResult(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
			ConvertRateImpl.class, convertRate.getPrimaryKey(), convertRate,
			false);

		convertRate.resetOriginalValues();

		return convertRate;
	}

	protected ConvertRate toUnwrappedModel(ConvertRate convertRate) {
		if (convertRate instanceof ConvertRateImpl) {
			return convertRate;
		}

		ConvertRateImpl convertRateImpl = new ConvertRateImpl();

		convertRateImpl.setNew(convertRate.isNew());
		convertRateImpl.setPrimaryKey(convertRate.getPrimaryKey());

		convertRateImpl.setRateId(convertRate.getRateId());
		convertRateImpl.setFromCurrency(convertRate.getFromCurrency());
		convertRateImpl.setToCurrency(convertRate.getToCurrency());
		convertRateImpl.setIsActive(convertRate.getIsActive());
		convertRateImpl.setRate(convertRate.getRate());
		convertRateImpl.setStartTime(convertRate.getStartTime());
		convertRateImpl.setExpireTime(convertRate.getExpireTime());

		return convertRateImpl;
	}

	/**
	 * Returns the convert rate with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the convert rate
	 * @return the convert rate
	 * @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	 */
	@Override
	public ConvertRate findByPrimaryKey(Serializable primaryKey)
		throws NoSuchConvertRateException {
		ConvertRate convertRate = fetchByPrimaryKey(primaryKey);

		if (convertRate == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchConvertRateException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return convertRate;
	}

	/**
	 * Returns the convert rate with the primary key or throws a {@link NoSuchConvertRateException} if it could not be found.
	 *
	 * @param rateId the primary key of the convert rate
	 * @return the convert rate
	 * @throws NoSuchConvertRateException if a convert rate with the primary key could not be found
	 */
	@Override
	public ConvertRate findByPrimaryKey(String rateId)
		throws NoSuchConvertRateException {
		return findByPrimaryKey((Serializable)rateId);
	}

	/**
	 * Returns the convert rate with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the convert rate
	 * @return the convert rate, or <code>null</code> if a convert rate with the primary key could not be found
	 */
	@Override
	public ConvertRate fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
				ConvertRateImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ConvertRate convertRate = (ConvertRate)serializable;

		if (convertRate == null) {
			Session session = null;

			try {
				session = openSession();

				convertRate = (ConvertRate)session.get(ConvertRateImpl.class,
						primaryKey);

				if (convertRate != null) {
					cacheResult(convertRate);
				}
				else {
					entityCache.putResult(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
						ConvertRateImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
					ConvertRateImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return convertRate;
	}

	/**
	 * Returns the convert rate with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param rateId the primary key of the convert rate
	 * @return the convert rate, or <code>null</code> if a convert rate with the primary key could not be found
	 */
	@Override
	public ConvertRate fetchByPrimaryKey(String rateId) {
		return fetchByPrimaryKey((Serializable)rateId);
	}

	@Override
	public Map<Serializable, ConvertRate> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ConvertRate> map = new HashMap<Serializable, ConvertRate>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ConvertRate convertRate = fetchByPrimaryKey(primaryKey);

			if (convertRate != null) {
				map.put(primaryKey, convertRate);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
					ConvertRateImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ConvertRate)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_CONVERTRATE_WHERE_PKS_IN);

		for (int i = 0; i < uncachedPrimaryKeys.size(); i++) {
			query.append(StringPool.QUESTION);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			QueryPos qPos = QueryPos.getInstance(q);

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				qPos.add((String)primaryKey);
			}

			for (ConvertRate convertRate : (List<ConvertRate>)q.list()) {
				map.put(convertRate.getPrimaryKeyObj(), convertRate);

				cacheResult(convertRate);

				uncachedPrimaryKeys.remove(convertRate.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ConvertRateModelImpl.ENTITY_CACHE_ENABLED,
					ConvertRateImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the convert rates.
	 *
	 * @return the convert rates
	 */
	@Override
	public List<ConvertRate> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the convert rates.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of convert rates
	 * @param end the upper bound of the range of convert rates (not inclusive)
	 * @return the range of convert rates
	 */
	@Override
	public List<ConvertRate> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the convert rates.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of convert rates
	 * @param end the upper bound of the range of convert rates (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of convert rates
	 */
	@Override
	public List<ConvertRate> findAll(int start, int end,
		OrderByComparator<ConvertRate> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the convert rates.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ConvertRateModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of convert rates
	 * @param end the upper bound of the range of convert rates (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of convert rates
	 */
	@Override
	public List<ConvertRate> findAll(int start, int end,
		OrderByComparator<ConvertRate> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ConvertRate> list = null;

		if (retrieveFromCache) {
			list = (List<ConvertRate>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CONVERTRATE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CONVERTRATE;

				if (pagination) {
					sql = sql.concat(ConvertRateModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ConvertRate>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ConvertRate>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the convert rates from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ConvertRate convertRate : findAll()) {
			remove(convertRate);
		}
	}

	/**
	 * Returns the number of convert rates.
	 *
	 * @return the number of convert rates
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CONVERTRATE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ConvertRateModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the convert rate persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ConvertRateImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_CONVERTRATE = "SELECT convertRate FROM ConvertRate convertRate";
	private static final String _SQL_SELECT_CONVERTRATE_WHERE_PKS_IN = "SELECT convertRate FROM ConvertRate convertRate WHERE rateId IN (";
	private static final String _SQL_SELECT_CONVERTRATE_WHERE = "SELECT convertRate FROM ConvertRate convertRate WHERE ";
	private static final String _SQL_COUNT_CONVERTRATE = "SELECT COUNT(convertRate) FROM ConvertRate convertRate";
	private static final String _SQL_COUNT_CONVERTRATE_WHERE = "SELECT COUNT(convertRate) FROM ConvertRate convertRate WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "convertRate.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ConvertRate exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ConvertRate exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ConvertRatePersistenceImpl.class);
}