package com.aw.service.social.service.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Date;
import java.util.Locale;
import com.aw.common.util.service.CommonUtilLocalServiceUtil;
import com.aw.common.util.service.ErrorCodeLocalServiceUtil;
import com.aw.common.util.service.LogLocalServiceUtil;
import com.aw.service.social.model.AddressExt;
import com.aw.service.social.model.ContactExt;
import com.aw.service.social.model.UserExt;
import com.aw.service.social.service.persistence.AddressExtPK;
import com.aw.service.social.service.persistence.AddressExtUtil;
import com.aw.service.social.service.persistence.ContactExtUtil;
import com.aw.service.social.service.persistence.UserExtUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.LocaleUtil;

public class UserUpdateUtil {

	long COMPANY_ID = 20115;// TODO change it

	public JSONObject updateBasicUserInfo(User user, String birthday, String firstName, String middleName,
			String lastName, String title, String selfDesc, String secEmail, String phone,
			String extension, String cellPhone, String locale) {

		String userUid = user.getUserUuid();
		
		UserExt uExt = UserExtUtil.fetchByPrimaryKey(userUid);
		ContactExt cExt = ContactExtUtil.fetchByPrimaryKey(userUid);
		
		if(uExt == null){
			uExt = UserExtUtil.create(userUid);
			uExt.setUserId(user.getUserId());
			uExt.setAddTime(new Date());
		}
		uExt.setFirstName(firstName);
		uExt.setMiddleName(middleName);
		uExt.setLastName(lastName);		
		uExt.setBirthday(birthday);
		uExt.setTitle(title);
		uExt.setSelfDesc(selfDesc);
		uExt.persist();
		
		if(cExt == null){
			cExt = ContactExtUtil.create(userUid);
			cExt.setPrimEmail(user.getEmailAddress());
			cExt.setAddTime(new Date());
		}
		cExt.setSecEmail(secEmail);
		cExt.setPhone(phone);
		cExt.setExtension(extension);
		cExt.setCellPhone(cellPhone);
		cExt.persist();

		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}

	public JSONObject updateUserAddressInfo(User user, String addressType, String street1, String street2,
			String street3, String city, String regionId, String countryId, String zipCode, String isMailing,
			String locale) {

		try{
			String userUid = user.getUserUuid();
			AddressExtPK apk = new AddressExtPK(userUid, addressType);
			AddressExt a = AddressExtUtil.fetchByPrimaryKey(apk);
			if(a == null){
				a = AddressExtUtil.create(apk);
			}
			a.setStreet1(street1);
			a.setStreet2(street2);
			a.setStreet3(street3);
			a.setCity(city);
			a.setRegionId(regionId);
			a.setCountryId(countryId);
			a.setZipCode(zipCode);
			a.setIsMailing(isMailing);
			a.persist();
		}catch(Exception e){
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getUserNotExist(),locale);
		}

		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}

	public JSONObject updateUserPortrait(User user, byte[] portrait, String locale) {
		try {
			UserLocalServiceUtil.updatePortrait(user.getUserId(), portrait);
		} catch (PortalException e) {
			e.printStackTrace();
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getImageFormatError(), locale);
		}
		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	
	@SuppressWarnings("unused")
	public JSONObject updateIdImage(User user, String fileType, byte[] IdImage, String isActive, String publicFlag,
			String locale) {

		InputStream is = new ByteArrayInputStream(IdImage);
		String fileExt = "";
		String fileSize = String.valueOf(IdImage.length);

		try {
			fileExt = URLConnection.guessContentTypeFromStream(is);
			fileSize = Integer.toString(IdImage.length);
		} catch (IOException e) {
			LogLocalServiceUtil.appLog(this.getClass(), LogLocalServiceUtil.getDebug(),
					"  FileExt cannot be guessed!!! /n" + DataTypeConverterUtil.stackTraceToString(e), null);
		}

		if (fileExt == null)fileExt = "";
		
		String userUid = user.getUserUuid();
		try{
			//TODO do the update/insert;
		}catch(Exception e){
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getUserNotExist(),locale);
		}

		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}

	public JSONObject updateSensitiveInfo(User user, String IdIssueCountryId, String IdType, String IdNumber,
			String ssn, String locale) {
		String userUid = user.getUserUuid();

		UserExt u = UserExtUtil.fetchByPrimaryKey(userUid);
		if(u == null){
			u = UserExtUtil.create(userUid);
			u.setUserId(user.getUserId());
			u.setFirstName(user.getFirstName());
			u.setMiddleName(user.getMiddleName());
			u.setLastName(user.getLastName());			
			u.setAddTime(new Date());
		}
		u.setIdIssueCountry(IdIssueCountryId);
		u.setIdType(IdType);
		u.setIdNumber(IdNumber);
		u.setSsn(ssn);//TODO encrypt later;
		u.persist();

		return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);

	}

	// just for test, leave it for now
	// type option linemoney-reg-user / linemoney-agent
	// wait for the zhiyi's version of password1 password2
	public User addUser(String firstName, String middleName, String lastName, String emailAddress, String password1,
			String password2, int bY, int bM, int bD, String title, String selfDesc, String portraitId) {

		try {
			long creatorUserId = 0;// hard code

			boolean autoPassword = false;
			boolean autoScreenName = true;
			String screenName = "";
			long facebookId = 0;
			String openId = "";

			int prefixId = 0;
			int suffixId = 0;
			boolean male = true;

			String jobTitle = "";
			long[] groupIds = null;
			long[] organizationIds = null;
			long[] roleIds = null;
			long[] userGroupIds = null;
			boolean sendEmail = true;
			ServiceContext serviceContext = null;

			Locale locale = LocaleUtil.getDefault();

			User user = UserLocalServiceUtil.addUser(creatorUserId, COMPANY_ID, autoPassword, password1, password2,
					autoScreenName, screenName, emailAddress, facebookId, openId, locale, firstName, middleName,
					lastName, prefixId, suffixId, male, bM, bD, bY, jobTitle, groupIds, organizationIds, roleIds,
					userGroupIds, sendEmail, serviceContext);

			if (user != null) {
				// initial cassandra user records
				// userId userUUId birth instance.....
				// add userExt and contact, leave address empty
			}

			return user;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@SuppressWarnings("unused")
	private void junkYard() {
		
	}
	

}