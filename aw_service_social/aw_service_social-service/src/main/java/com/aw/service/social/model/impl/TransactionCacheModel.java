/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.Transaction;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Transaction in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Transaction
 * @generated
 */
@ProviderType
public class TransactionCacheModel implements CacheModel<Transaction>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TransactionCacheModel)) {
			return false;
		}

		TransactionCacheModel transactionCacheModel = (TransactionCacheModel)obj;

		if (transactionId.equals(transactionCacheModel.transactionId)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, transactionId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{transactionId=");
		sb.append(transactionId);
		sb.append(", orderId=");
		sb.append(orderId);
		sb.append(", userWalletAddr=");
		sb.append(userWalletAddr);
		sb.append(", type=");
		sb.append(type);
		sb.append(", currency=");
		sb.append(currency);
		sb.append(", amount=");
		sb.append(amount);
		sb.append(", status=");
		sb.append(status);
		sb.append(", addTime=");
		sb.append(addTime);
		sb.append(", updateTime=");
		sb.append(updateTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Transaction toEntityModel() {
		TransactionImpl transactionImpl = new TransactionImpl();

		if (transactionId == null) {
			transactionImpl.setTransactionId(StringPool.BLANK);
		}
		else {
			transactionImpl.setTransactionId(transactionId);
		}

		if (orderId == null) {
			transactionImpl.setOrderId(StringPool.BLANK);
		}
		else {
			transactionImpl.setOrderId(orderId);
		}

		if (userWalletAddr == null) {
			transactionImpl.setUserWalletAddr(StringPool.BLANK);
		}
		else {
			transactionImpl.setUserWalletAddr(userWalletAddr);
		}

		if (type == null) {
			transactionImpl.setType(StringPool.BLANK);
		}
		else {
			transactionImpl.setType(type);
		}

		if (currency == null) {
			transactionImpl.setCurrency(StringPool.BLANK);
		}
		else {
			transactionImpl.setCurrency(currency);
		}

		transactionImpl.setAmount(amount);

		if (status == null) {
			transactionImpl.setStatus(StringPool.BLANK);
		}
		else {
			transactionImpl.setStatus(status);
		}

		if (addTime == Long.MIN_VALUE) {
			transactionImpl.setAddTime(null);
		}
		else {
			transactionImpl.setAddTime(new Date(addTime));
		}

		if (updateTime == Long.MIN_VALUE) {
			transactionImpl.setUpdateTime(null);
		}
		else {
			transactionImpl.setUpdateTime(new Date(updateTime));
		}

		transactionImpl.resetOriginalValues();

		return transactionImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		transactionId = objectInput.readUTF();
		orderId = objectInput.readUTF();
		userWalletAddr = objectInput.readUTF();
		type = objectInput.readUTF();
		currency = objectInput.readUTF();

		amount = objectInput.readDouble();
		status = objectInput.readUTF();
		addTime = objectInput.readLong();
		updateTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (transactionId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(transactionId);
		}

		if (orderId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(orderId);
		}

		if (userWalletAddr == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userWalletAddr);
		}

		if (type == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(type);
		}

		if (currency == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(currency);
		}

		objectOutput.writeDouble(amount);

		if (status == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(status);
		}

		objectOutput.writeLong(addTime);
		objectOutput.writeLong(updateTime);
	}

	public String transactionId;
	public String orderId;
	public String userWalletAddr;
	public String type;
	public String currency;
	public double amount;
	public String status;
	public long addTime;
	public long updateTime;
}