package com.aw.service.social.service.util;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

public class JsonTempUtil {
	public static JSONObject getRespons(boolean isSuccess){
		JSONObject jo = JSONFactoryUtil.createJSONObject();
		if(isSuccess){
			jo.put("status", "success");
		}else{
			jo.put("status", "failed");
		}
		return jo;
	}
}
