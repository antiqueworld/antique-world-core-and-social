/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.impl;

import java.util.Date;

import com.aw.service.social.model.Transaction;
import com.aw.service.social.service.base.TransactionLocalServiceBaseImpl;
import com.aw.service.social.service.persistence.TransactionUtil;

/**
 * The implementation of the transaction local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.service.social.service.TransactionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TransactionLocalServiceBaseImpl
 * @see com.aw.service.social.service.TransactionLocalServiceUtil
 */
public class TransactionLocalServiceImpl extends TransactionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.aw.service.social.service.TransactionLocalServiceUtil} to access the transaction local service.
	 */
	public Transaction createTransaction(String trxId, String orderId, String wallet, String type
			, String currency, double amount){
		//valid input
		if("C".equalsIgnoreCase(type)){
			//is charge then nothing
		}else if("P".equalsIgnoreCase(type)){
			//is payout then wallet should not be null
			if(wallet == null){//TODO maybe use web3j to verify address
				return null;
			}
		}else{
			//invalid type return null
			return null;
		}
		
		Transaction t = TransactionUtil.create(trxId);
		t.setOrderId(orderId);
		t.setUserWalletAddr(wallet);
		t.setType(type);
		t.setCurrency(currency);
		t.setAmount(amount);
		t.setStatus("INIT");
		t.setAddTime(new Date());
		t.persist();
				
		return t;
	}
}