/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.exception.NoSuchTransactionException;
import com.aw.service.social.model.Transaction;
import com.aw.service.social.model.impl.TransactionImpl;
import com.aw.service.social.model.impl.TransactionModelImpl;
import com.aw.service.social.service.persistence.TransactionPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReflectionUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the transaction service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TransactionPersistence
 * @see com.aw.service.social.service.persistence.TransactionUtil
 * @generated
 */
@ProviderType
public class TransactionPersistenceImpl extends BasePersistenceImpl<Transaction>
	implements TransactionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TransactionUtil} to access the transaction persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TransactionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TransactionModelImpl.ENTITY_CACHE_ENABLED,
			TransactionModelImpl.FINDER_CACHE_ENABLED, TransactionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TransactionModelImpl.ENTITY_CACHE_ENABLED,
			TransactionModelImpl.FINDER_CACHE_ENABLED, TransactionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TransactionModelImpl.ENTITY_CACHE_ENABLED,
			TransactionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDER = new FinderPath(TransactionModelImpl.ENTITY_CACHE_ENABLED,
			TransactionModelImpl.FINDER_CACHE_ENABLED, TransactionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByorder",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDER = new FinderPath(TransactionModelImpl.ENTITY_CACHE_ENABLED,
			TransactionModelImpl.FINDER_CACHE_ENABLED, TransactionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByorder",
			new String[] { String.class.getName() },
			TransactionModelImpl.ORDERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORDER = new FinderPath(TransactionModelImpl.ENTITY_CACHE_ENABLED,
			TransactionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByorder",
			new String[] { String.class.getName() });

	/**
	 * Returns all the transactions where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @return the matching transactions
	 */
	@Override
	public List<Transaction> findByorder(String orderId) {
		return findByorder(orderId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the transactions where orderId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param start the lower bound of the range of transactions
	 * @param end the upper bound of the range of transactions (not inclusive)
	 * @return the range of matching transactions
	 */
	@Override
	public List<Transaction> findByorder(String orderId, int start, int end) {
		return findByorder(orderId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the transactions where orderId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param start the lower bound of the range of transactions
	 * @param end the upper bound of the range of transactions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching transactions
	 */
	@Override
	public List<Transaction> findByorder(String orderId, int start, int end,
		OrderByComparator<Transaction> orderByComparator) {
		return findByorder(orderId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the transactions where orderId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param start the lower bound of the range of transactions
	 * @param end the upper bound of the range of transactions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching transactions
	 */
	@Override
	public List<Transaction> findByorder(String orderId, int start, int end,
		OrderByComparator<Transaction> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDER;
			finderArgs = new Object[] { orderId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDER;
			finderArgs = new Object[] { orderId, start, end, orderByComparator };
		}

		List<Transaction> list = null;

		if (retrieveFromCache) {
			list = (List<Transaction>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Transaction transaction : list) {
					if (!Objects.equals(orderId, transaction.getOrderId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TRANSACTION_WHERE);

			boolean bindOrderId = false;

			if (orderId == null) {
				query.append(_FINDER_COLUMN_ORDER_ORDERID_1);
			}
			else if (orderId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ORDER_ORDERID_3);
			}
			else {
				bindOrderId = true;

				query.append(_FINDER_COLUMN_ORDER_ORDERID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TransactionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindOrderId) {
					qPos.add(orderId);
				}

				if (!pagination) {
					list = (List<Transaction>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Transaction>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first transaction in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction
	 * @throws NoSuchTransactionException if a matching transaction could not be found
	 */
	@Override
	public Transaction findByorder_First(String orderId,
		OrderByComparator<Transaction> orderByComparator)
		throws NoSuchTransactionException {
		Transaction transaction = fetchByorder_First(orderId, orderByComparator);

		if (transaction != null) {
			return transaction;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("orderId=");
		msg.append(orderId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTransactionException(msg.toString());
	}

	/**
	 * Returns the first transaction in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction, or <code>null</code> if a matching transaction could not be found
	 */
	@Override
	public Transaction fetchByorder_First(String orderId,
		OrderByComparator<Transaction> orderByComparator) {
		List<Transaction> list = findByorder(orderId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last transaction in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction
	 * @throws NoSuchTransactionException if a matching transaction could not be found
	 */
	@Override
	public Transaction findByorder_Last(String orderId,
		OrderByComparator<Transaction> orderByComparator)
		throws NoSuchTransactionException {
		Transaction transaction = fetchByorder_Last(orderId, orderByComparator);

		if (transaction != null) {
			return transaction;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("orderId=");
		msg.append(orderId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTransactionException(msg.toString());
	}

	/**
	 * Returns the last transaction in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction, or <code>null</code> if a matching transaction could not be found
	 */
	@Override
	public Transaction fetchByorder_Last(String orderId,
		OrderByComparator<Transaction> orderByComparator) {
		int count = countByorder(orderId);

		if (count == 0) {
			return null;
		}

		List<Transaction> list = findByorder(orderId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the transactions before and after the current transaction in the ordered set where orderId = &#63;.
	 *
	 * @param transactionId the primary key of the current transaction
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next transaction
	 * @throws NoSuchTransactionException if a transaction with the primary key could not be found
	 */
	@Override
	public Transaction[] findByorder_PrevAndNext(String transactionId,
		String orderId, OrderByComparator<Transaction> orderByComparator)
		throws NoSuchTransactionException {
		Transaction transaction = findByPrimaryKey(transactionId);

		Session session = null;

		try {
			session = openSession();

			Transaction[] array = new TransactionImpl[3];

			array[0] = getByorder_PrevAndNext(session, transaction, orderId,
					orderByComparator, true);

			array[1] = transaction;

			array[2] = getByorder_PrevAndNext(session, transaction, orderId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Transaction getByorder_PrevAndNext(Session session,
		Transaction transaction, String orderId,
		OrderByComparator<Transaction> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TRANSACTION_WHERE);

		boolean bindOrderId = false;

		if (orderId == null) {
			query.append(_FINDER_COLUMN_ORDER_ORDERID_1);
		}
		else if (orderId.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ORDER_ORDERID_3);
		}
		else {
			bindOrderId = true;

			query.append(_FINDER_COLUMN_ORDER_ORDERID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TransactionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindOrderId) {
			qPos.add(orderId);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(transaction);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Transaction> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the transactions where orderId = &#63; from the database.
	 *
	 * @param orderId the order ID
	 */
	@Override
	public void removeByorder(String orderId) {
		for (Transaction transaction : findByorder(orderId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(transaction);
		}
	}

	/**
	 * Returns the number of transactions where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @return the number of matching transactions
	 */
	@Override
	public int countByorder(String orderId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ORDER;

		Object[] finderArgs = new Object[] { orderId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TRANSACTION_WHERE);

			boolean bindOrderId = false;

			if (orderId == null) {
				query.append(_FINDER_COLUMN_ORDER_ORDERID_1);
			}
			else if (orderId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ORDER_ORDERID_3);
			}
			else {
				bindOrderId = true;

				query.append(_FINDER_COLUMN_ORDER_ORDERID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindOrderId) {
					qPos.add(orderId);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ORDER_ORDERID_1 = "transaction.orderId IS NULL";
	private static final String _FINDER_COLUMN_ORDER_ORDERID_2 = "transaction.orderId = ?";
	private static final String _FINDER_COLUMN_ORDER_ORDERID_3 = "(transaction.orderId IS NULL OR transaction.orderId = '')";

	public TransactionPersistenceImpl() {
		setModelClass(Transaction.class);

		try {
			Field field = ReflectionUtil.getDeclaredField(BasePersistenceImpl.class,
					"_dbColumnNames");

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("type", "type_");
			dbColumnNames.put("currency", "currency_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the transaction in the entity cache if it is enabled.
	 *
	 * @param transaction the transaction
	 */
	@Override
	public void cacheResult(Transaction transaction) {
		entityCache.putResult(TransactionModelImpl.ENTITY_CACHE_ENABLED,
			TransactionImpl.class, transaction.getPrimaryKey(), transaction);

		transaction.resetOriginalValues();
	}

	/**
	 * Caches the transactions in the entity cache if it is enabled.
	 *
	 * @param transactions the transactions
	 */
	@Override
	public void cacheResult(List<Transaction> transactions) {
		for (Transaction transaction : transactions) {
			if (entityCache.getResult(
						TransactionModelImpl.ENTITY_CACHE_ENABLED,
						TransactionImpl.class, transaction.getPrimaryKey()) == null) {
				cacheResult(transaction);
			}
			else {
				transaction.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all transactions.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(TransactionImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the transaction.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Transaction transaction) {
		entityCache.removeResult(TransactionModelImpl.ENTITY_CACHE_ENABLED,
			TransactionImpl.class, transaction.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Transaction> transactions) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Transaction transaction : transactions) {
			entityCache.removeResult(TransactionModelImpl.ENTITY_CACHE_ENABLED,
				TransactionImpl.class, transaction.getPrimaryKey());
		}
	}

	/**
	 * Creates a new transaction with the primary key. Does not add the transaction to the database.
	 *
	 * @param transactionId the primary key for the new transaction
	 * @return the new transaction
	 */
	@Override
	public Transaction create(String transactionId) {
		Transaction transaction = new TransactionImpl();

		transaction.setNew(true);
		transaction.setPrimaryKey(transactionId);

		return transaction;
	}

	/**
	 * Removes the transaction with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param transactionId the primary key of the transaction
	 * @return the transaction that was removed
	 * @throws NoSuchTransactionException if a transaction with the primary key could not be found
	 */
	@Override
	public Transaction remove(String transactionId)
		throws NoSuchTransactionException {
		return remove((Serializable)transactionId);
	}

	/**
	 * Removes the transaction with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the transaction
	 * @return the transaction that was removed
	 * @throws NoSuchTransactionException if a transaction with the primary key could not be found
	 */
	@Override
	public Transaction remove(Serializable primaryKey)
		throws NoSuchTransactionException {
		Session session = null;

		try {
			session = openSession();

			Transaction transaction = (Transaction)session.get(TransactionImpl.class,
					primaryKey);

			if (transaction == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTransactionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(transaction);
		}
		catch (NoSuchTransactionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Transaction removeImpl(Transaction transaction) {
		transaction = toUnwrappedModel(transaction);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(transaction)) {
				transaction = (Transaction)session.get(TransactionImpl.class,
						transaction.getPrimaryKeyObj());
			}

			if (transaction != null) {
				session.delete(transaction);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (transaction != null) {
			clearCache(transaction);
		}

		return transaction;
	}

	@Override
	public Transaction updateImpl(Transaction transaction) {
		transaction = toUnwrappedModel(transaction);

		boolean isNew = transaction.isNew();

		TransactionModelImpl transactionModelImpl = (TransactionModelImpl)transaction;

		Session session = null;

		try {
			session = openSession();

			if (transaction.isNew()) {
				session.save(transaction);

				transaction.setNew(false);
			}
			else {
				transaction = (Transaction)session.merge(transaction);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!TransactionModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { transactionModelImpl.getOrderId() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_ORDER, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDER,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((transactionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						transactionModelImpl.getOriginalOrderId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_ORDER, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDER,
					args);

				args = new Object[] { transactionModelImpl.getOrderId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_ORDER, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDER,
					args);
			}
		}

		entityCache.putResult(TransactionModelImpl.ENTITY_CACHE_ENABLED,
			TransactionImpl.class, transaction.getPrimaryKey(), transaction,
			false);

		transaction.resetOriginalValues();

		return transaction;
	}

	protected Transaction toUnwrappedModel(Transaction transaction) {
		if (transaction instanceof TransactionImpl) {
			return transaction;
		}

		TransactionImpl transactionImpl = new TransactionImpl();

		transactionImpl.setNew(transaction.isNew());
		transactionImpl.setPrimaryKey(transaction.getPrimaryKey());

		transactionImpl.setTransactionId(transaction.getTransactionId());
		transactionImpl.setOrderId(transaction.getOrderId());
		transactionImpl.setUserWalletAddr(transaction.getUserWalletAddr());
		transactionImpl.setType(transaction.getType());
		transactionImpl.setCurrency(transaction.getCurrency());
		transactionImpl.setAmount(transaction.getAmount());
		transactionImpl.setStatus(transaction.getStatus());
		transactionImpl.setAddTime(transaction.getAddTime());
		transactionImpl.setUpdateTime(transaction.getUpdateTime());

		return transactionImpl;
	}

	/**
	 * Returns the transaction with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the transaction
	 * @return the transaction
	 * @throws NoSuchTransactionException if a transaction with the primary key could not be found
	 */
	@Override
	public Transaction findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTransactionException {
		Transaction transaction = fetchByPrimaryKey(primaryKey);

		if (transaction == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTransactionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return transaction;
	}

	/**
	 * Returns the transaction with the primary key or throws a {@link NoSuchTransactionException} if it could not be found.
	 *
	 * @param transactionId the primary key of the transaction
	 * @return the transaction
	 * @throws NoSuchTransactionException if a transaction with the primary key could not be found
	 */
	@Override
	public Transaction findByPrimaryKey(String transactionId)
		throws NoSuchTransactionException {
		return findByPrimaryKey((Serializable)transactionId);
	}

	/**
	 * Returns the transaction with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the transaction
	 * @return the transaction, or <code>null</code> if a transaction with the primary key could not be found
	 */
	@Override
	public Transaction fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(TransactionModelImpl.ENTITY_CACHE_ENABLED,
				TransactionImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Transaction transaction = (Transaction)serializable;

		if (transaction == null) {
			Session session = null;

			try {
				session = openSession();

				transaction = (Transaction)session.get(TransactionImpl.class,
						primaryKey);

				if (transaction != null) {
					cacheResult(transaction);
				}
				else {
					entityCache.putResult(TransactionModelImpl.ENTITY_CACHE_ENABLED,
						TransactionImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(TransactionModelImpl.ENTITY_CACHE_ENABLED,
					TransactionImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return transaction;
	}

	/**
	 * Returns the transaction with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param transactionId the primary key of the transaction
	 * @return the transaction, or <code>null</code> if a transaction with the primary key could not be found
	 */
	@Override
	public Transaction fetchByPrimaryKey(String transactionId) {
		return fetchByPrimaryKey((Serializable)transactionId);
	}

	@Override
	public Map<Serializable, Transaction> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Transaction> map = new HashMap<Serializable, Transaction>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Transaction transaction = fetchByPrimaryKey(primaryKey);

			if (transaction != null) {
				map.put(primaryKey, transaction);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(TransactionModelImpl.ENTITY_CACHE_ENABLED,
					TransactionImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Transaction)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_TRANSACTION_WHERE_PKS_IN);

		for (int i = 0; i < uncachedPrimaryKeys.size(); i++) {
			query.append(StringPool.QUESTION);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			QueryPos qPos = QueryPos.getInstance(q);

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				qPos.add((String)primaryKey);
			}

			for (Transaction transaction : (List<Transaction>)q.list()) {
				map.put(transaction.getPrimaryKeyObj(), transaction);

				cacheResult(transaction);

				uncachedPrimaryKeys.remove(transaction.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(TransactionModelImpl.ENTITY_CACHE_ENABLED,
					TransactionImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the transactions.
	 *
	 * @return the transactions
	 */
	@Override
	public List<Transaction> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the transactions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of transactions
	 * @param end the upper bound of the range of transactions (not inclusive)
	 * @return the range of transactions
	 */
	@Override
	public List<Transaction> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the transactions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of transactions
	 * @param end the upper bound of the range of transactions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of transactions
	 */
	@Override
	public List<Transaction> findAll(int start, int end,
		OrderByComparator<Transaction> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the transactions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TransactionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of transactions
	 * @param end the upper bound of the range of transactions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of transactions
	 */
	@Override
	public List<Transaction> findAll(int start, int end,
		OrderByComparator<Transaction> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Transaction> list = null;

		if (retrieveFromCache) {
			list = (List<Transaction>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_TRANSACTION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TRANSACTION;

				if (pagination) {
					sql = sql.concat(TransactionModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Transaction>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Transaction>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the transactions from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Transaction transaction : findAll()) {
			remove(transaction);
		}
	}

	/**
	 * Returns the number of transactions.
	 *
	 * @return the number of transactions
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TRANSACTION);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return TransactionModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the transaction persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(TransactionImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_TRANSACTION = "SELECT transaction FROM Transaction transaction";
	private static final String _SQL_SELECT_TRANSACTION_WHERE_PKS_IN = "SELECT transaction FROM Transaction transaction WHERE transactionId IN (";
	private static final String _SQL_SELECT_TRANSACTION_WHERE = "SELECT transaction FROM Transaction transaction WHERE ";
	private static final String _SQL_COUNT_TRANSACTION = "SELECT COUNT(transaction) FROM Transaction transaction";
	private static final String _SQL_COUNT_TRANSACTION_WHERE = "SELECT COUNT(transaction) FROM Transaction transaction WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "transaction.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Transaction exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Transaction exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(TransactionPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"type", "currency"
			});
}