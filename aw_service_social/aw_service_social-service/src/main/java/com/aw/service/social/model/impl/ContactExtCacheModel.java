/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.ContactExt;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ContactExt in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see ContactExt
 * @generated
 */
@ProviderType
public class ContactExtCacheModel implements CacheModel<ContactExt>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ContactExtCacheModel)) {
			return false;
		}

		ContactExtCacheModel contactExtCacheModel = (ContactExtCacheModel)obj;

		if (userUid.equals(contactExtCacheModel.userUid)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, userUid);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{userUid=");
		sb.append(userUid);
		sb.append(", primEmail=");
		sb.append(primEmail);
		sb.append(", secEmail=");
		sb.append(secEmail);
		sb.append(", phone=");
		sb.append(phone);
		sb.append(", extension=");
		sb.append(extension);
		sb.append(", cellPhone=");
		sb.append(cellPhone);
		sb.append(", facebook=");
		sb.append(facebook);
		sb.append(", wechat=");
		sb.append(wechat);
		sb.append(", twitter=");
		sb.append(twitter);
		sb.append(", weibo=");
		sb.append(weibo);
		sb.append(", addTime=");
		sb.append(addTime);
		sb.append(", updateTime=");
		sb.append(updateTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ContactExt toEntityModel() {
		ContactExtImpl contactExtImpl = new ContactExtImpl();

		if (userUid == null) {
			contactExtImpl.setUserUid(StringPool.BLANK);
		}
		else {
			contactExtImpl.setUserUid(userUid);
		}

		if (primEmail == null) {
			contactExtImpl.setPrimEmail(StringPool.BLANK);
		}
		else {
			contactExtImpl.setPrimEmail(primEmail);
		}

		if (secEmail == null) {
			contactExtImpl.setSecEmail(StringPool.BLANK);
		}
		else {
			contactExtImpl.setSecEmail(secEmail);
		}

		if (phone == null) {
			contactExtImpl.setPhone(StringPool.BLANK);
		}
		else {
			contactExtImpl.setPhone(phone);
		}

		if (extension == null) {
			contactExtImpl.setExtension(StringPool.BLANK);
		}
		else {
			contactExtImpl.setExtension(extension);
		}

		if (cellPhone == null) {
			contactExtImpl.setCellPhone(StringPool.BLANK);
		}
		else {
			contactExtImpl.setCellPhone(cellPhone);
		}

		if (facebook == null) {
			contactExtImpl.setFacebook(StringPool.BLANK);
		}
		else {
			contactExtImpl.setFacebook(facebook);
		}

		if (wechat == null) {
			contactExtImpl.setWechat(StringPool.BLANK);
		}
		else {
			contactExtImpl.setWechat(wechat);
		}

		if (twitter == null) {
			contactExtImpl.setTwitter(StringPool.BLANK);
		}
		else {
			contactExtImpl.setTwitter(twitter);
		}

		if (weibo == null) {
			contactExtImpl.setWeibo(StringPool.BLANK);
		}
		else {
			contactExtImpl.setWeibo(weibo);
		}

		if (addTime == Long.MIN_VALUE) {
			contactExtImpl.setAddTime(null);
		}
		else {
			contactExtImpl.setAddTime(new Date(addTime));
		}

		if (updateTime == Long.MIN_VALUE) {
			contactExtImpl.setUpdateTime(null);
		}
		else {
			contactExtImpl.setUpdateTime(new Date(updateTime));
		}

		contactExtImpl.resetOriginalValues();

		return contactExtImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		userUid = objectInput.readUTF();
		primEmail = objectInput.readUTF();
		secEmail = objectInput.readUTF();
		phone = objectInput.readUTF();
		extension = objectInput.readUTF();
		cellPhone = objectInput.readUTF();
		facebook = objectInput.readUTF();
		wechat = objectInput.readUTF();
		twitter = objectInput.readUTF();
		weibo = objectInput.readUTF();
		addTime = objectInput.readLong();
		updateTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (userUid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userUid);
		}

		if (primEmail == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(primEmail);
		}

		if (secEmail == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(secEmail);
		}

		if (phone == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(phone);
		}

		if (extension == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(extension);
		}

		if (cellPhone == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cellPhone);
		}

		if (facebook == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(facebook);
		}

		if (wechat == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(wechat);
		}

		if (twitter == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(twitter);
		}

		if (weibo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(weibo);
		}

		objectOutput.writeLong(addTime);
		objectOutput.writeLong(updateTime);
	}

	public String userUid;
	public String primEmail;
	public String secEmail;
	public String phone;
	public String extension;
	public String cellPhone;
	public String facebook;
	public String wechat;
	public String twitter;
	public String weibo;
	public long addTime;
	public long updateTime;
}