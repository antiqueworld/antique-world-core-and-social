package com.aw.service.social.service.util;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.aw.common.util.service.CommonUtilLocalServiceUtil;
import com.aw.common.util.service.ErrorCodeLocalServiceUtil;

import com.aw.service.social.model.AddressExt;
import com.aw.service.social.model.ContactExt;
import com.aw.service.social.model.UserExt;
import com.aw.service.social.service.persistence.AddressExtUtil;
import com.aw.service.social.service.persistence.ContactExtUtil;
import com.aw.service.social.service.persistence.UserExtUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Country;
import com.liferay.portal.kernel.model.Region;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.persistence.CountryUtil;
import com.liferay.portal.kernel.service.persistence.RegionUtil;

public class UserGetUtil {
	public JSONObject getUserDetail(User user, String locale) {

		String userUid = user.getUserUuid();
		
		JSONObject userJson = JSONFactoryUtil.createJSONObject();
				
		// if no results returned there is no data_details content;
		UserExt uExt = UserExtUtil.fetchByPrimaryKey(userUid);
		ContactExt cExt = ContactExtUtil.fetchByPrimaryKey(userUid);		
		
		List<AddressExt> aExtList = AddressExtUtil.findByuser(userUid);
		
		if(uExt == null){
			//add uExt
			uExt = UserExtUtil.create(userUid);
			uExt.setUserId(user.getUserId());
			uExt.setFirstName(user.getFirstName());
			uExt.setMiddleName(user.getMiddleName());
			uExt.setLastName(user.getLastName());
			
			uExt.setAddTime(new Date());
			uExt.persist();
		}
		
		if(cExt == null){
			//add contact
			cExt = ContactExtUtil.create(userUid);
			cExt.setPrimEmail(user.getEmailAddress());
			cExt.setAddTime(new Date());
			cExt.persist();			
		}
		
		userJson.put("basicInfo", CommonUtilLocalServiceUtil.getJson(uExt));
		userJson.put("contact", CommonUtilLocalServiceUtil.getJson(cExt));
		userJson.put("addresses", CommonUtilLocalServiceUtil.getJsonArray(aExtList));
	
		return CommonUtilLocalServiceUtil.getResponse(userJson, null,
				ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}
	
	public JSONObject getCountryList(String locale) {

		List<Country> list = CountryUtil.findAll();
		JSONArray resultSet = JSONFactoryUtil.createJSONArray();
		for(Country c : list){
			JSONObject j = JSONFactoryUtil.createJSONObject();
			j.put("id", c.getCountryId());
			j.put("code", c.getA2());
			j.put("name", c.getName(new Locale(locale)));
			resultSet.put(j);
		}
		return CommonUtilLocalServiceUtil.getResponse(null, resultSet, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}

	public JSONObject getRegionList(String countryId, String locale) {
		if (countryId == null || countryId.isEmpty()) {
			return CommonUtilLocalServiceUtil.getResponse(null, null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
		}
		List<Region> list = RegionUtil.findByCountryId(Long.parseLong(countryId));
		JSONArray resultSet = JSONFactoryUtil.createJSONArray();
		for(Region c : list){
			JSONObject j = JSONFactoryUtil.createJSONObject();
			j.put("code", c.getRegionCode());
			j.put("name", c.getName());
			j.put("id", c.getRegionId());
			resultSet.put(j);
		}
		return CommonUtilLocalServiceUtil.getResponse(null, resultSet, ErrorCodeLocalServiceUtil.getSuccess(), locale);
	}

	@SuppressWarnings("unused")
	private void junkYard() {
		
//		public JSONObject getUserMedia(String mediaUid, String locale){
//			
//			JSONObject mediaJSON = null;
	//
//			return CommonUtilLocalServiceUtil.getResponse(mediaJSON.getJSONArray("data_details").getJSONObject(0), null, ErrorCodeLocalServiceUtil.getSuccess(), locale);
//		}
		
//		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//		Date dateTime = new Date();
//		String dateTimeStr = formatter.format(dateTime);
	}
}
