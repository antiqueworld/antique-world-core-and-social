/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.AddressExt;
import com.aw.service.social.service.persistence.AddressExtPK;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing AddressExt in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see AddressExt
 * @generated
 */
@ProviderType
public class AddressExtCacheModel implements CacheModel<AddressExt>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AddressExtCacheModel)) {
			return false;
		}

		AddressExtCacheModel addressExtCacheModel = (AddressExtCacheModel)obj;

		if (addressExtPK.equals(addressExtCacheModel.addressExtPK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, addressExtPK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{userUid=");
		sb.append(userUid);
		sb.append(", addressType=");
		sb.append(addressType);
		sb.append(", street1=");
		sb.append(street1);
		sb.append(", street2=");
		sb.append(street2);
		sb.append(", street3=");
		sb.append(street3);
		sb.append(", city=");
		sb.append(city);
		sb.append(", regionId=");
		sb.append(regionId);
		sb.append(", countryId=");
		sb.append(countryId);
		sb.append(", zipCode=");
		sb.append(zipCode);
		sb.append(", isMailing=");
		sb.append(isMailing);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AddressExt toEntityModel() {
		AddressExtImpl addressExtImpl = new AddressExtImpl();

		if (userUid == null) {
			addressExtImpl.setUserUid(StringPool.BLANK);
		}
		else {
			addressExtImpl.setUserUid(userUid);
		}

		if (addressType == null) {
			addressExtImpl.setAddressType(StringPool.BLANK);
		}
		else {
			addressExtImpl.setAddressType(addressType);
		}

		if (street1 == null) {
			addressExtImpl.setStreet1(StringPool.BLANK);
		}
		else {
			addressExtImpl.setStreet1(street1);
		}

		if (street2 == null) {
			addressExtImpl.setStreet2(StringPool.BLANK);
		}
		else {
			addressExtImpl.setStreet2(street2);
		}

		if (street3 == null) {
			addressExtImpl.setStreet3(StringPool.BLANK);
		}
		else {
			addressExtImpl.setStreet3(street3);
		}

		if (city == null) {
			addressExtImpl.setCity(StringPool.BLANK);
		}
		else {
			addressExtImpl.setCity(city);
		}

		if (regionId == null) {
			addressExtImpl.setRegionId(StringPool.BLANK);
		}
		else {
			addressExtImpl.setRegionId(regionId);
		}

		if (countryId == null) {
			addressExtImpl.setCountryId(StringPool.BLANK);
		}
		else {
			addressExtImpl.setCountryId(countryId);
		}

		if (zipCode == null) {
			addressExtImpl.setZipCode(StringPool.BLANK);
		}
		else {
			addressExtImpl.setZipCode(zipCode);
		}

		if (isMailing == null) {
			addressExtImpl.setIsMailing(StringPool.BLANK);
		}
		else {
			addressExtImpl.setIsMailing(isMailing);
		}

		addressExtImpl.resetOriginalValues();

		return addressExtImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		userUid = objectInput.readUTF();
		addressType = objectInput.readUTF();
		street1 = objectInput.readUTF();
		street2 = objectInput.readUTF();
		street3 = objectInput.readUTF();
		city = objectInput.readUTF();
		regionId = objectInput.readUTF();
		countryId = objectInput.readUTF();
		zipCode = objectInput.readUTF();
		isMailing = objectInput.readUTF();

		addressExtPK = new AddressExtPK(userUid, addressType);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (userUid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userUid);
		}

		if (addressType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(addressType);
		}

		if (street1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(street1);
		}

		if (street2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(street2);
		}

		if (street3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(street3);
		}

		if (city == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(city);
		}

		if (regionId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(regionId);
		}

		if (countryId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(countryId);
		}

		if (zipCode == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(zipCode);
		}

		if (isMailing == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(isMailing);
		}
	}

	public String userUid;
	public String addressType;
	public String street1;
	public String street2;
	public String street3;
	public String city;
	public String regionId;
	public String countryId;
	public String zipCode;
	public String isMailing;
	public transient AddressExtPK addressExtPK;
}