package com.aw.service.social.service.util;

public class StatementUtil {
	public static final String selectUserStmt = "select * from aw.userext where userUid = ?";
	public static final String insertUserStmt = "insert into aw.userext(userUid, userId,firstName,middleName,lastName,addTime) values(?,?,?,?,?,?)";
	public static final String insertUserEmailStmt = "insert into aw.contact(userUid, primEmail) values (?, ?)";
	
	public static final String selectCountryListStmt = "select * from aw.country";
	public static final String selectRegionListStmt = "select * from aw.region where countryId = ?";
	
	public static final String updateUserBasicStmt = "update aw.userext set firstName = ?, middleName = ?, lastName = ?, birthday = ?, title = ?, selfDesc = ?, updateTime = ? where userUid = ?";
	public static final String updateUserSensiStmt =  "update aw.userext set IdIssueCountryId = ?, IdType = ?, IdNumber = ?, ssn = ?, updateTime = ? where userUid = ?";
	public static final String updateAddressStmt = "update aw.address set street1 = ?, street2 = ?, street3 = ?, city = ?, regionId = ?, countryId = ?, zipCode = ?, isMailling = ? where userUid = ? and addressType = ?";

	public static final String selectPortraitIdStmt =  "select portraitId from aw.userext where userUid = ?";
	public static final String selectImageIdStmt = "select idImageId from aw.userext where userUid = ?";
	
	public static final String updateUserPortraitStmt =  "update aw.userext set portraitId = ?, updateTime = ? where userUid = ?";
	public static final String updateUserIdImageStmt = "update aw.userext set IdImageId = ?, updateTime = ? where userUid = ?";
	public static final String insertMediaStmt = "insert into aw.media (mediaUid, isActive, ownerUid, publicFlag, fileType,fileExt,fileSize,fileData,addTime,updateTime) values(?, ?,?, ?,?,?,?,?,?,?)";
	public static final String updateMediaStmt = "update aw.media set fileType = ?, fileExt = ?, fileSize = ?, fileData = ?, isActive = ?, publicFlag = ?, updateTime = ? where mediaUId = ?";
	
	public static final String selectUserContactStmt = "select * from aw.Contact where userUid = ?";
	public static final String selectUserAddressStmt = "select * from aw.Address where userUid = ?";
	public static final String selectUserMediaStmt = "select * from aw.Media where mediaUid = ?";
	
	public static final String updateUserContactStmt = "update aw.contact set primEmail = ?, secEmail = ?, phone = ?, extension = ?, cellPhone = ? where userUid = ?";

}
