/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.exception.NoSuchContactExtException;
import com.aw.service.social.model.ContactExt;
import com.aw.service.social.model.impl.ContactExtImpl;
import com.aw.service.social.model.impl.ContactExtModelImpl;
import com.aw.service.social.service.persistence.ContactExtPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the contact ext service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ContactExtPersistence
 * @see com.aw.service.social.service.persistence.ContactExtUtil
 * @generated
 */
@ProviderType
public class ContactExtPersistenceImpl extends BasePersistenceImpl<ContactExt>
	implements ContactExtPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ContactExtUtil} to access the contact ext persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ContactExtImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
			ContactExtModelImpl.FINDER_CACHE_ENABLED, ContactExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
			ContactExtModelImpl.FINDER_CACHE_ENABLED, ContactExtImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
			ContactExtModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public ContactExtPersistenceImpl() {
		setModelClass(ContactExt.class);
	}

	/**
	 * Caches the contact ext in the entity cache if it is enabled.
	 *
	 * @param contactExt the contact ext
	 */
	@Override
	public void cacheResult(ContactExt contactExt) {
		entityCache.putResult(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
			ContactExtImpl.class, contactExt.getPrimaryKey(), contactExt);

		contactExt.resetOriginalValues();
	}

	/**
	 * Caches the contact exts in the entity cache if it is enabled.
	 *
	 * @param contactExts the contact exts
	 */
	@Override
	public void cacheResult(List<ContactExt> contactExts) {
		for (ContactExt contactExt : contactExts) {
			if (entityCache.getResult(
						ContactExtModelImpl.ENTITY_CACHE_ENABLED,
						ContactExtImpl.class, contactExt.getPrimaryKey()) == null) {
				cacheResult(contactExt);
			}
			else {
				contactExt.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all contact exts.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ContactExtImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the contact ext.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ContactExt contactExt) {
		entityCache.removeResult(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
			ContactExtImpl.class, contactExt.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ContactExt> contactExts) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ContactExt contactExt : contactExts) {
			entityCache.removeResult(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
				ContactExtImpl.class, contactExt.getPrimaryKey());
		}
	}

	/**
	 * Creates a new contact ext with the primary key. Does not add the contact ext to the database.
	 *
	 * @param userUid the primary key for the new contact ext
	 * @return the new contact ext
	 */
	@Override
	public ContactExt create(String userUid) {
		ContactExt contactExt = new ContactExtImpl();

		contactExt.setNew(true);
		contactExt.setPrimaryKey(userUid);

		return contactExt;
	}

	/**
	 * Removes the contact ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userUid the primary key of the contact ext
	 * @return the contact ext that was removed
	 * @throws NoSuchContactExtException if a contact ext with the primary key could not be found
	 */
	@Override
	public ContactExt remove(String userUid) throws NoSuchContactExtException {
		return remove((Serializable)userUid);
	}

	/**
	 * Removes the contact ext with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the contact ext
	 * @return the contact ext that was removed
	 * @throws NoSuchContactExtException if a contact ext with the primary key could not be found
	 */
	@Override
	public ContactExt remove(Serializable primaryKey)
		throws NoSuchContactExtException {
		Session session = null;

		try {
			session = openSession();

			ContactExt contactExt = (ContactExt)session.get(ContactExtImpl.class,
					primaryKey);

			if (contactExt == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchContactExtException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(contactExt);
		}
		catch (NoSuchContactExtException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ContactExt removeImpl(ContactExt contactExt) {
		contactExt = toUnwrappedModel(contactExt);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(contactExt)) {
				contactExt = (ContactExt)session.get(ContactExtImpl.class,
						contactExt.getPrimaryKeyObj());
			}

			if (contactExt != null) {
				session.delete(contactExt);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (contactExt != null) {
			clearCache(contactExt);
		}

		return contactExt;
	}

	@Override
	public ContactExt updateImpl(ContactExt contactExt) {
		contactExt = toUnwrappedModel(contactExt);

		boolean isNew = contactExt.isNew();

		Session session = null;

		try {
			session = openSession();

			if (contactExt.isNew()) {
				session.save(contactExt);

				contactExt.setNew(false);
			}
			else {
				contactExt = (ContactExt)session.merge(contactExt);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
			ContactExtImpl.class, contactExt.getPrimaryKey(), contactExt, false);

		contactExt.resetOriginalValues();

		return contactExt;
	}

	protected ContactExt toUnwrappedModel(ContactExt contactExt) {
		if (contactExt instanceof ContactExtImpl) {
			return contactExt;
		}

		ContactExtImpl contactExtImpl = new ContactExtImpl();

		contactExtImpl.setNew(contactExt.isNew());
		contactExtImpl.setPrimaryKey(contactExt.getPrimaryKey());

		contactExtImpl.setUserUid(contactExt.getUserUid());
		contactExtImpl.setPrimEmail(contactExt.getPrimEmail());
		contactExtImpl.setSecEmail(contactExt.getSecEmail());
		contactExtImpl.setPhone(contactExt.getPhone());
		contactExtImpl.setExtension(contactExt.getExtension());
		contactExtImpl.setCellPhone(contactExt.getCellPhone());
		contactExtImpl.setFacebook(contactExt.getFacebook());
		contactExtImpl.setWechat(contactExt.getWechat());
		contactExtImpl.setTwitter(contactExt.getTwitter());
		contactExtImpl.setWeibo(contactExt.getWeibo());
		contactExtImpl.setAddTime(contactExt.getAddTime());
		contactExtImpl.setUpdateTime(contactExt.getUpdateTime());

		return contactExtImpl;
	}

	/**
	 * Returns the contact ext with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the contact ext
	 * @return the contact ext
	 * @throws NoSuchContactExtException if a contact ext with the primary key could not be found
	 */
	@Override
	public ContactExt findByPrimaryKey(Serializable primaryKey)
		throws NoSuchContactExtException {
		ContactExt contactExt = fetchByPrimaryKey(primaryKey);

		if (contactExt == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchContactExtException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return contactExt;
	}

	/**
	 * Returns the contact ext with the primary key or throws a {@link NoSuchContactExtException} if it could not be found.
	 *
	 * @param userUid the primary key of the contact ext
	 * @return the contact ext
	 * @throws NoSuchContactExtException if a contact ext with the primary key could not be found
	 */
	@Override
	public ContactExt findByPrimaryKey(String userUid)
		throws NoSuchContactExtException {
		return findByPrimaryKey((Serializable)userUid);
	}

	/**
	 * Returns the contact ext with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the contact ext
	 * @return the contact ext, or <code>null</code> if a contact ext with the primary key could not be found
	 */
	@Override
	public ContactExt fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
				ContactExtImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ContactExt contactExt = (ContactExt)serializable;

		if (contactExt == null) {
			Session session = null;

			try {
				session = openSession();

				contactExt = (ContactExt)session.get(ContactExtImpl.class,
						primaryKey);

				if (contactExt != null) {
					cacheResult(contactExt);
				}
				else {
					entityCache.putResult(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
						ContactExtImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
					ContactExtImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return contactExt;
	}

	/**
	 * Returns the contact ext with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userUid the primary key of the contact ext
	 * @return the contact ext, or <code>null</code> if a contact ext with the primary key could not be found
	 */
	@Override
	public ContactExt fetchByPrimaryKey(String userUid) {
		return fetchByPrimaryKey((Serializable)userUid);
	}

	@Override
	public Map<Serializable, ContactExt> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ContactExt> map = new HashMap<Serializable, ContactExt>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ContactExt contactExt = fetchByPrimaryKey(primaryKey);

			if (contactExt != null) {
				map.put(primaryKey, contactExt);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
					ContactExtImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ContactExt)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_CONTACTEXT_WHERE_PKS_IN);

		for (int i = 0; i < uncachedPrimaryKeys.size(); i++) {
			query.append(StringPool.QUESTION);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			QueryPos qPos = QueryPos.getInstance(q);

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				qPos.add((String)primaryKey);
			}

			for (ContactExt contactExt : (List<ContactExt>)q.list()) {
				map.put(contactExt.getPrimaryKeyObj(), contactExt);

				cacheResult(contactExt);

				uncachedPrimaryKeys.remove(contactExt.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ContactExtModelImpl.ENTITY_CACHE_ENABLED,
					ContactExtImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the contact exts.
	 *
	 * @return the contact exts
	 */
	@Override
	public List<ContactExt> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the contact exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact exts
	 * @param end the upper bound of the range of contact exts (not inclusive)
	 * @return the range of contact exts
	 */
	@Override
	public List<ContactExt> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the contact exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact exts
	 * @param end the upper bound of the range of contact exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of contact exts
	 */
	@Override
	public List<ContactExt> findAll(int start, int end,
		OrderByComparator<ContactExt> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the contact exts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactExtModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact exts
	 * @param end the upper bound of the range of contact exts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of contact exts
	 */
	@Override
	public List<ContactExt> findAll(int start, int end,
		OrderByComparator<ContactExt> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ContactExt> list = null;

		if (retrieveFromCache) {
			list = (List<ContactExt>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CONTACTEXT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CONTACTEXT;

				if (pagination) {
					sql = sql.concat(ContactExtModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ContactExt>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ContactExt>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the contact exts from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ContactExt contactExt : findAll()) {
			remove(contactExt);
		}
	}

	/**
	 * Returns the number of contact exts.
	 *
	 * @return the number of contact exts
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CONTACTEXT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ContactExtModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the contact ext persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ContactExtImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_CONTACTEXT = "SELECT contactExt FROM ContactExt contactExt";
	private static final String _SQL_SELECT_CONTACTEXT_WHERE_PKS_IN = "SELECT contactExt FROM ContactExt contactExt WHERE userUid IN (";
	private static final String _SQL_COUNT_CONTACTEXT = "SELECT COUNT(contactExt) FROM ContactExt contactExt";
	private static final String _ORDER_BY_ENTITY_ALIAS = "contactExt.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ContactExt exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(ContactExtPersistenceImpl.class);
}