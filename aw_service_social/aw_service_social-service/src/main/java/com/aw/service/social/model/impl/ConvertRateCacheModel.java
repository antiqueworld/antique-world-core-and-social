/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.service.social.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.aw.service.social.model.ConvertRate;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ConvertRate in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see ConvertRate
 * @generated
 */
@ProviderType
public class ConvertRateCacheModel implements CacheModel<ConvertRate>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ConvertRateCacheModel)) {
			return false;
		}

		ConvertRateCacheModel convertRateCacheModel = (ConvertRateCacheModel)obj;

		if (rateId.equals(convertRateCacheModel.rateId)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, rateId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{rateId=");
		sb.append(rateId);
		sb.append(", fromCurrency=");
		sb.append(fromCurrency);
		sb.append(", toCurrency=");
		sb.append(toCurrency);
		sb.append(", isActive=");
		sb.append(isActive);
		sb.append(", rate=");
		sb.append(rate);
		sb.append(", startTime=");
		sb.append(startTime);
		sb.append(", expireTime=");
		sb.append(expireTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ConvertRate toEntityModel() {
		ConvertRateImpl convertRateImpl = new ConvertRateImpl();

		if (rateId == null) {
			convertRateImpl.setRateId(StringPool.BLANK);
		}
		else {
			convertRateImpl.setRateId(rateId);
		}

		if (fromCurrency == null) {
			convertRateImpl.setFromCurrency(StringPool.BLANK);
		}
		else {
			convertRateImpl.setFromCurrency(fromCurrency);
		}

		if (toCurrency == null) {
			convertRateImpl.setToCurrency(StringPool.BLANK);
		}
		else {
			convertRateImpl.setToCurrency(toCurrency);
		}

		if (isActive == null) {
			convertRateImpl.setIsActive(StringPool.BLANK);
		}
		else {
			convertRateImpl.setIsActive(isActive);
		}

		convertRateImpl.setRate(rate);

		if (startTime == Long.MIN_VALUE) {
			convertRateImpl.setStartTime(null);
		}
		else {
			convertRateImpl.setStartTime(new Date(startTime));
		}

		if (expireTime == Long.MIN_VALUE) {
			convertRateImpl.setExpireTime(null);
		}
		else {
			convertRateImpl.setExpireTime(new Date(expireTime));
		}

		convertRateImpl.resetOriginalValues();

		return convertRateImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		rateId = objectInput.readUTF();
		fromCurrency = objectInput.readUTF();
		toCurrency = objectInput.readUTF();
		isActive = objectInput.readUTF();

		rate = objectInput.readDouble();
		startTime = objectInput.readLong();
		expireTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (rateId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(rateId);
		}

		if (fromCurrency == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(fromCurrency);
		}

		if (toCurrency == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(toCurrency);
		}

		if (isActive == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(isActive);
		}

		objectOutput.writeDouble(rate);
		objectOutput.writeLong(startTime);
		objectOutput.writeLong(expireTime);
	}

	public String rateId;
	public String fromCurrency;
	public String toCurrency;
	public String isActive;
	public double rate;
	public long startTime;
	public long expireTime;
}