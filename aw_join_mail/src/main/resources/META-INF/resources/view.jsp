<%@ include file="/init.jsp" %>
<style>
    #mail{
        border:solid 2px grey;
        text-align:center;  
        margin:auto;   
        padding-bottom:50px;
    }
    #mail #title{
       margin-top:50px;
       font-size:35px;
    }
    #mail #content{	
         font-size:15px;
         padding:15px;
         width:80%;
         margin:auto;
    }
    #mail #input{
       width:50%;
       margin:auto;
       min-height:40px;
       padding:10px;
       display:block;
    }
    #mail .button{
       display:inline-block;
       padding:10px;
       margin-top:30px;
       width:25%;
    }
    #mail #confirm{
       background-color:red;
       margin-left:30px;
       border-radius:5px;
       
    }
    #mail .button:hover{
       cursor: pointer
    }
    #mail #decline{
      margin-right:30px;
    }
</style>
<div style="display:flex;">
<div id='mail'>
<p style="border-bottom:solid 1px;padding:10px;background-color:#D3D3D3"><strong>Leaving our website</strong></p>
<p id='title'>
    <strong>Stay in Touch?</strong>
</p>
<p id='content'>
     Join our mailing list to receive sporadic updates about company news, job openings, and industry gossip.
</p>
<input id='input'/>
<div class='button' id='confirm'><strong>Yes! It would be great</strong></div>
<div class='button' id='decline'>No. Farewell</div>
</div>
</div>
<script>
function validateEmail(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email.toLowerCase());
}
     $('#confirm').click(function(){
    	 console.log($('#input').val());
    	 if(validateEmail($('#input').val())){
    		 alert('confirmed');
    	 }else{
    		 alert('invalid email');
    	 }
    	 
     })
</script>