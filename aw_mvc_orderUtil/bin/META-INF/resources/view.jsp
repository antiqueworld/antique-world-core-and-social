<%@ include file="/init.jsp" %>
<style>
    .showinfo{
        width:30%;
        display:inline-block;
        border:solid 1px;
        padding:10px;

    }
    input.currency {
    	text-align: right;
    	padding-right: 5px;
    }
	.search {
		width: 50%;
	}
	/* when action btn enable */
	.actbtnenable {
		background-color: #00cfff;
		border-radius: 10px;
		text-align: center;
    	text-decoration: none;
    	display: inline-block;
    	color: black; 
    	border: 1px solid black;
    	width: 86px;
	}
	/* when action pay btn enable */
	.actbtnwait {
		opacity: 0.6;
		background-color: yellow;
		border-radius: 10px;
		text-align: center;
    	text-decoration: none;
    	display: inline-block;
    	color: black; 
    	border: 1px solid black;
    	width: 86px;
	}
	/* when action btn disable */
	.actbtndisable {
		background-color: red;
		border-radius: 10px;
		text-align: center;
    	text-decoration: none;
    	display: inline-block;
    	color: black; 
    	border: 1px solid black;
    	width: 86px;
	}
</style>
<aui:script>
	
	var payMap = new Map(); 
	var completeMap = new Map();
	
	
     function createOrder(){
          Liferay.Service(
			  '/aw_social.commerce/create-order-for-user',
			  {
			    userUid: $('#userUid').val(),
			    payType: $('#payType option:selected').val(),
			    payCurrency: $('#payCurrency').val(),
			    getCurrency: $('#getCurrency option:selected').val(),
			    amount: $('#amount').val(),
			    walletAddress: $('#walletAddress').val(),
			    locale: ''
			  },
			  function(obj) {
			  	console.log("im here");
			     if(obj.status.code=='00'){
			          alert('The order has been created!');
			          <!-- getOrderDetail(); -->
			    }else{
			         alert(obj.status.message);
			    }
			  }
			);
     }
     
     function getOrderDetail() {
     	if($('#orderId').val() != '') { 
		     Liferay.Service(
				  '/aw_social.commerce/get-order-detail',
				  {
				    orderId: $('#orderId').val(),
				    locale: ''
				  },
				  function(obj) {
				    console.log(obj);
				    try{
				    	if(obj.status.code == '00') {
				    		alert("Order number is okay, but I dont have data for you");
				    		var data = obj.data;
				    		orderDetailDisplay();
				    		$('#addTime').html("add time: " + data.addTime);
				    		$('#chargedFlag').html("charged: " + data.chargedFlag);
				    		$('#fromAmount').html("from Amount: " + data.fromAmount);
				    		$('#toAmount').html("to Amount: " + data.toAmount);
				    		$('#updateTime').html("update time: " + data.updateTime);
				    		$('#OrderId').html("order ID: " + data.OrderId);
				    		$('#rateId').html("rate ID: " + data.rateId);
				    		$('#completedFlag').html("complete: " + data.completedFlag);
				    		$('#userWalletAddr').html("wallet address: " + data.userWalletAddr);
				    		$('#sentFlag').html("sent: " + data.sentFlag);
				    		$('#fromType').html("from type: " + data.fromType);
				    		$('#toCurrency').html("to Currency: " + data.toCurrency);
				    		$('#fromCurrency').html("from Currency: " + data.fromCurrency);
				    		$('#userid').html("user ID: " + data.userUid);
				    	} else {	
				    		alert("Order is not good");
				    	}
				    } catch(err) {
				    	alert("Order number is incorrect");
				    }
				    
				  }
			);
		} else {
			alert("Order number can't be NULL");
		}
     }
     function helloworld() {
     	console.log("hello");
     }
     
     function getAllOrders() {
     	console.log("jje");
     	Liferay.Service(
		  '/aw_social.commerce/get-all-orders',
		  {
		    chargeFlag: '',
		    sentFlag: '',
		    locale: ''
		  },
		  function(obj) {
		    console.log(obj);
		    
		    var data = obj.data;
		    var list = '';

		    <!-- paybtnX & completebtnX are named for action buttons -->
		    data.map(function(order, index) {
		    	index++;
		    	payMap.set("paybtn"+index, order.orderId);
		    	completeMap.set("completebtn"+index, order.orderId);
		    	var completeBtnEnable = ' disabled';
		    	var payBtnEnable = ' disabled';
		    	var completeBtnClass = 'actbtndisable';
		    	var payBtnClass = 'actbtndisable';
		    	var completeBtnVal ='Done';
		    	var payBtnVal = 'Done';
		    	
		    	if(order.completed === "N" && order.charged === "N") {
		    		
		    		payBtnEnable = '';
		    		completeBtnClass = 'actbtnwait';
		    		payBtnClass = 'actbtnenable';
		    		completeBtnVal ='Complete';
		    		payBtnVal = 'Pay';
		    		
		    	} else if(order.completed === "N") {
		    		completeBtnEnable = '';
		    		completeBtnClass = 'actbtnenable';
		    		completeBtnVal = 'Complete';
		    		
		    	} 
		    	
				list += '<tr>'+
				'<td>'+index+'</td>'+
				'<td>'+order.orderId+'</td>'+
				'<td>'+order.toCurrency+'</td>'+
				'<td>'+order.fromCurrency+'</td>'+
				'<td>'+order.toAmt+'</td>'+
				'<td>'+order.completed+'</td>'+
				'<td>'+order.fromAmt+'</td>'+
				'<td>'+order.charged+'</td>'+
				'<td>'+order.sent+'</td>'+
				'<td>'+order.userWallet+'</td>'+
				'<td>'+order.userUid+'</td>'+
				'<td><button id="paybtn'+index+'" class="'+payBtnClass+'" onclick="payOrder(this.id)"'+payBtnEnable+'>'+payBtnVal+'</button>'+
				'<button id="completebtn'+index+'" class="'+completeBtnClass+'" onclick="completeOrder(this.id)"'+completeBtnEnable+'>'+completeBtnVal+'</button></td>'+
				'</tr>';
		    })
		    $('#orderTable').html(list);
		    
		    		$('#completebtn1').attr('disabled', true);
		  }
		  );
     }
     
     function completeOrder(id) {
     	Liferay.Service(
		  '/aw_social.commerce/order-complete',
		  {
		    orderId: completeMap.get(id),
		    locale: ''
		  },
		  function(obj) {
		    try{
				if(obj.status.code == '00') {
					alert("Order has been completed!");
					$('#completebtn1').attr('disabled', true);
					<!-- refresh table -->
					getAllOrders();
				} else {	
					alert("Can't complete order!");
				}
			} catch(err) {
				alert("Error ocurred during the completion!");
			}
		  }
		);
     }
     
     function payOrder(id) {
     	
     	Liferay.Service(
		  '/aw_social.commerce/order-paid',
		  {
		    orderId: payMap.get(id),
		    locale: ''
		  },
		  function(obj) {
		    try{
				if(obj.status.code == '00') {
					alert("Order has been charged!");
					<!-- refresh table -->
					getAllOrders();
				} else {	
					alert("Can't charge order!");
				}
			} catch(err) {
				alert("Error ocurred during the charging!");
			}
		  }
		);
     }
     
     function orderDetailDisplay(){  
         $('#orderlist').attr('style','display: block');
     }
</aui:script>
<div>
	<div class="row">
	  <div class="col-lg-6">
		<p>Create Order</p>
		<form class="form-horizontal">
			<div class="form-group">
		 		<label class="col-md-4 control-label">UserUid</label>
		      	<div class="col-md-7">
		        	<input class="form-control" id="userUid" type="text" placeholder="UserUid" onfocus="this.placeholder = ''" onblur="this.placeholder = 'UserUid...'">
		      	</div>
			</div>
			<div class="form-group">
		 		<label class="col-md-4 control-label">PayType</label>
		      	<div class="col-md-7">
		       		<select id="payType" class="form-control">
	            		<option value='F'>Fiat</option>
	            		<option value='C'>Crypto</option>
	          		</select>
		      	</div>
			</div>
			<div class="form-group">
		 		<label class="col-md-4 control-label">PayCurrency</label>
		      	<div class="col-md-7">
		        	<input class="form-control" id="payCurrency" type="text" placeholder="Currency..." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Currency...'">
		      	</div>
			</div>
			<div class="form-group">
		 		<label class="col-md-4 control-label">GetCurrency</label>
		      	<div class="col-md-7">
		       		<select id="getCurrency" class="form-control">
	            		<option value='NAEX'>NAEX</option>
	          		</select>
		      	</div>
			</div>
			<div class="form-group">
		 		<label class="col-md-4 control-label">Amount <a href="#" title="What is this?" data-toggle="popover" data-trigger="hover" data-content="amount you are going to pay">?</a></label>
		      	<div class="col-md-7  hide-inputbtns">
		        	<input class="form-control" id="amount" type="text" value="0.00" />
		      	</div>
			</div>
			<div class="form-group">
		 		<label class="col-md-4 control-label">Wallet Address</label>
		      	<div class="col-md-7">
		        	<input class="form-control" id="walletAddress" type="text" placeholder="Wallet Address..." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Wallet Address...'">
		      	</div>
			</div>
		</form>
		<button id="createOrder" type="button" class="btn btn-primary btn-block" onclick="createOrder()">Create Order</button>
	  </div>
	  <div class="col-lg-6">
	  	<div>
	  		<div>
			  Order Information
			 </div>
			 <div>
			  	<div class="form-horizontal">
			  		<div class="form-group">
				 		<label class="col-md-4 control-label">Order Search</label>
				      	<div class="col-md-7 input-group">
				        		<input class="form-control search" id="orderId" type="text" placeholder="Search..." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search...'" />
				        		<span class="input-group-btn">
				        			<button type="button" class="btn btn-default" aria-label="Left Align" onclick="getOrderDetail()">
				        				<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
				        			</button>
				      			</span>
				      	</div>
					</div>
			      	<div id="orderlist" style="display: none;">
			     		  <ul class="list-group">
						    <li class="list-group-item" id="addTime"></li>
						    <li class="list-group-item" id="chargedFlag"></li>
						    <li class="list-group-item" id="fromAmount"></li>
						    <li class="list-group-item" id="toAmount"></li>
						    <li class="list-group-item" id="updateTime"></li>
						    <li class="list-group-item" id="OrderId"></li>
						    <li class="list-group-item" id="rateId"></li>
						    <li class="list-group-item" id="completedFlag"></li>
						    <li class="list-group-item" id="userWalletAddr"></li>
						    <li class="list-group-item" id="sentFlag"></li>
						    <li class="list-group-item" id="fromType"></li>
						    <li class="list-group-item" id="toCurrency"></li>
						    <li class="list-group-item" id="fromCurrency"></li>
						    <li class="list-group-item" id="userid"></li>
						  </ul>
				    </div>
				</div>
			</div>
		</div>
	  </div>
	</div>
	<hr style="border-width: 2px;">
	<button style="cursor: not-allowed;background-color: yellow;
		border-radius: 8px;
		text-align: center;
    	text-decoration: none;
    	display: inline-block;
    	color: black; 
    	border: 1px solid black;
    	width: 86px;" disabled>not</button>
	<div class="container">
		<div>
			<h2 style="text-align:center">All the orders</h2>
		</div>
		<div>
			<button type="button" onclick="getAllOrders()" style="margin-bottom: 5px;" class="btn btn-sm btn-primary">get orders</button>
		</div>
		<div>
			<div class="table-responsive">
				<table class="table table-hover table-striped">
				    <thead>
				      <tr>
				        <th>#</th>
				        <th>order ID</th>
				        <th>to currency</th>
				        <th>from currency</th>
				        <th>to amount</th>
				        <th>completed</th>
				        <th>from amount</th>
				        <th>charged</th>
				        <th>sent</th>
				        <th>user wallet</th>
				        <th>user ID</th>
				        <th>action</th>
				      </tr>
				    </thead>
				    <tbody id="orderTable">
		
				    </tbody>			
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
<!--
//-->
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({
		placement: 'right',
		container: 'body'
	});
});
document.getElementById("amount").onblur = function() {
		this.value = parseFloat(this.value.replace(/,/g, "0.00"))
        .toFixed(2)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
</script>