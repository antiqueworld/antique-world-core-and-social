package aw_mvc_orderUtil.portlet;

import aw_mvc_orderUtil.constants.Aw_mvc_orderUtilPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author fangmingzhao
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + Aw_mvc_orderUtilPortletKeys.Aw_mvc_orderUtil,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.display-name=aw_mvc_orderUtil Portlet"
	},
	service = Portlet.class
)
public class Aw_mvc_orderUtilPortlet extends MVCPortlet {
	
	
}