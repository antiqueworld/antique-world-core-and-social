package com.aw.bootservice.util;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.EthBlockNumber;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;

public class Web3jUtil {
	
	private static final String url = PropertyUtil.getProp("rpcEndPoint");
	
	public static String getBalance(String walletAddr, String tokenAddr) throws Exception{
		if(tokenAddr == null || !tokenAddr.contains("0x")) {
			return ethBalance(walletAddr);
		}
		Web3j web3 = Web3j.build(new HttpService(url));

		//create credential for input walletAddr
        Credentials credentials = Credentials.create(walletAddr);
        
        //create SmartContract wrapper object for given tokenAddr
		GenericToken contract = GenericToken.load(tokenAddr, web3, credentials
				, Contract.GAS_PRICE, Contract.GAS_LIMIT);
		
		//prepare rpc call
		RemoteCall<BigInteger> call = contract.balanceOf(walletAddr);		
		
	    return String.valueOf(call.send());
	}
	
	public static String getTotalSupply(String tokenAddr) throws Exception{
		
		Web3j web3 = Web3j.build(new HttpService(url));

		//create credential for input walletAddr
        Credentials credentials = Credentials.create(tokenAddr);
        
        //create SmartContract wrapper object for given tokenAddr
		GenericToken contract = GenericToken.load(tokenAddr, web3, credentials
				, Contract.GAS_PRICE, Contract.GAS_LIMIT);
		
		//prepare rpc call
		RemoteCall<BigInteger> call = contract.totalSuply();	
		
	    return String.valueOf(call.send());
	}
	
	public static String getDecimals(String tokenAddr) throws Exception{
		
		Web3j web3 = Web3j.build(new HttpService(url));

		//create credential for input walletAddr
        Credentials credentials = Credentials.create(tokenAddr);
        
        //create SmartContract wrapper object for given tokenAddr
		GenericToken contract = GenericToken.load(tokenAddr, web3, credentials
				, Contract.GAS_PRICE, Contract.GAS_LIMIT);
		
		//prepare rpc call
		RemoteCall<BigInteger> call = contract.decimals();	
		
	    return String.valueOf(call.send());
	}
	
	private static String ethBalance(String addr) throws IOException {
		Web3j web3 = Web3j.build(new HttpService(url));
		EthGetBalance balance = web3.ethGetBalance(addr,DefaultBlockParameterName.LATEST).send();
		return String.valueOf(balance.getBalance());
	}
	
	public static String getBlockNumber() throws IOException {
		Web3j web3 = Web3j.build(new HttpService(url));
		
		EthBlockNumber block = new EthBlockNumber();
		block = web3.ethBlockNumber().send();
		return ""+block.getBlockNumber();

	}
	
	public static String getVersion() throws IOException {
		Web3j web3 = Web3j.build(new HttpService(url));
		
		Web3ClientVersion block = web3.web3ClientVersion().send();
		return ""+block.getWeb3ClientVersion();

	}
}

class GenericToken extends Contract{

	
	@SuppressWarnings("deprecation")
	protected GenericToken(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice,
			BigInteger gasLimit) {
		super(contractAddress, web3j, credentials, gasPrice, gasLimit);
	}
	
	
	public static GenericToken load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new GenericToken(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }
	
	public RemoteCall<BigInteger> balanceOf(String address) {
		
		//String poly_addr = "0xb06d72a24df50d4e2cac133b320c5e7de3ef94cb";
		Function func = new Function("balanceOf", Arrays.asList(new Address(address))
				, Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
		
		return executeRemoteCallSingleValueReturn(func, BigInteger.class);
	}
	
	public RemoteCall<BigInteger> totalSuply() {
		
		//String poly_addr = "0xb06d72a24df50d4e2cac133b320c5e7de3ef94cb";
		Function func = new Function("totalSupply", Arrays.asList()
				, Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
		
		return executeRemoteCallSingleValueReturn(func, BigInteger.class);
	}
	
	public RemoteCall<BigInteger> decimals() {
		
		//String poly_addr = "0xb06d72a24df50d4e2cac133b320c5e7de3ef94cb";
		Function func = new Function("decimals", Arrays.asList()
				, Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
		
		return executeRemoteCallSingleValueReturn(func, BigInteger.class);
	}
	
}
