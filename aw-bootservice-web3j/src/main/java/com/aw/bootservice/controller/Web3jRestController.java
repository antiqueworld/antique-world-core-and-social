package com.aw.bootservice.controller;

import java.io.IOException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aw.bootservice.util.PropertyUtil;
import com.aw.bootservice.util.Web3jUtil;

@RestController
public class Web3jRestController {


    @RequestMapping("/test")
    public String greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return "hello "+name+" I am server at "+PropertyUtil.getProp("springboot") ;
    }
    
    @RequestMapping("/blocks")
    public String blocks() throws IOException {
        return Web3jUtil.getBlockNumber();
    }
    
    @RequestMapping("/balance")
    public String balance(
    		@RequestParam(value="address") String walletAddress
    		,@RequestParam(value="tokenAddress") String tokenAddress) throws Exception {
        return Web3jUtil.getBalance(walletAddress, tokenAddress) + "with decimal of "+Web3jUtil.getDecimals(tokenAddress);
    }
    
    @RequestMapping("/supply")
    public String totalSupply(@RequestParam(value="tokenAddress") String tokenAddress) throws Exception {
        return Web3jUtil.getTotalSupply(tokenAddress)+ "with decimal of "+Web3jUtil.getDecimals(tokenAddress);
    }
    
}
