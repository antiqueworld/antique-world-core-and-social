	<div class="navbar-wrapper nav">
		<#if is_signed_in>
		<#if showcontrolmenu>
			   	<@liferay.control_menu />
		</#if>	
		<div class="container" style="height: 58px">
		    <div class="pull-right user-personal-bar" style="padding-right: 45px; padding-top: 4px">
				<@liferay.user_personal_bar />
			</div>
		</div>
		</#if>
		<div class="container">
			<nav class="navbar navbar-inverse navbar-static-top cl-effect-20">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a data-senna-off="true" class="navbar-brand" href="${site_default_url}/home">NAEX</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse navbar-right">
						<ul class="nav navbar-nav">		
							<#list nav_items as nav_item>
								<#assign
									nav_item_attr_has_popup = ""
									nav_item_attr_selected = ""
									nav_item_css_class = ""
									nav_item_layout = nav_item.getLayout()
								/>
					
								<#if nav_item.isSelected()>
									<#assign
										nav_item_attr_has_popup = "aria-haspopup='true'"
										nav_item_attr_selected = "aria-selected='true'"
										nav_item_css_class = "selected"
									/>
								</#if>
					
								<li ${nav_item_attr_selected} class="${nav_item_css_class}" id="layout_${nav_item.getLayoutId()}" role="presentation">
									<a data-senna-off="true" aria-labelledby="layout_${nav_item.getLayoutId()}" ${nav_item_attr_has_popup} href="${nav_item.getURL()}" ${nav_item.getTarget()} role="menuitem"><span data-hover="${nav_item.getName()}"><@liferay_theme["layout-icon"] layout=nav_item_layout /> ${nav_item.getName()}</span></a>
									<#if nav_item.hasChildren()>
										<ul class="child-menu" role="menu">
											<#list nav_item.getChildren() as nav_child>
												<#assign
													nav_child_attr_selected = ""
													nav_child_css_class = ""
												/>
					
												<#if nav_item.isSelected()>
													<#assign
														nav_child_attr_selected = "aria-selected='true'"
														nav_child_css_class = "selected"
													/>
												</#if>
					
												<li ${nav_child_attr_selected} class="${nav_child_css_class}" id="layout_${nav_child.getLayoutId()}" role="presentation">
													<a aria-labelledby="layout_${nav_child.getLayoutId()}" href="${nav_child.getURL()}" ${nav_child.getTarget()} role="menuitem">${nav_child.getName()}</a>
												</li>
											</#list>
										</ul>
									</#if>
									
								</li>
							</#list>
							<#if !is_signed_in>
								<li><a data-redirect="false" class="sign-in test-default" href="${sign_in_url}" id="sign-in" rel="nofollow"><span data-hover="${sign_in_text}">${sign_in_text}</span></a></li>
							<#else>
								<li><a data-redirect="false" class="sign-out test-default" href="${sign_out_url}" id="sign-out" rel="nofollow"><span data-hover="${sign_out_text}">${sign_out_text}</span></a></li>
							</#if>
						</ul>
					</div>
				</div>
	        </nav>
		</div>
	</div>