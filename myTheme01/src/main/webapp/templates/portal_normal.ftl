<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>National Art Exchange</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<@liferay_util["include"] page=top_head_include />
	
	<!-- font files -->
  	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<!-- /font files -->
	
	<!-- js files -->
		<script src="${javascript_folder}/modernizr.custom.js"></script>
	<!-- /js files -->
</head>

<body id="mypage" class="${css_class}" data-spy="scroll" data-offset="60">

<!-- <@liferay_ui["quick-access"] contentId="#main-content" /> -->

<!-- <@liferay_util["include"] page=body_top_include /> -->

<!-- <@liferay.control_menu /> -->

		<#if has_navigation && is_setup_complete>	
			<#include "${full_templates_path}/navigation.ftl" />
		</#if>
<div class="container-fluid fluid" id="wrapper1">
	<!-- About Section -->
<section class="about-us" id="about">
	<div class="centered">
		<@liferay_portlet["runtime"]
			instanceId="49149"
		    portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet"
		/>
	</div>
</section>
<!-- /About Section -->
<!-- Services Section -->
<section class="our-services slideanim" id="service">
		<@liferay_portlet["runtime"]
		    instanceId="74314"
		    portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet"
		/>
</section>
<!-- /Services Section -->

<!-- Gallery Section -->
<section class="our-gallery" id="gallery">
	<div class="slideanim centered">
		<@liferay_portlet["runtime"]
		    instanceId="49165"
		    portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet"
		/>
	</div>
</section>
<!-- /Gallery Section -->
	
<!-- Testimonials -->
<section class="our-testimonials slideanim" id="testimonials">
	<div class="slideanim centered">
		<@liferay_portlet["runtime"]
		    instanceId="49181"
		    portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet"
		/>
	</div>
</section>
<!-- Testimonials -->

<!-- Events -->
<section class="our-events slideanim" id="events">
	<div class="slideanim centered">
		<@liferay_portlet["runtime"]
		    instanceId="49173"
		    portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet"
		/>
	</div>
</section>

<!-- /Events -->

<!-- Footer Section -->
<section class="footer">
    <!-- <h2 class="text-center">THANKS FOR VISITING US</h2>
    <hr> -->
    <div class="container">
        <div class="row">
            <div class="col-md-4 footer-left">
                <h4>Contact Information</h4>
                <div class="contact-info">
                    <div class="address">    
                        <i class="glyphicon glyphicon-globe"></i>
                        <p class="p3">200 Vesey Street</p>
                        <p class="p4">New York, NY</p>
                    </div>
                    <div class="phone">
                        <i class="glyphicon glyphicon-phone-alt"></i>
                        <p class="p3">+1 (800) 988-8230</p>
                        <p class="p4">+1 (800) 988-8230</p>
                    </div>
                    <div class="email-info">
                        <i class="glyphicon glyphicon-envelope"></i>
                        <p class="p3"><a href="mailto:info@nationalartexchange.com">info@nationalartexchange.com</a></p>
                        <p class="p4"><a href="mailto:info@antiqueworld.com">info@antiqueworld.com</a></p>
                    </div>
                </div>
            </div><!-- col -->
            <div class="col-md-4 footer-center">
                <h4>Newsletter</h4>
                <p>Register to our newsletter and be updated with the latests information regarding our services, offers and much more.</p>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-4 control-label"></label>
                        <div class="col-lg-10">
                            <input type="email" class="form-control" id="inputEmail1" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="col-lg-4 control-label"></label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="text1" placeholder="Your Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-10">
                            <button type="submit" class="btn-outline">Submit</button>
                        </div>
                    </div>
                </form><!-- form -->
            </div><!-- col -->
            <div class="col-md-4 footer-right">
                <h4>Support Us</h4>
                <p>National Art Exchange Inc. (OTC: NAEX) was found in 2015 and headquartered in the Financial District of the World Trade Center in Manhattan, New York. National Art Exchange Inc. is an international integrated service organization dedicated to the collection and exchange of art and cultural objects. </p>
                <p><a href="${site_default_url}/privacy-policy">Privacy Policy</a></p>
                <p><a href="${site_default_url}/term-of-use">Term of Use</a></p>
                <ul class="social-icons2">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div><!-- col -->
        </div><!-- row -->
    </div><!-- container -->
    <hr>
    <div class="copyright">
        <p>Copyright &copy; 2018.National Art Exchange Inc. All rights reserved.</p>
    </div>
</section>
<!-- /Footer Section -->
	<!-- Back To Top -->
	<a href="#0" class="cd-top">Top</a>	


	<!-- <footer id="footer" role="contentinfo">
		<p class="powered-by">
			<@liferay.language key="powered-by" /> <a href="#" rel="external">Liferay</a>
		</p>
	</footer> -->
</div>
<#if showNav>
	<@liferay_util["include"] page=body_bottom_include />
</#if>

<@liferay_util["include"] page=bottom_include />
<!-- js files -->
<script src="${javascript_folder}/SmoothScroll.min.js"></script>
<!-- js for gallery -->
<script src="${javascript_folder}/darkbox.js"></script>
<!-- /js for gallery -->

<script>
$(window).scroll(function() {
  $(".slideanim").each(function(){
    var pos = $(this).offset().top;
    var winTop = $(window).scrollTop();
    if (pos < winTop + 600) {
      $(this).addClass("slide");
    }
  });
});
</script>

</body>

</html>