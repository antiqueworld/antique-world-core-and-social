<#--
This file allows you to override and define new FreeMarker variables.
-->
<#assign roles = user.getRoles() 
       showcontrolmenu = false
       showNav = false
       css_class = css_class?replace("has-control-menu", "")
       css_class = css_class?replace("open", "closed")
       css_class = css_class + " controls"
/>

<#if is_signed_in>
    <#list roles as role>
  		<#if role.getName() == "Administrator" || role.getName() == "Other Role" >
			<#assign showcontrolmenu = true
					 showNav = true
					 css_class = css_class + " has-control-menu"
					 css_class = css_class?replace("controls", "controls-show")
					 />
			<#break>
        </#if>             
    </#list> 
</#if>
