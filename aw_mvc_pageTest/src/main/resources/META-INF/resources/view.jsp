<%@ include file="/init.jsp" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- <title>Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" /> -->
<!-- css files -->
<link href="/o/aw_mvc_pageTest/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="/o/aw_mvc_pageTest/css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- /css files -->
<!-- font files -->
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!-- /font files -->
<!-- js files -->
<script src="/o/aw_mvc_pageTest/js/modernizr.custom.js"></script>
<!-- /js files -->

<div class="navbar-wrapper">
    <div class="container">
		<!-- <nav class="navbar navbar-inverse navbar-static-top cl-effect-20">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html">Outing</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse navbar-right">
					<ul class="nav navbar-nav">
						<li><a href="#"><span data-hover="Home">Home</span></a></li>
						<li><a href="#about"><span data-hover="About">About</span></a></li>
						<li><a href="#service"><span data-hover="Services">Services</span></a></li>
						<li><a href="#events"><span data-hover="Events">Events</span></a></li>
						<li><a href="#gallery"><span data-hover="Gallery">Gallery</span></a></li>
						<li><a href="#testimonials"><span data-hover="Testimonials">Testimonials</span></a></li>
						<li><a href="#contact"><span data-hover="Contact">Contact</span></a></li>
					</ul>
				</div>
			</div>
        </nav> -->
    </div>
</div>
<!-- Banner Section -->
<!-- Carousel -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
	    <!-- Indicators -->
	    <ol class="carousel-indicators">
	        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	        <li data-target="#myCarousel" data-slide-to="1"></li>
	        <li data-target="#myCarousel" data-slide-to="2"></li>
	    </ol>
	    <div class="carousel-inner" role="listbox">
	        <div class="item active">
				<img class="first-slide" src="/o/aw_mvc_pageTest/images/banner01.jpg" alt="First slide">
	        </div>
	        <div class="item">
				<img class="second-slide" src="/o/aw_mvc_pageTest/images/banner02.jpg" alt="Second slide" style="height:100%;">
	        </div>
	        <div class="item">
				<img class="third-slide" src="/o/aw_mvc_pageTest/images/banner03.png" alt="Third slide" style="height:100%;">
	        </div>
	    </div>
	    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	        <span class="sr-only">Previous</span>
	    </a>
	    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
	        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	        <span class="sr-only">Next</span>
	    </a>
	</div>
<!-- /.carousel -->
<!-- /Banner Section -->
<!-- About Section -->
<section class="about-us" id="about">
	<h3 class="text-center slideanim">National Art Exchange Heading</h3>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 disp">
				<img src="/o/aw_mvc_pageTest/images/about-img.jpg" class="img-responsive slideanim img-size" alt="service">
				<div class="BuySection">
					<h2>Pyrography</h2>
					<hr>
					<h2>What We Paid</h2>
					<p>$1,300,0000(Purchased from Xixi)</p>
					<hr>
					<h2>Auction House Estimate</h2>
					<p>$1,500,0000 - $10,000,000</p>
					<hr>
					<h2>Offering Size</h2>
					<p>$1,800,0000</p>
					<hr>
					<h2>SEC Filling</h2>
					<p>Offering Circular<a href="">Link</a></p>
					<hr>
					<div class="BuyButton">
					<button type="button" class="btn btn-light">Buy</button>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8">
				<div class="about-info">
					<h4 class="slideanim">NAEX</h4>
					<p class="abt slideanim">National Art Exchange Inc. (OTC: NAEX) was found in 2015 and headquartered in the Financial District of the World Trade Center in Manhattan, New York. </p>
					<p class="abt slideanim">National Art Exchange Inc. is an international integrated service organization dedicated to the collection and exchange of art and cultural objects. </p>
					<p class="abt slideanim">National Art Exchange Inc. will provide the world’s most advanced cultural and art objects identification and testing technology to collectors and devote themselves to realizing a standardized service system of identification, valuation, acquisition, mortgage, insurance and monetization of cultural artefacts.</p>
					<p class="abt slideanim">National Art Exchange Inc. will provide the world’s most advanced cultural and art objects identification and testing technology to collectors and devote themselves to realizing a standardized service system of identification, valuation, acquisition, mortgage, insurance and monetization of cultural artefacts.</p>
					<p class="abt slideanim">National Art Exchange Inc. will provide the world’s most advanced cultural and art objects identification and testing technology to collectors and devote themselves to realizing a standardized service system of identification, valuation, acquisition, mortgage, insurance and monetization of cultural artefacts.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /About Section -->

<!-- Services Section -->
<section class="our-services slideanim" id="service">
	<h3 class="text-center slideanim">The Painting</h3>
	<p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
	<div id="features">
		<div class="container">
			<div class="row">
				<div class="col-md-6 centered">
					<!-- ACCORDION -->
					<div class="accordion ac" id="accordion2">
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle slideanim" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">History</a>
							</div><!-- /accordion-heading -->
							<div id="collapseOne" class="accordion-body collapse in">
								<div class="accordion-inner slideanim">
									<p>Pyrography or pyrogravure is the art of decorating wood or other materials with burn marks resulting from the controlled application of a heated object such as a poker. It is also known as pokerwork or wood burning.</p>
								</div><!-- /accordion-inner -->
							</div><!-- /collapse -->
						</div><!-- /accordion-group -->
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle slideanim" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">The Artist</a>
							</div>
							<div id="collapseTwo" class="accordion-body collapse">
								<div class="accordion-inner slideanim">
									<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
								</div><!-- /accordion-inner -->
							</div><!-- /collapse -->
						</div><!-- /accordion-group -->
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle slideanim" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">Assessment</a>
							</div>
							<div id="collapseThree" class="accordion-body collapse">
								<div class="accordion-inner slideanim">
									<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
								</div><!-- /accordion-inner -->
							</div><!-- /collapse -->
						</div><!-- /accordion-group -->
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle slideanim" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">Value</a>
							</div>
							<div id="collapseFour" class="accordion-body collapse">
								<div class="accordion-inner slideanim">
									<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
								</div><!-- /accordion-inner -->
							</div><!-- /collapse -->
						</div><!-- /accordion-group -->
					</div><!-- Accordion -->
				</div>
				<div class="col-md-6">
					<img src="/o/aw_mvc_pageTest/images/service-img.jpg" class="img-responsive slideanim" alt="service">
				</div>
			</div>
		</div><!--/ .container -->
	</div><!--/ #features -->
</section>
<!-- /Services Section -->

<!-- Gallery Section -->
<section class="our-gallery" id="gallery">
	<h3 class="text-center slideanim">ART Gallery</h3>
	<p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
	<div class="container">
		<img src="/o/aw_mvc_pageTest/images/gallery-img1.jpg" data-darkbox="/o/aw_mvc_pageTest/images/gallery-img1-1.jpg" data-darkbox-description="<b>Lorem Ipsum</b><br>Lorem ipsum dolor sit amet" class="img-responsive slideanim">
        <img src="/o/aw_mvc_pageTest/images/gallery-img2.jpg" data-darkbox="/o/aw_mvc_pageTest/images/gallery-img2-2.jpg" data-darkbox-description="<b>Lorem Ipsum</b><br>Lorem ipsum dolor sit amet" class="img-responsive slideanim">
		<img src="/o/aw_mvc_pageTest/images/gallery-img3.jpg" data-darkbox="/o/aw_mvc_pageTest/images/gallery-img3-3.jpg" data-darkbox-description="<b>Lorem Ipsum</b><br>Lorem ipsum dolor sit amet" class="img-responsive slideanim">
		<img src="/o/aw_mvc_pageTest/images/gallery-img4.jpg" data-darkbox="/o/aw_mvc_pageTest/images/gallery-img4-4.jpg" data-darkbox-description="<b>Lorem Ipsum</b><br>Lorem ipsum dolor sit amet" class="img-responsive slideanim">
		<img src="/o/aw_mvc_pageTest/images/gallery-img5.jpg" data-darkbox="/o/aw_mvc_pageTest/images/gallery-img5-5.jpg" data-darkbox-description="<b>Lorem Ipsum</b><br>Lorem ipsum dolor sit amet" class="img-responsive slideanim">
		<img src="/o/aw_mvc_pageTest/images/gallery-img6.jpg" data-darkbox="/o/aw_mvc_pageTest/images/gallery-img6-6.jpg" data-darkbox-description="<b>Lorem Ipsum</b><br>Lorem ipsum dolor sit amet" class="img-responsive slideanim">
		<img src="/o/aw_mvc_pageTest/images/gallery-img7.jpg" data-darkbox="/o/aw_mvc_pageTest/images/gallery-img7-7.jpg" data-darkbox-description="<b>Lorem Ipsum</b><br>Lorem ipsum dolor sit amet" class="img-responsive slideanim">
		<img src="/o/aw_mvc_pageTest/images/gallery-img8.jpg" data-darkbox="/o/aw_mvc_pageTest/images/gallery-img8-8.jpg" data-darkbox-description="<b>Lorem Ipsum</b><br>Lorem ipsum dolor sit amet" class="img-responsive slideanim">
	</div>
</section>
<!-- /Gallery Section -->
<!-- Events -->
<section class="our-events slideanim" id="events">
	<h3 class="text-center slideanim">Future Reading</h3>
	<p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
					<h4 class="text-center slideanim">Condition Report</h4>
					<p class="eve slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
					<h4 class="text-center slideanim">Offering Statement</h4>
					<p class="eve slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				</div>
			</div>	
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
					<h4 class="text-center slideanim">Literature</h4>
					<p class="eve slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				</div>
			</div>	
		</div>
	</div>
</section>

<!-- /Events -->

<!-- Testimonials -->
<section class="our-testimonials slideanim" id="testimonials">
	<h3 class="text-center slideanim">What Our Clients Say</h3>
	<p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<div class="test">
					<img src="/o/aw_mvc_pageTest/images/test-img1.png" class="img-responsive slideanim" alt="">
					<h4 class="text-center slideanim">Lorem Ipsum</h4>
					<p class="t1 slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>	
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="test">
					<img src="/o/aw_mvc_pageTest/images/test-img2.png" class="img-responsive slideanim" alt="">
					<h4 class="text-center slideanim">Lorem Ipsum</h4>
					<p class="t1 slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>	
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="test">
					<img src="/o/aw_mvc_pageTest/images/test-img3.png" class="img-responsive slideanim" alt="">
					<h4 class="text-center slideanim">Lorem Ipsum</h4>
					<p class="t1 slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>	
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Testimonials -->
<!-- Contact Section -->
<section class="our-contacts slideanim" id="contact">
	<h3 class="text-center slideanim">Contact Us</h3>
	<p class="text-center slideanim">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<form role="form">
					<div class="row">
						<div class="form-group col-lg-4 slideanim">
							<input type="text" class="form-control user-name" placeholder="Your Name" required/>
						</div>
						<div class="form-group col-lg-4 slideanim">
							<input type="email" class="form-control mail" placeholder="Your Email" required/>
						</div>
						<div class="form-group col-lg-4 slideanim">
							<input type="tel" class="form-control pno" placeholder="Your Phone Number" required/>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-lg-12 slideanim">
							<textarea class="form-control" rows="6" placeholder="Your Message" required/></textarea>
						</div>
						<div class="form-group col-lg-12 slideanim">
							<button type="submit" href="#" class="btn-outline1">Submit</button>
						</div>
					</div>
				</form>
			</div>	
		</div>
	</div>
</section>
<!-- /Contact Section -->
<!-- Footer Section -->
<section class="footer">
	<h2 class="text-center">THANKS FOR VISITING US</h2>
	<hr>
	<div class="container">
		<div class="row">
			<div class="col-md-4 footer-left">
				<h4>Contact Information</h4>
				<div class="contact-info">
					<div class="address">	
						<i class="glyphicon glyphicon-globe"></i>
						<p class="p3">200 Vesey Street</p>
						<p class="p4">New York, NY</p>
					</div>
					<div class="phone">
						<i class="glyphicon glyphicon-phone-alt"></i>
						<p class="p3">+1 (800) 988-8230</p>
						<p class="p4">+1 (800) 988-8230</p>
					</div>
					<div class="email-info">
						<i class="glyphicon glyphicon-envelope"></i>
						<p class="p3"><a href="mailto:info@nationalartexchange.com">info@nationalartexchange.com</a></p> 
						<p class="p4"><a href="mailto:info@nationalartexchange.com">info@nationalartexchange.com</a></p>
					</div>
				</div>
			</div><!-- col -->
			<div class="col-md-4 footer-center">
				<h4>Newsletter</h4>
				<p>Register to our newsletter and be updated with the latests information regarding our services, offers and much more.</p>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="inputEmail1" class="col-lg-4 control-label"></label>
						<div class="col-lg-10">
							<input type="email" class="form-control" id="inputEmail1" placeholder="Email" required>
						</div>
					</div>
					<div class="form-group">
						<label for="text1" class="col-lg-4 control-label"></label>
						<div class="col-lg-10">
							<input type="text" class="form-control" id="text1" placeholder="Your Name" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-10">
							<button type="submit" class="btn-outline">Sign in</button>
						</div>
					</div>
				</form><!-- form -->
			</div><!-- col -->
			<div class="col-md-4 footer-right">
				<h4>Support Us</h4>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				<ul class="social-icons2">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				</ul>
			</div><!-- col -->
		</div><!-- row -->
	</div><!-- container -->
	<hr>
	<div class="copyright">
		<p>Copyright &copy; 2018.National Art Exchange Inc. All rights reserved.</p>
	</div>
</section>
<!-- /Footer Section -->
<!-- Back To Top -->
<a href="#0" class="cd-top">Top</a>
<!-- /Back To Top -->

<!-- js files -->
<script src="/o/aw_mvc_pageTest/js/jquery.min.js"></script>
<script src="/o/aw_mvc_pageTest/js/bootstrap.min.js"></script>
<script src="/o/aw_mvc_pageTest/js/SmoothScroll.min.js"></script>
<!-- js for gallery -->
<script src="/o/aw_mvc_pageTest/js/darkbox.js"></script>
<!-- /js for gallery -->
<!-- js for back to top -->
<script src="/o/aw_mvc_pageTest/js/main.js"></script>
<!-- /js for back to top -->
<!-- js for nav-smooth scroll -->
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

  // Store hash
  var hash = this.hash;

  // Using jQuery's animate() method to add smooth page scroll
  // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
  $('html, body').animate({
    scrollTop: $(hash).offset().top
  }, 900, function(){

    // Add hash (#) to URL when done scrolling (default click behavior)
    window.location.hash = hash;
    });
  });
})
</script>
<!-- /js for nav-smooth scroll -->
<!-- js for slide animations -->
<script>
$(window).scroll(function() {
  $(".slideanim").each(function(){
    var pos = $(this).offset().top;

    var winTop = $(window).scrollTop();
    if (pos < winTop + 600) {
      $(this).addClass("slide");
    }
  });
});
</script>
<!-- /js for slide animations -->
<!-- /js files -->