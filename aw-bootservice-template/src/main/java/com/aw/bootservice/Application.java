package com.aw.bootservice;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.aw.bootservice.util.PropertyUtil;

//@ComponentScan(basePackages="com.aw.bootservice")
//@EnableJpaRepositories(basePackages="com.aw.bootservice.entity")
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
    	
    	SpringApplication app = new SpringApplication(Application.class);
        app.setDefaultProperties(Collections.singletonMap("server.port", PropertyUtil.getProp("springboot")));
    	app.run(args);
    	
    }
}


