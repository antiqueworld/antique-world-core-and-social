package com.aw.bootservice.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtil {
	private static Properties prop = null;
	static{
		
		InputStream input = null;

		try {

			input = new FileInputStream("/apps/microservice.properties");
			prop = new Properties();
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String getProp(String key) {
		String s = prop.getProperty(key);
		return s;
	}
}


