This micro-service contains:
The basic structure of our micro service, with property management and db connection
property file: /apps/microservice.properties

Db connect: please go to src/main/resources/application.properties to change to your local mysql url and user/pwd

Dependency: please add dependency in pom.xml

Build: please right click project, run as -> maven clean then run as -> maven install

Run: runnable jar file will be in target folder, use terminal to run "java -jar filename.jar". Or right click 
     Application.java, run as java application in IDE.