<%@ include file="/init.jsp" %>
<style>
    #selectpart{
        
        flex:0.2;
        list-style:none;
        padding:20px;
        
    }
    #infopart{
        padding:30px;
        flex:0.7;
       
       
    }
      #orderpart{
        display:none;
        flex:0.7;
        padding:30px;
        
       
    }
     #passwordpart{
        display:none;
        padding:30px;
        flex:0.7;
        
       
    }
    .neverdisplay{
        display:none;
    }
    .showinfo{
       flex:0.5;
       padding-right:30px;
    }
    .showinfo p{
       font-size:20px;
    }
    .showinfo input{
        width:100%;
        margin:auto;
        height:40px;
        margin-bottom:30px;
        
        
    }
    #profile .btn{
       background-color:red;
       padding:10px;
       display:inline-block;
       
       margin:20px;
      
    }
    #selectpart li{
       padding:10px;
       
    }
    #selectpart li:hover{
      cursor: pointer;
    }
    #passwordpart input{
        width:60%;
        margin:auto;
        height:40px;
        margin-bottom:20px;
        display:inline-block;
    }
    #strenght{
      
      font-size:10px;
      
    }
    #showstrenght{
      width:100%;
      background-color:red;
      height:20px;
     
      vertical-align: middle;
    }
    #orderpart th{
        background-color:grey;
        margin-right:5px;
        width:30%;
        padding:10px 0 10px 0;
        text-align:center;
        
    }
</style>

<div style='display:flex;min-height:500px' id='profile'>
<div id='selectpart'>
   <p style='font-size:30px'><strong>My Account</strong></p>
   <ul style='font-size:20px;list-style:none'>
      <li id='clickprofile'>Profile</li>
      <li id='clickorder'>Orders</li>
      <li id='clickpassword'>Password</li>
   </ul>
</div>

<div id='passwordpart'>
   <p style='font-size:30px'><strong>UPDATE PASSWORD</strong></p>
   <div style='width:100%;margin-bottom:30px'><p style='font-size:20px'><strong>Current Password</strong></p><input style='width:60%;height:40px' id='password'  /></div>
   <div style='display:flex;padding-right:30px'>
     <div style='flex:1'>
       <p style='font-size:20px'><strong>New Password</strong></p>
       <input id='newpassowrd'/>
       <!-- <div style='width:45%;display:inline-block'>
       <p id='strenght'>Password Strenght</p>
       <div id='showstrenght'></div>
       </div> -->
       <div class='btn' id='changepassword' >Save</div>
     </div>
     
   </div>
</div>
<div id='orderpart'>
    <p style='font-size:30px'><strong>ORDERS</strong></p>
         <table style='width:100%;text-align:center'>
             <thead>
               <th>Orders</th>
               <th>From Amount</th>
               <th>To Amount</th>
             </thead>
             <tbody id='tbody'>
                
             </tbody>
             
         </table>
</div>
<div id='infopart'>
   
    <p style='font-size:30px'><strong>PROFILE</strong></p>
    
     
    <div style='width:100%;margin-bottom:30px'><p style='font-size:20px'><strong>Email Address</strong></p><input style='width:60%;height:40px;background:#D3D3D3' id='secEmail'  disabled/></div>
    <div style='display:flex'>
    <div id='userBasicInfo' class='showinfo'>
      <p style='font-size:20px'><strong>PERSONAL INFORMATION</strong></p>
      <p>First Name</p><input id='fn' style='background:#D3D3D3' disabled/>     
      <p>Last Name</p><input id='ln' style='background:#D3D3D3' disabled/>	  
	  <p>Phone</p><input id='phone' class='basichold' disabled/>     
      
    </div> 
    <div id='userContactInfo' class='showinfo'>
                    <p style='font-size:20px'><strong>ADDRESS</strong></p>
                    
				    <p>Country:</p><select id='countryId' class='contacthold' style='width:100%;height:40px;margin-bottom:30px' disabled></select>
				    <p>State:</p><select id='regionId' class='contacthold' style='width:100%;height:40px;margin-bottom:30px' disabled></select>
				    <p>Street1:</p><input id='street1' class='contacthold' disabled/>
				    <p>Street2:</p><input id='street2' class='contacthold' disabled/>				    
				    <p>City:</p><input id='city' class='contacthold' disabled/>
				    <p>Zip Code:</p><input id='zipCode' class='contacthold' disabled/>                 
    </div>
    </div>

    <div class='btn' id='submit' onclick='update()' disabled>Save</div><div class='btn' onclick='edit()'>Edit</div>
</div>
</div>


<!-- Please do not delete tag backup, if you want to add any fields, just pick it up from backup -->
	<div id='backup' style='display:none'>
			<p class='neverdisplay'>addressType:</p><select class='neverdisplay' id='addressType' class='contacthold' disabled><option value='private'>Private</option><option value='business'>Business</option></select>
			<p class='neverdisplay'>isMailing:</p><select class='neverdisplay' id='isMailing' class='contacthold' disabled><option value='y'>Y</option><option value='n'>N</option></select><br>
			<p>Birthday</p><input class='neverdisplay' type='date' id='birthday' class='basichold' disabled/>
			<p>selfDesc</p><input class='neverdisplay' id='selfDesc' class='basichold' disabled/>
			<p class='neverdisplay'>cellPhone<p><input class='neverdisplay' id='cellPhone' class='basichold' disabled/>  
			<p class='neverdisplay'>extension<p><input class='neverdisplay' id='extension' class='basichold' disabled/>	 
			<p class='neverdisplay'>Middle Name</p><input class='neverdisplay' id='middleName' class='basichold' disabled/>
			<p class='neverdisplay'>Title</p><select class='neverdisplay' id='title' class='basichold' disabled><option value='Mr'>Mr</option><option value='Mrs'>Mrs</option></select>
	</div>

<script>
$('#changepassword').click(function(){
	 var oldpassword = $('#password').val().trim();
	 var newpassword = $('#newpassword').val().trim();
	 if(newpassword.length>0&&oldpassword!=newpassword){
		 
	 }else{
		 alert('Please input new password and new password can not be same as old password');
	 }
})
$('#clickprofile').click(function(){
    $('#passwordpart').css('display','none');	
    $('#orderpart').css('display','none');	
    $('#infopart').show();
})
$('#clickorder').click(function(){
    $('#passwordpart').css('display','none');	
    $('#orderpart').show();	
    $('#infopart').css('display','none');
})
$('#clickpassword').click(function(){
    $('#passwordpart').show();	
    $('#orderpart').css('display','none');	
    $('#infopart').css('display','none');
})
function getorders(){
	Liferay.Service(
			  '/aw_social.commerce/get-all-orders',
			  {
			    chargeFlag: '',
			    sentFlag: '',
			    locale: ''
			  },
			  function(obj) {
			    var orders = obj.data;
			    var show;
			    data.map(function(details,index){
			    	 show += '<tr><td>'+details.orderId+'</td><td>'+details.fromAmt+' '+details.fromCurrency+'</td><td>'+details.toAmt+' '+details.toCurrency+'</td></tr>';
			    })
			    $('#tbody').html(show);
			  }
			);
}
getorders();
function getcountrylist(){
    Liferay.Service(
			  '/aw_social.userinfo/get-country-list',
			  {
			    locale: ''
			  },
			  function(obj) {
			     var data = obj.data;
			     var info = '';
			     data.map(function(country,index){
			             info += '<option value='+country.id+'>'+country.name+'</option>';
			     })
			     $('#issuecountry').html(info);
			     $('#countryId').html(info);
			     getregioninfo();
				
			  }
			);
}
function getregioninfo(){
     Liferay.Service(
					  '/aw_social.userinfo/get-region-list',
					  {
					    countryId: $('#countryId option:selected').val(),
					    locale: ''
					  },
					  function(obj) {
					     var data = obj.data;
					     var info = '';
					     data.map(function(region,index){
					             info += '<option value='+region.id+'>'+region.name+'</option>';
					     })
					     $('#regionId').html(info);
					  }
					);

}
getcountrylist();
function getuserinfo(){
    Liferay.Service(
			  '/aw_social.userinfo/get-user-info',
			  {
			    locale: ''
			  },
			  function(obj) {
			    $('#fn').val(obj.data.basicInfo.firstName);
			    $('#ln').val(obj.data.basicInfo.lastName);
			    $('#in').val(obj.data.basicInfo.IdNumber);
			    $('#ssn').val(obj.data.basicInfo.ssn);
			    $('#type').val(obj.data.basicInfo.IdType);
			    $('#phone').val(obj.data.contact.phone);
			    $('#birthday').val(obj.data.basicInfo.birthday);
			    $('#selfDesc').val(obj.data.basicInfo.selfDesc),
		        $('#secEmail').val(obj.data.contact.secEmail),
		        $('#middleName').val(obj.data.basicInfo.middleName);
		        $('#extension').val(obj.data.contact.extension);
		        $('#cellPhone').val(obj.data.contact.cellPhone);
			    if(obj.data.basicInfo.IdIssueCountry){
			       $("#issuecountry option[value="+obj.data.basicInfo.IdIssueCountry+"]").attr("selected", true);
			    }
			    try{
			    var length = obj.data.addresses.length;
			   
			    if(obj.data.addresses[length-1].addressType){
			       $('#addressType option[value='+obj.data.addresses[length-1].addressType+']').attr("selected", true);
			    }
			    if(obj.data.basicInfo.title){
			      $('#title option[value='+obj.data.basicInfo.title+']').attr("selected", true);
			    }
			    
			    $('#city').val(obj.data.addresses[length-1].city);
			    $('#street1').val(obj.data.addresses[length-1].street1);
			    $('#street2').val(obj.data.addresses[length-1].street2);
			    $('#zipCode').val(obj.data.addresses[length-1].zipCode);
			    if(obj.data.addresses[length-1].isMailing){
			        $('#isMailing option[value='+obj.data.addresses[length-1].isMailing+']').attr("selected", true);
			    }
			    if($('#countryId').val(obj.data.addresses[length-1].countryId)){
			       $('#countryId option[value='+obj.data.addresses[length-1].countryId+']').attr("selected", true);
			        Liferay.Service(
						  '/aw_social.userinfo/get-region-list',
						  {
						    countryId: $('#countryId option:selected').val(),
						    locale: ''
						  },
						  function(obj1) {
						     var data = obj1.data;
						     var info = '';
						     data.map(function(region,index){
						             info += '<option value='+region.id+'>'+region.name+'</option>';
						     })
						     $('#regionId').html(info);
						     if(obj.data.addresses[length-1].regionId){
						           $('#regionId option[value='+obj.data.addresses[length-1].regionId+']').attr("selected", true);
						       }
						  }
						);
			       
			    }
			    }catch(err){
			       console.log('no address info');
			    }
			  }
			);
}
getuserinfo();
function updatebasicinfo(){
    Liferay.Service(
		  '/aw_social.userinfo/update-basic-user-info',
		  {
		    birthday: $('#birthday').val(),
		    firstName: $('#fn').val(),
		    middleName: $('#middleName').val(),
		    lastName: $('#ln').val(),
		    title: $('#title option:selected').val(),
		    selfDesc: $('#selfDesc').val(),
		    secEmail: $('#secEmail').val(),
		    phone: $('#phone').val(),
		    extension: $('#extension').val(),
		    cellPhone: $('#cellPhone').val(),
		    locale: ''
		  },
		  function(obj) {
		    if(obj.status.code=='00'){
		         
		          
		          
		          updateaddressinfo();
                
		          
		    }else{
		         alert(obj.status.message);
		    }
		  }
		);
}
function updateaddressinfo(){
	Liferay.Service(
			  '/aw_social.userinfo/update-user-address-info',
			  {
			    addressType: $('#addressType option:selected').val(),
			    street1: $('#street1').val(),
			    street2: $('#street2').val(),
			    street3: '',
			    city: $('#city').val(),
			    regionId: $('#regionId option:selected').val(),
			    countryId: $('#countryId option:selected').val(),
			    zipCode: $('#zipCode').val(),
			    isMailing: $('#isMailing option:selected').val(),
			    locale: ''
			  },
			  function(obj) {
			     if(obj.status.code=='00'){
		          alert('success');
		          getuserinfo();
				  $('.contacthold').attr('disabled',true);
				  $('.basichold').attr('disabled',true);
                  $('#submit').attr('disabled',true);       
				    }else{
				         alert(obj.status.message);
				    }
			  }
	);
}
document.getElementById('countryId').addEventListener('change',function(){
    console.log('change');
    try{
       getregioninfo();
    }catch(err){
      console.log(err);
    }
});
function edit(){
    $('.basichold').attr('disabled',false);
    $('.contacthold').attr('disabled',false);
    $('#submit').attr('disabled',false);
}
function update(){
	updatebasicinfo();
	
}
</script>



