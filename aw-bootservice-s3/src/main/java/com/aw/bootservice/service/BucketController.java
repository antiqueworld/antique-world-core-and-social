package com.aw.bootservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

//import com.aw.bootservice.util.PropertyUtil;

@RestController
public class BucketController {


    @RequestMapping("/test")
    public String greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return "hello "+name+" I am server at ";
    }
    
    private AmazonClient amazonClient;

    @Autowired
    BucketController(AmazonClient amazonClient) {
        this.amazonClient = amazonClient;
    }

    @PostMapping("/uploadFile")
    public String uploadFile(@RequestPart(value = "file") MultipartFile file,
    		@RequestParam(value="name") String name) {
        return this.amazonClient.uploadFile(file, name);
    }

    @RequestMapping("/deleteFile")
    public String deleteFile(@RequestParam(value = "fileName") String fileUrl) {
        return this.amazonClient.deleteFileFromS3Bucket(fileUrl);
    }
   
}
