package com.aw.bootservice;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.aw.bootservice.util.PropertyUtil;

@ComponentScan("com.aw.bootservice.service")
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
    	
    	SpringApplication app = new SpringApplication(Application.class);
        app.setDefaultProperties(Collections.singletonMap("server.port", PropertyUtil.getProp("springboots3")));
    	app.run(args);
    	
    }
}
