<%@ include file="/init.jsp" %>
<style>
    .showinfo{
        width:30%;
        display:inline-block;
        border:solid 1px;
        padding:10px;

    }
</style>
<aui:script>

		 /* $('#submit').on('click',function(){
			 updatebasicinfo();
		 }) */
	
 function getcountrylist(){
       Liferay.Service(
			  '/aw_social.userinfo/get-country-list',
			  {
			    locale: ''
			  },
			  function(obj) {
			     var data = obj.data;
			     var info = '';
			     data.map(function(country,index){
			             info += '<option value='+country.id+'>'+country.name+'</option>';
			     })
			     $('#issuecountry').html(info);
			     $('#countryId').html(info);
			     getregioninfo();
				
			  }
			);
 }
 function getregioninfo(){
        Liferay.Service(
					  '/aw_social.userinfo/get-region-list',
					  {
					    countryId: $('#countryId option:selected').val(),
					    locale: ''
					  },
					  function(obj) {
					     var data = obj.data;
					     var info = '';
					     data.map(function(region,index){
					             info += '<option value='+region.id+'>'+region.name+'</option>';
					     })
					     $('#regionId').html(info);
					  }
					);
 
 }
 getcountrylist();
 function getuserinfo(){
           Liferay.Service(
				  '/aw_social.userinfo/get-user-info',
				  {
				    locale: ''
				  },
				  function(obj) {
				    $('#fn').val(obj.data.basicInfo.firstName);
				    $('#ln').val(obj.data.basicInfo.lastName);
				    $('#in').val(obj.data.basicInfo.IdNumber);
				    $('#ssn').val(obj.data.basicInfo.ssn);
				    $('#type').val(obj.data.basicInfo.IdType);
				    $('#phone').val(obj.data.contact.phone);
				    $('#birthday').val(obj.data.basicInfo.birthday);
				    $('#selfDesc').val(obj.data.basicInfo.selfDesc),
			        $('#secEmail').val(obj.data.contact.secEmail),
			        $('#middleName').val(obj.data.basicInfo.middleName);
			        $('#extension').val(obj.data.contact.extension);
			        $('#cellPhone').val(obj.data.contact.cellPhone);
				    if(obj.data.basicInfo.IdIssueCountry){
				       $("#issuecountry option[value="+obj.data.basicInfo.IdIssueCountry+"]").attr("selected", true);
				    }
				    try{
				    var length = obj.data.addresses.length;
				   
				    if(obj.data.addresses[length-1].addressType){
				       $('#addressType option[value='+obj.data.addresses[length-1].addressType+']').attr("selected", true);
				    }
				    if(obj.data.basicInfo.title){
				      $('#title option[value='+obj.data.basicInfo.title+']').attr("selected", true);
				    }
				    
				    $('#city').val(obj.data.addresses[length-1].city);
				    $('#street1').val(obj.data.addresses[length-1].street1);
				    $('#street2').val(obj.data.addresses[length-1].street2);
				    $('#zipCode').val(obj.data.addresses[length-1].zipCode);
				    if(obj.data.addresses[length-1].isMailing){
				        $('#isMailing option[value='+obj.data.addresses[length-1].isMailing+']').attr("selected", true);
				    }
				    if($('#countryId').val(obj.data.addresses[length-1].countryId)){
				       $('#countryId option[value='+obj.data.addresses[length-1].countryId+']').attr("selected", true);
				        Liferay.Service(
							  '/aw_social.userinfo/get-region-list',
							  {
							    countryId: $('#countryId option:selected').val(),
							    locale: ''
							  },
							  function(obj1) {
							     var data = obj1.data;
							     var info = '';
							     data.map(function(region,index){
							             info += '<option value='+region.id+'>'+region.name+'</option>';
							     })
							     $('#regionId').html(info);
							     if(obj.data.addresses[length-1].regionId){
							           $('#regionId option[value='+obj.data.addresses[length-1].regionId+']').attr("selected", true);
							       }
							  }
							);
				       
				    }
				    }catch(err){
				       console.log('no address info');
				    }
				  }
				);
     }
     getuserinfo();
     function updatebasicinfo(){
          Liferay.Service(
			  '/aw_social.userinfo/update-basic-user-info',
			  {
			    birthday: $('#birthday').val(),
			    firstName: $('#fn').val(),
			    middleName: $('#middleName').val(),
			    lastName: $('#ln').val(),
			    title: $('#title option:selected').val(),
			    selfDesc: $('#selfDesc').val(),
			    secEmail: $('#secEmail').val(),
			    phone: $('#phone').val(),
			    extension: $('#extension').val(),
			    cellPhone: $('#cellPhone').val(),
			    locale: ''
			  },
			  function(obj) {
			    if(obj.status.code=='00'){
			          alert('success');
			          getuserinfo();
			          $('.basichold').attr('disabled',true);
                      $('#submit').attr('disabled',true);
			          
			    }else{
			         alert(obj.status.message);
			    }
			  }
			);
     }
     function updateaddressinfo(){
		Liferay.Service(
				  '/aw_social.userinfo/update-user-address-info',
				  {
				    addressType: $('#addressType option:selected').val(),
				    street1: $('#street1').val(),
				    street2: $('#street2').val(),
				    street3: '',
				    city: $('#city').val(),
				    regionId: $('#regionId option:selected').val(),
				    countryId: $('#countryId option:selected').val(),
				    zipCode: $('#zipCode').val(),
				    isMailing: $('#isMailing option:selected').val(),
				    locale: ''
				  },
				  function(obj) {
				     if(obj.status.code=='00'){
			          alert('success');
			          getuserinfo();
					  $('.contacthold').attr('disabled',true);
                      $('#contactsubmit').attr('disabled',true);       
					    }else{
					         alert(obj.status.message);
					    }
				  }
		);
     }
     function sensitiveinfo(){
          Liferay.Service(
			  '/aw_social.userinfo/update-sensitive-info',
			  {
			    IdIssueCountryId: $('#issuecountry option:selected').val(),
			    IdType: $('#type').val(),
			    IdNumber: $('#in').val(),
			    ssn: $('#ssn').val(),
			    locale: ''
			  },
			  function(obj) {
			     if(obj.status.code=='00'){
			          alert('success');
			          getuserinfo();
			          $('.sensitivehold').attr('disabled',true);
                      $('#sensitivesubmit').attr('disabled',true);
			    }else{
			         alert(obj.status.message);
			    }
			  }
			);
         
     }
     document.getElementById('countryId').addEventListener('change',function(){
         console.log('change');
         try{
            getregioninfo();
         }catch(err){
           console.log(err);
         }
     });
     function basicedit(){
         $('.basichold').attr('disabled',false);
         $('#submit').attr('disabled',false);
     }
     function sensitiveedit(){
         $('.sensitivehold').attr('disabled',false);
         $('#sensitivesubmit').attr('disabled',false);
     }
     function contactedit(){  
         $('.contacthold').attr('disabled',false);
         $('#contactsubmit').attr('disabled',false);
     }
</aui:script>

<p><h1>Profile sample</h1></p>
<p>Choose image</p>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects /> 

<portlet:actionURL var="uplaodURL" name="uploadFile"></portlet:actionURL>
<portlet:resourceURL var="downloadURL">
	<portlet:param name="imageId" value="toBeReplaced" />
</portlet:resourceURL>
<liferay-ui:success key="success" message="File Upload SuccessFully"/>
<liferay-ui:error key="failure" message="Upload Error" />
<liferay-ui:error key="file.extension.error" message="Upload only text File" />

<%-- <p><%=themeDisplay.getUser().getUserId()%></p>
<p><%=themeDisplay.getUser().getFirstName()%></p>
<p><%=themeDisplay.getUser().getEmailAddress()%></p>  --%>
<img src="<%=themeDisplay.getUser().getPortraitURL(themeDisplay)%>" id='image' />
<br/>
<form action="<%=uplaodURL%>" method="post" enctype="multipart/form-data">
<input type="file" name="imageFile"/>
<input type="hidden" name="type" value="portrait"/>
<input type="Submit" name="Submit">
</form>
 <div id='userBasicInfo' class='showinfo'>
      <p>Title</p><select id='title' class='basichold' disabled><option value='Mr'>Mr</option><option value='Mrs'>Mrs</option></select>
      <p>First Name</p><input id='fn' class='basichold' disabled/>
      <p>Middle Name</p><input id='middleName' class='basichold' disabled/>
      <p>Last Name</p><input id='ln' class='basichold' disabled/>
	  <p>secEmail<p><input id='secEmail' class='basichold' disabled/>
	  <p>extension<p><input id='extension' class='basichold' disabled/>	 
	  <p>Phone</p><input id='phone' class='basichold' disabled/>
	  <p>cellPhone<p><input id='cellPhone' class='basichold' disabled/>  
      <p>Birthday</p><input type='date' id='birthday' class='basichold' disabled/>
      <p>selfDesc</p><input id='selfDesc' class='basichold' disabled/>
      <br>
      <button class='btn' onclick='basicedit()'>Edit</button><button class='btn' id='submit' onclick='updatebasicinfo()' disabled>Submit</button>
</div> 
<div id='userSensitiveInfo' class='showinfo'>
      <p>ID Nubmer</p><input id='in' class='sensitivehold' disabled/>
      <p>SSN</p><input id='ssn' class='sensitivehold' disabled/>
      <p>Type</p><input id='type'/>
      <p>IdIssueCountry</p><select id='issuecountry' class='sensitivehold' disabled></select><br>
      <button class='btn' onclick='sensitiveedit()'>Edit</button><button id='sensitivesubmit' class='btn' onclick='sensitiveinfo()' disabled>Submit</button>
</div>
<div id='userContactInfo' class='showinfo'>
                    <p>addressType:</p><select id='addressType' class='contacthold' disabled><option value='private'>Private</option><option value='business'>Business</option></select>
				    <p>street1:</p><input id='street1' class='contacthold' disabled/>
				    <p>street2:</p><input id='street2' class='contacthold' disabled/>				    
				    <p>city:</p><input id='city' class='contacthold' disabled/>
				    <p>regionId:</p><select id='regionId' class='contacthold' disabled></select>
				    <p>countryId:</p><select id='countryId' class='contacthold' disabled></select>
				    <p>zipCode:</p><input id='zipCode' class='contacthold' disabled/>
				    <p>isMailing:</p><select id='isMailing' class='contacthold' disabled><option value='y'>Y</option><option value='n'>N</option></select><br>
                    <button class='btn' onclick='contactedit()'>Edit</button><button id='contactsubmit' class='btn' onclick='updateaddressinfo()' disabled>Submit</button>
</div>
<%-- <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects /> 

<portlet:actionURL var="uplaodURL" name="uploadFile"></portlet:actionURL>
<portlet:resourceURL var="downloadURL">
	<portlet:param name="imageId" value="toBeReplaced" />
</portlet:resourceURL>
<liferay-ui:success key="success" message="File Upload SuccessFully"/>
<liferay-ui:error key="failure" message="Upload Error" />
<liferay-ui:error key="file.extension.error" message="Upload only text File" />

<p><%=themeDisplay.getUser().getUserId()%></p>
<p><%=themeDisplay.getUser().getFirstName()%></p>
<p><%=themeDisplay.getUser().getEmailAddress()%></p>  --%>

<%-- <img src="<%=themeDisplay.getUser().getPortraitURL(themeDisplay)%>" id='image' />
<br/>
<b>upload to change</b>  
<form action="<%=uplaodURL%>" method="post" enctype="multipart/form-data">
<input type="file" name="imageFile"/>
<input type="hidden" name="type" value="portrait"/>  --%>
<!--  
<p>userId</p><input type="text" name="userId"/> <br />
<p>itemId</p><input type="text" name="itemId"/> <br />
<p>isPublic</p><input type="text" name="isPublic"/> <br />-->
<!-- <input type="Submit" name="Submit">
</form> -->

<!--
<form action="downloadURL" method="get">
 <input type="text" name="input" value="1"/>
 <input type=submit value="submit" />
</form>

<a href="<%= downloadURL %>">Click Here for Download</a>
<img src="<%= downloadURL %>" id='image' />
<input value='' id='url'/>
<button onclick='test()'>changeimage</button>

<aui:script>
   function test(){
	     var url = $('#url').val();
	     console.log(url);
	     var oldimage = document.getElementById('image').src;
	     var newimage = oldimage.replace('toBeReplaced',url);
	     console.log('old',oldimage);
	     console.log('new',newimage);
	     $('#image').attr('src',newimage);
	   
   }
</aui:script>
-->