package aw_mvc_blobUtil.portlet;

import aw_mvc_blobUtil.constants.Aw_mvc_blobUtilPortletKeys;

import com.aw.common.util.service.CommonUtilLocalServiceUtil;
import com.liferay.portal.kernel.model.User;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author lilei
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=aw_mvc_blobUtil Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + Aw_mvc_blobUtilPortletKeys.Aw_mvc_blobUtil,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class Aw_mvc_blobUtilPortlet extends MVCPortlet {

	//<img src="<%=themeDisplay.getUser().getPortraitURL(themeDisplay)%>" id='image' />
	public final void uploadFile(final ActionRequest request, final ActionResponse response) throws FileNotFoundException {
		
		SessionMessages.add(request, "" + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
		
		if(user != null){
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);
			
			//potraId/IdCopy/ToItem
			//then call the related method depend on the type
			String type = uploadPortletRequest.getParameter("type");
			String userUid = uploadPortletRequest.getParameter("userId");
			String itemId = uploadPortletRequest.getParameter("itemId");
			String isPublic = uploadPortletRequest.getParameter("isPulbic");
			File file = uploadPortletRequest.getFile("imageFile");
			
			System.out.println("params are: "+type+" "+userUid+" "+itemId+" "+isPublic);
			
			boolean result = CommonUtilLocalServiceUtil.uploadMedia(file, type, userUid, itemId, isPublic);								
			response.setProperty("result", String.valueOf(result));			
		}
	}
	

	
	@Override
	public final void serveResource(final ResourceRequest request, final ResourceResponse response) 
			throws IOException, PortletException {
		
		String imageId = request.getParameter("imageId");				
		byte[] source = CommonUtilLocalServiceUtil.getMedia(imageId);
		
		if(source.length < 5){
			//TODO link it to some default image
		}
		
		OutputStream out = null;
		InputStream in = new ByteArrayInputStream(source);	
		
		response.setContentType("text/plain");
		response.addProperty("Content-disposition", "atachment; filename="+imageId);
		
		try {
			out = response.getPortletOutputStream();
			copy(in, out);
		} catch (final IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (Validator.isNotNull(out)) {
					out.flush();
					out.close();
				}
				if (Validator.isNotNull(in)) {
					in.close();
				}

			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
		
		
	}

	private static long copy(InputStream source, OutputStream sink)
	        throws IOException
	    {
	        long nread = 0L;
	        byte[] buf = new byte[1024];
	        int n;
	        while ((n = source.read(buf)) > 0) {
	            sink.write(buf, 0, n);
	            nread += n;
	        }
	        return nread;
	    }
	
}